<?php
// Load a resource file from the views folder, must be either a .js or .css file
//if(is_array($_GET))
$resource = array_keys($_GET)[0];
$content_type = '';

// Validate the resource file
if($resource) {
	if(substr($resource,-3,3) == '_js') {
		$resource = str_replace('_js','.js',$resource);
		$content_type = 'application/javascript';
	} elseif(substr($resource,-4,4) == '_css') {
		$resource = str_replace('_css','.css',$resource);
		$content_type = 'text/css';
	} else {
		$resource = null;
	}

	if(!file_exists($_SERVER['DOCUMENT_ROOT'].'/application/views/'.$resource))
		$resource = null;
}

// Send it through
if($resource) {
	header('Content-Type: '.$content_type);
	@readfile($_SERVER['DOCUMENT_ROOT'].'/application/views/'.$resource);
} else {
	header('404 - Resource not found', TRUE, 404);
}

exit();
?>