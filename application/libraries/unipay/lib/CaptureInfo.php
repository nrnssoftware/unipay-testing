<?php



/*
 *$comment
 */
class CaptureInfo extends ClientObject {
		/*
	 *@var null
	 */
	private $requestDate;
		/*
	 *@var null
	 */
	private $responseDate;
		/*
	 *@var null
	 */
	private $holderName;
		/*
	 *@var null
	 */
	private $taxAmount;
		/*
	 *@var null
	 */
	private $street;
		/*
	 *@var null
	 */
	private $city;
		/*
	 *@var null
	 */
	private $state;
		/*
	 *@var null
	 */
	private $phone;
		/*
	 *@var null
	 */
	private $zipCode;
		/*
	 *@var null
	 */
	private $email;
		/*
	 *@var null
	 */
	private $returnType;
		/*
	 *@var null
	 */
	private $issuerBankName;
		/*
	 *@var null
	 */
	private $issuerBankPhone;
		/*
	 *@var null
	 */
	private $approvalCode;
		/*
	 *@var null
	 */
	private $referenceNumber;
		/*
	 *@var null
	 */
	private $accountNumber;
		/*
	 *@var null
	 */
	private $accessory;
		/*
	 *@var null
	 */
	private $transactionType;
		/*
	 *@var null
	 */
	private $cvv2;
		/*
	 *@var null
	 */
	private $trackData;
		/*
	 *@var null
	 */
	private $isProxynizationEnabled;
		/*
	 *@var null
	 */
	private $binEncoded;
		/*
	 *@var null
	 */
	private $riskScore;
		/*
	 *@var null
	 */
	private $riskExplanation;
		/*
	 *@var null
	 */
	private $sysMessage;
		/*
	 *@var null
	 */
	private $tokenCode;
		/*
	 *@var null
	 */
	private $bankInfo;
		
	/*
	 *$constructor.comment
	 */
	function __construct($requestDate = null, $responseDate = null, $holderName = null, $taxAmount = null, $street = null, $city = null, $state = null, $phone = null, $zipCode = null, $email = null, $returnType = null, $approvalCode = null, $referenceNumber = null, $merchantAccountCode = null, $accountNumber = null, $accessory = null, $code = null, $transactionType = null, $id = null){
		//initialization block
		$this->isProxynizationEnabled = false;
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
		//constructor with parameters
		$this->requestDate = $requestDate;
		$this->responseDate = $responseDate;
		$this->holderName = $holderName;
		$this->taxAmount = $taxAmount;
		$this->street = $street;
		$this->city = $city;
		$this->state = $state;
		$this->phone = $phone;
		$this->zipCode = $zipCode;
		$this->email = $email;
		$this->returnType = $returnType;
		$this->approvalCode = $approvalCode;
		$this->referenceNumber = $referenceNumber;
		$this->setMerchantAccountCode($merchantAccountCode);
		$this->accountNumber = $accountNumber;
		$this->accessory = $accessory;
		$this->setCode($code);
		$this->transactionType = $transactionType;
		$this->setId($id);
		$this->setRefIdSpecial($this->hashCode());
	
	}

	/*
	 *$method.comment
	 */
	public function getRequestDate(){
		return $this->requestDate;
	}
	/*
	 *$method.comment
	 */
	 function setRequestDate($requestDate){
		$this->requestDate = $requestDate;
	}
	/*
	 *$method.comment
	 */
	public function getResponseDate(){
		return $this->responseDate;
	}
	/*
	 *$method.comment
	 */
	 function setResponseDate($responseDate){
		$this->responseDate = $responseDate;
	}
	/*
	 *$method.comment
	 */
	public function getHolderName(){
		return $this->holderName;
	}
	/*
	 *$method.comment
	 */
	public function setHolderName($holderName){
		$this->holderName = $holderName;
	}
	/*
	 *$method.comment
	 */
	public function getTaxAmount(){
		return $this->taxAmount;
	}
	/*
	 *$method.comment
	 */
	 function setTaxAmount($taxAmount){
		$this->taxAmount = $taxAmount;
	}
	/*
	 *$method.comment
	 */
	public function getStreet(){
		return $this->street;
	}
	/*
	 *$method.comment
	 */
	public function setStreet($street){
		$this->street = $street;
	}
	/*
	 *$method.comment
	 */
	public function getCity(){
		return $this->city;
	}
	/*
	 *$method.comment
	 */
	public function setCity($city){
		$this->city = $city;
	}
	/*
	 *$method.comment
	 */
	public function getState(){
		return $this->state;
	}
	/*
	 *$method.comment
	 */
	public function setState($state){
		$this->state = $state;
	}
	/*
	 *$method.comment
	 */
	public function getPhone(){
		return $this->phone;
	}
	/*
	 *$method.comment
	 */
	public function setPhone($phone){
		$this->phone = $phone;
	}
	/*
	 *$method.comment
	 */
	public function getZipCode(){
		return $this->zipCode;
	}
	/*
	 *$method.comment
	 */
	public function setZipCode($zipCode){
		$this->zipCode = $zipCode;
	}
	/*
	 *$method.comment
	 */
	public function getEmail(){
		return $this->email;
	}
	/*
	 *$method.comment
	 */
	public function setEmail($email){
		$this->email = $email;
	}
	/*
	 *$method.comment
	 */
	public function getReturnType(){
		return $this->returnType;
	}
	/*
	 *$method.comment
	 */
	 function setReturnType($returnType){
		$this->returnType = $returnType;
	}
	/*
	 *$method.comment
	 */
	public function getApprovalCode(){
		return $this->approvalCode;
	}
	/*
	 *$method.comment
	 */
	 function setApprovalCode($approvalCode){
		$this->approvalCode = $approvalCode;
	}
	/*
	 *$method.comment
	 */
	public function getReferenceNumber(){
		return $this->referenceNumber;
	}
	/*
	 *$method.comment
	 */
	public function setReferenceNumber($referenceNumber){
		$this->referenceNumber = $referenceNumber;
	}
	/*
	 *$method.comment
	 */
	public function getAccountNumber(){
		return $this->accountNumber;
	}
	/*
	 *$method.comment
	 */
	 function setAccountNumber($accountNumber){
		$this->accountNumber = $accountNumber;
	}
	/*
	 *$method.comment
	 */
	public function getAccessory(){
		return $this->accessory;
	}
	/*
	 *$method.comment
	 */
	 function setAccessory($accessory){
		$this->accessory = $accessory;
	}
	/*
	 *$method.comment
	 */
	public function getCvv2(){
		return $this->cvv2;
	}
	/*
	 *$method.comment
	 */
	public function setCvv2($cvv2){
		$this->cvv2 = $cvv2;
	}
	/*
	 *$method.comment
	 */
	public function getTrackData(){
		return $this->trackData;
	}
	/*
	 *$method.comment
	 */
	public function setTrackData($trackData){
		$this->trackData = $trackData;
	}
	/*
	 *$method.comment
	 */
	public function getTransactionType(){
		return $this->transactionType;
	}
	/*
	 *$method.comment
	 */
	 function setTransactionType($transactionType){
		$this->transactionType = $transactionType;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "CaptureInfo";
	}
	/*
	 *$method.comment
	 */
	public function getIssuerBankName(){
		return $issuerBankName;
	}
	/*
	 *$method.comment
	 */
	public function setIssuerBankName($issuerBankName){
		$this->issuerBankName = $issuerBankName;
	}
	/*
	 *$method.comment
	 */
	public function getIssuerBankPhone(){
		return $issuerBankPhone;
	}
	/*
	 *$method.comment
	 */
	public function setIssuerBankPhone($issuerBankPhone){
		$this->issuerBankPhone = $issuerBankPhone;
	}
	/*
	 *$method.comment
	 */
	public function getRiskScore(){
		return $riskScore;
	}
	/*
	 *$method.comment
	 */
	public function setRiskScore($riskScore){
		$this->riskScore = $riskScore;
	}
	/*
	 *$method.comment
	 */
	public function getRiskExplanation(){
		return $riskExplanation;
	}
	/*
	 *$method.comment
	 */
	public function setRiskExplanation($riskExplanation){
		$this->riskExplanation = $riskExplanation;
	}
	/*
	 *$method.comment
	 */
	public function getSysMessage(){
		return $this->sysMessage;
	}
	/*
	 *$method.comment
	 */
	public function setSysMessage($sysMessage){
		$this->sysMessage = $sysMessage;
	}
	/*
	 *$method.comment
	 */
	public function getTokenCode(){
		return $this->tokenCode;
	}
	/*
	 *$method.comment
	 */
	public function setTokenCode($tokenCode){
		$this->tokenCode = $tokenCode;
	}
	/*
	 *$method.comment
	 */
	public function getBankInfo(){
		return $this->bankInfo;
	}
	/*
	 *$method.comment
	 */
	 function setBankInfo($bankInfo){
		$this->bankInfo = $bankInfo;
	}
	/*
	 *$method.comment
	 */
	public function getIsProxynizationEnabled(){
		return $this->isProxynizationEnabled;
	}
	/*
	 *$method.comment
	 */
	 function setIsProxynizationEnabled($isProxynizationEnabled){
		$this->isProxynizationEnabled = $isProxynizationEnabled;
	}
	/*
	 *$method.comment
	 */
	public function getBinEncoded(){
		return $this->binEncoded;
	}
	/*
	 *$method.comment
	 */
	 function setBinEncoded($binEncoded){
		$this->binEncoded = $binEncoded;
	}
}

?>

