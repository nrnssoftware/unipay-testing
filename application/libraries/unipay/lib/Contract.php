<?php



/*
 *$comment
 */
class Contract extends ClientObject {
		/*
	 *@var null
	 */
	private $name;
		/*
	 *@var null
	 */
	private $type;
		/*
	 *@var null
	 */
	private $status;
		/*
	 *@var null
	 */
	private $creatorCode;
		/*
	 *@var null
	 */
	private $sellerCode;
		/*
	 *@var null
	 */
	private $importCode;
		/*
	 *@var null
	 */
	private $effectiveDate;
		/*
	 *@var null
	 */
	private $expirationDate;
		/*
	 *@var null
	 */
	private $level;
		/*
	 *@var null
	 */
	private $length;
		/*
	 *@var null
	 */
	private $value;
		/*
	 *@var null
	 */
	private $industryType;
		/*
	 *@var null
	 */
	private $freezeBeginDate;
		/*
	 *@var null
	 */
	private $freezeEndDate;
		/*
	 *@var null
	 */
	private $documentCode;
		/*
	 *@var null
	 */
	private $isVisibleExternally;
		/*
	 *@var null
	 */
	private $defaultPaymentPlan;
		/*
	 *@var null
	 */
	private $invoice;
		/*
	 *@var null
	 */
	private $customerAccount;
		/*
	 *@var null
	 */
	private $freezeAdjustment;
		/*
	 *@var null
	 */
	private $cancellationAdjustment;
		
	/*
	 *$constructor.comment
	 */
	function __construct(){
		//initialization block
		$this->isVisibleExternally = false;
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
	}

	/*
	 *$method.comment
	 */
	public function getName(){
		return $this->name;
	}
	/*
	 *$method.comment
	 */
	public function setName($name){
		$this->name = $name;
	}
	/*
	 *$method.comment
	 */
	public function getType(){
		return $this->type;
	}
	/*
	 *$method.comment
	 */
	public function setType($type){
		$this->type = $type;
	}
	/*
	 *$method.comment
	 */
	public function getStatus(){
		return $this->status;
	}
	/*
	 *$method.comment
	 */
	 function setStatus($status){
		$this->status = $status;
	}
	/*
	 *$method.comment
	 */
	public function getCreatorCode(){
		return $this->creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function setCreatorCode($creatorCode){
		$this->creatorCode = $creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function getSellerCode(){
		return $this->sellerCode;
	}
	/*
	 *$method.comment
	 */
	public function setSellerCode($sellerCode){
		$this->sellerCode = $sellerCode;
	}
	/*
	 *$method.comment
	 */
	public function getImportCode(){
		return $this->importCode;
	}
	/*
	 *$method.comment
	 */
	public function setImportCode($importCode){
		$this->importCode = $importCode;
	}
	/*
	 *$method.comment
	 */
	public function getEffectiveDate(){
		return $this->effectiveDate;
	}
	/*
	 *$method.comment
	 */
	public function setEffectiveDate($effectiveDate){
		$this->effectiveDate = $effectiveDate;
	}
	/*
	 *$method.comment
	 */
	public function getExpirationDate(){
		return $this->expirationDate;
	}
	/*
	 *$method.comment
	 */
	public function setExpirationDate($expirationDate){
		$this->expirationDate = $expirationDate;
	}
	/*
	 *$method.comment
	 */
	public function getLevel(){
		return $this->level;
	}
	/*
	 *$method.comment
	 */
	public function setLevel($level){
		$this->level = $level;
	}
	/*
	 *$method.comment
	 */
	public function getLength(){
		return $this->length;
	}
	/*
	 *$method.comment
	 */
	public function setLength($length){
		$this->length = $length;
	}
	/*
	 *$method.comment
	 */
	public function getValue(){
		return $this->value;
	}
	/*
	 *$method.comment
	 */
	public function setValue($value){
		$this->value = $value;
	}
	/*
	 *$method.comment
	 */
	public function getIndustryType(){
		return $industryType;
	}
	/*
	 *$method.comment
	 */
	public function setIndustryType($industryType){
		$this->industryType = $industryType;
	}
	/*
	 *$method.comment
	 */
	public function getFreezeBeginDate(){
		return $this->freezeBeginDate;
	}
	/*
	 *$method.comment
	 */
	public function setFreezeBeginDate($freezeBeginDate){
		if (!is_null($this->freezeBeginDate) && !($this->freezeBeginDate == $freezeBeginDate)){
			if (!is_null($freezeBeginDate) && Helper::before($freezeBeginDate, new DateTime(null, new DateTimeZone('EST')))){
				throw new Exception("Invalid field value Freeze Begin Date: Freeze Begin Date should be in future. Object: %1$s; Code: %2$s;", "Contract", is_null($this->getCode()) ? "unspecified" : $this->getCode());
			}
		}
		$this->freezeBeginDate = $freezeBeginDate;
	}
	/*
	 *$method.comment
	 */
	public function getFreezeEndDate(){
		return $this->freezeEndDate;
	}
	/*
	 *$method.comment
	 */
	public function setFreezeEndDate($freezeEndDate){
		if (!is_null($freezeEndDate) && ContractType::PIF == $this->type){
			if (is_null($this->freezeBeginDate)){
				throw new Exception("Unable to set Freeze End Date: freeze is not active.");
			}
			if (Helper::before($freezeEndDate, $this->freezeBeginDate)){
				throw new Exception("Invalid field value Freeze End Date: Freeze End Date should be great or equal Freeze Begin Date. Object: %1$s; Code: %2$s;", "Contract", is_null($this->getCode()) ? "unspecified" : $this->getCode());
			}
		}
		$this->freezeEndDate = $freezeEndDate;
	}
	/*
	 *$method.comment
	 */
	public function getDocumentCode(){
		return $this->documentCode;
	}
	/*
	 *$method.comment
	 */
	public function setDocumentCode($documentCode){
		$this->documentCode = $documentCode;
	}
	/*
	 *$method.comment
	 */
	public function getDefaultPaymentPlan(){
		return $this->defaultPaymentPlan;
	}
	/*
	 *$method.comment
	 */
	public function getIsVisibleExternally(){
		return $this->isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function setIsVisibleExternally($isVisibleExternally){
		$this->isVisibleExternally = $isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function setDefaultPaymentPlan($defaultPaymentPlan){
		$this->defaultPaymentPlan = $defaultPaymentPlan;
	}
	/*
	 *$method.comment
	 */
	public function getInvoice(){
		return $this->invoice;
	}
	/*
	 *$method.comment
	 */
	public function setInvoice($invoice){
		$this->invoice = $invoice;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccount($customerAccount){
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getFreezeAdjustment(){
		return $freezeAdjustment;
	}
	/*
	 *$method.comment
	 */
	public function setFreezeAdjustment($freezeAdjustment){
		$this->freezeAdjustment = $freezeAdjustment;
	}
	/*
	 *$method.comment
	 */
	public function getCancellationAdjustment(){
		return $cancellationAdjustment;
	}
	/*
	 *$method.comment
	 */
	public function setCancellationAdjustment($cancellationAdjustment){
		$this->cancellationAdjustment = $cancellationAdjustment;
	}
	/*
	 *$method.comment
	 */
	public function cancel(){
		if (ContractStatusType::Rollover == $this->status){
			$this->status = ContractStatusType::Expired;
		}
		else {
			$this->status = ContractStatusType::Cancelled;
		}
	}
	/*
	 *$method.comment
	 */
	public function setPendingStatus(){
		if (!(ContractType::PIF == $this->type)){
			throw new Exception("Unable to change Contract status to Pending: only for PIF Contract it can be done.");
		}
		$this->status = ContractStatusType::Pending;
	}
	/*
	 *$method.comment
	 */
	public function isActive(){
		return !((ContractStatusType::Cancelled == $this->status || ContractStatusType::Expired == $this->status || ContractStatusType::Pending == $this->status));
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "Contract";
	}
	/*
	 *$method.comment
	 */
	public function createAdjustment(){
		if (ContractStatusType::Cancelled == $this->status || ContractStatusType::Expired == $this->status){
			throw new Exception("Unable to create Adjustment: Contract is inactive.");
		}
		$adjustment = new Adjustment();
		$adjustment->setMerchantAccountCode($this->getMerchantAccountCode());
		$adjustment->setCollectorAccountCode(is_null($this->customerAccount->getSentCollectionsDate()) ? null : $this->customerAccount->getCollectorAccountCode());
		$adjustment->setIsProcessed(true);
		$adjustment->setIsVisibleExternally(true);
		if (ContractType::PIF == $this->getType()){
			$adjustment->setLength($this->length);
			$adjustment->setValue($this->value);
			$adjustment->setExpirationDate($this->expirationDate);
		}
		else {
			if (!is_null($this->defaultPaymentPlan) && PaymentPlanType::Fixed == $this->defaultPaymentPlan->getType()){
				$adjustment->setLength($this->defaultPaymentPlan->getLength());
				$adjustment->setValue($this->defaultPaymentPlan->getValue());
				$adjustment->setExpirationDate($this->expirationDate);
				$adjustment->setRenewalAmount($this->defaultPaymentPlan->getRenewalAmount());
			}
		}
		$adjustment->setContract($this);
		$adjustment->setCustomerAccount($this->customerAccount);
		$adjustment->setPaymentPlan($this->defaultPaymentPlan);
		return $adjustment;
	}
}

?>

