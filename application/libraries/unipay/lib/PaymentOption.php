<?php


/*
 *$comment
 */
class PaymentOption extends ClientObject {
		/*
	 *@var null
	 */
	const MERCHANT_ACCOUNT_CODE_ASC = "paymentOption.merchantAccountCode";
		/*
	 *@var null
	 */
	const MERCHANT_ACCOUNT_CODE_DESC = "paymentOption.merchantAccountCode desc";
		/*
	 *@var null
	 */
	const CODE_ASC = "paymentOption.refCode";
		/*
	 *@var null
	 */
	const CODE_DESC = "paymentOption.refCode desc";
		/*
	 *@var null
	 */
	const IS_ACTIVE_ASC = "paymentOption.isActive";
		/*
	 *@var null
	 */
	const IS_ACTIVE_DESC = "paymentOption.isActive desc";
		/*
	 *@var null
	 */
	const CREATE_DATE_ASC = "paymentOption.createDate";
		/*
	 *@var null
	 */
	const CREATE_DATE_DESC = "paymentOption.createDate desc";
		/*
	 *@var null
	 */
	const HOLDER_NAME_ASC = "paymentOption.holderName";
		/*
	 *@var null
	 */
	const HOLDER_NAME_DESC = "paymentOption.holderName desc";
		/*
	 *@var null
	 */
	const STREET1_ASC = "paymentOption.street1";
		/*
	 *@var null
	 */
	const STREET1_DESC = "paymentOption.street1 desc";
		/*
	 *@var null
	 */
	const STREET2_ASC = "paymentOption.street2";
		/*
	 *@var null
	 */
	const STREET2_DESC = "paymentOption.street2 desc";
		/*
	 *@var null
	 */
	const CITY_ASC = "paymentOption.city";
		/*
	 *@var null
	 */
	const CITY_DESC = "paymentOption.city desc";
		/*
	 *@var null
	 */
	const STATE_ASC = "paymentOption.state";
		/*
	 *@var null
	 */
	const STATE_DESC = "paymentOption.state desc";
		/*
	 *@var null
	 */
	const ZIP_CODE_ASC = "paymentOption.zipCode";
		/*
	 *@var null
	 */
	const ZIP_CODE_DESC = "paymentOption.zipCode desc";
		/*
	 *@var null
	 */
	const TYPE_ASC = "paymentOption.paymentOptionCl";
		/*
	 *@var null
	 */
	const TYPE_DESC = "paymentOption.paymentOptionCl desc";
		/*
	 *@var null
	 */
	const CUSTOMER_ACCOUNT_CODE_ASC = "paymentOption.customerAccount.refCode";
		/*
	 *@var null
	 */
	const CUSTOMER_ACCOUNT_CODE_DESC = "paymentOption.customerAccount.refCode desc";
		/*
	 *@var null
	 */
	private $isActive;
		/*
	 *@var null
	 */
	private $holderName;
		/*
	 *@var null
	 */
	private $number;
		/*
	 *@var null
	 */
	private $accessory;
		/*
	 *@var null
	 */
	private $street1;
		/*
	 *@var null
	 */
	private $street2;
		/*
	 *@var null
	 */
	private $city;
		/*
	 *@var null
	 */
	private $state;
		/*
	 *@var null
	 */
	private $zipCode;
		/*
	 *@var null
	 */
	private $type;
		/*
	 *@var null
	 */
	private $lastUpdateDate;
		/*
	 *@var null
	 */
	private $issue;
		/*
	 *@var null
	 */
	private $cvv2;
		/*
	 *@var null
	 */
	private $maskedNumber;
		/*
	 *@var null
	 */
	private $maskedAccessory;
		/*
	 *@var null
	 */
	private $issuerBankName;
		/*
	 *@var null
	 */
	private $issuerBankPhone;
		/*
	 *@var null
	 */
	private $countryType;
		/*
	 *@var null
	 */
	private $phone;
		/*
	 *@var null
	 */
	private $customerAccount;
		/*
	 *@var null
	 */
	private $isVisibleExternally;
		/*
	 *@var null
	 */
	private $tokenCode;
		/*
	 *@var null
	 */
	private $isUpdateRequired;
		/*
	 *@var null
	 */
	private $lastAccountUpdateDate;
		/*
	 *@var null
	 */
	private $bankInfo;
		/*
	 *@var null
	 */
	private $isProxynizationEnabled;
		/*
	 *@var null
	 */
	private $binEncoded;
		
	/*
	 *$constructor.comment
	 */
	function __construct($code = null, $createDate = null, $isActive = null, $holderName = null, $number = null, $accessory = null, $street1 = null, $street2 = null, $city = null, $state = null, $zipCode = null, $type = null, $merchantAccountCode = null, $lastUpdateDate = null, $customerAccount = null, $issue = null, $id = null, $maskedNumber = null, $maskedAccessory = null){
		//initialization block
		$this->MERCHANT_ACCOUNT_CODE_ASC = "paymentOption.merchantAccountCode";
		$this->MERCHANT_ACCOUNT_CODE_DESC = "paymentOption.merchantAccountCode desc";
		$this->CODE_ASC = "paymentOption.refCode";
		$this->CODE_DESC = "paymentOption.refCode desc";
		$this->IS_ACTIVE_ASC = "paymentOption.isActive";
		$this->IS_ACTIVE_DESC = "paymentOption.isActive desc";
		$this->CREATE_DATE_ASC = "paymentOption.createDate";
		$this->CREATE_DATE_DESC = "paymentOption.createDate desc";
		$this->HOLDER_NAME_ASC = "paymentOption.holderName";
		$this->HOLDER_NAME_DESC = "paymentOption.holderName desc";
		$this->STREET1_ASC = "paymentOption.street1";
		$this->STREET1_DESC = "paymentOption.street1 desc";
		$this->STREET2_ASC = "paymentOption.street2";
		$this->STREET2_DESC = "paymentOption.street2 desc";
		$this->CITY_ASC = "paymentOption.city";
		$this->CITY_DESC = "paymentOption.city desc";
		$this->STATE_ASC = "paymentOption.state";
		$this->STATE_DESC = "paymentOption.state desc";
		$this->ZIP_CODE_ASC = "paymentOption.zipCode";
		$this->ZIP_CODE_DESC = "paymentOption.zipCode desc";
		$this->TYPE_ASC = "paymentOption.paymentOptionCl";
		$this->TYPE_DESC = "paymentOption.paymentOptionCl desc";
		$this->CUSTOMER_ACCOUNT_CODE_ASC = "paymentOption.customerAccount.refCode";
		$this->CUSTOMER_ACCOUNT_CODE_DESC = "paymentOption.customerAccount.refCode desc";
		$this->isActive = false;
		$this->isVisibleExternally = false;
		$this->isUpdateRequired = false;
		$this->isProxynizationEnabled = false;
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
		//constructor with parameters
		$this->setCode($code);
		$this->setCreateDate($createDate);
		$this->isActive = $isActive;
		$this->holderName = $holderName;
		$this->number = $number;
		$this->accessory = $accessory;
		$this->street1 = $street1;
		$this->street2 = $street2;
		$this->city = $city;
		$this->state = $state;
		$this->zipCode = $zipCode;
		$this->phone = $phone;
		$this->type = $type;
		$this->setMerchantAccountCode($merchantAccountCode);
		$this->lastUpdateDate = $lastUpdateDate;
		$this->customerAccount = $customerAccount;
		$this->issue = $issue;
		$this->setId($id);
		$this->maskedNumber = $maskedNumber;
		$this->maskedAccessory = $maskedAccessory;
		$this->setRefId(is_null($id) ? $this->hashCode() : $id);
	
	}

	/*
	 *$method.comment
	 */
	public function getIsActive(){
		return $this->isActive;
	}
	/*
	 *$method.comment
	 */
	public function setIsActive($isActive){
		$this->isActive = $isActive;
	}
	/*
	 *$method.comment
	 */
	public function getHolderName(){
		return $this->holderName;
	}
	/*
	 *$method.comment
	 */
	public function setHolderName($holderName){
		$this->holderName = $holderName;
	}
	/*
	 *$method.comment
	 */
	public function getNumber(){
		return $this->number;
	}
	/*
	 *$method.comment
	 */
	public function setNumber($number){
		$this->number = $number;
	}
	/*
	 *$method.comment
	 */
	public function getAccessory(){
		return $this->accessory;
	}
	/*
	 *$method.comment
	 */
	public function setAccessory($accessory){
		$this->accessory = $accessory;
	}
	/*
	 *$method.comment
	 */
	public function getStreet1(){
		return $this->street1;
	}
	/*
	 *$method.comment
	 */
	public function setStreet1($street1){
		$this->street1 = $street1;
	}
	/*
	 *$method.comment
	 */
	public function getStreet2(){
		return $this->street2;
	}
	/*
	 *$method.comment
	 */
	public function setStreet2($street2){
		$this->street2 = $street2;
	}
	/*
	 *$method.comment
	 */
	public function getCity(){
		return $this->city;
	}
	/*
	 *$method.comment
	 */
	public function setCity($city){
		$this->city = $city;
	}
	/*
	 *$method.comment
	 */
	public function getState(){
		return $this->state;
	}
	/*
	 *$method.comment
	 */
	public function setState($state){
		$this->state = $state;
	}
	/*
	 *$method.comment
	 */
	public function getZipCode(){
		return $this->zipCode;
	}
	/*
	 *$method.comment
	 */
	public function setZipCode($zipCode){
		$this->zipCode = $zipCode;
	}
	/*
	 *$method.comment
	 */
	public function getType(){
		return $this->type;
	}
	/*
	 *$method.comment
	 */
	public function setType($type){
		$this->type = $type;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccount($customerAccount){
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getLastUpdateDate(){
		return $this->lastUpdateDate;
	}
	/*
	 *$method.comment
	 */
	 function setLastUpdateDate($lastUpdateDate){
		$this->lastUpdateDate = $lastUpdateDate;
	}
	/*
	 *$method.comment
	 */
	public function getIssue(){
		return $this->issue;
	}
	/*
	 *$method.comment
	 */
	public function setIssue($issue){
		$this->issue = $issue;
	}
	/*
	 *$method.comment
	 */
	public function clearIssue(){
		$this->issue = null;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		if (is_null($this->type)){
			throw new Exception("Cannot resolve type for PaymentOption.");
		}
		else {
			if (PaymentOptionType::Checking == $this->type || PaymentOptionType::Savings == $this->type){
				return "BankAccount";
			}
			else {
				return "CreditCard";
			}
		}
	}
	/*
	 *$method.comment
	 */
	public function getCvv2(){
		return $this->cvv2;
	}
	/*
	 *$method.comment
	 */
	public function setCvv2($cvv2){
		$this->cvv2 = $cvv2;
	}
	/*
	 *$method.comment
	 */
	public function getMaskedNumber(){
		return $this->maskedNumber;
	}
	/*
	 *$method.comment
	 */
	 function setMaskedNumber($maskedNumber){
		$this->maskedNumber = $maskedNumber;
	}
	/*
	 *$method.comment
	 */
	public function getMaskedAccessory(){
		return $this->maskedAccessory;
	}
	/*
	 *$method.comment
	 */
	 function setMaskedAccessory($maskedAccessory){
		$this->maskedAccessory = $maskedAccessory;
	}
	/*
	 *$method.comment
	 */
	public function getIssuerBankName(){
		return $this->issuerBankName;
	}
	/*
	 *$method.comment
	 */
	public function setIssuerBankName($issuerBankName){
		$this->issuerBankName = $issuerBankName;
	}
	/*
	 *$method.comment
	 */
	public function getIssuerBankPhone(){
		return $this->issuerBankPhone;
	}
	/*
	 *$method.comment
	 */
	public function setIssuerBankPhone($issuerBankPhone){
		$this->issuerBankPhone = $issuerBankPhone;
	}
	/*
	 *$method.comment
	 */
	public function getCountryType(){
		return $this->countryType;
	}
	/*
	 *$method.comment
	 */
	public function setCountryType($countryType){
		$this->countryType = $countryType;
	}
	/*
	 *$method.comment
	 */
	public function getIsVisibleExternally(){
		return $this->isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function setIsVisibleExternally($isVisibleExternally){
		$this->isVisibleExternally = $isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function getTokenCode(){
		return $this->tokenCode;
	}
	/*
	 *$method.comment
	 */
	public function setTokenCode($tokenCode){
		$this->tokenCode = $tokenCode;
	}
	/*
	 *$method.comment
	 */
	public function getIsUpdateRequired(){
		return $this->isUpdateRequired;
	}
	/*
	 *$method.comment
	 */
	public function setIsUpdateRequired($isUpdateRequired){
		$this->isUpdateRequired = $isUpdateRequired;
	}
	/*
	 *$method.comment
	 */
	public function getLastAccountUpdateDate(){
		return $this->lastAccountUpdateDate;
	}
	/*
	 *$method.comment
	 */
	 function setLastAccountUpdateDate($lastAccountUpdateDate){
		$this->lastAccountUpdateDate = $lastAccountUpdateDate;
	}
	/*
	 *$method.comment
	 */
	public function getBankInfo(){
		return $this->bankInfo;
	}
	/*
	 *$method.comment
	 */
	 function setBankInfo($bankInfo){
		$this->bankInfo = $bankInfo;
	}
	/*
	 *$method.comment
	 */
	public function getIsProxynizationEnabled(){
		return $this->isProxynizationEnabled;
	}
	/*
	 *$method.comment
	 */
	 function setIsProxynizationEnabled($isProxynizationEnabled){
		$this->isProxynizationEnabled = $isProxynizationEnabled;
	}
	/*
	 *$method.comment
	 */
	public function getBinEncoded(){
		return $this->binEncoded;
	}
	/*
	 *$method.comment
	 */
	 function setBinEncoded($binEncoded){
		$this->binEncoded = $binEncoded;
	}
	/*
	 *$method.comment
	 */
	public function getPhone(){
		return $this->phone;
	}
	/*
	 *$method.comment
	 */
	public function setPhone($phone){
		$this->phone = $phone;
	}
}

?>

