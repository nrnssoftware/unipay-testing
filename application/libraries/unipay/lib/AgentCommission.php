<?php



/*
 *$comment
 */
class AgentCommission extends ClientObject {
		/*
	 *@var null
	 */
	private $creatorCode;
		/*
	 *@var null
	 */
	private $agentCode;
		/*
	 *@var null
	 */
	private $amount;
		/*
	 *@var null
	 */
	private $commissionType;
		/*
	 *@var null
	 */
	private $note;
		/*
	 *@var null
	 */
	private $user;
		/*
	 *@var null
	 */
	private $customerAccount;
		/*
	 *@var null
	 */
	private $paymentOption;
		/*
	 *@var null
	 */
	private $source;
		/*
	 *@var null
	 */
	private $target;
		/*
	 *@var null
	 */
	private $adjustmentAgentCommission;
		
	/*
	 *$constructor.comment
	 */
	function __construct(){
		//initialization block
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
	}

	/*
	 *$method.comment
	 */
	public function getCreatorCode(){
		return $this->creatorCode;
	}
	/*
	 *$method.comment
	 */
	 function setCreatorCode($creatorCode){
		$this->creatorCode = $creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function getAgentCode(){
		return $this->agentCode;
	}
	/*
	 *$method.comment
	 */
	 function setAgentCode($agentCode){
		$this->agentCode = $agentCode;
	}
	/*
	 *$method.comment
	 */
	public function getAmount(){
		return $this->amount;
	}
	/*
	 *$method.comment
	 */
	 function setAmount($amount){
		$this->amount = $amount;
	}
	/*
	 *$method.comment
	 */
	public function getCommissionType(){
		return $this->commissionType;
	}
	/*
	 *$method.comment
	 */
	 function setCommissionType($commissionType){
		$this->commissionType = $commissionType;
	}
	/*
	 *$method.comment
	 */
	public function getNote(){
		return $this->note;
	}
	/*
	 *$method.comment
	 */
	 function setNote($note){
		$this->note = $note;
	}
	/*
	 *$method.comment
	 */
	public function getUser(){
		return $this->user;
	}
	/*
	 *$method.comment
	 */
	 function setUser($user){
		$this->user = $user;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccount($customerAccount){
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentOption(){
		return $this->paymentOption;
	}
	/*
	 *$method.comment
	 */
	 function setPaymentOption($paymentOption){
		$this->paymentOption = $paymentOption;
	}
	/*
	 *$method.comment
	 */
	public function getSource(){
		return $this->source;
	}
	/*
	 *$method.comment
	 */
	 function setSource($source){
		$this->source = $source;
	}
	/*
	 *$method.comment
	 */
	public function getTarget(){
		return $this->target;
	}
	/*
	 *$method.comment
	 */
	 function setTarget($target){
		$this->target = $target;
	}
	/*
	 *$method.comment
	 */
	public function getAdjustmentAgentCommission(){
		return $this->adjustmentAgentCommission;
	}
	/*
	 *$method.comment
	 */
	 function setAdjustmentAgentCommission($adjustmentAgentCommission){
		$this->adjustmentAgentCommission = $adjustmentAgentCommission;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "AgentCommission";
	}
}

?>

