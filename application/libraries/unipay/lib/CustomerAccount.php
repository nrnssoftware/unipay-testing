<?php



/*
 *$comment
 */
class CustomerAccount extends ClientObject {
		/*
	 *@var null
	 */
	const MERCHANT_ACCOUNT_CODE_ASC = "customerAccount.merchantAccountCode";
		/*
	 *@var null
	 */
	const MERCHANT_ACCOUNT_CODE_DESC = "customerAccount.merchantAccountCode desc";
		/*
	 *@var null
	 */
	const CODE_ASC = "customerAccount.refCode";
		/*
	 *@var null
	 */
	const CODE_DESC = "customerAccount.refCode desc";
		/*
	 *@var null
	 */
	const FIRST_NAME_ASC = "customerAccount.customer.firstName";
		/*
	 *@var null
	 */
	const FIRST_NAME_DESC = "customerAccount.customer.firstName desc";
		/*
	 *@var null
	 */
	const LAST_NAME_ASC = "customerAccount.customer.lastName";
		/*
	 *@var null
	 */
	const LAST_NAME_DESC = "customerAccount.customer.lastName desc";
		/*
	 *@var null
	 */
	const MIDDLE_NAME_ASC = "customerAccount.customer.middleName";
		/*
	 *@var null
	 */
	const MIDDLE_NAME_DESC = "customerAccount.customer.middleName desc";
		/*
	 *@var null
	 */
	const TITLE_ASC = "customerAccount.customer.title";
		/*
	 *@var null
	 */
	const TITLE_DESC = "customerAccount.customer.title desc";
		/*
	 *@var null
	 */
	const SUFFIX_ASC = "customerAccount.customer.suffix";
		/*
	 *@var null
	 */
	const SUFFIX_DESC = "customerAccount.customer.suffix desc";
		/*
	 *@var null
	 */
	const IS_ACTIVE_ASC = "customerAccount.isActive";
		/*
	 *@var null
	 */
	const IS_ACTIVE_DESC = "customerAccount.isActive desc";
		/*
	 *@var null
	 */
	const CREATE_DATE_ASC = "customerAccount.createDate";
		/*
	 *@var null
	 */
	const CREATE_DATE_DESC = "customerAccount.createDate desc";
		/*
	 *@var null
	 */
	const HOME_PHONE_ASC = "customerAccount.customer.homePhone";
		/*
	 *@var null
	 */
	const HOME_PHONE_DESC = "customerAccount.customer.homePhone desc";
		/*
	 *@var null
	 */
	const WORK_PHONE_ASC = "customerAccount.customer.homePhone";
		/*
	 *@var null
	 */
	const WORK_PHONE_DESC = "customerAccount.customer.homePhone desc";
		/*
	 *@var null
	 */
	const CELL_PHONE_ASC = "customerAccount.customer.homePhone";
		/*
	 *@var null
	 */
	const CELL_PHONE_DESC = "customerAccount.customer.homePhone desc";
		/*
	 *@var null
	 */
	const EMAIL_ASC = "customerAccount.customer.email";
		/*
	 *@var null
	 */
	const EMAIL_DESC = "customerAccount.customer.email desc";
		/*
	 *@var null
	 */
	const ZIP_CODE_ASC = "customerAccount.customer.zipCode";
		/*
	 *@var null
	 */
	const ZIP_CODE_DESC = "customerAccount.customer.zipCode desc";
		/*
	 *@var null
	 */
	const BALANCE_ASC = "customerAccount.balance";
		/*
	 *@var null
	 */
	const BALANCE_DESC = "customerAccount.balance desc";
		/*
	 *@var null
	 */
	const ACCOUNT_GROUP_CODE_ASC = "customerAccount.accountGroupCode.refCode";
		/*
	 *@var null
	 */
	const ACCOUNT_GROUP_CODE_DESC = "customerAccount.accountGroupCode.refCode desc";
		/*
	 *@var null
	 */
	private $customerCode;
		/*
	 *@var null
	 */
	private $firstName;
		/*
	 *@var null
	 */
	private $lastName;
		/*
	 *@var null
	 */
	private $middleName;
		/*
	 *@var null
	 */
	private $title;
		/*
	 *@var null
	 */
	private $suffix;
		/*
	 *@var null
	 */
	private $type;
		/*
	 *@var null
	 */
	private $isActive;
		/*
	 *@var null
	 */
	private $balance;
		/*
	 *@var null
	 */
	private $adjustedBalance;
		/*
	 *@var null
	 */
	private $beneficiaryInfo;
		/*
	 *@var null
	 */
	private $customerAccountGroupCode;
		/*
	 *@var null
	 */
	private $isVerified;
		/*
	 *@var null
	 */
	private $notes;
		/*
	 *@var null
	 */
	private $lastUpdateDate;
		/*
	 *@var null
	 */
	private $birthDate;
		/*
	 *@var null
	 */
	private $lastPaymentDate;
		/*
	 *@var null
	 */
	private $lastReturnReason;
		/*
	 *@var null
	 */
	private $statusChangeDate;
		/*
	 *@var null
	 */
	private $firstBillingDate;
		/*
	 *@var null
	 */
	private $socialSecurity;
		/*
	 *@var null
	 */
	private $collectorCode;
		/*
	 *@var null
	 */
	private $sentCollectionsBalance;
		/*
	 *@var null
	 */
	private $sentCollectionsDate;
		/*
	 *@var null
	 */
	private $isMailDisallowed;
		/*
	 *@var null
	 */
	private $isMarketingEmailAllowed;
		/*
	 *@var null
	 */
	private $isBillingEmailAllowed;
		/*
	 *@var null
	 */
	private $isEmailDisallowed;
		/*
	 *@var null
	 */
	private $isCallingDisallowed;
		/*
	 *@var null
	 */
	private $lastActionDate;
		/*
	 *@var null
	 */
	private $lastAction;
		/*
	 *@var null
	 */
	private $collectionsPriorityType;
		/*
	 *@var null
	 */
	private $externalIdentifier;
		/*
	 *@var null
	 */
	private $writeOffBalance;
		/*
	 *@var null
	 */
	private $customFields;
		/*
	 *@var null
	 */
	private $isChargebackRequested;
		/*
	 *@var null
	 */
	private $isBankruptcyFiled;
		/*
	 *@var null
	 */
	private $isLegalDispute;
		/*
	 *@var null
	 */
	private $isCancellationDispute;
		/*
	 *@var null
	 */
	private $contractDocumentCode;
		/*
	 *@var null
	 */
	private $isDeceased;
		/*
	 *@var null
	 */
	private $isSkipTraceRequired;
		/*
	 *@var null
	 */
	private $skipTraceModeType;
		/*
	 *@var null
	 */
	private $returnFeeBalance;
		/*
	 *@var null
	 */
	private $lateFeeBalance;
		/*
	 *@var null
	 */
	private $synchronizationCode;
		/*
	 *@var null
	 */
	private $paymentPlanValue;
		/*
	 *@var null
	 */
	private $defaultBillingCycleCode;
		/*
	 *@var null
	 */
	private $defaultBillingDate;
		/*
	 *@var null
	 */
	private $defaultPaymentOption;
		/*
	 *@var null
	 */
	private $defaultPaymentPlan;
		/*
	 *@var null
	 */
	private $defaultContract;
		/*
	 *@var null
	 */
	private $defaultAddress;
		/*
	 *@var null
	 */
	private $defaultWorkPhone;
		/*
	 *@var null
	 */
	private $defaultHomePhone;
		/*
	 *@var null
	 */
	private $defaultCellPhone;
		/*
	 *@var null
	 */
	private $defaultEmailAddress;
		/*
	 *@var null
	 */
	private $accountAge;
		/*
	 *@var null
	 */
	private $accountPhase;
		/*
	 *@var null
	 */
	private $isReinstated;
		/*
	 *@var null
	 */
	private $isReinstateProcess;
		/*
	 *@var null
	 */
	private $status;
		/*
	 *@var null
	 */
	private $isCollectionsFeeApplied;
		/*
	 *@var null
	 */
	private $driverLicenseNumber;
		/*
	 *@var null
	 */
	private $driverLicenseState;
		/*
	 *@var null
	 */
	private $buyerCode;
		/*
	 *@var null
	 */
	private $buyerFirstName;
		/*
	 *@var null
	 */
	private $buyerLastName;
		/*
	 *@var null
	 */
	private $buyerMiddleName;
		/*
	 *@var null
	 */
	private $buyerStreet1;
		/*
	 *@var null
	 */
	private $buyerStreet2;
		/*
	 *@var null
	 */
	private $buyerCity;
		/*
	 *@var null
	 */
	private $buyerState;
		/*
	 *@var null
	 */
	private $buyerZipCode;
		/*
	 *@var null
	 */
	private $buyerBirthDate;
		/*
	 *@var null
	 */
	private $buyerSocialSecurity;
		/*
	 *@var null
	 */
	private $buyerDriverLicenseNumber;
		/*
	 *@var null
	 */
	private $buyerPhone;
		/*
	 *@var null
	 */
	private $creditAgencyStatus;
		/*
	 *@var null
	 */
	private $customDate1;
		/*
	 *@var null
	 */
	private $isTax1Exempt;
		/*
	 *@var null
	 */
	private $isTax2Exempt;
		/*
	 *@var null
	 */
	private $isTax3Exempt;
		/*
	 *@var null
	 */
	private $isTax4Exempt;
		/*
	 *@var null
	 */
	private $isTax5Exempt;
		/*
	 *@var null
	 */
	private $merchantAccountExternalCode;
		/*
	 *@var null
	 */
	private $user;
		/*
	 *@var null
	 */
	private $session;
		/*
	 *@var null
	 */
	private $addresses;
		/*
	 *@var null
	 */
	private $phones;
		/*
	 *@var null
	 */
	private $emailAddresses;
		/*
	 *@var null
	 */
	private $paymentOptions;
		/*
	 *@var null
	 */
	private $revenueTransactions;
		/*
	 *@var null
	 */
	private $assetTransactions;
		/*
	 *@var null
	 */
	private $paymentPlans;
		/*
	 *@var null
	 */
	private $notesList;
		/*
	 *@var null
	 */
	private $actions;
		/*
	 *@var null
	 */
	private $letters;
		/*
	 *@var null
	 */
	private $emails;
		/*
	 *@var null
	 */
	private $scheduledPayments;
		/*
	 *@var null
	 */
	private $documents;
		/*
	 *@var null
	 */
	private $auditLogs;
		/*
	 *@var null
	 */
	private $signatures;
		/*
	 *@var null
	 */
	private $contracts;
		/*
	 *@var null
	 */
	private $adjustments;
		
	/*
	 *$constructor.comment
	 */
	function __construct(){
		//initialization block
		$this->MERCHANT_ACCOUNT_CODE_ASC = "customerAccount.merchantAccountCode";
		$this->MERCHANT_ACCOUNT_CODE_DESC = "customerAccount.merchantAccountCode desc";
		$this->CODE_ASC = "customerAccount.refCode";
		$this->CODE_DESC = "customerAccount.refCode desc";
		$this->FIRST_NAME_ASC = "customerAccount.customer.firstName";
		$this->FIRST_NAME_DESC = "customerAccount.customer.firstName desc";
		$this->LAST_NAME_ASC = "customerAccount.customer.lastName";
		$this->LAST_NAME_DESC = "customerAccount.customer.lastName desc";
		$this->MIDDLE_NAME_ASC = "customerAccount.customer.middleName";
		$this->MIDDLE_NAME_DESC = "customerAccount.customer.middleName desc";
		$this->TITLE_ASC = "customerAccount.customer.title";
		$this->TITLE_DESC = "customerAccount.customer.title desc";
		$this->SUFFIX_ASC = "customerAccount.customer.suffix";
		$this->SUFFIX_DESC = "customerAccount.customer.suffix desc";
		$this->IS_ACTIVE_ASC = "customerAccount.isActive";
		$this->IS_ACTIVE_DESC = "customerAccount.isActive desc";
		$this->CREATE_DATE_ASC = "customerAccount.createDate";
		$this->CREATE_DATE_DESC = "customerAccount.createDate desc";
		$this->HOME_PHONE_ASC = "customerAccount.customer.homePhone";
		$this->HOME_PHONE_DESC = "customerAccount.customer.homePhone desc";
		$this->WORK_PHONE_ASC = "customerAccount.customer.homePhone";
		$this->WORK_PHONE_DESC = "customerAccount.customer.homePhone desc";
		$this->CELL_PHONE_ASC = "customerAccount.customer.homePhone";
		$this->CELL_PHONE_DESC = "customerAccount.customer.homePhone desc";
		$this->EMAIL_ASC = "customerAccount.customer.email";
		$this->EMAIL_DESC = "customerAccount.customer.email desc";
		$this->ZIP_CODE_ASC = "customerAccount.customer.zipCode";
		$this->ZIP_CODE_DESC = "customerAccount.customer.zipCode desc";
		$this->BALANCE_ASC = "customerAccount.balance";
		$this->BALANCE_DESC = "customerAccount.balance desc";
		$this->ACCOUNT_GROUP_CODE_ASC = "customerAccount.accountGroupCode.refCode";
		$this->ACCOUNT_GROUP_CODE_DESC = "customerAccount.accountGroupCode.refCode desc";
		$this->isActive = false;
		$this->adjustedBalance = 0;
		$this->isVerified = false;
		$this->sentCollectionsBalance = 0;
		$this->isMailDisallowed = false;
		$this->isMarketingEmailAllowed = false;
		$this->isBillingEmailAllowed = false;
		$this->isEmailDisallowed = false;
		$this->isCallingDisallowed = false;
		$this->isChargebackRequested = false;
		$this->isBankruptcyFiled = false;
		$this->isLegalDispute = false;
		$this->isCancellationDispute = false;
		$this->isDeceased = false;
		$this->isSkipTraceRequired = false;
		$this->returnFeeBalance = 0;
		$this->lateFeeBalance = 0;
		$this->isReinstated = false;
		$this->isReinstateProcess = false;
		$this->isCollectionsFeeApplied = false;
		$this->isTax1Exempt = false;
		$this->isTax2Exempt = false;
		$this->isTax3Exempt = false;
		$this->isTax4Exempt = false;
		$this->isTax5Exempt = false;
		$this->addresses = new ArrayList();
		$this->phones = new ArrayList();
		$this->emailAddresses = new ArrayList();
		$this->paymentOptions = new ArrayList();
		$this->revenueTransactions = new ArrayList();
		$this->assetTransactions = new ArrayList();
		$this->paymentPlans = new ArrayList();
		$this->notesList = new ArrayList();
		$this->actions = new ArrayList();
		$this->letters = new ArrayList();
		$this->emails = new ArrayList();
		$this->scheduledPayments = new ArrayList();
		$this->documents = new ArrayList();
		$this->auditLogs = new ArrayList();
		$this->signatures = new ArrayList();
		$this->contracts = new ArrayList();
		$this->adjustments = new ArrayList();
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
	}

	/*
	 *$method.comment
	 */
	public function setCustomerCode($customerCode){
		$this->customerCode = $customerCode;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerCode(){
		return $this->customerCode;
	}
	/*
	 *$method.comment
	 */
	public function getType(){
		return $this->type;
	}
	/*
	 *$method.comment
	 */
	public function setType($type){
		$this->type = $type;
	}
	/*
	 *$method.comment
	 */
	public function getIsActive(){
		return $this->isActive;
	}
	/*
	 *$method.comment
	 */
	public function setIsActive($isActive){
		$this->isActive = $isActive;
	}
	/*
	 *$method.comment
	 */
	public function getHomePhone(){
		return is_null($this->defaultHomePhone) ? null : $this->getDefaultHomePhone()->getPhone();
	}
	/*
	 *$method.comment
	 */
	public function setHomePhone($homePhone){
		if (is_null($this->defaultHomePhone) && is_null($homePhone)){
			return ;
		}
		if (is_null($this->defaultHomePhone)){
			$this->createDefaultHomePhone();
		}
		if (is_null($homePhone)){
			if (is_null($this->defaultHomePhone->getId())){
				$this->removePhone($this->defaultHomePhone);
				$this->defaultHomePhone = null;
			}
			else {
				$this->defaultHomePhone->setIsBad(true);
				$this->addPhone($this->defaultHomePhone);
				$this->defaultHomePhone = null;
			}
			return ;
		}
		$this->defaultHomePhone->setPhone($homePhone);
	}
	/*
	 *$method.comment
	 */
	public function getWorkPhone(){
		return is_null($this->defaultWorkPhone) ? null : $this->getDefaultWorkPhone()->getPhone();
	}
	/*
	 *$method.comment
	 */
	public function setWorkPhone($workPhone){
		if (is_null($this->defaultWorkPhone) && is_null($workPhone)){
			return ;
		}
		if (is_null($this->defaultWorkPhone)){
			$this->createDefaultWorkPhone();
		}
		if (is_null($workPhone)){
			if (is_null($this->defaultWorkPhone->getId())){
				$this->removePhone($this->defaultWorkPhone);
				$this->defaultWorkPhone = null;
			}
			else {
				$this->defaultWorkPhone->setIsBad(true);
				$this->addPhone($this->defaultWorkPhone);
				$this->defaultWorkPhone = null;
			}
			return ;
		}
		$this->defaultWorkPhone->setPhone($workPhone);
	}
	/*
	 *$method.comment
	 */
	public function getCellPhone(){
		return is_null($this->defaultCellPhone) ? null : $this->defaultCellPhone->getPhone();
	}
	/*
	 *$method.comment
	 */
	public function setCellPhone($cellPhone){
		if (is_null($this->defaultCellPhone) && is_null($cellPhone)){
			return ;
		}
		if (is_null($this->defaultCellPhone)){
			$this->createDefaultCellPhone();
		}
		if (is_null($cellPhone)){
			if (is_null($this->defaultCellPhone->getId())){
				$this->removePhone($this->defaultCellPhone);
				$this->defaultCellPhone = null;
			}
			else {
				$this->defaultCellPhone->setIsBad(true);
				$this->addPhone($this->defaultCellPhone);
				$this->defaultCellPhone = null;
			}
			return ;
		}
		$this->defaultCellPhone->setPhone($cellPhone);
	}
	/*
	 *$method.comment
	 */
	public function getEmail(){
		return is_null($this->defaultEmailAddress) ? null : $this->getDefaultEmailAddress()->getEmail();
	}
	/*
	 *$method.comment
	 */
	public function setEmail($email){
		if (is_null($this->defaultEmailAddress) && is_null($email)){
			return ;
		}
		if (is_null($this->defaultEmailAddress)){
			$this->createDefaultEmailAddress();
		}
		if (is_null($email)){
			if (is_null($this->defaultEmailAddress->getId())){
				$this->removeEmailAddress($this->defaultEmailAddress);
				$this->defaultEmailAddress = null;
			}
			else {
				$this->defaultEmailAddress->setIsBad(true);
				$this->addEmailAddress($this->defaultEmailAddress);
				$this->defaultEmailAddress = null;
			}
			return ;
		}
		$this->defaultEmailAddress->setEmail($email);
	}
	/*
	 *$method.comment
	 */
	public function getStreet1(){
		return is_null($this->defaultAddress) ? null : $this->getDefaultAddress()->getStreet1();
	}
	/*
	 *$method.comment
	 */
	public function setStreet1($street1){
		if (is_null($this->defaultAddress)){
			$this->createDefaultAddress();
		}
		$this->defaultAddress->setStreet1($street1);
	}
	/*
	 *$method.comment
	 */
	public function getCity(){
		return is_null($this->defaultAddress) ? null : $this->getDefaultAddress()->getCity();
	}
	/*
	 *$method.comment
	 */
	public function setCity($city){
		if (is_null($this->defaultAddress)){
			$this->createDefaultAddress();
		}
		$this->defaultAddress->setCity($city);
	}
	/*
	 *$method.comment
	 */
	public function getState(){
		return is_null($this->defaultAddress) ? null : $this->getDefaultAddress()->getState();
	}
	/*
	 *$method.comment
	 */
	public function setState($state){
		if (is_null($this->defaultAddress)){
			$this->createDefaultAddress();
		}
		$this->defaultAddress->setState($state);
	}
	/*
	 *$method.comment
	 */
	public function getZipCode(){
		return is_null($this->defaultAddress) ? null : $this->getDefaultAddress()->getZipCode();
	}
	/*
	 *$method.comment
	 */
	public function setZipCode($zipCode){
		if (is_null($this->defaultAddress)){
			$this->createDefaultAddress();
		}
		$this->defaultAddress->setZipCode($zipCode);
	}
	/*
	 *$method.comment
	 */
	public function getBalance(){
		return $this->balance;
	}
	/*
	 *$method.comment
	 */
	 function setBalance($balance){
		$this->balance = $balance;
	}
	/*
	 *$method.comment
	 */
	public function getAdjustedBalance(){
		return $this->adjustedBalance;
	}
	/*
	 *$method.comment
	 */
	 function setAdjustedBalance($adjustedBalance){
		$this->adjustedBalance = $adjustedBalance;
	}
	/*
	 *$method.comment
	 */
	public function getBeneficiaryInfo(){
		return $this->beneficiaryInfo;
	}
	/*
	 *$method.comment
	 */
	public function setBeneficiaryInfo($beneficiaryInfo){
		$this->beneficiaryInfo = $beneficiaryInfo;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccountGroupCode(){
		return $this->customerAccountGroupCode;
	}
	/*
	 *$method.comment
	 */
	public function setCustomerAccountGroupCode($customerAccountGroupCode){
		$this->customerAccountGroupCode = $customerAccountGroupCode;
	}
	/*
	 *$method.comment
	 */
	public function getNotes(){
		return $this->notes;
	}
	/*
	 *$method.comment
	 */
	public function setNotes($notes){
		$this->notes = $notes;
	}
	/*
	 *$method.comment
	 */
	public function getBirthDate(){
		return $this->birthDate;
	}
	/*
	 *$method.comment
	 */
	public function setBirthDate($birthDate){
		$this->birthDate = $birthDate;
	}
	/*
	 *$method.comment
	 */
	public function getIsVerified(){
		return $this->isVerified;
	}
	/*
	 *$method.comment
	 */
	public function getLastUpdateDate(){
		return $this->lastUpdateDate;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentOptions(){
		return new ArrayList($this->paymentOptions);
	}
	/*
	 *$method.comment
	 */
	public function addPaymentOption($paymentOption){
		$hasObject = false;
		foreach ($this->paymentOptions as $option){
			if ($option->getRefId() == $paymentOption->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->paymentOptions->contains($paymentOption)) && !($hasObject)){
			$this->paymentOptions->add($paymentOption);
		}
	}
	/*
	 *$method.comment
	 */
	public function removePaymentOption($paymentOption){
		$this->paymentOptions->remove($paymentOption);
	}
	/*
	 *$method.comment
	 */
	public function getRevenueTransactions(){
		return new ArrayList($this->revenueTransactions);
	}
	/*
	 *$method.comment
	 */
	public function addRevenueTransaction($revenueTransaction){
		$hasObject = false;
		foreach ($this->revenueTransactions as $transaction){
			if ($transaction->getRefId() == $revenueTransaction->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->revenueTransactions->contains($revenueTransaction)) && !($hasObject)){
			$this->revenueTransactions->add($revenueTransaction);
		}
	}
	/*
	 *$method.comment
	 */
	public function removeRevenueTransaction($revenueTransaction){
		$this->revenueTransactions->remove($revenueTransaction);
	}
	/*
	 *$method.comment
	 */
	public function getAssetTransactions(){
		return new ArrayList($this->assetTransactions);
	}
	/*
	 *$method.comment
	 */
	public function addAssetTransaction($assetTransaction){
		$hasObject = false;
		foreach ($this->assetTransactions as $transaction){
			if ($transaction->getRefId() == $assetTransaction->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->assetTransactions->contains($assetTransaction)) && !($hasObject)){
			$this->assetTransactions->add($assetTransaction);
		}
	}
	/*
	 *$method.comment
	 */
	public function removeAssetTransaction($assetTransaction){
		$this->assetTransactions->remove($assetTransaction);
	}
	/*
	 *$method.comment
	 */
	public function getPaymentPlans(){
		return new ArrayList($this->paymentPlans);
	}
	/*
	 *$method.comment
	 */
	public function addPaymentPlan($paymentPlan){
		$hasObject = false;
		foreach ($this->paymentPlans as $plan){
			if ($paymentPlan->getRefId() == $plan->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->paymentPlans->contains($paymentPlan)) && !($hasObject)){
			$this->paymentPlans->add($paymentPlan);
		}
	}
	/*
	 *$method.comment
	 */
	public function removePaymentPlan($paymentPlan){
		$this->paymentPlans->remove($paymentPlan);
	}
	/*
	 *$method.comment
	 */
	public function getNotesList(){
		return new ArrayList($this->notesList);
	}
	/*
	 *$method.comment
	 */
	public function addNote($note){
		$hasObject = false;
		foreach ($this->notesList as $n){
			if ($n->getRefId() == $note->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->notesList->contains($note)) && !($hasObject)){
			$this->notesList->add($note);
		}
	}
	/*
	 *$method.comment
	 */
	public function removeNote($note){
		$this->notesList->remove($note);
	}
	/*
	 *$method.comment
	 */
	public function getActions(){
		return new ArrayList($this->actions);
	}
	/*
	 *$method.comment
	 */
	public function addAction($action){
		$hasObject = false;
		foreach ($this->actions as $a){
			if ($a->getRefId() == $action->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->actions->contains($action)) && !($hasObject)){
			$this->actions->add($action);
		}
	}
	/*
	 *$method.comment
	 */
	public function removeAction($action){
		$this->actions->remove($action);
	}
	/*
	 *$method.comment
	 */
	public function getAddresses(){
		return new ArrayList($this->addresses);
	}
	/*
	 *$method.comment
	 */
	public function addAddress($address){
		$hasObject = false;
		foreach ($this->addresses as $a){
			if ($a->getRefId() == $address->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->addresses->contains($address)) && !($hasObject)){
			$this->addresses->add($address);
		}
	}
	/*
	 *$method.comment
	 */
	public function removeAddress($address){
		$this->addresses->remove($address);
	}
	/*
	 *$method.comment
	 */
	public function getPhones(){
		return new ArrayList($this->phones);
	}
	/*
	 *$method.comment
	 */
	public function addPhone($phone){
		$hasObject = false;
		foreach ($this->phones as $p){
			if ($p->getRefId() == $phone->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->phones->contains($phone)) && !($hasObject)){
			$this->phones->add($phone);
		}
	}
	/*
	 *$method.comment
	 */
	public function removePhone($phone){
		$this->phones->remove($phone);
	}
	/*
	 *$method.comment
	 */
	public function getEmailAddresses(){
		return new ArrayList($this->emailAddresses);
	}
	/*
	 *$method.comment
	 */
	public function addEmailAddress($emailAddress){
		$hasObject = false;
		foreach ($this->emailAddresses as $a){
			if ($a->getRefId() == $emailAddress->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->emailAddresses->contains($emailAddress)) && !($hasObject)){
			$this->emailAddresses->add($emailAddress);
		}
	}
	/*
	 *$method.comment
	 */
	public function removeEmailAddress($emailAddress){
		$this->emailAddresses->remove($emailAddress);
	}
	/*
	 *$method.comment
	 */
	public function getLetters(){
		return new ArrayList($this->letters);
	}
	/*
	 *$method.comment
	 */
	public function addLetter($letter){
		$hasObject = false;
		foreach ($this->letters as $l){
			if ($l->getRefId() == $letter->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->letters->contains($letter)) && !($hasObject)){
			$this->letters->add($letter);
		}
	}
	/*
	 *$method.comment
	 */
	public function removeLetter($letter){
		$this->letters->remove($letter);
	}
	/*
	 *$method.comment
	 */
	public function getEmails(){
		return new ArrayList($this->emails);
	}
	/*
	 *$method.comment
	 */
	public function addEmail($email){
		$hasObject = false;
		foreach ($this->emails as $e){
			if ($e->getRefId() == $email->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->emails->contains($email)) && !($hasObject)){
			$this->emails->add($email);
		}
	}
	/*
	 *$method.comment
	 */
	public function removeEmail($email){
		$this->emails->remove($email);
	}
	/*
	 *$method.comment
	 */
	public function getDocuments(){
		return new ArrayList($this->documents);
	}
	/*
	 *$method.comment
	 */
	public function addDocument($document){
		$hasObject = false;
		foreach ($this->documents as $d){
			if ($d->getRefId() == $document->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->documents->contains($document)) && !($hasObject)){
			$this->documents->add($document);
		}
	}
	/*
	 *$method.comment
	 */
	public function removeDocument($document){
		$this->documents->remove($document);
	}
	/*
	 *$method.comment
	 */
	public function getAuditLogs(){
		return new ArrayList($this->auditLogs);
	}
	/*
	 *$method.comment
	 */
	public function addAuditLog($auditLog){
		$hasObject = false;
		foreach ($this->auditLogs as $d){
			if ($d->getRefId() == $auditLog->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->auditLogs->contains($auditLog)) && !($hasObject)){
			$this->auditLogs->add($auditLog);
		}
	}
	/*
	 *$method.comment
	 */
	public function removeAuditLog($auditLog){
		$this->auditLogs->remove($auditLog);
	}
	/*
	 *$method.comment
	 */
	public function getSignatures(){
		return new ArrayList($this->signatures);
	}
	/*
	 *$method.comment
	 */
	public function addSignature($signature){
		$hasObject = false;
		foreach ($this->signatures as $d){
			if ($d->getRefId() == $signature->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->signatures->contains($signature)) && !($hasObject)){
			$this->signatures->add($signature);
		}
	}
	/*
	 *$method.comment
	 */
	public function removeSignature($signature){
		$this->signatures->remove($signature);
	}
	/*
	 *$method.comment
	 */
	public function getContracts(){
		return new ArrayList($this->contracts);
	}
	/*
	 *$method.comment
	 */
	public function addContract($contract){
		$hasObject = false;
		foreach ($this->contracts as $c){
			if ($c->getRefId() == $contract->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->contracts->contains($contract)) && !($hasObject)){
			$this->contracts->add($contract);
		}
	}
	/*
	 *$method.comment
	 */
	public function removeContract($contract){
		$this->contracts->remove($contract);
	}
	/*
	 *$method.comment
	 */
	public function removeAdjustment($adjustment){
		$this->adjustments->remove($adjustment);
	}
	/*
	 *$method.comment
	 */
	public function getAdjustments(){
		return new ArrayList($this->adjustments);
	}
	/*
	 *$method.comment
	 */
	public function addAdjustment($adjustment){
		$hasObject = false;
		foreach ($this->adjustments as $a){
			if ($a->getRefId() == $adjustment->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->adjustments->contains($adjustment)) && !($hasObject)){
			$this->adjustments->add($adjustment);
		}
	}
	/*
	 *$method.comment
	 */
	public function clear(){
		$this->paymentOptions->clear();
		$this->revenueTransactions->clear();
		$this->assetTransactions->clear();
		$this->paymentPlans->clear();
		$this->notesList->clear();
		$this->actions->clear();
		$this->letters->clear();
		$this->emails->clear();
		$this->documents->clear();
		$this->auditLogs->clear();
		$this->signatures->clear();
		$this->scheduledPayments->clear();
		$this->contracts->clear();
		$this->adjustments->clear();
		$this->addresses->clear();
		$this->phones->clear();
		$this->emailAddresses->clear();
	}
	/*
	 *$method.comment
	 */
	public function createPaymentOption($addToCustomerAccount=true){
		$paymentOption = new PaymentOption();
		$paymentOption->setCustomerAccount($this);
		$paymentOption->setIsActive(true);
		$paymentOption->setMerchantAccountCode($this->getMerchantAccountCode());
		$paymentOption->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$paymentOption->setIsVisibleExternally(true);
		if ($addToCustomerAccount){
			$this->addPaymentOption($paymentOption);
		}
		return $paymentOption;
	}
	/*
	 *$method.comment
	 */
	public function createCreditCard($code,$holderName,$creditCardNumber,$expirationDate,$type){
		$paymentOption = $this->createPaymentOption(true);
		$paymentOption->setStreet1($this->getStreet1());
		$paymentOption->setStreet2($this->getStreet2());
		$paymentOption->setCity($this->getCity());
		$paymentOption->setState($this->getState());
		$paymentOption->setZipCode($this->getZipCode());
		$paymentOption->setCode($code);
		$paymentOption->setHolderName($holderName);
		$paymentOption->setNumber($creditCardNumber);
		$paymentOption->setType($type);
		$paymentOption->setAccessory($expirationDate);
		return $paymentOption;
	}
	/*
	 *$method.comment
	 */
	public function createBankAccount($code,$holderName,$bankNumber,$routingNumber,$isSaving){
		$paymentOption = $this->createPaymentOption(true);
		$paymentOption->setStreet1($this->getStreet1());
		$paymentOption->setStreet2($this->getStreet2());
		$paymentOption->setCity($this->getCity());
		$paymentOption->setState($this->getState());
		$paymentOption->setZipCode($this->getZipCode());
		$paymentOption->setCode($code);
		$paymentOption->setHolderName($holderName);
		$paymentOption->setNumber($bankNumber);
		$paymentOption->setAccessory($routingNumber);
		$paymentOption->setType($isSaving ? PaymentOptionType::Savings : PaymentOptionType::Checking);
		return $paymentOption;
	}
	/*
	 *$method.comment
	 */
	public function createPaymentOptionExtended($street1,$street2,$city,$state,$zipCode){
		$paymentOption = $this->createPaymentOption(true);
		$paymentOption->setStreet1($street1);
		$paymentOption->setStreet2($street2);
		$paymentOption->setCity($city);
		$paymentOption->setState($state);
		$paymentOption->setZipCode($zipCode);
		return $paymentOption;
	}
	/*
	 *$method.comment
	 */
	public function createCreditCardExtended($code,$holderName,$creditCardNumber,$expirationDate,$type,$street1,$street2,$city,$state,$zipCode){
		$paymentOption = $this->createPaymentOption(true);
		$paymentOption->setCode($code);
		$paymentOption->setHolderName($holderName);
		$paymentOption->setNumber($creditCardNumber);
		$paymentOption->setAccessory($expirationDate);
		$paymentOption->setType($type);
		$paymentOption->setStreet1($street1);
		$paymentOption->setStreet2($street2);
		$paymentOption->setCity($city);
		$paymentOption->setState($state);
		$paymentOption->setZipCode($zipCode);
		return $paymentOption;
	}
	/*
	 *$method.comment
	 */
	public function createBankAccountExtended($code,$holderName,$bankNumber,$routingNumber,$street1,$street2,$city,$state,$zipCode,$isSaving){
		$paymentOption = $this->createPaymentOption(true);
		$paymentOption->setCode($code);
		$paymentOption->setHolderName($holderName);
		$paymentOption->setNumber($bankNumber);
		$paymentOption->setAccessory($routingNumber);
		$paymentOption->setType($isSaving ? PaymentOptionType::Savings : PaymentOptionType::Checking);
		$paymentOption->setStreet1($street1);
		$paymentOption->setStreet2($street2);
		$paymentOption->setCity($city);
		$paymentOption->setState($state);
		$paymentOption->setZipCode($zipCode);
		return $paymentOption;
	}
	/*
	 *$method.comment
	 */
	public function createPaymentPlan($addToCustomerAccount=true){
		$paymentPlan = new PaymentPlan();
		$paymentPlan->setStatus(PaymentPlanStatus::Unbilled);
		$paymentPlan->setCustomerAccount($this);
		$paymentPlan->setMerchantAccountCode($this->getMerchantAccountCode());
		$paymentPlan->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$paymentPlan->setIsVisibleExternally(true);
		if ($addToCustomerAccount){
			$this->addPaymentPlan($paymentPlan);
		}
		return $paymentPlan;
	}
	/*
	 *$method.comment
	 */
	public function createPaymentPlanBasic($code,$amount,$billingCycleCode,$firstBillingDate,$length,$type,$isAgainstBalance){
		if ($isAgainstBalance && (is_null($type) || !($type == PaymentPlanType::Fixed))){
			throw new Exception("Invalid payment plan type for isAgainstBalance.");
		}
		$this->validateFirstBillingDate($firstBillingDate, $billingCycleCode, $code);
		$paymentPlan = $this->createPaymentPlan(true);
		$paymentPlan->setCode($code);
		$paymentPlan->setAmount($amount);
		$paymentPlan->setBillingCycleCode($billingCycleCode);
		$paymentPlan->setFirstBillingDate($firstBillingDate);
		$paymentPlan->setNextBillingDate($firstBillingDate);
		$paymentPlan->setType($type);
		$paymentPlan->setIsAgainstBalance($isAgainstBalance);
		if (!is_null($type) && $type == PaymentPlanType::Fixed){
			$paymentPlan->add(0, $length, false);
		}
		return $paymentPlan;
	}
	/*
	 *$method.comment
	 */
	public function createPaymentPlanExtended($code,$sellerCode,$amount,$groupCode1,$groupCode2,$groupCode3,$groupCode4,$groupCode5,$groupCode6,$groupCode7,$groupCode8,$itemCode,$taxCode,$paymentOption,$length,$billingCycleCode,$firstBillingDate,$type,$isAgainstBalance){
		$paymentPlan = $this->createPaymentPlanBasic($code, $amount, $billingCycleCode, $firstBillingDate, $length, $type, $isAgainstBalance);
		$paymentPlan->setSellerCode($sellerCode);
		$paymentPlan->setGroupCode1($groupCode1);
		$paymentPlan->setGroupCode2($groupCode2);
		$paymentPlan->setGroupCode3($groupCode3);
		$paymentPlan->setGroupCode4($groupCode4);
		$paymentPlan->setGroupCode5($groupCode5);
		$paymentPlan->setGroupCode6($groupCode6);
		$paymentPlan->setGroupCode7($groupCode7);
		$paymentPlan->setGroupCode8($groupCode8);
		$paymentPlan->setItemCode($itemCode);
		$paymentPlan->setTaxCode($taxCode);
		$paymentPlan->setPaymentOption($paymentOption);
		return $paymentPlan;
	}
	/*
	 *$method.comment
	 */
	public function createInvoice($type,$amount,$note,$creatorCode,$sellerCode,$shiftCode,$terminalCode,$dueDate,$taxAmount,$itemCode,$paymentOption){
		$revenueTransaction = $this->createRevenueTransaction(true);
		$revenueTransaction->setAccountActivityType($type);
		$revenueTransaction->setAmount($amount);
		$revenueTransaction->setNote($note);
		$revenueTransaction->setCreatorCode($creatorCode);
		$revenueTransaction->setSellerCode($sellerCode);
		$revenueTransaction->setShiftCode($shiftCode);
		$revenueTransaction->setTerminalCode($terminalCode);
		$revenueTransaction->setDueDate($dueDate);
		$revenueTransaction->setTaxAmount($taxAmount);
		$revenueTransaction->setItemCode($itemCode);
		$revenueTransaction->setPaymentOption($paymentOption);
		return $revenueTransaction;
	}
	/*
	 *$method.comment
	 */
	public function createCreditExtended($amount,$note,$creatorCode,$sellerCode,$shiftCode,$terminalCode,$dueDate,$taxAmount,$itemCode,$code){
		$revenueTransaction = $this->createCredit();
		$revenueTransaction->setNote($note);
		$revenueTransaction->setAmount($amount);
		$revenueTransaction->setCreatorCode($creatorCode);
		$revenueTransaction->setSellerCode($sellerCode);
		$revenueTransaction->setShiftCode($shiftCode);
		$revenueTransaction->setTerminalCode($terminalCode);
		$revenueTransaction->setDueDate($dueDate);
		$revenueTransaction->setTaxAmount($taxAmount);
		$revenueTransaction->setItemCode($itemCode);
		$revenueTransaction->setCode($code);
		return $revenueTransaction;
	}
	/*
	 *$method.comment
	 */
	public function reverseRevenueTransaction($transaction){
		if (!is_null($transaction->getAdjustmentTransaction())){
			throw new Exception("Unable to reverse selected transaction; selected transaction was reversed.");
		}
		if (!($transaction->getCustomerAccount()->getId() == $this->getId())){
			throw new Exception("Unable to create cancel transaction, original transaction not found for the selected customer account.");
		}
		$reverseTransaction = new RevenueTransaction();
		$reverseTransaction->setCustomerAccount($this);
		$reverseTransaction->setMerchantAccountCode($transaction->getMerchantAccountCode());
		$reverseTransaction->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$reverseTransaction->setAmount($transaction->getAmount() > 0 ? $transaction->getAmount() : -($transaction->getAmount()));
		$reverseTransaction->setSellerCode($transaction->getSellerCode());
		$reverseTransaction->setShiftCode($transaction->getShiftCode());
		$reverseTransaction->setTerminalCode($transaction->getTerminalCode());
		$reverseTransaction->setIsVisibleExternally(true);
		$reverseTransaction->setItemCode($transaction->getItemCode());
		if (!is_null($transaction->getTaxAmount())){
			$reverseTransaction->setTaxAmount($transaction->getTaxAmount() > 0 ? $transaction->getTaxAmount() : -($transaction->getTaxAmount()));
		}
		if (AccountActivityType::Invoice == $transaction->getAccountActivityType()){
			$reverseTransaction->setAccountActivityType(AccountActivityType::Reversal);
		}
		else {
			if (AccountActivityType::Credit == $transaction->getAccountActivityType() || AccountActivityType::Reinstatement == $transaction->getAccountActivityType()){
				$reverseTransaction->setAccountActivityType(AccountActivityType::CancelCredit);
			}
			else {
				if (AccountActivityType::Reversal == $transaction->getAccountActivityType()){
					$reverseTransaction->setAccountActivityType(AccountActivityType::Invoice);
				}
				else {
					if (AccountActivityType::Fee == $transaction->getAccountActivityType() || AccountActivityType::LateFee == $transaction->getAccountActivityType() || AccountActivityType::AccessFee == $transaction->getAccountActivityType() || AccountActivityType::ServiceFee == $transaction->getAccountActivityType() || AccountActivityType::CollectionsFee == $transaction->getAccountActivityType() || AccountActivityType::CreditCardFee == $transaction->getAccountActivityType() || AccountActivityType::AchFee == $transaction->getAccountActivityType() || AccountActivityType::CancellationFee == $transaction->getAccountActivityType() || AccountActivityType::CreditReportingFee == $transaction->getAccountActivityType() || AccountActivityType::ReinstatementFee == $transaction->getAccountActivityType()){
						$reverseTransaction->setAccountActivityType(AccountActivityType::Reversal);
					}
					else {
						if (AccountActivityType::CancelCredit == $transaction->getAccountActivityType()){
							$reverseTransaction->setAccountActivityType(AccountActivityType::Credit);
						}
					}
				}
			}
		}
		$transaction->setHistoricalBalance(-($transaction->getAmount()));
		$transaction->setAdjustmentTransaction($reverseTransaction);
		$this->addRevenueTransaction($transaction);
		$this->addRevenueTransaction($reverseTransaction);
		return $reverseTransaction;
	}
	/*
	 *$method.comment
	 */
	public function reverseRevenueTransactionExtended($revenueTransaction,$code,$note){
		$voidRevenueTransaction = $this->reverseRevenueTransaction($revenueTransaction);
		$voidRevenueTransaction->setCode($code);
		$voidRevenueTransaction->setNote($note);
		return $voidRevenueTransaction;
	}
	/*
	 *$method.comment
	 */
	public function reversePayment($transaction){
		if (!(AccountActivityType::Payment == $transaction->getAccountActivityType())){
			throw new Exception("Unable to void selected transaction; only payments can be voided.");
		}
		if (!is_null($transaction->getAdjustmentTransaction())){
			throw new Exception("Unable to void selected transaction; selected transaction was voided.");
		}
		if (!($transaction->getCustomerAccount()->getId() == $this->getId())){
			throw new Exception("Unable to create cancel transaction, original transaction not found for the selected customer account.");
		}
		$reverseTransaction = new AssetTransaction();
		$reverseTransaction->setCustomerAccount($this);
		$reverseTransaction->setMerchantAccountCode($transaction->getMerchantAccountCode());
		$reverseTransaction->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$reverseTransaction->setAccountActivityType(AccountActivityType::Void);
		$reverseTransaction->setAmount(-($transaction->getAmount()));
		if (!is_null($transaction->getTaxAmount())){
			$reverseTransaction->setTaxAmount($transaction->getTaxAmount() > 0 ? $transaction->getTaxAmount() : -($transaction->getTaxAmount()));
		}
		$reverseTransaction->setAccessory($transaction->getAccessory());
		$reverseTransaction->setAccountNumber($transaction->getAccountNumber());
		$reverseTransaction->setSellerCode($transaction->getSellerCode());
		$reverseTransaction->setShiftCode($transaction->getShiftCode());
		$reverseTransaction->setTerminalCode($transaction->getTerminalCode());
		$reverseTransaction->setTransactionType($transaction->getTransactionType());
		$reverseTransaction->setIsVisibleExternally(true);
		$transaction->setHistoricalBalance(-($transaction->getAmount()));
		$transaction->setAdjustmentTransaction($reverseTransaction);
		$this->addAssetTransaction($transaction);
		$this->addAssetTransaction($reverseTransaction);
		$captureInfo = $transaction->getCaptureInfo();
		if (!is_null($captureInfo)){
			$reverseCaptureInfo = $reverseTransaction->createCaptureInfoExtended($captureInfo->getHolderName(), $captureInfo->getStreet(), $captureInfo->getCity(), $captureInfo->getState(), $captureInfo->getPhone(), $captureInfo->getZipCode(), $captureInfo->getEmail(), null, null);
			$reverseCaptureInfo->setTransactionType($transaction->getTransactionType());
			$reverseCaptureInfo->setAccountNumber($captureInfo->getAccountNumber());
			$reverseCaptureInfo->setAccessory($captureInfo->getAccessory());
			$reverseCaptureInfo->setTaxAmount($captureInfo->getTaxAmount());
			$reverseCaptureInfo->setRequestDate(new DateTime(null, new DateTimeZone('EST')));
			$reverseCaptureInfo->setReferenceNumber($captureInfo->getReferenceNumber());
			$reverseTransaction->setCaptureInfo($reverseCaptureInfo);
		}
		return $reverseTransaction;
	}
	/*
	 *$method.comment
	 */
	public function reverseClaim($transaction){
		if (!(AccountActivityType::Claim == $transaction->getAccountActivityType())){
			throw new Exception("Unable to reverse selected transaction; only claims can be reversed.");
		}
		if (!is_null($transaction->getAdjustmentTransaction())){
			throw new Exception("Unable to reverse selected transaction; selected transaction was reversed.");
		}
		if (!($transaction->getCustomerAccount()->getId() == $this->getId())){
			throw new Exception("Unable to create cancel transaction, original transaction not found for the selected customer account.");
		}
		$reverseTransaction = new AssetTransaction();
		$reverseTransaction->setCustomerAccount($this);
		$reverseTransaction->setMerchantAccountCode($transaction->getMerchantAccountCode());
		$reverseTransaction->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$reverseTransaction->setAccountActivityType(AccountActivityType::Refund);
		$reverseTransaction->setAmount(-($transaction->getAmount()));
		if (!is_null($transaction->getTaxAmount())){
			$reverseTransaction->setTaxAmount($transaction->getTaxAmount() > 0 ? $transaction->getTaxAmount() : -($transaction->getTaxAmount()));
		}
		$reverseTransaction->setAccessory($transaction->getAccessory());
		$reverseTransaction->setAccountNumber($transaction->getAccountNumber());
		$reverseTransaction->setSellerCode($transaction->getSellerCode());
		$reverseTransaction->setShiftCode($transaction->getShiftCode());
		$reverseTransaction->setTerminalCode($transaction->getTerminalCode());
		$reverseTransaction->setTransactionType($transaction->getTransactionType());
		$reverseTransaction->setIsVisibleExternally(true);
		$transaction->setHistoricalBalance(-($transaction->getAmount()));
		$transaction->setAdjustmentTransaction($reverseTransaction);
		$this->addAssetTransaction($transaction);
		$this->addAssetTransaction($reverseTransaction);
		$captureInfo = $transaction->getCaptureInfo();
		if (!is_null($captureInfo)){
			$reverseCaptureInfo = $reverseTransaction->createCaptureInfoExtended($captureInfo->getHolderName(), $captureInfo->getStreet(), $captureInfo->getCity(), $captureInfo->getState(), $captureInfo->getPhone(), $captureInfo->getZipCode(), $captureInfo->getEmail(), null, null);
			$reverseCaptureInfo->setTransactionType($transaction->getTransactionType());
			$reverseCaptureInfo->setAccountNumber($captureInfo->getAccountNumber());
			$reverseCaptureInfo->setAccessory($captureInfo->getAccessory());
			$reverseCaptureInfo->setTaxAmount($captureInfo->getTaxAmount());
			$reverseCaptureInfo->setRequestDate(new DateTime(null, new DateTimeZone('EST')));
			$reverseTransaction->setCaptureInfo($reverseCaptureInfo);
		}
		return $reverseTransaction;
	}
	/*
	 *$method.comment
	 */
	public function reversePaymentExtended($assetTransaction,$code,$note){
		$voidAssetTransaction = $this->reversePayment($assetTransaction);
		$voidAssetTransaction->setCode($code);
		$voidAssetTransaction->setNote($note);
		return $voidAssetTransaction;
	}
	/*
	 *$method.comment
	 */
	public function reverseRefund($transaction){
		if (!(AccountActivityType::Refund == $transaction->getAccountActivityType())){
			throw new Exception("Unable to void selected transaction; only refunds can be voided.");
		}
		if (!is_null($transaction->getAdjustmentTransaction())){
			throw new Exception("Unable to void selected transaction; selected transaction was voided.");
		}
		if (!($transaction->getCustomerAccount()->getId() == $this->getId())){
			throw new Exception("Unable to create cancel transaction, original transaction not found for the selected customer account.");
		}
		$reverseTransaction = new AssetTransaction();
		$reverseTransaction->setCustomerAccount($this);
		$reverseTransaction->setMerchantAccountCode($transaction->getMerchantAccountCode());
		$reverseTransaction->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$reverseTransaction->setAccountActivityType(AccountActivityType::CancelRefund);
		$reverseTransaction->setAmount($transaction->getAmount());
		if (!is_null($transaction->getTaxAmount())){
			$reverseTransaction->setTaxAmount($transaction->getTaxAmount() > 0 ? $transaction->getTaxAmount() : -($transaction->getTaxAmount()));
		}
		$reverseTransaction->setAccessory($transaction->getAccessory());
		$reverseTransaction->setAccountNumber($transaction->getAccountNumber());
		$reverseTransaction->setSellerCode($transaction->getSellerCode());
		$reverseTransaction->setShiftCode($transaction->getShiftCode());
		$reverseTransaction->setTerminalCode($transaction->getTerminalCode());
		$reverseTransaction->setTransactionType($transaction->getTransactionType());
		$reverseTransaction->setIsVisibleExternally(true);
		$transaction->setHistoricalBalance(-($transaction->getAmount()));
		$transaction->setAdjustmentTransaction($reverseTransaction);
		$this->addAssetTransaction($transaction);
		$this->addAssetTransaction($reverseTransaction);
		$captureInfo = $transaction->getCaptureInfo();
		if (!is_null($captureInfo)){
			$reverseCaptureInfo = $reverseTransaction->createCaptureInfoExtended($captureInfo->getHolderName(), $captureInfo->getStreet(), $captureInfo->getCity(), $captureInfo->getState(), $captureInfo->getPhone(), $captureInfo->getZipCode(), $captureInfo->getEmail(), null, null);
			$reverseCaptureInfo->setTransactionType($transaction->getTransactionType());
			$reverseCaptureInfo->setAccountNumber($captureInfo->getAccountNumber());
			$reverseCaptureInfo->setAccessory($captureInfo->getAccessory());
			$reverseCaptureInfo->setTaxAmount($captureInfo->getTaxAmount());
			$reverseCaptureInfo->setRequestDate(new DateTime(null, new DateTimeZone('EST')));
			$reverseCaptureInfo->setReferenceNumber($captureInfo->getReferenceNumber());
			$reverseTransaction->setCaptureInfo($reverseCaptureInfo);
		}
		return $reverseTransaction;
	}
	/*
	 *$method.comment
	 */
	public function reverseRefundExtended($assetTransaction,$code,$note){
		$voidAssetTransaction = $this->reverseRefund($assetTransaction);
		$voidAssetTransaction->setCode($code);
		$voidAssetTransaction->setNote($note);
		return $voidAssetTransaction;
	}
	/*
	 *$method.comment
	 */
	public function createCashPayment($amount,$note,$creatorCode,$sellerCode,$shiftCode,$terminalCode,$code){
		$assetTransaction = $this->createAssetTransaction(true);
		$assetTransaction->setCaptureInfo(null);
		$assetTransaction->setAccountActivityType(AccountActivityType::Payment);
		$assetTransaction->setAmount($amount);
		$assetTransaction->setNote($note);
		$assetTransaction->setCreatorCode($creatorCode);
		$assetTransaction->setSellerCode($sellerCode);
		$assetTransaction->setShiftCode($shiftCode);
		$assetTransaction->setTerminalCode($terminalCode);
		$assetTransaction->setTransactionType(AssetTransactionType::Cash);
		$assetTransaction->setCode($code);
		return $assetTransaction;
	}
	/*
	 *$method.comment
	 */
	public function createCheckPayment($amount,$note,$creatorCode,$sellerCode,$shiftCode,$terminalCode,$checkNumber,$code){
		$assetTransaction = $this->createAssetTransaction(true);
		$assetTransaction->setCaptureInfo(null);
		$assetTransaction->setAccountActivityType(AccountActivityType::Payment);
		$assetTransaction->setAmount($amount);
		$assetTransaction->setNote($note);
		$assetTransaction->setCreatorCode($creatorCode);
		$assetTransaction->setSellerCode($sellerCode);
		$assetTransaction->setShiftCode($shiftCode);
		$assetTransaction->setTerminalCode($terminalCode);
		$assetTransaction->setTransactionType(AssetTransactionType::Check);
		$assetTransaction->setAccountNumber($checkNumber);
		$assetTransaction->setCode($code);
		return $assetTransaction;
	}
	/*
	 *$method.comment
	 */
	public function createCreditCardPayment($amount,$note,$creatorCode,$sellerCode,$shiftCode,$terminalCode,$creditCardNumber,$expirationDate,$transactionType,$holderName,$city,$state,$street1,$street2,$zipCode,$phone,$email,$cvv2,$code){
		$assetTransaction = $this->createAssetTransaction(true);
		$assetTransaction->setAccountActivityType(AccountActivityType::Payment);
		$assetTransaction->setAmount($amount);
		$assetTransaction->setNote($note);
		$assetTransaction->setCreatorCode($creatorCode);
		$assetTransaction->setSellerCode($sellerCode);
		$assetTransaction->setShiftCode($shiftCode);
		$assetTransaction->setTerminalCode($terminalCode);
		$assetTransaction->setAccountNumber($creditCardNumber);
		$assetTransaction->setAccessory($expirationDate);
		$assetTransaction->setTransactionType($transactionType);
		$assetTransaction->setCode($code);
		$captureInfo = $assetTransaction->getCaptureInfo();
		$captureInfo->setHolderName($holderName);
		$captureInfo->setCity($city);
		$captureInfo->setState($state);
		$captureInfo->setStreet(is_null($street2) ? $street1 : $street1 . " " . $street2);
		$captureInfo->setZipCode($zipCode);
		$captureInfo->setPhone($phone);
		$captureInfo->setEmail($email);
		$captureInfo->setCvv2($cvv2);
		return $assetTransaction;
	}
	/*
	 *$method.comment
	 */
	public function createCashRefund($amount,$note,$creatorCode,$sellerCode,$shiftCode,$terminalCode,$accountNumber,$accessory,$code){
		$assetTransaction = $this->createRefund();
		$assetTransaction->setCaptureInfo(null);
		$assetTransaction->setCode($code);
		$assetTransaction->setAmount($amount);
		$assetTransaction->setNote($note);
		$assetTransaction->setCreatorCode($creatorCode);
		$assetTransaction->setSellerCode($sellerCode);
		$assetTransaction->setShiftCode($shiftCode);
		$assetTransaction->setTerminalCode($terminalCode);
		$assetTransaction->setTransactionType(AssetTransactionType::Cash);
		return $assetTransaction;
	}
	/*
	 *$method.comment
	 */
	public function createCreditCardRefund($amount,$note,$creatorCode,$sellerCode,$shiftCode,$terminalCode,$creditCardNumber,$expirationDate,$transactionType,$holderName,$city,$state,$street1,$street2,$zipCode,$phone,$email,$cvv2,$code){
		$assetTransaction = $this->createRefund();
		$assetTransaction->setCode($code);
		$assetTransaction->setAmount($amount);
		$assetTransaction->setNote($note);
		$assetTransaction->setCreatorCode($creatorCode);
		$assetTransaction->setSellerCode($sellerCode);
		$assetTransaction->setShiftCode($shiftCode);
		$assetTransaction->setTerminalCode($terminalCode);
		$assetTransaction->setTransactionType($transactionType);
		$assetTransaction->setAccountNumber($creditCardNumber);
		$assetTransaction->setAccessory($expirationDate);
		$captureInfo = $assetTransaction->getCaptureInfo();
		$captureInfo->setHolderName($holderName);
		$captureInfo->setCity($city);
		$captureInfo->setState($state);
		$captureInfo->setStreet(is_null($street2) ? $street1 : $street1 . " " . $street2);
		$captureInfo->setZipCode($zipCode);
		$captureInfo->setPhone($phone);
		$captureInfo->setEmail($email);
		$captureInfo->setCvv2($cvv2);
		return $assetTransaction;
	}
	/*
	 *$method.comment
	 */
	public function createRevenueTransaction($addToCustomerAccount=true){
		$revenueTransaction = new RevenueTransaction();
		$revenueTransaction->setCustomerAccount($this);
		$revenueTransaction->setMerchantAccountCode($this->getMerchantAccountCode());
		$revenueTransaction->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$revenueTransaction->setIsVisibleExternally(true);
		if ($addToCustomerAccount){
			$this->addRevenueTransaction($revenueTransaction);
		}
		return $revenueTransaction;
	}
	/*
	 *$method.comment
	 */
	public function createRevenueTransactionExtended($accountActivityType,$code,$amount,$taxAmount,$isComplimantary){
		$revenueTransaction = $this->createRevenueTransaction(true);
		$revenueTransaction->setAccountActivityType($accountActivityType);
		$revenueTransaction->setCode($code);
		$revenueTransaction->setAmount($amount);
		$revenueTransaction->setTaxAmount($taxAmount);
		$revenueTransaction->setIsComplimentary($isComplimantary);
		return $revenueTransaction;
	}
	/*
	 *$method.comment
	 */
	public function createAssetTransaction($addToCustomerAccount=true){
		$assetTransaction = new AssetTransaction();
		$assetTransaction->setCustomerAccount($this);
		$assetTransaction->setMerchantAccountCode($this->getMerchantAccountCode());
		$assetTransaction->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$assetTransaction->setIsVisibleExternally(true);
		$captureInfo = new CaptureInfo();
		$captureInfo->setMerchantAccountCode($this->getMerchantAccountCode());
		$captureInfo->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$captureInfo->setRequestDate(new DateTime(null, new DateTimeZone('EST')));
		$captureInfo->setTransactionType($assetTransaction->getTransactionType());
		$assetTransaction->setCaptureInfo($captureInfo);
		if ($addToCustomerAccount){
			$this->addAssetTransaction($assetTransaction);
		}
		return $assetTransaction;
	}
	/*
	 *$method.comment
	 */
	public function createAssetTransactionExtended($accountActivityType,$code,$amount,$accountNumber,$accessory){
		$assetTransaction = $this->createAssetTransaction(true);
		$assetTransaction->setAccountActivityType($accountActivityType);
		$assetTransaction->setCode($code);
		$assetTransaction->setAmount($amount);
		$assetTransaction->setAccountNumber($accountNumber);
		$assetTransaction->setAccessory($accessory);
		return $assetTransaction;
	}
	/*
	 *$method.comment
	 */
	public function createAllowance($code,$amount,$note,$creatorCode,$sellerCode,$shiftCode,$terminalCode){
		$assetTransaction = $this->createAssetTransaction(true);
		$assetTransaction->setCode($code);
		$assetTransaction->setCaptureInfo(null);
		$assetTransaction->setAccountActivityType(AccountActivityType::Payment);
		$assetTransaction->setAmount($amount);
		$assetTransaction->setNote($note);
		$assetTransaction->setCreatorCode($creatorCode);
		$assetTransaction->setSellerCode($sellerCode);
		$assetTransaction->setShiftCode($shiftCode);
		$assetTransaction->setTerminalCode($terminalCode);
		$assetTransaction->setTransactionType(AssetTransactionType::Allowance);
		return $assetTransaction;
	}
	/*
	 *$method.comment
	 */
	public function createAllowanceRefund($code,$amount,$note,$creatorCode,$sellerCode,$shiftCode,$terminalCode,$accountNumber,$accessory){
		$assetTransaction = $this->createAssetTransaction(true);
		$assetTransaction->setCode($code);
		$assetTransaction->setCaptureInfo(null);
		$assetTransaction->setAccountActivityType(AccountActivityType::Refund);
		$assetTransaction->setAmount($amount);
		$assetTransaction->setNote($note);
		$assetTransaction->setCreatorCode($creatorCode);
		$assetTransaction->setSellerCode($sellerCode);
		$assetTransaction->setShiftCode($shiftCode);
		$assetTransaction->setTerminalCode($terminalCode);
		$assetTransaction->setTransactionType(AssetTransactionType::Allowance);
		return $assetTransaction;
	}
	/*
	 *$method.comment
	 */
	public function createNote($addToCustomerAccount=true){
		$note = new Note();
		$note->setCustomerAccount($this);
		$note->setMerchantAccountCode($this->getMerchantAccountCode());
		$note->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$note->setSession($this->session);
		if ($addToCustomerAccount){
			$this->addNote($note);
		}
		return $note;
	}
	/*
	 *$method.comment
	 */
	public function createNoteExtended($noteCode,$creatorCode,$description){
		$note = $this->createNote(true);
		$note->setNoteCode($noteCode);
		$note->setCreatorCode($creatorCode);
		$note->setDescription($description);
		return $note;
	}
	/*
	 *$method.comment
	 */
	public function createAddress($addToCustomerAccount=true){
		$address = new Address();
		$address->setCustomerAccount($this);
		$address->setMerchantAccountCode($this->getMerchantAccountCode());
		$address->setAddressType(AddressType::Unknown);
		$address->setSession($this->session);
		if ($addToCustomerAccount){
			$this->addAddress($address);
		}
		return $address;
	}
	/*
	 *$method.comment
	 */
	public function createPhone($addToCustomerAccount=true){
		$phone = new Phone();
		$phone->setCustomerAccount($this);
		$phone->setMerchantAccountCode($this->getMerchantAccountCode());
		$phone->setSession($this->session);
		if ($addToCustomerAccount){
			$this->addPhone($phone);
		}
		return $phone;
	}
	/*
	 *$method.comment
	 */
	public function createEmailAddress($addToCustomerAccount=true){
		$emailAddress = new EmailAddress();
		$emailAddress->setCustomerAccount($this);
		$emailAddress->setMerchantAccountCode($this->getMerchantAccountCode());
		$emailAddress->setSession($this->session);
		if ($addToCustomerAccount){
			$this->addEmailAddress($emailAddress);
		}
		return $emailAddress;
	}
	/*
	 *$method.comment
	 */
	public function createAction($addToCustomerAccount=true){
		$action = new Action();
		$action->setCustomerAccount($this);
		$action->setMerchantAccountCode($this->getMerchantAccountCode());
		$action->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$action->setSession($this->session);
		$this->lastActionDate = new DateTime(null, new DateTimeZone('EST'));
		$this->lastAction = $action;
		if ($addToCustomerAccount){
			$this->addAction($action);
		}
		return $action;
	}
	/*
	 *$method.comment
	 */
	public function createActionExtended($actionCode,$creatorCode){
		$action = $this->createAction(true);
		$action->setActionCode($actionCode);
		$action->setCreatorCode($creatorCode);
		return $action;
	}
	/*
	 *$method.comment
	 */
	public function createLetter($addToCustomerAccount=true){
		$letter = new Letter();
		$letter->setCustomerAccount($this);
		$letter->setMerchantAccountCode($this->getMerchantAccountCode());
		$letter->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$letter->setName(is_null($this->firstName) ? $this->lastName : $this->firstName . " " . $this->lastName);
		$letter->setStreet1($this->getStreet1());
		$letter->setStreet2($this->getStreet2());
		$letter->setCity($this->getCity());
		$letter->setState($this->getState());
		$letter->setZipCode($this->getZipCode());
		$letter->setEmail($this->getEmail());
		$letter->setStatus(LetterStatusType::Pending);
		$letter->setIsVisibleExternally(true);
		if ($addToCustomerAccount){
			$this->addLetter($letter);
		}
		return $letter;
	}
	/*
	 *$method.comment
	 */
	public function createLetterExtended($letterCode,$creatorCode){
		$letter = $this->createLetter(true);
		$letter->setLetterCode($letterCode);
		$letter->setCreatorCode($creatorCode);
		return $letter;
	}
	/*
	 *$method.comment
	 */
	public function createEmail($addToCustomerAccount=true){
		if ($this->getIsEmailDisallowed()){
			throw new Exception("Sending emails to this Customer Account is not allowed");
		}
		$email = new Email();
		$email->setCustomerAccount($this);
		$email->setMerchantAccountCode($this->getMerchantAccountCode());
		$email->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$email->setFirstName($this->getFirstName());
		$email->setLastName($this->getLastName());
		$email->setIsVisibleExternally(true);
		if (!is_null($this->getEmail()) && !($this->getIsBadEmail())){
			$email->setToEmail($this->getEmail());
		}
		if ($addToCustomerAccount){
			$this->addEmail($email);
		}
		return $email;
	}
	/*
	 *$method.comment
	 */
	public function createEmailExtended($emailCode,$creatorCode){
		$email = $this->createEmail(true);
		$email->setEmailCode($emailCode);
		$email->setCreatorCode($creatorCode);
		return $email;
	}
	/*
	 *$method.comment
	 */
	public function createScheduledPayment($addToCustomerAccount=true){
		$scheduledPayment = new ScheduledPayment();
		$scheduledPayment->setCustomerAccount($this);
		$scheduledPayment->setMerchantAccountCode($this->getMerchantAccountCode());
		$scheduledPayment->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$scheduledPayment->setStatus(ScheduledPaymentStatusType::Pending);
		$scheduledPayment->setIsVisibleExternally(true);
		if ($addToCustomerAccount){
			$this->addScheduledPayment($scheduledPayment);
		}
		return $scheduledPayment;
	}
	/*
	 *$method.comment
	 */
	public function createScheduledPaymentExtended($amount,$dueDate,$creatorCode,$posterCode,$paymentOption){
		$scheduledPayment = $this->createScheduledPayment(true);
		$scheduledPayment->setAmount($amount);
		$scheduledPayment->setDueDate($dueDate);
		$scheduledPayment->setCreatorCode($creatorCode);
		$scheduledPayment->setPosterCode($posterCode);
		$scheduledPayment->setPaymentOption($paymentOption);
		return $scheduledPayment;
	}
	/*
	 *$method.comment
	 */
	public function createContract($addToCustomerAccount=true){
		$contract = new Contract();
		$contract->setCustomerAccount($this);
		$contract->setMerchantAccountCode($this->getMerchantAccountCode());
		$contract->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$contract->setStatus(ContractStatusType::Current);
		$contract->setIsVisibleExternally(true);
		if ($addToCustomerAccount){
			$this->addContract($contract);
		}
		return $contract;
	}
	/*
	 *$method.comment
	 */
	public function createContractExtended($name,$type){
		$contract = $this->createContract();
		$contract->setName($name);
		$contract->setType($type);
		return $contract;
	}
	/*
	 *$method.comment
	 */
	public function createContractBasic($paymentPlan=true){
		$contract = $this->createContract();
		$contract->setType($paymentPlan->getType());
		$contract->setLength($paymentPlan->getLength());
		$contract->setValue($paymentPlan->getValue());
		$contract->setStatus($paymentPlan->getStatus());
		$contract->setDefaultPaymentPlan($paymentPlan);
		$paymentPlan->setContract($contract);
		return $contract;
	}
	/*
	 *$method.comment
	 */
	public function getScheduledPayments(){
		return new ArrayList($this->scheduledPayments);
	}
	/*
	 *$method.comment
	 */
	public function addScheduledPayment($scheduledPayment){
		$hasObject = false;
		foreach ($this->scheduledPayments as $s){
			if ($s->getRefId() == $scheduledPayment->getRefId()){
				$hasObject = true;
			}
		}
		if (!($this->scheduledPayments->contains($scheduledPayment)) && !($hasObject)){
			$this->scheduledPayments->add($scheduledPayment);
		}
	}
	/*
	 *$method.comment
	 */
	public function removeScheduledPayment($scheduledPayment){
		$this->scheduledPayments->remove($scheduledPayment);
	}
	/*
	 *$method.comment
	 */
	public function createDocument($addToCustomerAccount=true){
		$document = new Document();
		$document->setCustomerAccount($this);
		$document->setMerchantAccountCode($this->getMerchantAccountCode());
		$document->setCollectorAccountCode(is_null($this->sentCollectionsDate) ? null : $this->getCollectorAccountCode());
		$document->setIsVisibleExternally(true);
		if ($addToCustomerAccount){
			$this->addDocument($document);
		}
		return $document;
	}
	/*
	 *$method.comment
	 */
	public function createDocumentExtended($code,$documentCode,$creatorCode,$documentContentCode,$description){
		$document = $this->createDocument();
		$document->setCode($code);
		$document->setDocumentCode($documentCode);
		$document->setCreatorCode($creatorCode);
		$document->setDocumentContentCode($documentContentCode);
		$document->setDescription($description);
		return $document;
	}
	/*
	 *$method.comment
	 */
	public function createSignature($addToCustomerAccount=true){
		$signature = new Signature();
		$signature->setCustomerAccount($this);
		$signature->setMerchantAccountCode($this->getMerchantAccountCode());
		if ($addToCustomerAccount){
			$this->addSignature($signature);
		}
		return $signature;
	}
	/*
	 *$method.comment
	 */
	public function createSignatureExtended($content,$image,$document){
		$signature = $this->createSignature(true);
		$signature->setContent($content);
		$signature->setImage($image);
		$signature->setDocument($document);
		return $signature;
	}
	/*
	 *$method.comment
	 */
	public function getCountryType(){
		return is_null($this->defaultAddress) ? null : $this->defaultAddress->getCountryType();
	}
	/*
	 *$method.comment
	 */
	public function setCountryType($countryType){
		if (is_null($this->defaultAddress)){
			$this->createDefaultAddress();
		}
		$this->defaultAddress->setCountryType($countryType);
	}
	/*
	 *$method.comment
	 */
	public function type(){
		if (is_null($this->type)){
			throw new Exception("Cannot resolve type for CustomerAccount.");
		}
		else {
			if (CustomerAccountType::Organization == $this->type){
				return "Organization";
			}
			else {
				return "Person";
			}
		}
	}
	/*
	 *$method.comment
	 */
	private function validateFirstBillingDate($firstBillingDate,$billingCycleCode,$code){
		if (is_null($firstBillingDate)){
			return ;
		}
		if (!(Helper::validateFirstBillingDate($firstBillingDate, $billingCycleCode))){
			throw new Exception("Specified value does not fall within the billing cycle. Object: %1$s; Code: %2$s; Field: %3$s.", "PaymentPlan", is_null($code) ? "unspecified" : $code, "firstBillingDate");
		}
	}
	/*
	 *$method.comment
	 */
	public function getFirstName(){
		return $this->firstName;
	}
	/*
	 *$method.comment
	 */
	public function setFirstName($firstName){
		$this->firstName = $firstName;
	}
	/*
	 *$method.comment
	 */
	public function getLastName(){
		return $this->lastName;
	}
	/*
	 *$method.comment
	 */
	public function setLastName($lastName){
		$this->lastName = $lastName;
	}
	/*
	 *$method.comment
	 */
	public function getMiddleName(){
		return $this->middleName;
	}
	/*
	 *$method.comment
	 */
	public function setMiddleName($middleName){
		$this->middleName = $middleName;
	}
	/*
	 *$method.comment
	 */
	public function getTitle(){
		return $this->title;
	}
	/*
	 *$method.comment
	 */
	public function setTitle($title){
		$this->title = $title;
	}
	/*
	 *$method.comment
	 */
	public function getSuffix(){
		return $this->suffix;
	}
	/*
	 *$method.comment
	 */
	public function setSuffix($suffix){
		$this->suffix = $suffix;
	}
	/*
	 *$method.comment
	 */
	public function getName(){
		if (!is_null($this->firstName) && !is_null($this->middleName)){
			return $this->firstName . " " . $this->middleName . " " . $this->lastName;
		}
		else {
			if (!is_null($this->firstName)){
				return $this->firstName . " " . $this->lastName;
			}
			else {
				return $this->lastName;
			}
		}
	}
	/*
	 *$method.comment
	 */
	public function getStreet2(){
		return is_null($this->defaultAddress) ? null : $this->getDefaultAddress()->getStreet2();
	}
	/*
	 *$method.comment
	 */
	public function setStreet2($street2){
		if (is_null($this->defaultAddress)){
			$this->createDefaultAddress();
		}
		$this->defaultAddress->setStreet2($street2);
	}
	/*
	 *$method.comment
	 */
	public function getLastPaymentDate(){
		return $this->lastPaymentDate;
	}
	/*
	 *$method.comment
	 */
	 function setLastPaymentDate($lastPaymentDate){
		$this->lastPaymentDate = $lastPaymentDate;
	}
	/*
	 *$method.comment
	 */
	public function getLastReturnReason(){
		return $this->lastReturnReason;
	}
	/*
	 *$method.comment
	 */
	 function setLastReturnReason($lastReturnReason){
		$this->lastReturnReason = $lastReturnReason;
	}
	/*
	 *$method.comment
	 */
	 function setIsVerified($isVerified){
		$this->isVerified = $isVerified;
	}
	/*
	 *$method.comment
	 */
	 function setLastUpdateDate($lastUpdateDate){
		$this->lastUpdateDate = $lastUpdateDate;
	}
	/*
	 *$method.comment
	 */
	public function getStatusChangeDate(){
		return $this->statusChangeDate;
	}
	/*
	 *$method.comment
	 */
	 function setStatusChangeDate($statusChangeDate){
		$this->statusChangeDate = $statusChangeDate;
	}
	/*
	 *$method.comment
	 */
	public function getFirstBillingDate(){
		return $this->firstBillingDate;
	}
	/*
	 *$method.comment
	 */
	 function setFirstBillingDate($firstBillingDate){
		$this->firstBillingDate = $firstBillingDate;
	}
	/*
	 *$method.comment
	 */
	public function getSocialSecurity(){
		return $this->socialSecurity;
	}
	/*
	 *$method.comment
	 */
	public function setSocialSecurity($socialSecurity){
		$this->socialSecurity = $socialSecurity;
	}
	/*
	 *$method.comment
	 */
	public function getCollectorCode(){
		return $this->collectorCode;
	}
	/*
	 *$method.comment
	 */
	public function setCollectorCode($collectorCode){
		$this->collectorCode = $collectorCode;
	}
	/*
	 *$method.comment
	 */
	public function getSentCollectionsBalance(){
		return $this->sentCollectionsBalance;
	}
	/*
	 *$method.comment
	 */
	public function setSentCollectionsBalance($sentCollectionsBalance){
		$this->sentCollectionsBalance = $sentCollectionsBalance;
	}
	/*
	 *$method.comment
	 */
	public function getSentCollectionsDate(){
		return $this->sentCollectionsDate;
	}
	/*
	 *$method.comment
	 */
	public function setSentCollectionsDate($sentCollectionsDate){
		$this->sentCollectionsDate = $sentCollectionsDate;
	}
	/*
	 *$method.comment
	 */
	public function getIsBadPrimaryAddress(){
		return is_null($this->defaultAddress) ? null : $this->getDefaultAddress()->getIsBad();
	}
	/*
	 *$method.comment
	 */
	public function setIsBadPrimaryAddress($isBadPrimaryAddress){
		if (is_null($this->defaultAddress)){
			$this->createDefaultAddress();
		}
		$this->defaultAddress->setIsBad($isBadPrimaryAddress);
	}
	/*
	 *$method.comment
	 */
	public function getIsBadHomePhone(){
		return is_null($this->defaultHomePhone) ? null : $this->defaultHomePhone->getIsBad();
	}
	/*
	 *$method.comment
	 */
	public function setIsBadHomePhone($isBadHomePhone){
		if (is_null($this->defaultHomePhone) && (is_null($isBadHomePhone) || !($isBadHomePhone))){
			return ;
		}
		if (is_null($this->defaultHomePhone)){
			$this->createDefaultHomePhone();
		}
		$this->defaultHomePhone->setIsBad($isBadHomePhone);
	}
	/*
	 *$method.comment
	 */
	public function getIsBadWorkPhone(){
		return is_null($this->defaultWorkPhone) ? null : $this->defaultWorkPhone->getIsBad();
	}
	/*
	 *$method.comment
	 */
	public function setIsBadWorkPhone($isBadWorkPhone){
		if (is_null($this->defaultWorkPhone) && (is_null($isBadWorkPhone) || !($isBadWorkPhone))){
			return ;
		}
		if (is_null($this->defaultWorkPhone)){
			$this->createDefaultWorkPhone();
		}
		$this->defaultWorkPhone->setIsBad($isBadWorkPhone);
	}
	/*
	 *$method.comment
	 */
	public function getIsBadCellPhone(){
		return is_null($this->defaultCellPhone) ? null : $this->defaultCellPhone->getIsBad();
	}
	/*
	 *$method.comment
	 */
	public function setIsBadCellPhone($isBadCellPhone){
		if (is_null($this->defaultCellPhone) && (is_null($isBadCellPhone) || !($isBadCellPhone))){
			return ;
		}
		if (is_null($this->defaultCellPhone)){
			$this->createDefaultCellPhone();
		}
		$this->defaultCellPhone->setIsBad($isBadCellPhone);
	}
	/*
	 *$method.comment
	 */
	public function getIsMailDisallowed(){
		return $this->isMailDisallowed;
	}
	/*
	 *$method.comment
	 */
	public function setIsMailDisallowed($isMailDisallowed){
		$this->isMailDisallowed = $isMailDisallowed;
	}
	/*
	 *$method.comment
	 */
	public function getIsMarketingEmailAllowed(){
		return $this->isMarketingEmailAllowed;
	}
	/*
	 *$method.comment
	 */
	public function setIsMarketingEmailAllowed($isMarketingEmailAllowed){
		$this->isMarketingEmailAllowed = $isMarketingEmailAllowed;
	}
	/*
	 *$method.comment
	 */
	public function getIsBillingEmailAllowed(){
		return $this->isBillingEmailAllowed;
	}
	/*
	 *$method.comment
	 */
	public function setIsBillingEmailAllowed($isBillingEmailAllowed){
		$this->isBillingEmailAllowed = $isBillingEmailAllowed;
	}
	/*
	 *$method.comment
	 */
	public function getIsEmailDisallowed(){
		return $this->isEmailDisallowed;
	}
	/*
	 *$method.comment
	 */
	public function setIsEmailDisallowed($isEmailDisallowed){
		$this->isEmailDisallowed = $isEmailDisallowed;
	}
	/*
	 *$method.comment
	 */
	public function getIsCallingDisallowed(){
		return $this->isCallingDisallowed;
	}
	/*
	 *$method.comment
	 */
	public function setIsCallingDisallowed($isCallingDisallowed){
		$this->isCallingDisallowed = $isCallingDisallowed;
	}
	/*
	 *$method.comment
	 */
	public function getLastActionDate(){
		return $this->lastActionDate;
	}
	/*
	 *$method.comment
	 */
	public function setLastActionDate($lastActionDate){
		$this->lastActionDate = $lastActionDate;
	}
	/*
	 *$method.comment
	 */
	public function getLastAction(){
		return $this->lastAction;
	}
	/*
	 *$method.comment
	 */
	public function setLastAction($lastAction){
		$this->lastAction = $lastAction;
	}
	/*
	 *$method.comment
	 */
	public function getIsBadEmail(){
		return is_null($this->defaultEmailAddress) ? null : $this->getDefaultEmailAddress()->getIsBad();
	}
	/*
	 *$method.comment
	 */
	public function setIsBadEmail($isBadEmail){
		if (is_null($this->defaultEmailAddress) && (is_null($isBadEmail) || !($isBadEmail))){
			return ;
		}
		if (is_null($this->defaultEmailAddress)){
			$this->createDefaultEmailAddress();
		}
		$this->defaultEmailAddress->setIsBad($isBadEmail);
	}
	/*
	 *$method.comment
	 */
	public function getCollectionsPriorityType(){
		return $this->collectionsPriorityType;
	}
	/*
	 *$method.comment
	 */
	public function setCollectionsPriorityType($collectionsPriorityType){
		$this->collectionsPriorityType = $collectionsPriorityType;
	}
	/*
	 *$method.comment
	 */
	public function getExternalIdentifier(){
		return $this->externalIdentifier;
	}
	/*
	 *$method.comment
	 */
	public function setExternalIdentifier($externalIdentifier){
		$this->externalIdentifier = $externalIdentifier;
	}
	/*
	 *$method.comment
	 */
	public function getWriteOffBalance(){
		return $this->writeOffBalance;
	}
	/*
	 *$method.comment
	 */
	 function setWriteOffBalance($writeOffBalance){
		$this->writeOffBalance = $writeOffBalance;
	}
	/*
	 *$method.comment
	 */
	public function getCustomFields(){
		return $this->customFields;
	}
	/*
	 *$method.comment
	 */
	public function setCustomFields($customFields){
		$this->customFields = $customFields;
	}
	/*
	 *$method.comment
	 */
	public function getIsChargebackRequested(){
		return $this->isChargebackRequested;
	}
	/*
	 *$method.comment
	 */
	public function setIsChargebackRequested($isChargebackRequested){
		$this->isChargebackRequested = $isChargebackRequested;
	}
	/*
	 *$method.comment
	 */
	public function getIsBankruptcyFiled(){
		return $this->isBankruptcyFiled;
	}
	/*
	 *$method.comment
	 */
	public function setIsBankruptcyFiled($isBankruptcyFiled){
		$this->isBankruptcyFiled = $isBankruptcyFiled;
	}
	/*
	 *$method.comment
	 */
	public function getIsLegalDispute(){
		return $this->isLegalDispute;
	}
	/*
	 *$method.comment
	 */
	public function setIsLegalDispute($isLegalDispute){
		$this->isLegalDispute = $isLegalDispute;
	}
	/*
	 *$method.comment
	 */
	public function getIsCancellationDispute(){
		return $this->isCancellationDispute;
	}
	/*
	 *$method.comment
	 */
	public function setIsCancellationDispute($isCancellationDispute){
		$this->isCancellationDispute = $isCancellationDispute;
	}
	/*
	 *$method.comment
	 */
	public function getContractDocumentCode(){
		return $this->contractDocumentCode;
	}
	/*
	 *$method.comment
	 */
	public function setContractDocumentCode($contractDocumentCode){
		if (!is_null($contractDocumentCode) && is_null($this->getId())){
			throw new Exception("Unable to set Contract Document Code: Customer Account is unpersisted.");
		}
		$this->contractDocumentCode = $contractDocumentCode;
	}
	/*
	 *$method.comment
	 */
	public function getIsDeceased(){
		return $this->isDeceased;
	}
	/*
	 *$method.comment
	 */
	public function setIsDeceased($isDeceased){
		$this->isDeceased = $isDeceased;
	}
	/*
	 *$method.comment
	 */
	public function getIsSkipTraceRequired(){
		return $this->isSkipTraceRequired;
	}
	/*
	 *$method.comment
	 */
	public function setIsSkipTraceRequired($isSkipTraceRequired){
		$this->isSkipTraceRequired = $isSkipTraceRequired;
	}
	/*
	 *$method.comment
	 */
	public function getSkipTraceModeType(){
		return $this->skipTraceModeType;
	}
	/*
	 *$method.comment
	 */
	public function setSkipTraceModeType($skipTraceModeType){
		$this->skipTraceModeType = $skipTraceModeType;
	}
	/*
	 *$method.comment
	 */
	public function getLastSkipTraceDate(){
		return is_null($this->defaultAddress) ? null : $this->getDefaultAddress()->getLastSkipTraceDate();
	}
	/*
	 *$method.comment
	 */
	public function setLastSkipTraceDate($lastSkipTraceDate){
		if (is_null($this->defaultAddress)){
			$this->createDefaultAddress();
		}
		$this->defaultAddress->setLastSkipTraceDate($lastSkipTraceDate);
	}
	/*
	 *$method.comment
	 */
	public function getSkipTraceSourceType(){
		return is_null($this->defaultAddress) ? null : $this->getDefaultAddress()->getAddressSourceType();
	}
	/*
	 *$method.comment
	 */
	public function setSkipTraceSourceType($skipTraceSourceType){
		if (is_null($this->defaultAddress)){
			$this->createDefaultAddress();
		}
		$this->defaultAddress->setAddressSourceType($skipTraceSourceType);
	}
	/*
	 *$method.comment
	 */
	public function getReturnFeeBalance(){
		return $this->returnFeeBalance;
	}
	/*
	 *$method.comment
	 */
	 function setReturnFeeBalance($returnFeeBalance){
		$this->returnFeeBalance = $returnFeeBalance;
	}
	/*
	 *$method.comment
	 */
	public function getLateFeeBalance(){
		return $this->lateFeeBalance;
	}
	/*
	 *$method.comment
	 */
	 function setLateFeeBalance($lateFeeBalance){
		$this->lateFeeBalance = $lateFeeBalance;
	}
	/*
	 *$method.comment
	 */
	public function getSynchronizationCode(){
		return $this->synchronizationCode;
	}
	/*
	 *$method.comment
	 */
	 function setSynchronizationCode($synchronizationCode){
		$this->synchronizationCode = $synchronizationCode;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentPlanValue(){
		return $this->paymentPlanValue;
	}
	/*
	 *$method.comment
	 */
	 function setPaymentPlanValue($paymentPlanValue){
		$this->paymentPlanValue = $paymentPlanValue;
	}
	/*
	 *$method.comment
	 */
	public function getDefaultBillingCycleCode(){
		return $this->defaultBillingCycleCode;
	}
	/*
	 *$method.comment
	 */
	public function setDefaultBillingCycleCode($defaultBillingCycleCode){
		$this->defaultBillingCycleCode = $defaultBillingCycleCode;
	}
	/*
	 *$method.comment
	 */
	public function getDefaultBillingDate(){
		return $this->defaultBillingDate;
	}
	/*
	 *$method.comment
	 */
	 function setDefaultBillingDate($defaultBillingDate){
		$this->defaultBillingDate = $defaultBillingDate;
	}
	/*
	 *$method.comment
	 */
	public function getDefaultPaymentOption(){
		return $this->defaultPaymentOption;
	}
	/*
	 *$method.comment
	 */
	public function setDefaultPaymentOption($defaultPaymentOption){
		$this->defaultPaymentOption = $defaultPaymentOption;
	}
	/*
	 *$method.comment
	 */
	public function getDefaultPaymentPlan(){
		return $this->defaultPaymentPlan;
	}
	/*
	 *$method.comment
	 */
	 function setDefaultPaymentPlan($defaultPaymentPlan){
		$this->defaultPaymentPlan = $defaultPaymentPlan;
	}
	/*
	 *$method.comment
	 */
	public function getDefaultContract(){
		return $this->defaultContract;
	}
	/*
	 *$method.comment
	 */
	public function setDefaultContract($defaultContract){
		$this->defaultContract = $defaultContract;
	}
	/*
	 *$method.comment
	 */
	public function getAccountAge(){
		return $this->accountAge;
	}
	/*
	 *$method.comment
	 */
	 function setAccountAge($accountAge){
		$this->accountAge = $accountAge;
	}
	/*
	 *$method.comment
	 */
	public function getAccountPhase(){
		return $this->accountPhase;
	}
	/*
	 *$method.comment
	 */
	 function setAccountPhase($accountPhase){
		$this->accountPhase = $accountPhase;
	}
	/*
	 *$method.comment
	 */
	public function getIsReinstated(){
		return $this->isReinstated;
	}
	/*
	 *$method.comment
	 */
	 function setIsReinstated($isReinstated){
		$this->isReinstated = $isReinstated;
	}
	/*
	 *$method.comment
	 */
	public function getIsReinstateProcess(){
		return $this->isReinstateProcess;
	}
	/*
	 *$method.comment
	 */
	 function setIsReinstateProcess($isReinstateProcess){
		$this->isReinstateProcess = $isReinstateProcess;
	}
	/*
	 *$method.comment
	 */
	public function getStatus(){
		return $this->status;
	}
	/*
	 *$method.comment
	 */
	 function setStatus($status){
		$this->status = $status;
	}
	/*
	 *$method.comment
	 */
	public function getIsCollectionsFeeApplied(){
		return $this->isCollectionsFeeApplied;
	}
	/*
	 *$method.comment
	 */
	 function setIsCollectionsFeeApplied($isCollectionsFeeApplied){
		$this->isCollectionsFeeApplied = $isCollectionsFeeApplied;
	}
	/*
	 *$method.comment
	 */
	public function getDriverLicenseNumber(){
		return $this->driverLicenseNumber;
	}
	/*
	 *$method.comment
	 */
	public function setDriverLicenseNumber($driverLicenseNumber){
		$this->driverLicenseNumber = $driverLicenseNumber;
	}
	/*
	 *$method.comment
	 */
	public function getDriverLicenseState(){
		return $this->driverLicenseState;
	}
	/*
	 *$method.comment
	 */
	public function setDriverLicenseState($driverLicenseState){
		$this->driverLicenseState = $driverLicenseState;
	}
	/*
	 *$method.comment
	 */
	public function getBuyerCode(){
		return $this->buyerCode;
	}
	/*
	 *$method.comment
	 */
	public function setBuyerCode($buyerCode){
		$this->buyerCode = $buyerCode;
	}
	/*
	 *$method.comment
	 */
	public function getBuyerFirstName(){
		return $this->buyerFirstName;
	}
	/*
	 *$method.comment
	 */
	public function setBuyerFirstName($buyerFirstName){
		$this->buyerFirstName = $buyerFirstName;
	}
	/*
	 *$method.comment
	 */
	public function getBuyerLastName(){
		return $this->buyerLastName;
	}
	/*
	 *$method.comment
	 */
	public function setBuyerLastName($buyerLastName){
		$this->buyerLastName = $buyerLastName;
	}
	/*
	 *$method.comment
	 */
	public function getBuyerMiddleName(){
		return $this->buyerMiddleName;
	}
	/*
	 *$method.comment
	 */
	public function setBuyerMiddleName($buyerMiddleName){
		$this->buyerMiddleName = $buyerMiddleName;
	}
	/*
	 *$method.comment
	 */
	public function getBuyerStreet1(){
		return $this->buyerStreet1;
	}
	/*
	 *$method.comment
	 */
	public function setBuyerStreet1($buyerStreet1){
		$this->buyerStreet1 = $buyerStreet1;
	}
	/*
	 *$method.comment
	 */
	public function getBuyerStreet2(){
		return $this->buyerStreet2;
	}
	/*
	 *$method.comment
	 */
	public function setBuyerStreet2($buyerStreet2){
		$this->buyerStreet2 = $buyerStreet2;
	}
	/*
	 *$method.comment
	 */
	public function getBuyerCity(){
		return $this->buyerCity;
	}
	/*
	 *$method.comment
	 */
	public function setBuyerCity($buyerCity){
		$this->buyerCity = $buyerCity;
	}
	/*
	 *$method.comment
	 */
	public function getBuyerState(){
		return $this->buyerState;
	}
	/*
	 *$method.comment
	 */
	public function setBuyerState($buyerState){
		$this->buyerState = $buyerState;
	}
	/*
	 *$method.comment
	 */
	public function getBuyerZipCode(){
		return $this->buyerZipCode;
	}
	/*
	 *$method.comment
	 */
	public function setBuyerZipCode($buyerZipCode){
		$this->buyerZipCode = $buyerZipCode;
	}
	/*
	 *$method.comment
	 */
	public function getBuyerBirthDate(){
		return $this->buyerBirthDate;
	}
	/*
	 *$method.comment
	 */
	public function setBuyerBirthDate($buyerBirthDate){
		$this->buyerBirthDate = $buyerBirthDate;
	}
	/*
	 *$method.comment
	 */
	public function getBuyerSocialSecurity(){
		return $this->buyerSocialSecurity;
	}
	/*
	 *$method.comment
	 */
	public function setBuyerSocialSecurity($buyerSocialSecurity){
		$this->buyerSocialSecurity = $buyerSocialSecurity;
	}
	/*
	 *$method.comment
	 */
	public function getBuyerDriverLicenseNumber(){
		return $this->buyerDriverLicenseNumber;
	}
	/*
	 *$method.comment
	 */
	public function setBuyerDriverLicenseNumber($buyerDriverLicenseNumber){
		$this->buyerDriverLicenseNumber = $buyerDriverLicenseNumber;
	}
	/*
	 *$method.comment
	 */
	public function getBuyerPhone(){
		return $this->buyerPhone;
	}
	/*
	 *$method.comment
	 */
	public function setBuyerPhone($buyerPhone){
		$this->buyerPhone = $buyerPhone;
	}
	/*
	 *$method.comment
	 */
	public function getCreditAgencyStatus(){
		return $this->creditAgencyStatus;
	}
	/*
	 *$method.comment
	 */
	public function setCreditAgencyStatus($creditAgencyStatus){
		$this->creditAgencyStatus = $creditAgencyStatus;
	}
	/*
	 *$method.comment
	 */
	public function getCustomDate1(){
		return $this->customDate1;
	}
	/*
	 *$method.comment
	 */
	 function setCustomDate1($customDate1){
		$this->customDate1 = $customDate1;
	}
	/*
	 *$method.comment
	 */
	public function getIsTax1Exempt(){
		return $this->isTax1Exempt;
	}
	/*
	 *$method.comment
	 */
	public function setIsTax1Exempt($isTax1Exempt){
		$this->isTax1Exempt = $isTax1Exempt;
	}
	/*
	 *$method.comment
	 */
	public function getIsTax2Exempt(){
		return $this->isTax2Exempt;
	}
	/*
	 *$method.comment
	 */
	public function setIsTax2Exempt($isTax2Exempt){
		$this->isTax2Exempt = $isTax2Exempt;
	}
	/*
	 *$method.comment
	 */
	public function getIsTax3Exempt(){
		return $this->isTax3Exempt;
	}
	/*
	 *$method.comment
	 */
	public function setIsTax3Exempt($isTax3Exempt){
		$this->isTax3Exempt = $isTax3Exempt;
	}
	/*
	 *$method.comment
	 */
	public function getIsTax4Exempt(){
		return $this->isTax4Exempt;
	}
	/*
	 *$method.comment
	 */
	public function setIsTax4Exempt($isTax4Exempt){
		$this->isTax4Exempt = $isTax4Exempt;
	}
	/*
	 *$method.comment
	 */
	public function getIsTax5Exempt(){
		return $this->isTax5Exempt;
	}
	/*
	 *$method.comment
	 */
	public function setIsTax5Exempt($isTax5Exempt){
		$this->isTax5Exempt = $isTax5Exempt;
	}
	/*
	 *$method.comment
	 */
	public function getMerchantAccountExternalCode(){
		return $this->merchantAccountExternalCode;
	}
	/*
	 *$method.comment
	 */
	public function setMerchantAccountExternalCode($merchantAccountExternalCode){
		$this->merchantAccountExternalCode = $merchantAccountExternalCode;
	}
	/*
	 *$method.comment
	 */
	public function getDefaultAddress(){
		return $this->defaultAddress;
	}
	/*
	 *$method.comment
	 */
	 function setDefaultAddress($defaultAddress){
		$this->defaultAddress = $defaultAddress;
	}
	/*
	 *$method.comment
	 */
	public function getDefaultWorkPhone(){
		return $this->defaultWorkPhone;
	}
	/*
	 *$method.comment
	 */
	 function setDefaultWorkPhone($defaultWorkPhone){
		$this->defaultWorkPhone = $defaultWorkPhone;
	}
	/*
	 *$method.comment
	 */
	public function getDefaultHomePhone(){
		return $this->defaultHomePhone;
	}
	/*
	 *$method.comment
	 */
	 function setDefaultHomePhone($defaultHomePhone){
		$this->defaultHomePhone = $defaultHomePhone;
	}
	/*
	 *$method.comment
	 */
	public function getDefaultCellPhone(){
		return $this->defaultCellPhone;
	}
	/*
	 *$method.comment
	 */
	 function setDefaultCellPhone($defaultCellPhone){
		$this->defaultCellPhone = $defaultCellPhone;
	}
	/*
	 *$method.comment
	 */
	public function getDefaultEmailAddress(){
		return $this->defaultEmailAddress;
	}
	/*
	 *$method.comment
	 */
	 function setDefaultEmailAddress($defaultEmailAddress){
		$this->defaultEmailAddress = $defaultEmailAddress;
	}
	/*
	 *$method.comment
	 */
	public function getUser(){
		if (!is_null($this->user)){
			return $this->user;
		}
		else {
			if (is_null($this->collectorCode)){
				return null;
			}
			else {
				return $this->session->loadUser($this->collectorCode);
			}
		}
	}
	/*
	 *$method.comment
	 */
	 function setUser($user){
		$this->user = $user;
	}
	/*
	 *$method.comment
	 */
	 function setSession($session){
		$this->session = $session;
	}
	/*
	 *$method.comment
	 */
	public function reinstate($merchantAccountCode){
		if (!is_null($this->balance) && $this->balance > 0){
			throw new Exception("Unable to reinstate Customer Account with non-zero balance.");
		}
		$this->setSentCollectionsBalance(0);
		$this->setSentCollectionsDate(null);
		$this->setCustomFields(null);
		$this->setIsCollectionsFeeApplied(false);
		$this->setIsReinstated(true);
		$this->setIsReinstateProcess(true);
		$this->setMerchantAccountCode($merchantAccountCode);
	}
	/*
	 *$method.comment
	 */
	public function reinstatePaymentPlans(){
		$this->setIsReinstateProcess(true);
	}
	/*
	 *$method.comment
	 */
	public function writeOff(){
		if (is_null($this->balance) || $this->balance == 0){
			return null;
		}
		$this->setWriteOffBalance(is_null($this->getWriteOffBalance()) ? $this->balance : $this->getWriteOffBalance() + $this->balance);
		$transaction = $this->createAssetTransaction(true);
		$transaction->setTransactionType(AssetTransactionType::WriteOff);
		$transaction->setTransactionSourceType(TransactionSourceType::Agent);
		if ($this->balance > 0){
			$transaction->setAmount($this->balance);
			$transaction->setAccountActivityType(AccountActivityType::Adjustment);
		}
		else {
			$transaction->setAmount(-($this->balance));
			$transaction->setAccountActivityType(AccountActivityType::Refund);
		}
		return $transaction;
	}
	/*
	 *$method.comment
	 */
	public function getIsLite(){
		return parent::getIsLite();
	}
	/*
	 *$method.comment
	 */
	private function createDefaultAddress(){
		$this->defaultAddress = $this->createAddress();
		$this->defaultAddress->setAddressType(AddressType::Unknown);
		$this->defaultAddress->setIsSkipTraceAllowed(true);
		$this->defaultAddress->setIsBad(false);
	}
	/*
	 *$method.comment
	 */
	private function createDefaultCellPhone(){
		$this->defaultCellPhone = $this->createPhone();
		$this->defaultCellPhone->setPhoneType(PhoneType::Mobile);
		$this->defaultCellPhone->setIsSkipTraceAllowed(true);
		$this->defaultCellPhone->setIsBad(false);
	}
	/*
	 *$method.comment
	 */
	private function createDefaultWorkPhone(){
		$this->defaultWorkPhone = $this->createPhone();
		$this->defaultWorkPhone->setPhoneType(PhoneType::Work);
		$this->defaultWorkPhone->setIsSkipTraceAllowed(true);
		$this->defaultWorkPhone->setIsBad(false);
	}
	/*
	 *$method.comment
	 */
	private function createDefaultEmailAddress(){
		$this->defaultEmailAddress = $this->createEmailAddress();
		$this->defaultEmailAddress->setEmailType(AddressType::Unknown);
	}
	/*
	 *$method.comment
	 */
	public function createRefund(){
		$assetTransaction = $this->createAssetTransaction(true);
		$assetTransaction->setAccountActivityType(AccountActivityType::Refund);
		return $assetTransaction;
	}
	/*
	 *$method.comment
	 */
	public function createCredit(){
		$revenueTransaction = $this->createRevenueTransaction(true);
		$revenueTransaction->setAccountActivityType(AccountActivityType::Credit);
		return $revenueTransaction;
	}
	/*
	 *$method.comment
	 */
	private function createDefaultHomePhone(){
		$this->defaultHomePhone = $this->createPhone();
		$this->defaultHomePhone->setPhoneType(PhoneType::Home);
		$this->defaultHomePhone->setIsSkipTraceAllowed(true);
		$this->defaultHomePhone->setIsBad(false);
	}
}

?>

