<?php


/*
 *$comment
 */
class Phone extends ClientObject {
		/*
	 *@var null
	 */
	private $phone;
		/*
	 *@var null
	 */
	private $extention;
		/*
	 *@var null
	 */
	private $addressSourceType;
		/*
	 *@var null
	 */
	private $phoneType;
		/*
	 *@var null
	 */
	private $isBad;
		/*
	 *@var null
	 */
	private $badReason;
		/*
	 *@var null
	 */
	private $isSkipTraceAllowed;
		/*
	 *@var null
	 */
	private $lastSkipTraceDate;
		/*
	 *@var null
	 */
	private $creatorCode;
		/*
	 *@var null
	 */
	private $customerAccount;
		/*
	 *@var null
	 */
	private $user;
		/*
	 *@var null
	 */
	private $session;
		
	/*
	 *$constructor.comment
	 */
	function __construct(){
		//initialization block
		$this->isBad = false;
		$this->isSkipTraceAllowed = true;
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
	}

	/*
	 *$method.comment
	 */
	public function getCreatorCode(){
		return $this->creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function setCreatorCode($creatorCode){
		$this->creatorCode = $creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function setPhone($phone){
		$this->phone = $phone;
	}
	/*
	 *$method.comment
	 */
	public function getPhone(){
		return $this->phone;
	}
	/*
	 *$method.comment
	 */
	public function setExtention($extention){
		$this->extention = $extention;
	}
	/*
	 *$method.comment
	 */
	public function getExtention(){
		return $this->extention;
	}
	/*
	 *$method.comment
	 */
	public function getAddressSourceType(){
		return $this->addressSourceType;
	}
	/*
	 *$method.comment
	 */
	public function setAddressSourceType($addressSourceType){
		$this->addressSourceType = $addressSourceType;
	}
	/*
	 *$method.comment
	 */
	public function setPhoneType($phoneType){
		$this->phoneType = $phoneType;
	}
	/*
	 *$method.comment
	 */
	public function getPhoneType(){
		return $this->phoneType;
	}
	/*
	 *$method.comment
	 */
	public function setIsBad($isBad){
		$this->isBad = $isBad;
	}
	/*
	 *$method.comment
	 */
	public function getIsBad(){
		return $this->isBad;
	}
	/*
	 *$method.comment
	 */
	public function setBadReason($badReason){
		$this->badReason = $badReason;
	}
	/*
	 *$method.comment
	 */
	public function getBadReason(){
		return $this->badReason;
	}
	/*
	 *$method.comment
	 */
	public function setIsSkipTraceAllowed($isSkipTraceAllowed){
		$this->isSkipTraceAllowed = $isSkipTraceAllowed;
	}
	/*
	 *$method.comment
	 */
	public function getIsSkipTraceAllowed(){
		return $this->isSkipTraceAllowed;
	}
	/*
	 *$method.comment
	 */
	public function setLastSkipTraceDate($lastSkipTraceDate){
		$this->lastSkipTraceDate = $lastSkipTraceDate;
	}
	/*
	 *$method.comment
	 */
	public function getLastSkipTraceDate(){
		return $this->lastSkipTraceDate;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccount($customerAccount){
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getUser(){
		if (!is_null($this->user)){
			return $this->user;
		}
		else {
			if (is_null($this->creatorCode)){
				return null;
			}
			else {
				return $this->session->loadUser($this->creatorCode);
			}
		}
	}
	/*
	 *$method.comment
	 */
	 function setUser($user){
		$this->user = $user;
	}
	/*
	 *$method.comment
	 */
	 function setSession($session){
		$this->session = $session;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "Phone";
	}
}

?>

