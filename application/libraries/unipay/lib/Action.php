<?php



/*
 *$comment
 */
class Action extends ClientObject {
		/*
	 *@var null
	 */
	private $actionCode;
		/*
	 *@var null
	 */
	private $customerAccount;
		/*
	 *@var null
	 */
	private $creatorCode;
		/*
	 *@var null
	 */
	private $session;
		/*
	 *@var null
	 */
	private $user;
		
	/*
	 *$constructor.comment
	 */
	function __construct($actionCode = null, $creatorCode = null, $createDate = null, $customerAccount = null, $merchantAccountCode = null, $id = null){
		//initialization block
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
		//constructor with parameters
		$this->actionCode = $actionCode;
		$this->creatorCode = $creatorCode;
		$this->setCreateDate($createDate);
		$this->customerAccount = $customerAccount;
		$this->setMerchantAccountCode($merchantAccountCode);
		if (is_null($id)){
			$this->setRefIdSpecial($this->hashCode());
		}
		else {
			$this->setId($id);
			$this->setRefId($id);
			$this->setCode(strval($id));
		}
	
	}

	/*
	 *$method.comment
	 */
	public function getActionCode(){
		return $this->actionCode;
	}
	/*
	 *$method.comment
	 */
	public function setActionCode($actionCode){
		$this->actionCode = $actionCode;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccount($customerAccount){
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getCreatorCode(){
		return $this->creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function setCreatorCode($creatorCode){
		$this->creatorCode = $creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function getUser(){
		if (!is_null($this->user)){
			return $this->user;
		}
		else {
			if (is_null($this->creatorCode)){
				return null;
			}
			else {
				return $this->session->loadUser($this->creatorCode);
			}
		}
	}
	/*
	 *$method.comment
	 */
	 function setUser($user){
		$this->user = $user;
	}
	/*
	 *$method.comment
	 */
	 function setSession($session){
		$this->session = $session;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "Action";
	}
}

?>

