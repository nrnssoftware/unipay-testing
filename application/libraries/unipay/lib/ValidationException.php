<?php

/**
 * Define our own exception class
 */
class ValidationException extends Exception
{
    private $errorField;
    private $errorValue;
    private $errorCode;
    private $errorObject;
    // ovveride constructor
    public function __construct($message, DOMDocument $document) {
        // call parent constructor
        parent::__construct($message);
        // set errorField, errorValue, errorCode and errorObject. 
	$this -> errorField = $document->documentElement->getAttribute('field');
	$this -> errorValue = $document->documentElement->getAttribute('value');
	$this -> errorCode = $document->documentElement->getAttribute('code');
	$this -> errorObject= $document->documentElement->getAttribute('errorObject');
        
    }
      // public getters 
    public function getErrorField(){
	return $this -> errorField;
    }
    public function getErrorValue(){
	return $this -> errorValue;
    }
    public function getErrorCode(){
	return $this -> errorCode;
    }
    public function getErrorObject(){
	return $this -> errorObject;
    }
    
     public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
?>