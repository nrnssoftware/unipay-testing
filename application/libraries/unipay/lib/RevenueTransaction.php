<?php


/*
 *$comment
 */
class RevenueTransaction extends AccountTransaction {
		/*
	 *@var null
	 */
	private $itemCode;
		/*
	 *@var null
	 */
	private $isComplimentary;
		/*
	 *@var null
	 */
	private $isForceStatement;
		/*
	 *@var null
	 */
	private $paymentOption;
		
	/*
	 *$constructor.comment
	 */
	function __construct($createDate = null, $isComplimentary = null){
		//initialization block
		$this->isComplimentary = false;
		$this->isForceStatement = false;
		if (func_num_args() == 0) {
		//default constructor
		parent::__construct();		return;
		}
		//constructor with parameters
		parent::__construct();
		$this->setCreateDate($createDate);
		$this->isComplimentary = $isComplimentary;
	
	}

	/*
	 *$method.comment
	 */
	public function getItemCode(){
		return $this->itemCode;
	}
	/*
	 *$method.comment
	 */
	public function setItemCode($itemCode){
		$this->itemCode = $itemCode;
	}
	/*
	 *$method.comment
	 */
	public function getIsComplimentary(){
		return $this->isComplimentary;
	}
	/*
	 *$method.comment
	 */
	public function setIsComplimentary($isComplimentary){
		$this->isComplimentary = $isComplimentary;
	}
	/*
	 *$method.comment
	 */
	public function getIsForceStatement(){
		return $this->isForceStatement;
	}
	/*
	 *$method.comment
	 */
	public function setIsForceStatement($isForceStatement){
		$this->isForceStatement = $isForceStatement;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentOption(){
		return $this->paymentOption;
	}
	/*
	 *$method.comment
	 */
	public function setPaymentOption($paymentOption){
		$this->paymentOption = $paymentOption;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		if (is_null($this->getAccountActivityType())){
			throw new Exception("Cannot resolve type for RevenueTransaction.");
		}
		else {
			if (AccountActivityType::Credit == $this->getAccountActivityType()){
				return "Credit";
			}
			else {
				return "RevenueTransaction";
			}
		}
	}
}

?>

