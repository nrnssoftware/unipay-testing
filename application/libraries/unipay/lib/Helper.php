<?php

define('CRLF', "\r\n");
define('BUFFER_LENGTH', 8192);

class Helper {

	private function __construct() {
	}

	public static function getNextBillingDate($nextBillingDate, $billingCycleCode, $chargeIndex, $recurrence) {
		
		$cycleType = strtoUpper ( $billingCycleCode {0} );
		$cycleDay = intval(substr ( $billingCycleCode, 1, 3 ));
		switch ( $cycleType ) {
			case "W" :
				$x = 7 * $chargeIndex * $recurrence;
				$nextBillingDate->modify ( "+$x day" );
				return $nextBillingDate;
			case "M" :
				$x = 1 * $chargeIndex;
				$nextBillingDate = Helper::shiftMonthlyNextBillingDate($nextBillingDate, $x, $cycleDay);
				return $nextBillingDate;		
			default :
				throw new Exception ( "Unknown Billing Cycle Type." );
		}
	}
	
	public static function shiftMonthlyNextBillingDate($billingDate, $monthCount, $billingDay) {
		$month = $billingDate->format("m");
		$year = $billingDate->format("y");
		$billingDate = new DateTime("$year-$month-01");
		$billingDate->modify("+$monthCount months");
		$month = $billingDate->format("m");
		$year = $billingDate->format("y");
		echo $year;
		$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
		if ($billingDay > $lastDay){
			$billingDate = new DateTime("$year-$month-$lastDay");
		} else {
			$billingDate = new DateTime("$year-$month-$billingDay");
		}
		return $billingDate;
	}
	

	public static function validateFirstBillingDate($firstBillingDate, $billingCycleCode, $code=null) {

		if (new DateTime() > $firstBillingDate) {
			return false;
		}

		$cycleType = strtoUpper ( $billingCycleCode{0} );
		$cycleDay = intval(substr ( $billingCycleCode, 1, 3 ));

		if ($cycleType == "W") {
			$dayOfWeek = date ( "N", strtotime ($firstBillingDate) );
			//adjust to begin week with Monday
			$dayOfWeek = $dayOfWeek - 1;
			if ($dayOfWeek == 0) $dayOfWeek = 7;
			
			if ($cycleDay != $dayOfWeek) {
				return false;
			} else {
				return true;
			}
		} else {
			$month = $firstBillingDate->format("m");
			$year = $firstBillingDate->format("y");
			$lastDay = cal_days_in_month(CAL_GREGORIAN, $month, $year);
			
			if ($cycleDay > $lastDay){
				$billingDate = new DateTime("$year-$month-$lastDay");
			} else {
				$billingDate = new DateTime("$year-$month-$cycleDay");
			}
			
			$shift = 1;
			if ($cycleType == "Q") {
				$shift = 3;
			} else if ($cycleType == "S") {
				$shift = 6;
			}
			if (new DateTime() > $firstBillingDate) {
				$billingDate =  Helper::shiftMonthlyNextBillingDate($billingDate, $shift, $cycleDay);
			}
			while (true) {
				if ($firstBillingDate == $billingDate ){
					return true;
				}
				if ($firstBillingDate < $billingDate){
					return false;
				}
				$billingDate =  Helper::shiftMonthlyNextBillingDate($billingDate, $shift, $cycleDay);
			}
		}
	}

	public static function parseDate($string_date) {
		if (empty($string_date))
			return null; 
		else
			return new DateTime ( date ( "Ymd", strtotime ( $string_date ) ), new DateTimeZone ( "EST" ) );
	}
	
	public static function formatDate($datetime) {
		return $datetime->format ( "Ymd" );
	}
	public static function getRefId($refId) {
		if ($refId {0} == '#')
			return intval(substr ( $refId, 1 ));
		return $refId;
	}
	public static function getId($id) {
		if ($id {0} == '#')
			return substr ( $id, 1 );
		return $id;
	}
	
	public static function isNewObject($id) {
		return $id {0} == '#' ? TRUE : FALSE;
	}
	
	public static function getClientException($exception) {
		return $exception;
	}
	
	public static function getSessionConnection($config) {
		return new XMLSessionConnection ( );
	}

	/**
	 * Compares two dates. Returns positive value if first date is greater than the second.
	 * Returns negative value if first date is less than the second. Returns zero if the dates
	 * are equal
	 *
	 * @param date1
	 *            first date
	 * @param date2
	 *            second date
	 * 
	 * @return result of comparison
	 */
    public static function compareDate($date1, $date2){
      if ($date1 == $date2) return 0;
      return $date1<$date2?-1:1;
	}
    
	public static function getCustomerAccountValues($map){
      return $map->values();
	}
	
	public static function getPaymentOptionValues($map){
      return $map->values();
	}

	public static function getAddressesValues($map){
      return $map->values();
	}

	public static function getPhonesValues($map){
      return $map->values();
	}

	public static function getEmailAddressesValues($map){
      return $map->values();
	}
        	
	public static function getPaymentPlanValues($map){
      return $map->values();
	}
	
	public static function getRevenueTransactionValues($map){
      return $map->values();
	}

	public static function getAssetTransactionValues($map){
      return $map->values();
	}
	//public static String toString(Properties properties) {
	public static function toString($properties){
		//used to set custom fields, will need to implement conversion of associative array to string in java properties
		return null;
	}
	public static function fromString($properties){
		//used to set custom fields, will need to implement conversion of associative array to string in java properties
		return null;
	}
	//
	public static function sort($list){
		//implement sorting using logic in ClientObjectComparator
		return $list;
	}
	public static function customerAccountListToClientObjectList($list){
      return $list;
	}
	public static function paymentOptionListToClientObjectList($list){
      return $list;
	}
	public static function revenueTransactionListToClientObjectList($list){
      return $list;
	}
	public static function assetTransactionListToClientObjectList($list){
      return $list;
	}
	public static function accountTransactionListToClientObjectList($list){
      return $list;
	}
	public static function paymentPlanListToClientObjectList($list){
      return $list;
	}
	public static function actionListToClientObjectList($list){
      return $list;
	}
	public static function noteListToClientObjectList($list){
      return $list;
	}
	public static function letterListToClientObjectList($list){
      return $list;
	}
	public static function emailListToClientObjectList($list){
      return $list;
	}
	public static function documentListToClientObjectList($list){
      return $list;
	}
	public static function contractListToClientObjectList($list){
      return $list;
	}
	public static function userListToClientObjectList($list){
      return $list;
	}
	public static function clientObjectListToAccountTransactionList($list){
      return $list;
	}
	public static function clientObjectListToActionList($list){
      return $list;
	}
	public static function clientObjectListToAdjustmentList($list){
      return $list;
	}
	public static function clientObjectListToAgentCommissionList($list){
      return $list;
	}
	public static function clientObjectListToAuditLogList($list){
      return $list;
	}
	public static function clientObjectListToContractList($list){
      return $list;
	}
	public static function clientObjectListToCustomerAccountList($list){
		$returnList = new ArrayList();
		foreach ($list as $customerAccount){
			if ($customerAccount instanceof CustomerAccount){
				$returnList->add($customerAccount);
			}
		}
		return $returnList;
	}
	public static function clientObjectListToDocumentList($list){
      return $list;
	}
	public static function clientObjectListToEmailList($list){
      return $list;
	}
	public static function clientObjectListToLetterList($list){
      return $list;
	}
	public static function clientObjectListToNoteList($list){
      return $list;
	}
	public static function clientObjectListToPaymentOptionList($list){
      return $list;
	}
	public static function clientObjectListToPaymentPlanList($list){
      return $list;
	}
	public static function clientObjectListToScheduledPaymentList($list){
      return $list;
	}
	public static function clientObjectListToSignatureList($list){
      return $list;
	}
	public static function clientObjectListToUserList($list){
      return $list;
	}
	
	public static function isEmpty($value){
      return $value == null || empty($value);
	}
	
	public static function doPost($requestFacade, $config) {
		$data = $requestFacade->getDocument ()->saveXML ();
		// parse the given URL
		$url = parse_url ( $config->get(SessionConnection::PROCESSOR_HOST) );
		
		// extract host and path:
		$host = $url ['host'];
		$path = $url ['path'];
		$port = array_key_exists ( 'port', $url ) ? $url ['port'] : null;
		
		if (is_null($port)) $port = 443;
		if ($url['scheme'] == 'https'){
			$hosturl = "ssl://".$host;
		}
		else $hosturl = $host;
		// open a socket connection on port 
		$fp = fsockopen ($hosturl, $port );
		
		if ($fp == null){
			throw new Exception('ERROR: No connection could be made, server is unavailable.');
		}
		
		// send the request headers:
		if ($config->get(SessionConnection::DEBUG)){
			print "<br/><br/>";
			print "********************************************************************************************<br/>";
			print "POST $path HTTP/1.1<br/>";
			print "Host: $host<br/>";
			print "Content-type: application/x-www-form-urlencoded<br/>";
			print "Content-length: " . strlen ( $data ) . "<br/>" ;
			print "Connection: close<br/><br/>";
			print "********************************************************************************************<br/>";
			print htmlspecialchars($data);		
		}

		fputs ( $fp, "POST $path HTTP/1.1\r\n" );
		fputs ( $fp, "Host: $host\r\n" );
		fputs ( $fp, "Content-type: application/x-www-form-urlencoded\r\n" );
		fputs ( $fp, "Content-length: " . strlen ( $data ) . "\r\n" );
		fputs ( $fp, "Connection: close\r\n\r\n" );
		fputs ( $fp, $data );

		$headers = '';
		$body = '';
		$length = 0;

		// get headers FIRST
		do
		{
			// use fgets() not fread(), fgets stops reading at first newline
			// or buffer which ever one is reached first
			$data = fgets($fp, BUFFER_LENGTH);
			// a sincle CRLF indicates end of headers
			if ($data === false || $data == CRLF || feof($fp)) {
				// break BEFORE OUTPUT
				break;
			}
			$headers .= $data;
		}	
		while (true);
		// end of headers
		
		if (strpos($headers, 'Transfer-Encoding: chunked') > 0) {
	
			// read from chunked stream
			// loop though the stream
			do
			{
				// NOTE: for chunked encoding to work properly make sure
				// there is NOTHING (besides newlines) before the first hexlength

				// get the line which has the length of this chunk (use fgets here)
				$line = fgets($fp, BUFFER_LENGTH);

				// if it's only a newline this normally means it's read
				// the total amount of data requested minus the newline
				// continue to next loop to make sure we're done
				if ($line == CRLF) {
					continue;
				}

				// the length of the block is sent in hex decode it then loop through
				// that much data get the length
				// NOTE: hexdec() ignores all non hexadecimal chars it finds
				$length = hexdec($line);

				if (!is_int($length)) {
					trigger_error('Most likely not chunked encoding', E_USER_ERROR);
				}

				// zero is sent when at the end of the chunks
				// or the end of the stream or error
				if ($line === false || $length < 1 || feof($fp)) {
					// break out of the streams loop
					break;
				}

				// loop though the chunk
				do
				{
					// read $length amount of data
					// (use fread here)
					$data = fread($fp, $length);

					// remove the amount received from the total length on the next loop
					// it'll attempt to read that much less data
					$length -= strlen($data);

					// PRINT out directly
					#print $data;
					#flush();
					// you could also save it directly to a file here

					// store in string for later use
					$body .= $data;

					// zero or less or end of connection break
					if ($length <= 0 || feof($fp))
					{
						// break out of the chunk loop
						break;
					}
				}
				while (true);
				// end of chunk loop
			}
			while (true);
			// end of stream loop	
		}
		else {
			while ( ! feof ( $fp ) ) {
				// receive the results of the request
				$body .= fgets ( $fp, 1024 );
			}		
		}

		// close the socket connection:
		fclose ( $fp );
	
		// split the result header from the content
		//		$result = explode ( "\r\n\r\n", $result, 2 );
		
//		$header = isset ( $result [0] ) ? $result [0] : '';
//		$content = isset ( $result [1] ) ? $result [1] : '';

//		print htmlspecialchars ( $content );	
		if ($config->get(SessionConnection::DEBUG)){
			print "<br/><br/>";
			print "********************************************************************************************<br/>";
			print nl2br(htmlspecialchars($headers));
			print "********************************************************************************************<br/>";
			print htmlspecialchars($body);
		}
		
		$document = new DOMDocument ( );
		$document->loadXML ( $body );
		
		if ($document->documentElement->nodeName == "exception"){
			throw new ValidationException($document->documentElement->firstChild->nodeValue, $document);
		}

		return new ElementFacade ( $document->documentElement );
	
	}
	public static function setQueryParameters($facade, $parametersMap) {

		if ($parametersMap == null)
			return;
			
		foreach ($parametersMap->entrySet() as $mapEntry){
			if ($mapEntry->getValue() instanceof ArrayList || $mapEntry->getValue() instanceof LinkedList) {
				$facade->setAttribute($mapEntry->getKey(), Helper::asString($mapEntry->getValue()));
			} else if ($mapEntry->getValue() instanceof Boolean) {
				$facade->setAttribute($mapEntry->getKey(), ((Boolean) $mapEntry->getValue()) ? '1' : '0');
			} else {
				$facade->setAttribute($mapEntry->getKey(), $mapEntry->getValue());
			}
		}
	}
	public static function asString($facade, $list) {
		
		if ($list == null || $list->isEmpty())
			return null;
		
		$output  = '';
		foreach ($list as $str){
			$output .= $str.';';
		}
		$output = substr ( $output, strlen($output-1) , strlen($output) );
		return $output;
	}
	public static function toClientObjectList($list){
      return $list;
	}
	public static function getOrderBy($orderList) {
		if ($orderList == null || $orderList.isEmpty())
			return null;
		
		$output  = '';
		foreach ($orderList as $str){
			$output .= $str.', ';
		}
		$output = substr ( $output, strlen($output-2) , strlen($output) );
		return $output;
	}
	public static function getClassName($clientObject){
		return get_class($clientObject);
	}
	public static function encode($value) {
		return $value == null ? null : md5($value);
	}
}

?>