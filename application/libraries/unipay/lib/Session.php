<?php


/*
 *$comment
 */
class Session {
		/*
	 *@var null
	 */
	const FIRST_RESULT = "firstResult";
		/*
	 *@var null
	 */
	const MAX_RESULTS = "maxResults";
		/*
	 *@var null
	 */
	const ORDER_LIST = "orderList";
		/*
	 *@var null
	 */
	const LOAD_ALL = "all";
		/*
	 *@var null
	 */
	const LOAD_NONE = "none";
		/*
	 *@var null
	 */
	const LOAD_ALL_ACTIVE = "allActive";
		/*
	 *@var null
	 */
	const LOAD_ALL_INACTIVE = "allInActive";
		/*
	 *@var null
	 */
	const ORDER_BY = "orderBy";
		/*
	 *@var null
	 */
	const SOFT = "S";
		/*
	 *@var null
	 */
	const HARD = "H";
		/*
	 *@var null
	 */
	const REPORT_ACCOUNT_ACTIVITY = "account_activity_export";
		/*
	 *@var null
	 */
	const REPORT_CUSTOMER_ACCOUNT_MASTER = "customer_master";
		/*
	 *@var null
	 */
	const REPORT_EXPORT_TYPE_CSV = 0;
		/*
	 *@var null
	 */
	const REPORT_EXPORT_TYPE_EXCEL = 1;
		/*
	 *@var null
	 */
	const REPORT_EXPORT_TYPE_HTML = 2;
		/*
	 *@var null
	 */
	const REPORT_EXPORT_TYPE_PDF = 3;
		/*
	 *@var null
	 */
	private $sessionConnection;
		/*
	 *@var null
	 */
	private $sessionContext;
		/*
	 *@var null
	 */
	private $userCode;
		
	/*
	 *$constructor.comment
	 */
	function __construct($sessionConnection = null){
		//initialization block
		$this->FIRST_RESULT = "firstResult";
		$this->MAX_RESULTS = "maxResults";
		$this->ORDER_LIST = "orderList";
		$this->LOAD_ALL = "all";
		$this->LOAD_NONE = "none";
		$this->LOAD_ALL_ACTIVE = "allActive";
		$this->LOAD_ALL_INACTIVE = "allInActive";
		$this->ORDER_BY = "orderBy";
		$this->SOFT = "S";
		$this->HARD = "H";
		$this->REPORT_ACCOUNT_ACTIVITY = "account_activity_export";
		$this->REPORT_CUSTOMER_ACCOUNT_MASTER = "customer_master";
		$this->REPORT_EXPORT_TYPE_CSV = 0;
		$this->REPORT_EXPORT_TYPE_EXCEL = 1;
		$this->REPORT_EXPORT_TYPE_HTML = 2;
		$this->REPORT_EXPORT_TYPE_PDF = 3;
		$this->sessionContext = new SessionContext();
		//constructor with parameters
		$this->sessionConnection = $sessionConnection;
	
	}

	/*
	 *$method.comment
	 */
	public function setUserCode($userCode){
		$this->userCode = $userCode;
	}
	/*
	 *$method.comment
	 */
	public function getUserCode(){
		return $this->userCode;
	}
	/*
	 *$method.comment
	 */
	public static function login($merchantAccountCode,$password,$config){
		if (is_null($config)){
			$config = new HashMap();
		}
		$sessionConnection = Helper::getSessionConnection($config);
		$config->put("version", "1.1");
		$sessionConnection->login($merchantAccountCode, $password, $config);
		return new Session($sessionConnection);
	}
	/*
	 *$method.comment
	 */
	public function logout(){
		if (!is_null($this->sessionConnection)){
			$this->sessionConnection->logout();
		}
		$this->sessionConnection = null;
		$this->sessionContext = null;
	}
	/*
	 *$method.comment
	 */
	public function createCustomerAccountExtended($account,$code,$merchantAccountCode,$firstName,$lastName,$middleName,$title,$suffix,$customerAccountType,$homePhone,$workPhone,$cellPhone,$email,$street1,$street2,$city,$state,$zipCode,$beneficiaryInfo,$customerAccountGroupCode,$notes,$birthDate){
		$customerAccount = $this->createCustomerAccount();
		$customerAccount->setCode($code);
		$customerAccount->setMerchantAccountCode($merchantAccountCode);
		$customerAccount->setFirstName($firstName);
		$customerAccount->setLastName($lastName);
		$customerAccount->setMiddleName($middleName);
		$customerAccount->setTitle($title);
		$customerAccount->setSuffix($suffix);
		$customerAccount->setType($customerAccountType);
		$customerAccount->setHomePhone($homePhone);
		$customerAccount->setWorkPhone($workPhone);
		$customerAccount->setCellPhone($cellPhone);
		$customerAccount->setEmail($email);
		$customerAccount->setStreet1($street1);
		$customerAccount->setStreet2($street2);
		$customerAccount->setCity($city);
		$customerAccount->setState($state);
		$customerAccount->setZipCode($zipCode);
		$customerAccount->setBeneficiaryInfo($beneficiaryInfo);
		$customerAccount->setCustomerAccountGroupCode($customerAccountGroupCode);
		$customerAccount->setNotes($notes);
		$customerAccount->setBirthDate($birthDate);
		if (!is_null($account)){
			$customerAccount->setCustomerCode($account->getCustomerCode());
		}
		return $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function save($clientObject){
		$this->sessionContext->addClientObject($clientObject);
	}
	/*
	 *$method.comment
	 */
	public function clearSaved(){
		$this->sessionContext->clearClientObjectsList();
	}
	/*
	 *$method.comment
	 */
	public function synchronize(){
		try{
			$this->sessionContext->toMap();
			$this->sessionConnection->save($this->sessionContext, $this->userCode, $this->sessionContext->getClientObjectsList());
			$this->sessionContext->clearClientObjectsList();
		}
		catch(Throwable $throwable){
			throw Helper::getClientException($throwable);
		}
	}
	/*
	 *$method.comment
	 */
	public function detachSynchronized(){
		$this->sessionContext->clearClientObjectsList();
		$this->sessionContext->clear();
	}
	/*
	 *$method.comment
	 */
	public function loadCustomerAccount($code){
		if ($this->isEmpty($code)){
			throw new Exception("Parameter code in method loadCustomerAccount() is required.");
		}
		$customerAccount = $this->loadCustomerAccountExtended($code, Session::LOAD_NONE, Session::LOAD_NONE, Session::LOAD_NONE, null, null);
		if (is_null($customerAccount)){
			return null;
		}
		return $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function loadCustomerAccountExtended($code,$loadPaymentOption,$loadPaymentPlan,$loadContract,$loadAccountTransactionFromDate,$loadAccountTransactionToDate){
		if ($this->isEmpty($code)){
			throw new Exception("Parameter code in method loadCustomerAccountExtended() is required.");
		}
		$possibleValues = array("all","none","allActive","allInActive");
		if (is_null($loadPaymentOption) || !($this->validateParameter($loadPaymentOption, $possibleValues))){
			throw new Exception("Unable to process loadCustomerAccount. Invalid parameter value. Parameter: loadPaymentOption. Possible values: %1$s.", Helper::printPossibleValues($possibleValues));
		}
		if (is_null($loadPaymentPlan) || !($this->validateParameter($loadPaymentPlan, $possibleValues))){
			throw new Exception("Invalid parameter value. Parameter: loadPaymentPlan. Possible values: %1$s.", Helper::printPossibleValues($possibleValues));
		}
		if (is_null($loadContract) || !($this->validateParameter($loadContract, $possibleValues))){
			throw new Exception("Invalid parameter value. Parameter: loadContract. Possible values: %1$s.", Helper::printPossibleValues($possibleValues));
		}
		$parameters = new HashMap();
		$parameters->put("code", $code);
		$parameters->put("loadPaymentOption", $loadPaymentOption);
		$parameters->put("loadPaymentPlan", $loadPaymentPlan);
		$parameters->put("loadContract", $loadContract);
		$parameters->put("loadAccountTransactionFromDate", $loadAccountTransactionFromDate);
		$parameters->put("loadAccountTransactionToDate", $loadAccountTransactionToDate);
		$parameters->put("merchantAccountCode", null);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::LOAD_CUSTOMER_ACCOUNT, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		$customerAccount = $responseList->get(0);
		$customerAccount->setSession($this);
		return $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function loadPaymentOption($code){
		if ($this->isEmpty($code)){
			throw new Exception("Parameter code in method loadPaymentOption() is required.");
		}
		$responseList = null;
		$parameters = new HashMap();
		$parameters->put("code", $code);
		$responseList = $this->queryPaymentOption($parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		return $responseList->get(0);
	}
	/*
	 *$method.comment
	 */
	public function loadPaymentPlan($code){
		if ($this->isEmpty($code)){
			throw new Exception("Parameter code in method loadPaymentPlan() is required.");
		}
		$responseList = null;
		$parameters = new HashMap();
		$parameters->put("code", $code);
		$responseList = $this->queryPaymentPlan($parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		return $responseList->get(0);
	}
	/*
	 *$method.comment
	 */
	public function loadRevenueTransaction($code){
		if ($this->isEmpty($code)){
			throw new Exception("Parameter code in method loadRevenueTransaction() is required.");
		}
		$responseList = null;
		$parameters = new HashMap();
		$parameters->put("code", $code);
		$responseList = $this->queryAccountTransaction($parameters);
		if (is_null($responseList) || $responseList->isEmpty() || ($responseList->get(0) instanceof AssetTransaction)){
			return null;
		}
		return $responseList->get(0);
	}
	/*
	 *$method.comment
	 */
	public function loadAssetTransaction($code){
		if ($this->isEmpty($code)){
			throw new Exception("Parameter code in method loadAssetTransaction() is required.");
		}
		$responseList = null;
		$parameters = new HashMap();
		$parameters->put("code", $code);
		$responseList = $this->queryAccountTransaction($parameters);
		if (is_null($responseList) || $responseList->isEmpty() || ($responseList->get(0) instanceof RevenueTransaction)){
			return null;
		}
		return $responseList->get(0);
	}
	/*
	 *$method.comment
	 */
	public function loadAction($code){
		if ($this->isEmpty($code)){
			throw new Exception("Parameter code in method loadAction() is required.");
		}
		$actionList = null;
		$parameters = new HashMap();
		$parameters->put("code", $code);
		$actionList = $this->queryAction($parameters);
		if (is_null($actionList) || $actionList->isEmpty()){
			return null;
		}
		return $actionList->get(0);
	}
	/*
	 *$method.comment
	 */
	public function loadNote($code){
		if ($this->isEmpty($code)){
			throw new Exception("Parameter code in method loadNote() is required.");
		}
		$noteList = null;
		$parameters = new HashMap();
		$parameters->put("code", $code);
		$noteList = $this->queryNote($parameters);
		if (is_null($noteList) || $noteList->isEmpty()){
			return null;
		}
		return $noteList->get(0);
	}
	/*
	 *$method.comment
	 */
	public function loadLetter($code){
		if ($this->isEmpty($code)){
			throw new Exception("Parameter code in method loadLetter() is required.");
		}
		$letterList = null;
		$parameters = new HashMap();
		$parameters->put("code", $code);
		$letterList = $this->queryLetter($parameters);
		if (is_null($letterList) || $letterList->isEmpty()){
			return null;
		}
		return $letterList->get(0);
	}
	/*
	 *$method.comment
	 */
	public function loadEmail($code){
		if ($this->isEmpty($code)){
			throw new Exception("Parameter code in method loadEmail() is required.");
		}
		$emailList = null;
		$parameters = new HashMap();
		$parameters->put("code", $code);
		$emailList = $this->queryEmail($parameters);
		if (is_null($emailList) || $emailList->isEmpty()){
			return null;
		}
		return $emailList->get(0);
	}
	/*
	 *$method.comment
	 */
	public function loadScheduledPayment($code){
		if ($this->isEmpty($code)){
			throw new Exception("Parameter code in method loadScheduledPayment() is required.");
		}
		$scheduledPaymentList = null;
		$parameters = new HashMap();
		$parameters->put("code", $code);
		$scheduledPaymentList = $this->queryScheduledPayment($parameters);
		if (is_null($scheduledPaymentList) || $scheduledPaymentList->isEmpty()){
			return null;
		}
		return $scheduledPaymentList->get(0);
	}
	/*
	 *$method.comment
	 */
	public function loadDocument($code){
		if ($this->isEmpty($code)){
			throw new Exception("Parameter code in method loadDocument() is required.");
		}
		$documentList = null;
		$parameters = new HashMap();
		$parameters->put("code", $code);
		$documentList = $this->queryDocument($parameters);
		if (is_null($documentList) || $documentList->isEmpty()){
			return null;
		}
		return $documentList->get(0);
	}
	/*
	 *$method.comment
	 */
	public function queryCustomerAccount($parameters){
		return $this->findCustomerAccountExtended($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("code") ? $parameters->get("code") : null, $parameters->containsKey("customerCode") ? $parameters->get("customerCode") : null, $parameters->containsKey("firstName") ? $parameters->get("firstName") : null, $parameters->containsKey("lastName") ? $parameters->get("lastName") : null, $parameters->containsKey("middleName") ? $parameters->get("middleName") : null, $parameters->containsKey("title") ? $parameters->get("title") : null, $parameters->containsKey("suffix") ? $parameters->get("suffix") : null, $parameters->containsKey("isActive") ? $parameters->get("isActive") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey("phone") ? $parameters->get("phone") : null, $parameters->containsKey("email") ? $parameters->get("email") : null, $parameters->containsKey("zipCode") ? $parameters->get("zipCode") : null, $parameters->containsKey("socialSecurity") ? $parameters->get("socialSecurity") : null, $parameters->containsKey("driverLicenseNumber") ? $parameters->get("driverLicenseNumber") : null, $parameters->containsKey("creditCardNumber") ? $parameters->get("creditCardNumber") : null, $parameters->containsKey("bankAccountNumber") ? $parameters->get("bankAccountNumber") : null, $parameters->containsKey("contractCode") ? $parameters->get("contractCode") : null, $parameters->containsKey("paymentPlanCode") ? $parameters->get("paymentPlanCode") : null, $parameters->containsKey("fromBalance") ? $parameters->get("fromBalance") : null, $parameters->containsKey("toBalance") ? $parameters->get("toBalance") : null, $parameters->containsKey("customerAccountGroupCode") ? $parameters->get("customerAccountGroupCode") : null, $parameters->containsKey("collectorCode") ? $parameters->get("collectorCode") : null, $parameters->containsKey("isNullCollectorCode") ? $parameters->get("isNullCollectorCode") : null, $parameters->containsKey("lastActionCode") ? $parameters->get("lastActionCode") : null, $parameters->containsKey("collectionsPriorityType") ? $parameters->get("collectionsPriorityType") : null, $parameters->containsKey("fromSentCollectionsDate") ? $parameters->get("fromSentCollectionsDate") : null, $parameters->containsKey("toSentCollectionsDate") ? $parameters->get("toSentCollectionsDate") : null, $parameters->containsKey("isNullSentCollectionsDate") ? $parameters->get("isNullSentCollectionsDate") : null, $parameters->containsKey("fromAgeCollectionsDate") ? $parameters->get("fromAgeCollectionsDate") : null, $parameters->containsKey("toAgeCollectionsDate") ? $parameters->get("toAgeCollectionsDate") : null, $parameters->containsKey("isLite") ? $parameters->get("isLite") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null, $parameters->containsKey(Session::ORDER_LIST) ? $parameters->get(Session::ORDER_LIST) : null);
	}
	/*
	 *$method.comment
	 */
	public function queryContract($parameters){
		return $this->findContract($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("collectorAccountCode") ? $parameters->get("collectorAccountCode") : null, $parameters->containsKey("code") ? $parameters->get("code") : null, $parameters->containsKey("name") ? $parameters->get("name") : null, $parameters->containsKey("type") ? $parameters->get("type") : null, $parameters->containsKey("status") ? $parameters->get("status") : null, $parameters->containsKey("creatorCode") ? $parameters->get("creatorCode") : null, $parameters->containsKey("sellerCode") ? $parameters->get("sellerCode") : null, $parameters->containsKey("importCode") ? $parameters->get("importCode") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey("fromFreezeBeginDate") ? $parameters->get("fromFreezeBeginDate") : null, $parameters->containsKey("toFreezeBeginDate") ? $parameters->get("toFreezeBeginDate") : null, $parameters->containsKey("fromFreezeEndDate") ? $parameters->get("fromFreezeEndDate") : null, $parameters->containsKey("toFreezeEndDate") ? $parameters->get("toFreezeEndDate") : null, $parameters->containsKey("isVisibleExternallyOnly") ? $parameters->get("isVisibleExternallyOnly") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("defaultPaymentPlanCode") ? $parameters->get("defaultPaymentPlanCode") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null);
	}
	/*
	 *$method.comment
	 */
	public function loadContract($code){
		if ($this->isEmpty($code)){
			throw new Exception("Parameter code in method loadContract() is required.");
		}
		$contracts = null;
		$parameters = new HashMap();
		$parameters->put("code", $code);
		$contracts = $this->queryContract($parameters);
		if (is_null($contracts) || $contracts->isEmpty()){
			return null;
		}
		return $contracts->get(0);
	}
	/*
	 *$method.comment
	 */
	public function findCustomerAccount($merchantAccountCode,$code,$customerCode,$firstName,$lastName,$middleName,$title,$suffix,$isActive,$fromCreateDate,$toCreateDate,$phone,$email,$zipCode,$creditCardNumber,$bankAccountNumber,$contractCode,$paymentPlanCode,$fromBalance,$toBalance,$customerAccountGroupCode,$isLite,$firstResult,$maxResults){
		return $this->findCustomerAccountExtended($merchantAccountCode, $code, $customerCode, $firstName, $lastName, $middleName, $title, $suffix, $isActive, $fromCreateDate, $toCreateDate, $phone, $email, $zipCode, null, null, $creditCardNumber, $bankAccountNumber, $contractCode, $paymentPlanCode, $fromBalance, $toBalance, $customerAccountGroupCode, null, null, null, null, null, null, null, null, null, $isLite, $firstResult, $maxResults, null);
	}
	/*
	 *$method.comment
	 */
	public function findCustomerAccountExtended($merchantAccountCode,$code,$customerCode,$firstName,$lastName,$middleName,$title,$suffix,$isActive,$fromCreateDate,$toCreateDate,$phone,$email,$zipCode,$socialSecurity,$driverLicenseNumber,$creditCardNumber,$bankAccountNumber,$contractCode,$paymentPlanCode,$fromBalance,$toBalance,$customerAccountGroupCode,$collectorCode,$isNullCollectorCode,$lastActionCode,$collectionsPriorityType,$fromSentCollectionsDate,$toSentCollectionsDate,$isNullSentCollectionsDate,$fromAgeCollectionsDate,$toAgeCollectionsDate,$isLite,$firstResult,$maxResults,$orderList){
		if (!is_null($orderList) && !($orderList->isEmpty())){
			$possibleValues = array(CustomerAccount::ACCOUNT_GROUP_CODE_ASC,CustomerAccount::ACCOUNT_GROUP_CODE_DESC,CustomerAccount::BALANCE_ASC,CustomerAccount::BALANCE_DESC,CustomerAccount::CODE_ASC,CustomerAccount::CODE_DESC,CustomerAccount::CREATE_DATE_ASC,CustomerAccount::CREATE_DATE_DESC,CustomerAccount::EMAIL_ASC,CustomerAccount::EMAIL_DESC,CustomerAccount::FIRST_NAME_ASC,CustomerAccount::FIRST_NAME_DESC,CustomerAccount::IS_ACTIVE_ASC,CustomerAccount::IS_ACTIVE_DESC,CustomerAccount::LAST_NAME_ASC,CustomerAccount::LAST_NAME_DESC,CustomerAccount::MERCHANT_ACCOUNT_CODE_ASC,CustomerAccount::MERCHANT_ACCOUNT_CODE_DESC,CustomerAccount::MIDDLE_NAME_ASC,CustomerAccount::MIDDLE_NAME_DESC,CustomerAccount::HOME_PHONE_ASC,CustomerAccount::HOME_PHONE_DESC,CustomerAccount::WORK_PHONE_ASC,CustomerAccount::WORK_PHONE_DESC,CustomerAccount::CELL_PHONE_ASC,CustomerAccount::CELL_PHONE_DESC,CustomerAccount::SUFFIX_ASC,CustomerAccount::SUFFIX_DESC,CustomerAccount::TITLE_ASC,CustomerAccount::TITLE_DESC,CustomerAccount::ZIP_CODE_ASC,CustomerAccount::ZIP_CODE_DESC);
			$invalidParameter = $this->validateParameterList($orderList, $possibleValues);
			if (!is_null($invalidParameter)){
				throw new Exception("Unable to process findCustomerAccount. Invalid sort field: %1$s.", $invalidParameter);
			}
		}
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("code", $code);
		$parameters->put("customerCode", $customerCode);
		$parameters->put("firstName", $firstName);
		$parameters->put("lastName", $lastName);
		$parameters->put("middleName", $middleName);
		$parameters->put("title", $title);
		$parameters->put("suffix", $suffix);
		$parameters->put("isActive", $isActive);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put("phone", $phone);
		$parameters->put("email", $email);
		$parameters->put("zipCode", $zipCode);
		$parameters->put("socialSecurity", Helper::encode($socialSecurity));
		$parameters->put("driverLicenseNumber", Helper::encode($driverLicenseNumber));
		$parameters->put("creditCardNumber", $creditCardNumber);
		$parameters->put("bankAccountNumber", $bankAccountNumber);
		$parameters->put("contractCode", $contractCode);
		$parameters->put("paymentPlanCode", $paymentPlanCode);
		$parameters->put("fromBalance", $fromBalance);
		$parameters->put("toBalance", $toBalance);
		$parameters->put("accountGroupCode", $customerAccountGroupCode);
		$parameters->put("collectorCode", $collectorCode);
		$parameters->put("isNullCollectorCode", $isNullCollectorCode);
		$parameters->put("lastActionCode", $lastActionCode);
		$parameters->put("collectionsPriorityType", $collectionsPriorityType);
		$parameters->put("fromSentCollectionsDate", $fromSentCollectionsDate);
		$parameters->put("toSentCollectionsDate", $toSentCollectionsDate);
		$parameters->put("isNullSentCollectionsDate", $isNullSentCollectionsDate);
		$parameters->put("fromAgeCollectionsDate", is_null($fromAgeCollectionsDate) ? null : Helper::getAgeDate(-($fromAgeCollectionsDate)));
		$parameters->put("toAgeCollectionsDate", is_null($toAgeCollectionsDate) ? null : Helper::getAgeDate(-($toAgeCollectionsDate)));
		$parameters->put("isLite", $isLite == true);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$parameters->put(Session::ORDER_BY, Helper::getOrderBy($orderList));
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_CUSTOMER_ACCOUNT, $parameters);
		if (is_null($responseList)){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		$customerAccounts = Helper::clientObjectListToCustomerAccountList($responseList);
		foreach ($customerAccounts as $customerAccount){
			$customerAccount->setSession($this);
		}
		return $customerAccounts;
	}
	/*
	 *$method.comment
	 */
	public function findCustomerAccountSpecial($merchantAccountCode,$creditCardExpirationPeriod,$hasInactivePaymentOptionReferences,$hasStatementPaymentOption,$fromPaymentPlanExpirationDate,$toPaymentPlanExpirationDate,$fromDeferredLength,$toDeferredLength,$fromAgeAccountTransactionDueDate,$toAgeAccountTransactionDueDate,$fromLastDeclineDate,$toLastDeclineDate,$fromSoftDeclineCount,$toSoftDeclineCount,$declineType,$fromAccountAge,$toAccountAge,$isChargebackRequested,$isBankruptcyFiled,$isLegalDispute,$isCancellationDispute,$lastActionCode,$needToBeReinstated,$isLite,$firstResult,$maxResults){
		$this->validateCustomerAccountSpecialParameters($merchantAccountCode, $creditCardExpirationPeriod, $hasInactivePaymentOptionReferences, $hasStatementPaymentOption, $fromPaymentPlanExpirationDate, $toPaymentPlanExpirationDate, $fromDeferredLength, $toDeferredLength, $fromAgeAccountTransactionDueDate, $toAgeAccountTransactionDueDate, $fromLastDeclineDate, $toLastDeclineDate, $fromSoftDeclineCount, $toSoftDeclineCount, $declineType, $fromAccountAge, $toAccountAge, $isChargebackRequested, $isBankruptcyFiled, $isLegalDispute, $isCancellationDispute, $lastActionCode, $needToBeReinstated);
		$parameters = $this->getCustomerAccountSpecialParametersAsMap($merchantAccountCode, $creditCardExpirationPeriod, $hasInactivePaymentOptionReferences, $hasStatementPaymentOption, $fromPaymentPlanExpirationDate, $toPaymentPlanExpirationDate, $fromDeferredLength, $toDeferredLength, $fromAgeAccountTransactionDueDate, $toAgeAccountTransactionDueDate, $fromLastDeclineDate, $toLastDeclineDate, $fromSoftDeclineCount, $toSoftDeclineCount, $declineType, $fromAccountAge, $toAccountAge, $isChargebackRequested, $isBankruptcyFiled, $isLegalDispute, $isCancellationDispute, $lastActionCode, $needToBeReinstated, $isLite, $firstResult, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_CUSTOMER_ACCOUNT_SPECIAL, $parameters);
		if (is_null($responseList)){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		return Helper::clientObjectListToCustomerAccountList($responseList);
	}
	/*
	 *$method.comment
	 */
	public function exportCustomerAccountSpecialBasic($merchantAccountCode,$creditCardExpirationPeriod,$hasInactivePaymentOptionReferences,$hasStatementPaymentOption,$fromPaymentPlanExpirationDate,$toPaymentPlanExpirationDate,$fromDeferredLength,$toDeferredLength,$fromAgeAccountTransactionDueDate,$toAgeAccountTransactionDueDate,$fromLastDeclineDate,$toLastDeclineDate,$fromSoftDeclineCount,$toSoftDeclineCount,$declineType,$fromAccountAge,$toAccountAge,$isChargebackRequested,$isBankruptcyFiled,$isLegalDispute,$isCancellationDispute,$lastActionCode,$needToBeReinstated,$reportExportType){
		$this->validateCustomerAccountSpecialParameters($merchantAccountCode, $creditCardExpirationPeriod, $hasInactivePaymentOptionReferences, $hasStatementPaymentOption, $fromPaymentPlanExpirationDate, $toPaymentPlanExpirationDate, $fromDeferredLength, $toDeferredLength, $fromAgeAccountTransactionDueDate, $toAgeAccountTransactionDueDate, $fromLastDeclineDate, $toLastDeclineDate, $fromSoftDeclineCount, $toSoftDeclineCount, $declineType, $fromAccountAge, $toAccountAge, $isChargebackRequested, $isBankruptcyFiled, $isLegalDispute, $isCancellationDispute, $lastActionCode, $needToBeReinstated);
		$parameters = $this->getCustomerAccountSpecialParametersAsMap($merchantAccountCode, $creditCardExpirationPeriod, $hasInactivePaymentOptionReferences, $hasStatementPaymentOption, $fromPaymentPlanExpirationDate, $toPaymentPlanExpirationDate, $fromDeferredLength, $toDeferredLength, $fromAgeAccountTransactionDueDate, $toAgeAccountTransactionDueDate, $fromLastDeclineDate, $toLastDeclineDate, $fromSoftDeclineCount, $toSoftDeclineCount, $declineType, $fromAccountAge, $toAccountAge, $isChargebackRequested, $isBankruptcyFiled, $isLegalDispute, $isCancellationDispute, $lastActionCode, $needToBeReinstated, null, null, null);
		return $sessionConnection->exportCustomerAccountSpecial($parameters, $reportExportType);
	}
	/*
	 *$method.comment
	 */
	public function exportCustomerAccountSpecial($parameters,$reportExportType){
		return $this->exportCustomerAccountSpecialBasic($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("creditCardExpirationPeriod") ? $parameters->get("creditCardExpirationPeriod") : null, $parameters->containsKey("hasInactivePaymentOptionReferences") ? $parameters->get("hasInactivePaymentOptionReferences") : null, $parameters->containsKey("hasStatementPaymentOption") ? $parameters->get("hasStatementPaymentOption") : null, $parameters->containsKey("fromPaymentPlanExpirationDate") ? $parameters->get("fromPaymentPlanExpirationDate") : null, $parameters->containsKey("toPaymentPlanExpirationDate") ? $parameters->get("toPaymentPlanExpirationDate") : null, $parameters->containsKey("fromDeferredLength") ? $parameters->get("fromDeferredLength") : null, $parameters->containsKey("toDeferredLength") ? $parameters->get("toDeferredLength") : null, $parameters->containsKey("fromAgeAccountTransactionDueDate") ? $parameters->get("fromAgeAccountTransactionDueDate") : null, $parameters->containsKey("toAgeAccountTransactionDueDate") ? $parameters->get("toAgeAccountTransactionDueDate") : null, $parameters->containsKey("fromLastDeclineDate") ? $parameters->get("fromLastDeclineDate") : null, $parameters->containsKey("toLastDeclineDate") ? $parameters->get("toLastDeclineDate") : null, $parameters->containsKey("fromSoftDeclineCount") ? $parameters->get("fromSoftDeclineCount") : null, $parameters->containsKey("toSoftDeclineCount") ? $parameters->get("toSoftDeclineCount") : null, $parameters->containsKey("declineType") ? $parameters->get("declineType") : null, $parameters->containsKey("fromAccountAge") ? $parameters->get("fromAccountAge") : null, $parameters->containsKey("toAccountAge") ? $parameters->get("toAccountAge") : null, $parameters->containsKey("isChargebackRequested") ? $parameters->get("isChargebackRequested") : null, $parameters->containsKey("isBankruptcyFiled") ? $parameters->get("isBankruptcyFiled") : null, $parameters->containsKey("isLegalDispute") ? $parameters->get("isLegalDispute") : null, $parameters->containsKey("isCancellationDispute") ? $parameters->get("isCancellationDispute") : null, $parameters->containsKey("lastActionCode") ? $parameters->get("lastActionCode") : null, $parameters->containsKey("needToBeReinstated") ? $parameters->get("needToBeReinstated") : null, $reportExportType);
	}
	/*
	 *$method.comment
	 */
	public function countCustomerAccountSpecialBasic($merchantAccountCode,$creditCardExpirationPeriod,$hasInactivePaymentOptionReferences,$hasStatementPaymentOption,$fromPaymentPlanExpirationDate,$toPaymentPlanExpirationDate,$fromDeferredLength,$toDeferredLength,$fromAgeAccountTransactionDueDate,$toAgeAccountTransactionDueDate,$fromLastDeclineDate,$toLastDeclineDate,$fromSoftDeclineCount,$toSoftDeclineCount,$declineType,$fromAccountAge,$toAccountAge,$isChargebackRequested,$isBankruptcyFiled,$isLegalDispute,$isCancellationDispute,$lastActionCode,$needToBeReinstated){
		$this->validateCustomerAccountSpecialParameters($merchantAccountCode, $creditCardExpirationPeriod, $hasInactivePaymentOptionReferences, $hasStatementPaymentOption, $fromPaymentPlanExpirationDate, $toPaymentPlanExpirationDate, $fromDeferredLength, $toDeferredLength, $fromAgeAccountTransactionDueDate, $toAgeAccountTransactionDueDate, $fromLastDeclineDate, $toLastDeclineDate, $fromSoftDeclineCount, $toSoftDeclineCount, $declineType, $fromAccountAge, $toAccountAge, $isChargebackRequested, $isBankruptcyFiled, $isLegalDispute, $isCancellationDispute, $lastActionCode, $needToBeReinstated);
		$parameters = $this->getCustomerAccountSpecialParametersAsMap($merchantAccountCode, $creditCardExpirationPeriod, $hasInactivePaymentOptionReferences, $hasStatementPaymentOption, $fromPaymentPlanExpirationDate, $toPaymentPlanExpirationDate, $fromDeferredLength, $toDeferredLength, $fromAgeAccountTransactionDueDate, $toAgeAccountTransactionDueDate, $fromLastDeclineDate, $toLastDeclineDate, $fromSoftDeclineCount, $toSoftDeclineCount, $declineType, $fromAccountAge, $toAccountAge, $isChargebackRequested, $isBankruptcyFiled, $isLegalDispute, $isCancellationDispute, $lastActionCode, $needToBeReinstated, null, null, null);
		return $this->sessionConnection->findSpecial(SessionConnection::COUNT_CUSTOMER_ACCOUNT_SPECIAL, $parameters)->get(0);
	}
	/*
	 *$method.comment
	 */
	public function queryCustomerAccountSpecial($parameters){
		return $this->findCustomerAccountSpecial($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("creditCardExpirationPeriod") ? $parameters->get("creditCardExpirationPeriod") : null, $parameters->containsKey("hasInactivePaymentOptionReferences") ? $parameters->get("hasInactivePaymentOptionReferences") : null, $parameters->containsKey("hasStatementPaymentOption") ? $parameters->get("hasStatementPaymentOption") : null, $parameters->containsKey("fromPaymentPlanExpirationDate") ? $parameters->get("fromPaymentPlanExpirationDate") : null, $parameters->containsKey("toPaymentPlanExpirationDate") ? $parameters->get("toPaymentPlanExpirationDate") : null, $parameters->containsKey("fromDeferredLength") ? $parameters->get("fromDeferredLength") : null, $parameters->containsKey("toDeferredLength") ? $parameters->get("toDeferredLength") : null, $parameters->containsKey("fromAgeAccountTransactionDueDate") ? $parameters->get("fromAgeAccountTransactionDueDate") : null, $parameters->containsKey("toAgeAccountTransactionDueDate") ? $parameters->get("toAgeAccountTransactionDueDate") : null, $parameters->containsKey("fromLastDeclineDate") ? $parameters->get("fromLastDeclineDate") : null, $parameters->containsKey("toLastDeclineDate") ? $parameters->get("toLastDeclineDate") : null, $parameters->containsKey("fromSoftDeclineCount") ? $parameters->get("fromSoftDeclineCount") : null, $parameters->containsKey("toSoftDeclineCount") ? $parameters->get("toSoftDeclineCount") : null, $parameters->containsKey("declineType") ? $parameters->get("declineType") : null, $parameters->containsKey("fromAccountAge") ? $parameters->get("fromAccountAge") : null, $parameters->containsKey("toAccountAge") ? $parameters->get("toAccountAge") : null, $parameters->containsKey("isChargebackRequested") ? $parameters->get("isChargebackRequested") : null, $parameters->containsKey("isBankruptcyFiled") ? $parameters->get("isBankruptcyFiled") : null, $parameters->containsKey("isLegalDispute") ? $parameters->get("isLegalDispute") : null, $parameters->containsKey("isCancellationDispute") ? $parameters->get("isCancellationDispute") : null, $parameters->containsKey("lastActionCode") ? $parameters->get("lastActionCode") : null, $parameters->containsKey("needToBeReinstated") ? $parameters->get("needToBeReinstated") : null, $parameters->containsKey("isLite") ? $parameters->get("isLite") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null);
	}
	/*
	 *$method.comment
	 */
	public function countCustomerAccountSpecial($parameters){
		return $this->countCustomerAccountSpecialBasic($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("creditCardExpirationPeriod") ? $parameters->get("creditCardExpirationPeriod") : null, $parameters->containsKey("hasInactivePaymentOptionReferences") ? $parameters->get("hasInactivePaymentOptionReferences") : null, $parameters->containsKey("hasStatementPaymentOption") ? $parameters->get("hasStatementPaymentOption") : null, $parameters->containsKey("fromPaymentPlanExpirationDate") ? $parameters->get("fromPaymentPlanExpirationDate") : null, $parameters->containsKey("toPaymentPlanExpirationDate") ? $parameters->get("toPaymentPlanExpirationDate") : null, $parameters->containsKey("fromDeferredLength") ? $parameters->get("fromDeferredLength") : null, $parameters->containsKey("toDeferredLength") ? $parameters->get("toDeferredLength") : null, $parameters->containsKey("fromAgeAccountTransactionDueDate") ? $parameters->get("fromAgeAccountTransactionDueDate") : null, $parameters->containsKey("toAgeAccountTransactionDueDate") ? $parameters->get("toAgeAccountTransactionDueDate") : null, $parameters->containsKey("fromLastDeclineDate") ? $parameters->get("fromLastDeclineDate") : null, $parameters->containsKey("toLastDeclineDate") ? $parameters->get("toLastDeclineDate") : null, $parameters->containsKey("fromSoftDeclineCount") ? $parameters->get("fromSoftDeclineCount") : null, $parameters->containsKey("toSoftDeclineCount") ? $parameters->get("toSoftDeclineCount") : null, $parameters->containsKey("declineType") ? $parameters->get("declineType") : null, $parameters->containsKey("fromAccountAge") ? $parameters->get("fromAccountAge") : null, $parameters->containsKey("toAccountAge") ? $parameters->get("toAccountAge") : null, $parameters->containsKey("isChargebackRequested") ? $parameters->get("isChargebackRequested") : null, $parameters->containsKey("isBankruptcyFiled") ? $parameters->get("isBankruptcyFiled") : null, $parameters->containsKey("isLegalDispute") ? $parameters->get("isLegalDispute") : null, $parameters->containsKey("isCancellationDispute") ? $parameters->get("isCancellationDispute") : null, $parameters->containsKey("lastActionCode") ? $parameters->get("lastActionCode") : null, $parameters->containsKey("needToBeReinstated") ? $parameters->get("needToBeReinstated") : null);
	}
	/*
	 *$method.comment
	 */
	public function findContract($merchantAccountCode,$collectorAccountCode,$code,$name,$type,$status,$creatorCode,$sellerCode,$importCode,$fromCreateDate,$toCreateDate,$fromFreezeBeginDate,$toFreezeBeginDate,$fromFreezeEndDate,$toFreezeEndDate,$isVisibleExternallyOnly,$customerAccountCode,$defaultPaymentPlanCode,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("collectorAccountCode", $collectorAccountCode);
		$parameters->put("code", $code);
		$parameters->put("name", $name);
		$parameters->put("type", $type);
		$parameters->put("status", $status);
		$parameters->put("creatorCode", $creatorCode);
		$parameters->put("sellerCode", $sellerCode);
		$parameters->put("importCode", $importCode);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put("fromFreezeBeginDate", $fromFreezeBeginDate);
		$parameters->put("toFreezeBeginDate", $toFreezeBeginDate);
		$parameters->put("fromFreezeEndDate", $fromFreezeEndDate);
		$parameters->put("isVisibleExternallyOnly", $isVisibleExternallyOnly);
		$parameters->put("toFreezeEndDate", $toFreezeEndDate);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("defaultPaymentPlanCode", $defaultPaymentPlanCode);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_CONTRACT, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		return Helper::clientObjectListToContractList($responseList);
	}
	/*
	 *$method.comment
	 */
	public function findPaymentOption($merchantAccountCode,$code,$isActive,$fromCreateDate,$toCreateDate,$holderName,$numberLast4,$accessory,$tokenCode,$street1,$street2,$city,$state,$zipCode,$paymentOptionType,$customerAccountCode,$firstResult,$maxResults){
		return $this->findPaymentOptionExtended($merchantAccountCode, $code, $isActive, $fromCreateDate, $toCreateDate, $holderName, $numberLast4, $accessory, $tokenCode, $street1, $street2, $city, $state, $zipCode, $paymentOptionType, $customerAccountCode, null, $firstResult, $maxResults, null);
	}
	/*
	 *$method.comment
	 */
	public function findPaymentOptionExtended($merchantAccountCode,$code,$isActive,$fromCreateDate,$toCreateDate,$holderName,$numberLast4,$accessory,$tokenCode,$street1,$street2,$city,$state,$zipCode,$paymentOptionType,$customerAccountCode,$isVisibleExternallyOnly,$firstResult,$maxResults,$orderList){
		if (!is_null($orderList) && !($orderList->isEmpty())){
			$possibleValues = array(PaymentOption::CITY_ASC,PaymentOption::CITY_DESC,PaymentOption::CODE_ASC,PaymentOption::CODE_DESC,PaymentOption::CREATE_DATE_ASC,PaymentOption::CREATE_DATE_DESC,PaymentOption::CUSTOMER_ACCOUNT_CODE_ASC,PaymentOption::CUSTOMER_ACCOUNT_CODE_DESC,PaymentOption::HOLDER_NAME_ASC,PaymentOption::HOLDER_NAME_DESC,PaymentOption::IS_ACTIVE_ASC,PaymentOption::IS_ACTIVE_DESC,PaymentOption::MERCHANT_ACCOUNT_CODE_ASC,PaymentOption::MERCHANT_ACCOUNT_CODE_DESC,PaymentOption::STATE_ASC,PaymentOption::STATE_DESC,PaymentOption::STREET1_ASC,PaymentOption::STREET1_DESC,PaymentOption::STREET2_ASC,PaymentOption::STREET2_DESC,PaymentOption::TYPE_ASC,PaymentOption::TYPE_DESC,PaymentOption::ZIP_CODE_ASC,PaymentOption::ZIP_CODE_DESC);
			$invalidParameter = $this->validateParameterList($orderList, $possibleValues);
			if (!is_null($invalidParameter)){
				throw new Exception("Unable to process findPaymentOption. Invalid sort field: %1$s.", $invalidParameter);
			}
		}
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("code", $code);
		$parameters->put("isActive", $isActive);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put("holderName", $holderName);
		$parameters->put("number", $numberLast4);
		$parameters->put("accessory", $accessory);
		$parameters->put("tokenCode", $tokenCode);
		$parameters->put("street1", $street1);
		$parameters->put("street2", $street2);
		$parameters->put("city", $city);
		$parameters->put("state", $state);
		$parameters->put("zipCode", $zipCode);
		$parameters->put("paymentOptionCl", $paymentOptionType);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("isVisibleExternallyOnly", $isVisibleExternallyOnly);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$parameters->put(Session::ORDER_BY, Helper::getOrderBy($orderList));
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_PAYMENT_OPTION, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		return Helper::clientObjectListToPaymentOptionList($responseList);
	}
	/*
	 *$method.comment
	 */
	public function queryPaymentOption($parameters){
		return $this->findPaymentOptionExtended($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("code") ? $parameters->get("code") : null, $parameters->containsKey("isActive") ? $parameters->get("isActive") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey("holderName") ? $parameters->get("holderName") : null, $parameters->containsKey("number") ? $parameters->get("number") : null, $parameters->containsKey("accessory") ? $parameters->get("accessory") : null, $parameters->containsKey("tokenCode") ? $parameters->get("tokenCode") : null, $parameters->containsKey("street1") ? $parameters->get("street1") : null, $parameters->containsKey("street2") ? $parameters->get("street2") : null, $parameters->containsKey("city") ? $parameters->get("city") : null, $parameters->containsKey("state") ? $parameters->get("state") : null, $parameters->containsKey("zipCode") ? $parameters->get("zipCode") : null, $parameters->containsKey("paymentOptionType") ? $parameters->get("paymentOptionType") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("isVisibleExternallyOnly") ? $parameters->get("isVisibleExternallyOnly") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null, $parameters->containsKey(Session::ORDER_LIST) ? $parameters->get(Session::ORDER_LIST) : null);
	}
	/*
	 *$method.comment
	 */
	public function findPaymentPlan($merchantAccountCode,$code,$fromCreateDate,$toCreateDate,$fromNextBillingDate,$toNextBillingDate,$fromFirstBillingDate,$toFirstBillingDate,$status,$fromAmount,$toAmount,$itemCode,$sellerCode,$billingCycleCode,$groupCode1,$groupCode2,$groupCode3,$groupCode4,$groupCode5,$groupCode6,$groupCode7,$groupCode8,$customerAccountCode,$contractCode,$paymentOptionCode,$paymentOptionNumber,$firstResult,$maxResults){
		return $this->findPaymentPlanExtended($merchantAccountCode, $code, $fromCreateDate, $toCreateDate, $fromNextBillingDate, $toNextBillingDate, $fromFirstBillingDate, $toFirstBillingDate, $status, $fromAmount, $toAmount, $itemCode, $sellerCode, $billingCycleCode, $groupCode1, $groupCode2, $groupCode3, $groupCode4, $groupCode5, $groupCode6, $groupCode7, $groupCode8, $customerAccountCode, $contractCode, $paymentOptionCode, $paymentOptionNumber, null, $firstResult, $maxResults, null);
	}
	/*
	 *$method.comment
	 */
	public function findPaymentPlanExtended($merchantAccountCode,$code,$fromCreateDate,$toCreateDate,$fromNextBillingDate,$toNextBillingDate,$fromFirstBillingDate,$toFirstBillingDate,$status,$fromAmount,$toAmount,$itemCode,$sellerCode,$billingCycleCode,$groupCode1,$groupCode2,$groupCode3,$groupCode4,$groupCode5,$groupCode6,$groupCode7,$groupCode8,$customerAccountCode,$contractCode,$paymentOptionCode,$paymentOptionNumber,$isVisibleExternallyOnly,$firstResult,$maxResults,$orderList){
		if (!is_null($orderList) && !($orderList->isEmpty())){
			$possibleValues = array(PaymentPlan::AMOUNT_ASC,PaymentPlan::AMOUNT_DESC,PaymentPlan::BILLING_CYCLE_CODE_ASC,PaymentPlan::BILLING_CYCLE_CODE_DESC,PaymentPlan::CODE_ASC,PaymentPlan::CODE_DESC,PaymentPlan::CREATE_DATE_ASC,PaymentPlan::CREATE_DATE_DESC,PaymentPlan::CUSTOMER_ACCOUNT_CODE_ASC,PaymentPlan::CUSTOMER_ACCOUNT_CODE_ASC,PaymentPlan::FIRST_BILLING_DATE_ASC,PaymentPlan::FIRST_BILLING_DATE_DESC,PaymentPlan::GROUP_CODE1_ASC,PaymentPlan::GROUP_CODE1_DESC,PaymentPlan::GROUP_CODE2_ASC,PaymentPlan::GROUP_CODE2_DESC,PaymentPlan::GROUP_CODE3_ASC,PaymentPlan::GROUP_CODE3_DESC,PaymentPlan::GROUP_CODE4_ASC,PaymentPlan::GROUP_CODE4_DESC,PaymentPlan::GROUP_CODE5_ASC,PaymentPlan::GROUP_CODE5_DESC,PaymentPlan::GROUP_CODE6_ASC,PaymentPlan::GROUP_CODE6_DESC,PaymentPlan::GROUP_CODE7_ASC,PaymentPlan::GROUP_CODE7_DESC,PaymentPlan::GROUP_CODE8_ASC,PaymentPlan::GROUP_CODE8_DESC,PaymentPlan::ITEM_CODE_ASC,PaymentPlan::ITEM_CODE_DESC,PaymentPlan::MERCHANT_ACCOUNT_CODE_ASC,PaymentPlan::MERCHANT_ACCOUNT_CODE_DESC,PaymentPlan::NEXT_BILLING_DATE_ASC,PaymentPlan::NEXT_BILLING_DATE_DESC,PaymentPlan::PAYMENT_OPTION_CODE_ASC,PaymentPlan::PAYMENT_OPTION_CODE_DESC,PaymentPlan::SELLER_CODE_ASC,PaymentPlan::SELLER_CODE_DESC,PaymentPlan::STATUS_ASC,PaymentPlan::STATUS_DESC);
			$invalidParameter = $this->validateParameterList($orderList, $possibleValues);
			if (!is_null($invalidParameter)){
				throw new Exception("Unable to process findPaymentPlan. Invalid sort field: %1$s.", $invalidParameter);
			}
		}
		$parameters = new HashMap();
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("code", $code);
		$parameters->put("fromNextBillingDate", $fromNextBillingDate);
		$parameters->put("toNextBillingDate", $toNextBillingDate);
		$parameters->put("fromFirstBillingDate", $fromFirstBillingDate);
		$parameters->put("toFirstBillingDate", $toFirstBillingDate);
		$parameters->put("status", $status);
		$parameters->put("fromAmount", $fromAmount);
		$parameters->put("toAmount", $toAmount);
		$parameters->put("itemCode", $itemCode);
		$parameters->put("refSellerCode", $sellerCode);
		$parameters->put("billingCycleCode", $billingCycleCode);
		$parameters->put("refGroupCode1", $groupCode1);
		$parameters->put("refGroupCode2", $groupCode2);
		$parameters->put("refGroupCode3", $groupCode3);
		$parameters->put("refGroupCode4", $groupCode4);
		$parameters->put("refGroupCode5", $groupCode5);
		$parameters->put("refGroupCode6", $groupCode6);
		$parameters->put("refGroupCode7", $groupCode7);
		$parameters->put("refGroupCode8", $groupCode8);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("contractCode", $contractCode);
		$parameters->put("paymentOptionCode", $paymentOptionCode);
		$parameters->put("paymentOptionNumber", $paymentOptionNumber);
		$parameters->put("isVisibleExternallyOnly", $isVisibleExternallyOnly);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$parameters->put(Session::ORDER_BY, Helper::getOrderBy($orderList));
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_PAYMENT_PLAN, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		return Helper::clientObjectListToPaymentPlanList($responseList);
	}
	/*
	 *$method.comment
	 */
	public function findUser($merchantAccountCode,$userName,$name,$roleType,$isActive,$customerAccountCode,$fromCreateDate,$toCreateDate,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("userName", $userName);
		$parameters->put("name", $name);
		$parameters->put("roleType", $roleType);
		$parameters->put("isActive", $isActive);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_USER, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		return Helper::clientObjectListToUserList($responseList);
	}
	/*
	 *$method.comment
	 */
	public function queryUser($parameters){
		return $this->findUser($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("userName") ? $parameters->get("userName") : null, $parameters->containsKey("name") ? $parameters->get("name") : null, $parameters->containsKey("roleType") ? $parameters->get("roleType") : null, $parameters->containsKey("isActive") ? $parameters->get("isActive") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null);
	}
	/*
	 *$method.comment
	 */
	public function loadUser($code){
		if ($this->isEmpty($code)){
			throw new Exception("Parameter code in method loadUser() is required.");
		}
		$userList = null;
		$parameters = new HashMap();
		$parameters->put("userName", $code);
		$userList = $this->queryUser($parameters);
		if (is_null($userList) || $userList->isEmpty()){
			return null;
		}
		return $userList->get(0);
	}
	/*
	 *$method.comment
	 */
	public function findAgentCommission($merchantAccountCode,$agentCode,$creatorCode,$commissionType,$customerAccountCode,$fromCreateDate,$toCreateDate,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("agentCode", $agentCode);
		$parameters->put("creatorCode", $creatorCode);
		$parameters->put("commissionType", $commissionType);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_AGENT_COMMISION, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		return Helper::clientObjectListToAgentCommissionList($responseList);
	}
	/*
	 *$method.comment
	 */
	public function queryAgentCommission($parameters){
		return $this->findAgentCommission($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("agentCode") ? $parameters->get("agentCode") : null, $parameters->containsKey("creatorCode") ? $parameters->get("creatorCode") : null, $parameters->containsKey("commissionType") ? $parameters->get("commissionType") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null);
	}
	/*
	 *$method.comment
	 */
	public function findSignature($merchantAccountCode,$customerAccountCode,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_SIGNATURE, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		return Helper::clientObjectListToSignatureList($responseList);
	}
	/*
	 *$method.comment
	 */
	public function querySignature($parameters){
		return $this->findSignature($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null);
	}
	/*
	 *$method.comment
	 */
	public function queryPaymentPlan($parameters){
		return $this->findPaymentPlanExtended($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("code") ? $parameters->get("code") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey("fromNextBillingDate") ? $parameters->get("fromNextBillingDate") : null, $parameters->containsKey("toNextBillingDate") ? $parameters->get("toNextBillingDate") : null, $parameters->containsKey("fromFirstBillingDate") ? $parameters->get("fromFirstBillingDate") : null, $parameters->containsKey("toFirstBillingDate") ? $parameters->get("toFirstBillingDate") : null, $parameters->containsKey("status") ? $parameters->get("status") : null, $parameters->containsKey("fromAmount") ? $parameters->get("fromAmount") : null, $parameters->containsKey("toAmount") ? $parameters->get("toAmount") : null, $parameters->containsKey("item") ? $parameters->get("item") : null, $parameters->containsKey("sellerCode") ? $parameters->get("sellerCode") : null, $parameters->containsKey("billingCycle") ? $parameters->get("billingCycle") : null, $parameters->containsKey("groupCode1") ? $parameters->get("groupCode1") : null, $parameters->containsKey("groupCode2") ? $parameters->get("groupCode2") : null, $parameters->containsKey("groupCode3") ? $parameters->get("groupCode3") : null, $parameters->containsKey("groupCode4") ? $parameters->get("groupCode4") : null, $parameters->containsKey("groupCode5") ? $parameters->get("groupCode5") : null, $parameters->containsKey("groupCode6") ? $parameters->get("groupCode6") : null, $parameters->containsKey("groupCode7") ? $parameters->get("groupCode7") : null, $parameters->containsKey("groupCode8") ? $parameters->get("groupCode8") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("contractCode") ? $parameters->get("contractCode") : null, $parameters->containsKey("paymentOptionCode") ? $parameters->get("paymentOptionCode") : null, $parameters->containsKey("paymentOptionNumber") ? $parameters->get("paymentOptionNumber") : null, $parameters->containsKey("isVisibleExternallyOnly") ? $parameters->get("isVisibleExternallyOnly") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null, $parameters->containsKey(Session::ORDER_LIST) ? $parameters->get(Session::ORDER_LIST) : null);
	}
	/*
	 *$method.comment
	 */
	public function findAccountTransaction($merchantAccountCode,$code,$accountActivityType,$fromCreateDate,$toCreateDate,$customerAccountCode,$customerAccountName,$isActiveCustomerAccount,$itemCode,$accountNumber,$fromAmount,$toAmount,$fromBalance,$toBalance,$firstResult,$maxResults){
		return $this->findAccountTransactionExtended($merchantAccountCode, $code, $accountActivityType, $fromCreateDate, $toCreateDate, $customerAccountCode, $customerAccountName, $isActiveCustomerAccount, $itemCode, $accountNumber, $fromAmount, $toAmount, $fromBalance, $toBalance, null, $firstResult, $maxResults, null);
	}
	/*
	 *$method.comment
	 */
	public function findAccountTransactionExtended($merchantAccountCode,$code,$accountActivityType,$fromCreateDate,$toCreateDate,$customerAccountCode,$customerAccountName,$isActiveCustomerAccount,$itemCode,$accountNumber,$fromAmount,$toAmount,$fromBalance,$toBalance,$isVisibleExternallyOnly,$firstResult,$maxResults,$orderList){
		if (!is_null($orderList) && !($orderList->isEmpty())){
			$possibleValues = array(AccountTransaction::ACCOUNT_ACTIVITY_TYPE_ASC,AccountTransaction::ACCOUNT_ACTIVITY_TYPE_DESC,AccountTransaction::AMOUNT_ASC,AccountTransaction::AMOUNT_DESC,AccountTransaction::BALANCE_ASC,AccountTransaction::BALANCE_DESC,AccountTransaction::CODE_ASC,AccountTransaction::CODE_DESC,AccountTransaction::CREATE_DATE_ASC,AccountTransaction::CREATE_DATE_DESC,AccountTransaction::CUSTOMER_ACCOUNT_CODE_ASC,AccountTransaction::CUSTOMER_ACCOUNT_CODE_DESC,AccountTransaction::MERCHANT_ACCOUNT_CODE_ASC,AccountTransaction::MERCHANT_ACCOUNT_CODE_DESC);
			$invalidParameter = $this->validateParameterList($orderList, $possibleValues);
			if (!is_null($invalidParameter)){
				throw new Exception("Unable to process findAccountTransaction. Invalid sort field: %1$s.", $invalidParameter);
			}
		}
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("code", $code);
		$parameters->put("accountActivityCl", $accountActivityType);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("customerAccountName", $customerAccountName);
		$parameters->put("isActiveCustomerAccount", $isActiveCustomerAccount);
		$parameters->put("itemCode", $itemCode);
		$parameters->put("accountNumber", $accountNumber);
		$parameters->put("fromAmount", $fromAmount);
		$parameters->put("toAmount", $toAmount);
		$parameters->put("fromBalance", $fromBalance);
		$parameters->put("toBalance", $toBalance);
		$parameters->put("isVisibleExternallyOnly", $isVisibleExternallyOnly);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$parameters->put(Session::ORDER_BY, Helper::getOrderBy($orderList));
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_ACCOUNT_TRANSACTION, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		return Helper::clientObjectListToAccountTransactionList($responseList);
	}
	/*
	 *$method.comment
	 */
	public function queryAccountTransaction($parameters){
		return $this->findAccountTransactionExtended($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("code") ? $parameters->get("code") : null, $parameters->containsKey("accountActivityType") ? $parameters->get("accountActivityType") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("customerAccountName") ? $parameters->get("customerAccountName") : null, $parameters->containsKey("isActiveCustomerAccount") ? $parameters->get("isActiveCustomerAccount") : null, $parameters->containsKey("itemCode") ? $parameters->get("itemCode") : null, $parameters->containsKey("accountNumber") ? $parameters->get("accountNumber") : null, $parameters->containsKey("fromAmount") ? $parameters->get("fromAmount") : null, $parameters->containsKey("toAmount") ? $parameters->get("toAmount") : null, $parameters->containsKey("fromBalance") ? $parameters->get("fromBalance") : null, $parameters->containsKey("toBalance") ? $parameters->get("toBalance") : null, $parameters->containsKey("isVisibleExternallyOnly") ? $parameters->get("isVisibleExternallyOnly") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null, $parameters->containsKey(Session::ORDER_LIST) ? $parameters->get(Session::ORDER_LIST) : null);
	}
	/*
	 *$method.comment
	 */
	public function findNote($merchantAccountCode,$code,$noteCode,$creatorCode,$customerAccountCode,$fromCreateDate,$toCreateDate,$description,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("code", $code);
		$parameters->put("noteCode", $noteCode);
		$parameters->put("creatorCode", $creatorCode);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put("description", $description);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_NOTE, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		$notes = Helper::clientObjectListToNoteList($responseList);
		foreach ($notes as $note){
			$note->setSession($this);
		}
		return $notes;
	}
	/*
	 *$method.comment
	 */
	public function queryNote($parameters){
		return $this->findNote($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("code") ? $parameters->get("code") : null, $parameters->containsKey("noteCode") ? $parameters->get("noteCode") : null, $parameters->containsKey("creatorCode") ? $parameters->get("creatorCode") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey("description") ? $parameters->get("description") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null);
	}
	/*
	 *$method.comment
	 */
	public function findAddress($merchantAccountCode,$customerAccountCode,$fromCreateDate,$toCreateDate,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_ADDRESS, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		$addresses = Helper::clientObjectListToAddressList($responseList);
		foreach ($addresses as $address){
			$address->setSession($this);
		}
		return $addresses;
	}
	/*
	 *$method.comment
	 */
	public function queryAddress($parameters){
		return $this->findAddress($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null);
	}
	/*
	 *$method.comment
	 */
	public function findPhone($merchantAccountCode,$customerAccountCode,$fromCreateDate,$toCreateDate,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_PHONE, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		$phones = Helper::clientObjectListToPhoneList($responseList);
		foreach ($phones as $phone){
			$phone->setSession($this);
		}
		return $phones;
	}
	/*
	 *$method.comment
	 */
	public function queryPhone($parameters){
		return $this->findPhone($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null);
	}
	/*
	 *$method.comment
	 */
	public function findEmailAddress($merchantAccountCode,$customerAccountCode,$fromCreateDate,$toCreateDate,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_EMAIL_ADDRESS, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		$emailAddresses = Helper::clientObjectListToEmailAddressList($responseList);
		foreach ($emailAddresses as $emailAddress){
			$emailAddress->setSession($this);
		}
		return $emailAddresses;
	}
	/*
	 *$method.comment
	 */
	public function queryEmailAddress($parameters){
		return $this->findEmailAddress($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null);
	}
	/*
	 *$method.comment
	 */
	public function findAction($merchantAccountCode,$code,$actionCode,$creatorCode,$customerAccountCode,$fromCreateDate,$toCreateDate,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("code", $code);
		$parameters->put("actionCode", $actionCode);
		$parameters->put("creatorCode", $creatorCode);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_ACTION, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		$actions = Helper::clientObjectListToActionList($responseList);
		foreach ($actions as $action){
			$action->setSession($this);
		}
		return $actions;
	}
	/*
	 *$method.comment
	 */
	public function queryAction($parameters){
		return $this->findAction($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("code") ? $parameters->get("code") : null, $parameters->containsKey("actionCode") ? $parameters->get("actionCode") : null, $parameters->containsKey("creatorCode") ? $parameters->get("creatorCode") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null);
	}
	/*
	 *$method.comment
	 */
	public function findLetter($merchantAccountCode,$code,$letterCode,$letterStatusType,$creatorCode,$customerAccountCode,$fromCreateDate,$toCreateDate,$isVisibleExternallyOnly,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("code", $code);
		$parameters->put("letterCode", $letterCode);
		$parameters->put("letterStatusType", $letterStatusType);
		$parameters->put("creatorCode", $creatorCode);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put("isVisibleExternallyOnly", $isVisibleExternallyOnly);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_LETTER, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		return Helper::clientObjectListToLetterList($responseList);
	}
	/*
	 *$method.comment
	 */
	public function findEmail($merchantAccountCode,$code,$emailCode,$status,$creatorCode,$customerAccountCode,$fromCreateDate,$toCreateDate,$isVisibleExternallyOnly,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("code", $code);
		$parameters->put("emailCode", $emailCode);
		$parameters->put("emailStatusType", $status);
		$parameters->put("creatorCode", $creatorCode);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put("isVisibleExternallyOnly", $isVisibleExternallyOnly);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_EMAIL, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		return Helper::clientObjectListToEmailList($responseList);
	}
	/*
	 *$method.comment
	 */
	public function findScheduledPayment($merchantAccountCode,$code,$status,$creatorCode,$posterCode,$customerAccountCode,$fromCreateDate,$toCreateDate,$fromDueDate,$toDueDate,$isVisibleExternallyOnly,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("code", $code);
		$parameters->put("status", $status);
		$parameters->put("creatorCode", $creatorCode);
		$parameters->put("posterCode", $posterCode);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put("fromDueDate", $fromDueDate);
		$parameters->put("toDueDate", $toDueDate);
		$parameters->put("isVisibleExternallyOnly", $isVisibleExternallyOnly);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_SCHEDULED_PAYMENT, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		return Helper::clientObjectListToScheduledPaymentList($responseList);
	}
	/*
	 *$method.comment
	 */
	public function findDocument($code,$documentCode,$creatorCode,$customerAccountCode,$fromCreateDate,$toCreateDate,$isVisibleExternallyOnly,$merchantAccountCode,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("code", $code);
		$parameters->put("documentCode", $documentCode);
		$parameters->put("creatorCode", $creatorCode);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put("isVisibleExternallyOnly", $isVisibleExternallyOnly);
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_DOCUMENT, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		return Helper::clientObjectListToDocumentList($responseList);
	}
	/*
	 *$method.comment
	 */
	public function queryLetter($parameters){
		return $this->findLetter($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("code") ? $parameters->get("code") : null, $parameters->containsKey("letterCode") ? $parameters->get("letterCode") : null, $parameters->containsKey("letterStatusType") ? $parameters->get("letterStatusType") : null, $parameters->containsKey("creatorCode") ? $parameters->get("creatorCode") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey("isVisibleExternallyOnly") ? $parameters->get("isVisibleExternallyOnly") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null);
	}
	/*
	 *$method.comment
	 */
	public function queryEmail($parameters){
		return $this->findEmail($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("code") ? $parameters->get("code") : null, $parameters->containsKey("emailCode") ? $parameters->get("emailCode") : null, $parameters->containsKey("emailStatusType") ? $parameters->get("emailStatusType") : null, $parameters->containsKey("creatorCode") ? $parameters->get("creatorCode") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey("isVisibleExternallyOnly") ? $parameters->get("isVisibleExternallyOnly") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null);
	}
	/*
	 *$method.comment
	 */
	public function queryScheduledPayment($parameters){
		return $this->findScheduledPayment($parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey("code") ? $parameters->get("code") : null, $parameters->containsKey("status") ? $parameters->get("status") : null, $parameters->containsKey("creatorCode") ? $parameters->get("creatorCode") : null, $parameters->containsKey("posterCode") ? $parameters->get("posterCode") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey("fromDueDate") ? $parameters->get("fromDueDate") : null, $parameters->containsKey("toDueDate") ? $parameters->get("toDueDate") : null, $parameters->containsKey("isVisibleExternallyOnly") ? $parameters->get("isVisibleExternallyOnly") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null);
	}
	/*
	 *$method.comment
	 */
	public function queryDocument($parameters){
		return $this->findDocument($parameters->containsKey("code") ? $parameters->get("code") : null, $parameters->containsKey("documentCode") ? $parameters->get("documentCode") : null, $parameters->containsKey("creatorCode") ? $parameters->get("creatorCode") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey("isVisibleExternallyOnly") ? $parameters->get("isVisibleExternallyOnly") : null, $parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null);
	}
	/*
	 *$method.comment
	 */
	public function findAuditLog($userCode,$objectType,$objectCode,$customerAccountCode,$fromCreateDate,$toCreateDate,$merchantAccountCode,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("userCode", $userCode);
		$parameters->put("objectType", $objectType);
		$parameters->put("objectCode", $objectCode);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("fromCreateDate", $fromCreateDate);
		$parameters->put("toCreateDate", $toCreateDate);
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_AUDIT_LOG, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		return Helper::clientObjectListToAuditLogList($responseList);
	}
	/*
	 *$method.comment
	 */
	public function queryAuditLog($parameters){
		return $this->findAuditLog($parameters->containsKey("userCode") ? $parameters->get("userCode") : null, $parameters->containsKey("objectType") ? $parameters->get("objectType") : null, $parameters->containsKey("objectCode") ? $parameters->get("objectCode") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("fromCreateDate") ? $parameters->get("fromCreateDate") : null, $parameters->containsKey("toCreateDate") ? $parameters->get("toCreateDate") : null, $parameters->containsKey("merchantAccountCode") ? $parameters->get("merchantAccountCode") : null, $parameters->containsKey(Session::FIRST_RESULT) ? $parameters->get(Session::FIRST_RESULT) : null, $parameters->containsKey(Session::MAX_RESULTS) ? $parameters->get(Session::MAX_RESULTS) : null);
	}
	/*
	 *$method.comment
	 */
	public function findAdjustment($code,$customerAccountCode,$contractCode,$paymentPlanCode,$adjustmentReasonCode,$type,$isVisibleExternallyOnly,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("code", $code);
		$parameters->put("customerAccountCode", $customerAccountCode);
		$parameters->put("contractCode", $contractCode);
		$parameters->put("paymentPlanCode", $paymentPlanCode);
		$parameters->put("adjustmentReasonCode", $adjustmentReasonCode);
		$parameters->put("type", $type);
		$parameters->put("isVisibleExternallyOnly", $isVisibleExternallyOnly);
		$parameters->put("merchantAccountCode", null);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		$responseList = $this->sessionConnection->find($this->sessionContext, SessionConnection::FIND_ADJUSTMENT, $parameters);
		if (is_null($responseList) || $responseList->isEmpty()){
			return null;
		}
		$this->sessionContext->toMaps($responseList);
		return Helper::clientObjectListToAdjustmentList($responseList);
	}
	/*
	 *$method.comment
	 */
	public function queryAdjustment($parameters){
		return $this->findAdjustment($parameters->containsKey("code") ? $parameters->get("code") : null, $parameters->containsKey("customerAccountCode") ? $parameters->get("customerAccountCode") : null, $parameters->containsKey("contractCode") ? $parameters->get("contractCode") : null, $parameters->containsKey("paymentPlanCode") ? $parameters->get("paymentPlanCode") : null, $parameters->containsKey("adjustmentReasonCode") ? $parameters->get("adjustmentReasonCode") : null, $parameters->containsKey("type") ? $parameters->get("type") : null, $parameters->containsKey("isVisibleExternallyOnly") ? $parameters->get("isVisibleExternallyOnly") : null, $parameters->containsKey("firstResult") ? $parameters->get("firstResult") : null, $parameters->containsKey("maxResults") ? $parameters->get("maxResults") : null);
	}
	/*
	 *$method.comment
	 */
	public function loadObject($clientObject){
		throw new Exception("Not supported");
	}
	/*
	 *$method.comment
	 */
	public function loadCustomerAccountContractDocument($customerAccount,$useDocumentAccesInterval){
		return $this->loadDocumentContent($customerAccount->getContractDocumentCode(), $customerAccount->getCode(), $useDocumentAccesInterval);
	}
	/*
	 *$method.comment
	 */
	public function loadPaymentPlanContractDocument($paymentPlan,$useDocumentAccesInterval){
		return $this->loadDocumentContent($paymentPlan->getContractDocumentCode(), $paymentPlan->getCustomerAccount()->getCode(), $useDocumentAccesInterval);
	}
	/*
	 *$method.comment
	 */
	public function loadLetterDocument($letter,$useDocumentAccesInterval){
		return $this->loadDocumentContent($letter->getDocumentCode(), $letter->getCustomerAccount()->getCode(), $useDocumentAccesInterval);
	}
	/*
	 *$method.comment
	 */
	public function loadDocumentContent($documentContentCode,$customerAccountCode,$useDocumentAccesInterval){
		if (is_null($documentContentCode)){
			return null;
		}
		return $this->sessionConnection->loadDocumentContent($documentContentCode, $customerAccountCode, $useDocumentAccesInterval);
	}
	/*
	 *$method.comment
	 */
	public function saveDocumentContent($customerAccount,$documentContent){
		if (is_null($customerAccount) || is_null($documentContent)){
			throw new Exception("Some parameters are not setting.");
		}
		if (is_null($customerAccount->getId())){
			throw new Exception("Unable to save Document Content: Customer Account is unpersisted.");
		}
		return $this->sessionConnection->saveDocumentContent($this->userCode, $customerAccount, $documentContent);
	}
	/*
	 *$method.comment
	 */
	public function applyAccessFee($customerAccount,$documentId,$amount,$creatorCode,$accountNumber,$accessory,$tokenCode,$transactionType,$holderName,$city,$street,$state,$zipCode,$cvv2){
		if (is_null($customerAccount)){
			throw new Exception("Unable to apply fee: customerAccount required parameter.");
		}
		if (!($customerAccount->getIsActive())){
			throw new Exception("Unable to apply fee: customerAccount is inactive.");
		}
		if ($amount < 1){
			throw new Exception("Unable to apply fee: amount should be great 0.");
		}
		$revenueTransaction = new RevenueTransaction();
		$revenueTransaction->setAmount($amount);
		$revenueTransaction->setCreatorCode($creatorCode);
		$revenueTransaction->setPosterCode($creatorCode);
		$revenueTransaction->setCustomerAccount($customerAccount);
		$revenueTransaction->setAccountActivityType(AccountActivityType::AccessFee);
		$revenueTransaction->setMerchantAccountCode($customerAccount->getMerchantAccountCode());
		$customerAccount->addRevenueTransaction($revenueTransaction);
		$assetTransaction = new AssetTransaction();
		$assetTransaction->setAmount($amount);
		$assetTransaction->setCreatorCode($creatorCode);
		$assetTransaction->setPosterCode($creatorCode);
		$assetTransaction->setCustomerAccount($customerAccount);
		$assetTransaction->setAccountActivityType(AccountActivityType::Payment);
		$assetTransaction->setAccountNumber($accountNumber);
		$assetTransaction->setAccessory($accessory);
		$assetTransaction->setTransactionType($transactionType);
		$assetTransaction->setTransactionSourceType(TransactionSourceType::Portal);
		$assetTransaction->setMerchantAccountCode($customerAccount->getMerchantAccountCode());
		$customerAccount->addAssetTransaction($assetTransaction);
		$captureInfo = $assetTransaction->createCaptureInfo();
		$captureInfo->setTokenCode($tokenCode);
		$captureInfo->setCvv2($cvv2);
		$captureInfo->setHolderName($holderName);
		$captureInfo->setCity($city);
		$captureInfo->setStreet($street);
		$captureInfo->setState($state);
		$captureInfo->setZipCode($zipCode);
		$this->save($customerAccount);
		try{
			$this->synchronize();
		}
		catch(Throwable $throwable){
			$customerAccount->removeRevenueTransaction($revenueTransaction);
			$customerAccount->removeAssetTransaction($assetTransaction);
			throw new Exception("Unable to apply fee: " . $throwable->getMessage());
		}
		try{
			$sessionConnection->updateLastPaymentDate($customerAccount->getCode(), $documentId);
		}
		catch(Throwable $throwable){
			$reverseRtransaction = $customerAccount->reverseRevenueTransaction($revenueTransaction);
			$reverseRtransaction->setCreatorCode($creatorCode);
			$reverseRtransaction->setPosterCode($creatorCode);
			$reverseATransaction = $customerAccount->reversePayment($assetTransaction);
			$reverseATransaction->setCreatorCode($creatorCode);
			$reverseATransaction->setPosterCode($creatorCode);
			$reverseATransaction->setTransactionSourceType(TransactionSourceType::Portal);
			$this->save($customerAccount);
			$this->synchronize();
			throw new Exception("Unable to apply fee: " . $throwable->getMessage());
		}
	}
	/*
	 *$method.comment
	 */
	public function exportReport($customerAccount,$reportName,$parameters){
		if (is_null($reportName) && "" == $reportName){
			throw new Exception("Parameter reportName in method exportReport() is required.");
		}
		$possibleValues = array(REPORT_ACCOUNT_ACTIVITY,REPORT_CUSTOMER_ACCOUNT_MASTER);
		if (!($this->validateParameter($reportName, $possibleValues))){
			throw new Exception("Unable to invoike method exportReport(). Invalid parameter value. Parameter: reportName. Possible values: %1$s.", Helper::printPossibleValues($possibleValues));
		}
		return $this->sessionConnection->executeReport($customerAccount, $reportName, $parameters);
	}
	/*
	 *$method.comment
	 */
	private function validateParameter($parameter,$possibleValues){
		if (is_null($parameter)){
			return false;
		}
		$isValid = false;
		foreach ($possibleValues as $value){
			if ($parameter == $value){
				$isValid = true;
				break;
			}
		}
		return $isValid;
	}
	/*
	 *$method.comment
	 */
	private function validateParameterList($list,$possibleValues){
		foreach ($list as $parameter){
			if (!($this->validateParameter($parameter, $possibleValues))){
				return $parameter;
			}
		}
		return null;
	}
	/*
	 *$method.comment
	 */
	private function getCustomerAccountSpecialParametersAsMap($merchantAccountCode,$creditCardExpirationPeriod,$hasInactivePaymentOptionReferences,$hasStatementPaymentOption,$fromPaymentPlanExpirationDate,$toPaymentPlanExpirationDate,$fromDeferredLength,$toDeferredLength,$fromAgeAccountTransactionDueDate,$toAgeAccountTransactionDueDate,$fromLastDeclineDate,$toLastDeclineDate,$fromSoftDeclineCount,$toSoftDeclineCount,$declineType,$fromAccountAge,$toAccountAge,$isChargebackRequested,$isBankruptcyFiled,$isLegalDispute,$isCancellationDispute,$lastActionCode,$needToBeReinstated,$isLite,$firstResult,$maxResults){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $merchantAccountCode);
		$parameters->put("creditCardExpirationPeriod", Helper::getExpirationDate($creditCardExpirationPeriod));
		$parameters->put("hasInactivePaymentOptionReferences", $hasInactivePaymentOptionReferences);
		$parameters->put("hasStatementPaymentOption", $hasStatementPaymentOption);
		$parameters->put("fromPaymentPlanExpirationDate", $fromPaymentPlanExpirationDate);
		$parameters->put("toPaymentPlanExpirationDate", $toPaymentPlanExpirationDate);
		$parameters->put("fromDeferredLength", $fromDeferredLength);
		$parameters->put("toDeferredLength", $toDeferredLength);
		$parameters->put("fromAgeAccountTransactionDueDate", is_null($toAgeAccountTransactionDueDate) ? null : Helper::getAgeDate(-($toAgeAccountTransactionDueDate)));
		$parameters->put("toAgeAccountTransactionDueDate", is_null($fromAgeAccountTransactionDueDate) ? null : Helper::getAgeDate(-($fromAgeAccountTransactionDueDate)));
		$parameters->put("fromLastDeclineDate", $fromLastDeclineDate);
		$parameters->put("toLastDeclineDate", $toLastDeclineDate);
		$parameters->put("fromSoftDeclineCount", $fromSoftDeclineCount);
		$parameters->put("toSoftDeclineCount", $toSoftDeclineCount);
		$parameters->put("declineType", $declineType);
		$parameters->put("fromAccountAge", is_null($toAccountAge) ? null : Helper::getAgeDate(-($toAccountAge)));
		$parameters->put("toAccountAge", is_null($fromAccountAge) ? null : Helper::getAgeDate(-($fromAccountAge)));
		$parameters->put("isChargebackRequested", $isChargebackRequested);
		$parameters->put("isBankruptcyFiled", $isBankruptcyFiled);
		$parameters->put("isLegalDispute", $isLegalDispute);
		$parameters->put("isCancellationDispute", $isCancellationDispute);
		$parameters->put("lastActionCode", $lastActionCode);
		$parameters->put("needToBeReinstated", $needToBeReinstated);
		$parameters->put("isLite", $isLite == true);
		$parameters->put(Session::FIRST_RESULT, $firstResult);
		$parameters->put(Session::MAX_RESULTS, $maxResults);
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	private function validateCustomerAccountSpecialParameters($merchantAccountCode,$creditCardExpirationPeriod,$hasInactivePaymentOptionReferences,$hasStatementPaymentOption,$fromPaymentPlanExpirationDate,$toPaymentPlanExpirationDate,$fromDeferredLength,$toDeferredLength,$fromAgeAccountTransactionDueDate,$toAgeAccountTransactionDueDate,$fromLastDeclineDate,$toLastDeclineDate,$fromSoftDeclineCount,$toSoftDeclineCount,$declineType,$fromAccountAge,$toAccountAge,$isChargebackRequested,$isBankruptcyFiled,$isLegalDispute,$isCancellationDispute,$lastActionCode,$needToBeReinstated){
		$count = 0;
		$currentDate = new DateTime(null, new DateTimeZone('EST'));
		if (!is_null($creditCardExpirationPeriod)){
			if ($creditCardExpirationPeriod < 1){
				throw new Exception("Credit card expiration period should be greater than 0");
			}
			$count++;
		}
		if ($hasInactivePaymentOptionReferences == true){
			$count++;
		}
		if ($hasStatementPaymentOption == true){
			$count++;
		}
		if (!is_null($fromPaymentPlanExpirationDate) || !is_null($toPaymentPlanExpirationDate)){
			if (!is_null($fromPaymentPlanExpirationDate) && $fromPaymentPlanExpirationDate->before($currentDate)){
				throw new Exception("Payment Plan Expiration From Date must be greater than the current date.");
			}
			if (!is_null($toPaymentPlanExpirationDate) && $toPaymentPlanExpirationDate->before($currentDate)){
				throw new Exception("Payment Plan Expiration To Date must be greater than the current date.");
			}
			if (!is_null($fromPaymentPlanExpirationDate) && !is_null($toPaymentPlanExpirationDate) && $toPaymentPlanExpirationDate->before($fromPaymentPlanExpirationDate)){
				throw new Exception("Payment Plan Expiration To Date must be greater than the Payment Plan Expiration From Date.");
			}
			$count++;
		}
		if (!is_null($fromDeferredLength) || !is_null($toDeferredLength)){
			$count++;
		}
		if (!is_null($fromAgeAccountTransactionDueDate) || !is_null($toAgeAccountTransactionDueDate)){
			$count++;
		}
		if (!is_null($fromLastDeclineDate) || !is_null($toLastDeclineDate) || !is_null($fromSoftDeclineCount) || !is_null($toSoftDeclineCount) || !is_null($declineType)){
			if (!is_null($fromLastDeclineDate) && !is_null($toLastDeclineDate) && $toLastDeclineDate->before($fromLastDeclineDate)){
				throw new Exception("To Last Decline Date must be greater than the From Last Decline Date.");
			}
			$possibleValues = array();
			if (!is_null($declineType)){
				if (!($this->validateParameter($declineType, $possibleValues))){
					throw new Exception("Invalid Decline Type parameter value. Possible values: %1$s.", Helper::printPossibleValues($possibleValues));
				}
				if (HARD == $declineType){
					$fromSoftDeclineCount = null;
					$toSoftDeclineCount = null;
				}
			}
			if (!is_null($fromSoftDeclineCount) && $fromSoftDeclineCount <= 0){
				throw new Exception("From Soft Decline Count must be greater than 0.");
			}
			if (!is_null($toSoftDeclineCount) && $toSoftDeclineCount <= 0){
				throw new Exception("To Soft Decline Count must be greater than 0.");
			}
			if (!is_null($fromSoftDeclineCount) && !is_null($toSoftDeclineCount) && $toSoftDeclineCount <= $fromSoftDeclineCount){
				if (!is_null($fromSoftDeclineCount) && $fromSoftDeclineCount <= 0){
					throw new Exception("To Soft Decline Count must be greater than From Soft Decline Count.");
				}
			}
			$count++;
		}
		if (!is_null($fromAccountAge) || !is_null($toAccountAge)){
			$count++;
		}
		if ($isChargebackRequested == true){
			$count++;
		}
		if ($isBankruptcyFiled == true){
			$count++;
		}
		if ($isLegalDispute == true){
			$count++;
		}
		if ($isCancellationDispute == true){
			$count++;
		}
		if (!is_null($lastActionCode)){
			$count++;
		}
		if ($needToBeReinstated == true){
			$count++;
		}
		if ($count == 0){
			throw new Exception("Unable to perform search, please specify a search criteria.");
		}
		else {
			if ($count > 1){
				throw new Exception("Unable to perform search by more than one search criteria.");
			}
		}
	}
	/*
	 *$method.comment
	 */
	private function isEmpty($value){
		return Helper::isEmpty($value);
	}
	/*
	 *$method.comment
	 */
	public function createCustomerAccount(){
		$customerAccount = new CustomerAccount();
		$customerAccount->setIsActive(true);
		$customerAccount->setSession($this);
		return $customerAccount;
	}
}

?>

