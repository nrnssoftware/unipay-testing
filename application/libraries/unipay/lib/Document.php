<?php


/*
 *$comment
 */
class Document extends ClientObject {
		/*
	 *@var null
	 */
	private $creatorCode;
		/*
	 *@var null
	 */
	private $description;
		/*
	 *@var null
	 */
	private $isVisibleExternally;
		/*
	 *@var null
	 */
	private $documentCode;
		/*
	 *@var null
	 */
	private $documentContentCode;
		/*
	 *@var null
	 */
	private $customerAccount;
		
	/*
	 *$constructor.comment
	 */
	function __construct(){
		//initialization block
		$this->isVisibleExternally = false;
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
	}

	/*
	 *$method.comment
	 */
	public function getCreatorCode(){
		return $this->creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function setCreatorCode($creatorCode){
		$this->creatorCode = $creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function getDescription(){
		return $this->description;
	}
	/*
	 *$method.comment
	 */
	public function setDescription($description){
		$this->description = $description;
	}
	/*
	 *$method.comment
	 */
	public function getIsVisibleExternally(){
		return $this->isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function setIsVisibleExternally($isVisibleExternally){
		$this->isVisibleExternally = $isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function getDocumentCode(){
		return $this->documentCode;
	}
	/*
	 *$method.comment
	 */
	public function setDocumentCode($documentCode){
		$this->documentCode = $documentCode;
	}
	/*
	 *$method.comment
	 */
	public function getDocumentContentCode(){
		return $this->documentContentCode;
	}
	/*
	 *$method.comment
	 */
	public function setDocumentContentCode($documentContentCode){
		$this->documentContentCode = $documentContentCode;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function setCustomerAccount($customerAccount){
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "Document";
	}
}

?>

