<?php


/*
 *$comment
 */
class Statement extends AccountActivity {
		/*
	 *@var null
	 */
	private $name;
		/*
	 *@var null
	 */
	private $totalAmount;
		/*
	 *@var null
	 */
	private $taxAmount;
		/*
	 *@var null
	 */
	private $street;
		/*
	 *@var null
	 */
	private $state;
		/*
	 *@var null
	 */
	private $phone;
		/*
	 *@var null
	 */
	private $email;
		/*
	 *@var null
	 */
	private $city;
		/*
	 *@var null
	 */
	private $zipCode;
		
	/*
	 *$constructor.comment
	 */
	function __construct($createDate = null, $name = null, $totalAmount = null, $taxAmount = null, $street = null, $state = null, $phone = null, $email = null, $city = null, $zipCode = null){
		//initialization block
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
		//constructor with parameters
		parent::__construct();
		$this->setCreateDate($createDate);
		$this->name = $name;
		$this->totalAmount = $totalAmount;
		$this->taxAmount = $taxAmount;
		$this->street = $street;
		$this->state = $state;
		$this->phone = $phone;
		$this->email = $email;
		$this->city = $city;
		$this->zipCode = $zipCode;
		$this->setRefIdSpecial($this->hashCode());
	
	}

	/*
	 *$method.comment
	 */
	public function getName(){
		return $this->name;
	}
	/*
	 *$method.comment
	 */
	public function setName($name){
		$this->name = $name;
	}
	/*
	 *$method.comment
	 */
	public function getTotalAmount(){
		return $this->totalAmount;
	}
	/*
	 *$method.comment
	 */
	public function setTotalAmount($totalAmount){
		$this->totalAmount = $totalAmount;
	}
	/*
	 *$method.comment
	 */
	public function getTaxAmount(){
		return $this->taxAmount;
	}
	/*
	 *$method.comment
	 */
	public function setTaxAmount($taxAmount){
		$this->taxAmount = $taxAmount;
	}
	/*
	 *$method.comment
	 */
	public function getStreet(){
		return $this->street;
	}
	/*
	 *$method.comment
	 */
	public function setStreet($street){
		$this->street = $street;
	}
	/*
	 *$method.comment
	 */
	public function getState(){
		return $this->state;
	}
	/*
	 *$method.comment
	 */
	public function setState($state){
		$this->state = $state;
	}
	/*
	 *$method.comment
	 */
	public function getPhone(){
		return $this->phone;
	}
	/*
	 *$method.comment
	 */
	public function setPhone($phone){
		$this->phone = $phone;
	}
	/*
	 *$method.comment
	 */
	public function getEmail(){
		return $this->email;
	}
	/*
	 *$method.comment
	 */
	public function setEmail($email){
		$this->email = $email;
	}
	/*
	 *$method.comment
	 */
	public function getCity(){
		return $this->city;
	}
	/*
	 *$method.comment
	 */
	public function setCity($city){
		$this->city = $city;
	}
	/*
	 *$method.comment
	 */
	public function getZipCode(){
		return $this->zipCode;
	}
	/*
	 *$method.comment
	 */
	public function setZipCode($zipCode){
		$this->zipCode = $zipCode;
	}
}

?>

