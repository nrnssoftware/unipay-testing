<?php



/*
 *$comment
 */
class AssetTransaction extends AccountTransaction {
		/*
	 *@var null
	 */
	private $transactionType;
		/*
	 *@var null
	 */
	private $isPrepayment;
		/*
	 *@var null
	 */
	private $accountNumber;
		/*
	 *@var null
	 */
	private $accessory;
		/*
	 *@var null
	 */
	private $maskedAccountNumber;
		/*
	 *@var null
	 */
	private $maskedAccessory;
		/*
	 *@var null
	 */
	private $captureInfo;
		/*
	 *@var null
	 */
	private $transactionSourceType;
		/*
	 *@var null
	 */
	private $checkNumber;
		
	/*
	 *$constructor.comment
	 */
	function __construct($createDate = null, $transactionType = null, $isPrepayment = null, $accountNumber = null, $accessory = null, $maskedAccountNumber = null, $maskedAccessory = null, $taxAmount = null){
		//initialization block
		$this->isPrepayment = false;
		if (func_num_args() == 0) {
		//default constructor
		parent::__construct();		return;
		}
		//constructor with parameters
		parent::__construct();
		$this->setCreateDate($createDate);
		$this->transactionType = $transactionType;
		$this->isPrepayment = $isPrepayment;
		$this->accountNumber = $accountNumber;
		$this->accessory = $accessory;
		$this->maskedAccountNumber = $maskedAccountNumber;
		$this->maskedAccessory = $maskedAccessory;
		$this->setTaxAmount($taxAmount);
	
	}

	/*
	 *$method.comment
	 */
	public function getTransactionType(){
		return $this->transactionType;
	}
	/*
	 *$method.comment
	 */
	public function setTransactionType($transactionType){
		$this->transactionType = $transactionType;
		if (!is_null($this->getCaptureInfo())){
			$this->getCaptureInfo()->setTransactionType($transactionType);
		}
		if (AssetTransactionType::Cash == $transactionType || AssetTransactionType::Check == $transactionType || AssetTransactionType::MoneyOrder == $transactionType || AssetTransactionType::External == $transactionType || AssetTransactionType::WriteOff == $transactionType || AssetTransactionType::PaymentPlan == $transactionType || AssetTransactionType::Settlement == $transactionType || AssetTransactionType::Allowance == $transactionType){
			$this->captureInfo = null;
		}
	}
	/*
	 *$method.comment
	 */
	public function getIsPrepayment(){
		return $this->isPrepayment;
	}
	/*
	 *$method.comment
	 */
	public function setIsPrepayment($isPrepayment){
		$this->isPrepayment = $isPrepayment;
	}
	/*
	 *$method.comment
	 */
	public function getAccountNumber(){
		return $this->accountNumber;
	}
	/*
	 *$method.comment
	 */
	public function setAccountNumber($accountNumber){
		$this->accountNumber = $accountNumber;
		if (!is_null($this->getCaptureInfo())){
			$this->getCaptureInfo()->setAccountNumber($accountNumber);
		}
	}
	/*
	 *$method.comment
	 */
	public function getAccessory(){
		return $this->accessory;
	}
	/*
	 *$method.comment
	 */
	public function setAccessory($accessory){
		$this->accessory = $accessory;
		if (!is_null($this->getCaptureInfo())){
			$this->getCaptureInfo()->setAccessory($accessory);
		}
	}
	/*
	 *$method.comment
	 */
	public function getCaptureInfo(){
		return $this->captureInfo;
	}
	/*
	 *$method.comment
	 */
	public function setCaptureInfo($captureInfo){
		$this->captureInfo = $captureInfo;
	}
	/*
	 *$method.comment
	 */
	public function setTaxAmount($taxAmount){
		parent::setTaxAmount($taxAmount);
		if (!is_null($this->captureInfo)){
			$this->captureInfo->setTaxAmount($taxAmount);
		}
	}
	/*
	 *$method.comment
	 */
	public function createCaptureInfoExtended($holderName,$street,$city,$state,$phone,$zipCode,$email,$cvv2,$trackData){
		$captureInfo = $this->createCaptureInfo();
		$captureInfo->setHolderName($holderName);
		$captureInfo->setStreet($street);
		$captureInfo->setCity($city);
		$captureInfo->setState($state);
		$captureInfo->setPhone($phone);
		$captureInfo->setZipCode($zipCode);
		$captureInfo->setEmail($email);
		$captureInfo->setCvv2($cvv2);
		$captureInfo->setTrackData($trackData);
		return $captureInfo;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		if (is_null($this->getAccountActivityType())){
			throw new Exception("Cannot resolve type for AssetTransaction.");
		}
		else {
			if (AccountActivityType::Refund == $this->getAccountActivityType()){
				return "Refund";
			}
			else {
				return "AssetTransaction";
			}
		}
	}
	/*
	 *$method.comment
	 */
	public function getMaskedAccountNumber(){
		return $this->maskedAccountNumber;
	}
	/*
	 *$method.comment
	 */
	 function setMaskedAccountNumber($maskedAccountNumber){
		$this->maskedAccountNumber = $maskedAccountNumber;
	}
	/*
	 *$method.comment
	 */
	public function getMaskedAccessory(){
		return $this->maskedAccessory;
	}
	/*
	 *$method.comment
	 */
	 function setMaskedAccessory($maskedAccessory){
		$this->maskedAccessory = $maskedAccessory;
	}
	/*
	 *$method.comment
	 */
	public function getTransactionSourceType(){
		return $this->transactionSourceType;
	}
	/*
	 *$method.comment
	 */
	public function setTransactionSourceType($transactionSourceType){
		$this->transactionSourceType = $transactionSourceType;
	}
	/*
	 *$method.comment
	 */
	public function getCheckNumber(){
		return $this->checkNumber;
	}
	/*
	 *$method.comment
	 */
	public function setCheckNumber($checkNumber){
		$this->checkNumber = $checkNumber;
	}
	/*
	 *$method.comment
	 */
	public function createCaptureInfo(){
		$captureInfo = new CaptureInfo();
		$captureInfo->setMerchantAccountCode($this->getMerchantAccountCode());
		$captureInfo->setCollectorAccountCode($this->getCollectorAccountCode());
		$captureInfo->setAccountNumber($this->accountNumber);
		$captureInfo->setAccessory($this->accessory);
		$captureInfo->setTransactionType($this->transactionType);
		$captureInfo->setTaxAmount($this->getTaxAmount());
		$this->setCaptureInfo($captureInfo);
		return $captureInfo;
	}
}

?>

