<?php



/*
 *$comment
 */
class AuditLog extends ClientObject {
		/*
	 *@var null
	 */
	private $userCode;
		/*
	 *@var null
	 */
	private $objectType;
		/*
	 *@var null
	 */
	private $objectCode;
		/*
	 *@var null
	 */
	private $auditLogDetails;
		/*
	 *@var null
	 */
	private $customerAccount;
		
	/*
	 *$constructor.comment
	 */
	function __construct(){
		//initialization block
		$this->auditLogDetails = new ArrayList();
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
	}

	/*
	 *$method.comment
	 */
	public function getUserCode(){
		return $this->userCode;
	}
	/*
	 *$method.comment
	 */
	 function setUserCode($userCode){
		$this->userCode = $userCode;
	}
	/*
	 *$method.comment
	 */
	public function getObjectType(){
		return $this->objectType;
	}
	/*
	 *$method.comment
	 */
	 function setObjectType($objectType){
		$this->objectType = $objectType;
	}
	/*
	 *$method.comment
	 */
	public function getObjectCode(){
		return $this->objectCode;
	}
	/*
	 *$method.comment
	 */
	 function setObjectCode($objectCode){
		$this->objectCode = $objectCode;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccount($customerAccount){
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getAuditLogDetails(){
		return $this->auditLogDetails;
	}
	/*
	 *$method.comment
	 */
	 function setAuditLogDetails($auditLogDetails){
		$this->auditLogDetails = $auditLogDetails;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "AuditLog";
	}
}

?>

