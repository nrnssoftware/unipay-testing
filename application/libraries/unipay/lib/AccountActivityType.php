<?php


/*
 *$comment
 */
class AccountActivityType {
		/*
	 *@var null
	 */
	const Invoice = "RI";	/*
	 *@var null
	 */
	const Credit = "RC";	/*
	 *@var null
	 */
	const Reversal = "RR";	/*
	 *@var null
	 */
	const Reinstatement = "RN";	/*
	 *@var null
	 */
	const Fee = "RF";	/*
	 *@var null
	 */
	const LateFee = "RL";	/*
	 *@var null
	 */
	const AccessFee = "RA";	/*
	 *@var null
	 */
	const CollectionsFee = "RO";	/*
	 *@var null
	 */
	const ServiceFee = "RS";	/*
	 *@var null
	 */
	const CreditCardFee = "RD";	/*
	 *@var null
	 */
	const CancellationFee = "RZ";	/*
	 *@var null
	 */
	const CreditReportingFee = "RE";	/*
	 *@var null
	 */
	const ReinstatementFee = "RM";	/*
	 *@var null
	 */
	const AchFee = "RB";	/*
	 *@var null
	 */
	const Payment = "AP";	/*
	 *@var null
	 */
	const Void = "AV";	/*
	 *@var null
	 */
	const Refund = "AR";	/*
	 *@var null
	 */
	const Claim = "AC";	/*
	 *@var null
	 */
	const Decline = "AD";	/*
	 *@var null
	 */
	const Chargeback = "AG";	/*
	 *@var null
	 */
	const Adjustment = "AA";	/*
	 *@var null
	 */
	const Statement = "S";	/*
	 *@var null
	 */
	const CancelCredit = "RX";	/*
	 *@var null
	 */
	const CancelRefund = "AX";	/*
	 *@var null
	 */
	const ChargebackReversal = "AS";
}
?>

