<?php


/*
 *$comment
 */
class SessionContext {
		/*
	 *@var null
	 */
	private $customerAccounts;
		/*
	 *@var null
	 */
	private $paymentOptions;
		/*
	 *@var null
	 */
	private $revenueTransactions;
		/*
	 *@var null
	 */
	private $assetTransactions;
		/*
	 *@var null
	 */
	private $paymentPlans;
		/*
	 *@var null
	 */
	private $addresses;
		/*
	 *@var null
	 */
	private $phones;
		/*
	 *@var null
	 */
	private $emailAddresses;
		/*
	 *@var null
	 */
	private $notes;
		/*
	 *@var null
	 */
	private $actions;
		/*
	 *@var null
	 */
	private $letters;
		/*
	 *@var null
	 */
	private $emails;
		/*
	 *@var null
	 */
	private $scheduledPayments;
		/*
	 *@var null
	 */
	private $documents;
		/*
	 *@var null
	 */
	private $auditLogs;
		/*
	 *@var null
	 */
	private $signatures;
		/*
	 *@var null
	 */
	private $contracts;
		/*
	 *@var null
	 */
	private $users;
		/*
	 *@var null
	 */
	private $agentCommissions;
		/*
	 *@var null
	 */
	private $adjustments;
		/*
	 *@var null
	 */
	private $clientObjectsList;
		
	/*
	 *$constructor.comment
	 */
	function __construct(){
		//initialization block
		$this->customerAccounts = new HashMap();
		$this->paymentOptions = new HashMap();
		$this->revenueTransactions = new HashMap();
		$this->assetTransactions = new HashMap();
		$this->paymentPlans = new HashMap();
		$this->addresses = new HashMap();
		$this->phones = new HashMap();
		$this->emailAddresses = new HashMap();
		$this->notes = new HashMap();
		$this->actions = new HashMap();
		$this->letters = new HashMap();
		$this->emails = new HashMap();
		$this->scheduledPayments = new HashMap();
		$this->documents = new HashMap();
		$this->auditLogs = new HashMap();
		$this->signatures = new HashMap();
		$this->contracts = new HashMap();
		$this->users = new HashMap();
		$this->agentCommissions = new HashMap();
		$this->adjustments = new HashMap();
		$this->clientObjectsList = new ArrayList();
		if (func_num_args() == 0) {
		//default constructor
				return;
		}
	}

	/*
	 *$method.comment
	 */
	public function addClientObject($clientObject){
		$customerAccount = null;
		$user = null;
		if ($clientObject instanceof CustomerAccount){
			$customerAccount = $clientObject;
		}
		else {
			if ($clientObject instanceof User){
				$user = $clientObject;
			}
			else {
				if ($clientObject instanceof PaymentPlan){
					$paymentPlan = $clientObject;
					$customerAccount = $paymentPlan->getCustomerAccount();
					if (!($customerAccount->getIsLite()) && !($customerAccount->getPaymentPlans()->contains($paymentPlan))){
						$customerAccount->addPaymentPlan($paymentPlan);
					}
					if (!is_null($paymentPlan->getAdjustment())){
						if (!($customerAccount->getIsLite()) && !($customerAccount->getAdjustments()->contains($paymentPlan->getAdjustment()))){
							$customerAccount->addAdjustment($paymentPlan->getAdjustment());
						}
					}
					if (!is_null($paymentPlan->getCancellationAdjustment())){
						if (!($customerAccount->getIsLite()) && !($customerAccount->getAdjustments()->contains($paymentPlan->getCancellationAdjustment()))){
							$customerAccount->addAdjustment($paymentPlan->getCancellationAdjustment());
						}
					}
				}
				else {
					if ($clientObject instanceof PaymentOption){
						$paymentOption = $clientObject;
						$customerAccount = $paymentOption->getCustomerAccount();
						if (!($customerAccount->getIsLite()) && !($customerAccount->getPaymentOptions()->contains($paymentOption))){
							$customerAccount->addPaymentOption($paymentOption);
						}
					}
					else {
						if ($clientObject instanceof RevenueTransaction){
							$transaction = $clientObject;
							$customerAccount = $transaction->getCustomerAccount();
							if (!($customerAccount->getIsLite()) && !($customerAccount->getRevenueTransactions()->contains($transaction))){
								$customerAccount->addRevenueTransaction($transaction);
							}
						}
						else {
							if ($clientObject instanceof AssetTransaction){
								$transaction = $clientObject;
								$customerAccount = $transaction->getCustomerAccount();
								if (!($customerAccount->getIsLite()) && !($customerAccount->getAssetTransactions()->contains($transaction))){
									$customerAccount->addAssetTransaction($transaction);
								}
							}
							else {
								if ($clientObject instanceof Address){
									$address = $clientObject;
									$customerAccount = $address->getCustomerAccount();
									if (!($customerAccount->getIsLite()) && !($customerAccount->getAddresses()->contains($address))){
										$customerAccount->addAddress($address);
									}
								}
								else {
									if ($clientObject instanceof Phone){
										$phone = $clientObject;
										$customerAccount = $phone->getCustomerAccount();
										if (!($customerAccount->getIsLite()) && !($customerAccount->getPhones()->contains($phone))){
											$customerAccount->addPhone($phone);
										}
									}
									else {
										if ($clientObject instanceof EmailAddress){
											$emailAddress = $clientObject;
											$customerAccount = $emailAddress->getCustomerAccount();
											if (!($customerAccount->getIsLite()) && !($customerAccount->getEmailAddresses()->contains($emailAddress))){
												$customerAccount->addEmailAddress($emailAddress);
											}
										}
										else {
											if ($clientObject instanceof Note){
												$note = $clientObject;
												$customerAccount = $note->getCustomerAccount();
												if (!($customerAccount->getIsLite()) && !($customerAccount->getNotesList()->contains($note))){
													$customerAccount->addNote($note);
												}
											}
											else {
												if ($clientObject instanceof Action){
													$action = $clientObject;
													$customerAccount = $action->getCustomerAccount();
													if (!($customerAccount->getIsLite()) && !($customerAccount->getActions()->contains($action))){
														$customerAccount->addAction($action);
													}
												}
												else {
													if ($clientObject instanceof Contract){
														$contract = $clientObject;
														$customerAccount = $contract->getCustomerAccount();
														if (!($customerAccount->getIsLite()) && !($customerAccount->getContracts()->contains($contract))){
															$customerAccount->addContract($contract);
														}
														if (!is_null($contract->getFreezeAdjustment())){
															if (!($customerAccount->getIsLite()) && !($customerAccount->getAdjustments()->contains($contract->getFreezeAdjustment()))){
																$customerAccount->addAdjustment($contract->getFreezeAdjustment());
															}
														}
														if (!is_null($contract->getCancellationAdjustment())){
															if (!($customerAccount->getIsLite()) && !($customerAccount->getAdjustments()->contains($contract->getCancellationAdjustment()))){
																$customerAccount->addAdjustment($contract->getCancellationAdjustment());
															}
														}
													}
													else {
														if ($clientObject instanceof Letter){
															$letter = $clientObject;
															$customerAccount = $letter->getCustomerAccount();
															if (!($customerAccount->getIsLite()) && !($customerAccount->getLetters()->contains($letter))){
																$customerAccount->addLetter($letter);
															}
														}
														else {
															if ($clientObject instanceof Email){
																$email = $clientObject;
																$customerAccount = $email->getCustomerAccount();
																if (!($customerAccount->getIsLite()) && !($customerAccount->getEmails()->contains($email))){
																	$customerAccount->addEmail($email);
																}
															}
															else {
																if ($clientObject instanceof ScheduledPayment){
																	$scheduledPayment = $clientObject;
																	$customerAccount = $scheduledPayment->getCustomerAccount();
																	if (!($customerAccount->getIsLite()) && !($customerAccount->getScheduledPayments()->contains($scheduledPayment))){
																		$customerAccount->addScheduledPayment($scheduledPayment);
																	}
																}
																else {
																	if ($clientObject instanceof Document){
																		$document = $clientObject;
																		$customerAccount = $document->getCustomerAccount();
																		if (!($customerAccount->getIsLite()) && !($customerAccount->getDocuments()->contains($document))){
																			$customerAccount->addDocument($document);
																		}
																	}
																	else {
																		if ($clientObject instanceof AgentCommission){
																			$agentCommission = $clientObject;
																			$user = $agentCommission->getUser();
																			if (!($user->getAgentCommissions()->contains($agentCommission))){
																				$user->addAgentCommission($agentCommission);
																			}
																		}
																		else {
																			if ($clientObject instanceof Adjustment){
																				$adjustment = $clientObject;
																				$customerAccount = $adjustment->getCustomerAccount();
																				if (!($customerAccount->getIsLite()) && !($customerAccount->getAdjustments()->contains($adjustment))){
																					$customerAccount->addAdjustment($adjustment);
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (!is_null($customerAccount)){
			if ($customerAccount->getIsLite()){
				throw new Exception("Unable to save: Customer Account is Lite.");
			}
			if (is_null($customerAccount->getDefaultAddress()) && (is_null($customerAccount->getId()) || !is_null($customerAccount->getCode()))){
				$customerAccount->setDefaultAddress($customerAccount->createAddress());
			}
			if (!($this->clientObjectsList->contains($customerAccount))){
				$this->clientObjectsList->add($customerAccount);
			}
		}
		else {
			if (!is_null($user) && !($this->clientObjectsList->contains($user))){
				$this->clientObjectsList->add($user);
			}
		}
	}
	/*
	 *$method.comment
	 */
	public function getClientObjectsList(){
		return $this->clientObjectsList;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccounts(){
		return $this->customerAccounts;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentOptions(){
		return $this->paymentOptions;
	}
	/*
	 *$method.comment
	 */
	public function getRevenueTransactions(){
		return $this->revenueTransactions;
	}
	/*
	 *$method.comment
	 */
	public function getAssetTransactions(){
		return $this->assetTransactions;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentPlans(){
		return $this->paymentPlans;
	}
	/*
	 *$method.comment
	 */
	public function getAddresses(){
		return $this->addresses;
	}
	/*
	 *$method.comment
	 */
	public function getPhones(){
		return $this->phones;
	}
	/*
	 *$method.comment
	 */
	public function getEmailAddresses(){
		return $this->emailAddresses;
	}
	/*
	 *$method.comment
	 */
	public function getNotes(){
		return $this->notes;
	}
	/*
	 *$method.comment
	 */
	public function getActions(){
		return $this->actions;
	}
	/*
	 *$method.comment
	 */
	public function getLetters(){
		return $this->letters;
	}
	/*
	 *$method.comment
	 */
	public function getEmails(){
		return $this->emails;
	}
	/*
	 *$method.comment
	 */
	public function getScheduledPayments(){
		return $this->scheduledPayments;
	}
	/*
	 *$method.comment
	 */
	public function getDocuments(){
		return $this->documents;
	}
	/*
	 *$method.comment
	 */
	public function getAuditLogs(){
		return $this->auditLogs;
	}
	/*
	 *$method.comment
	 */
	public function getSignatures(){
		return $this->signatures;
	}
	/*
	 *$method.comment
	 */
	public function getContracts(){
		return $this->contracts;
	}
	/*
	 *$method.comment
	 */
	public function getUsers(){
		return $this->users;
	}
	/*
	 *$method.comment
	 */
	public function getAgentCommissions(){
		return $this->agentCommissions;
	}
	/*
	 *$method.comment
	 */
	public function toMaps($list){
		if (is_null($list) || $list->isEmpty()){
			return ;
		}
		foreach ($list as $clientObject){
			if ($clientObject instanceof CustomerAccount){
				$customerAccount = $clientObject;
				$this->putCustomerAccount($customerAccount);
				foreach ($customerAccount->getPaymentOptions() as $paymentOption){
					$this->putPaymentOption($paymentOption);
				}
				foreach ($customerAccount->getPaymentPlans() as $paymentPlan){
					$this->putPaymentPlan($paymentPlan);
				}
				foreach ($customerAccount->getRevenueTransactions() as $transaction){
					$this->putRevenueTransaction($transaction);
				}
				foreach ($customerAccount->getAssetTransactions() as $transaction){
					$this->putAssetTransaction($transaction);
				}
				foreach ($customerAccount->getAddresses() as $address){
					$this->putAddress($address);
				}
				foreach ($customerAccount->getPhones() as $phone){
					$this->putPhone($phone);
				}
				foreach ($customerAccount->getEmailAddresses() as $emailAddress){
					$this->putEmailAddress($emailAddress);
				}
				foreach ($customerAccount->getNotesList() as $note){
					$this->putNote($note);
				}
				foreach ($customerAccount->getActions() as $action){
					$this->putAction($action);
				}
				foreach ($customerAccount->getLetters() as $letter){
					$this->putLetter($letter);
				}
				foreach ($customerAccount->getEmails() as $email){
					$this->putEmail($email);
				}
				foreach ($customerAccount->getScheduledPayments() as $scheduledPayment){
					$this->putScheduledPayment($scheduledPayment);
				}
				foreach ($customerAccount->getDocuments() as $document){
					$this->putDocument($document);
				}
				foreach ($customerAccount->getSignatures() as $signature){
					$this->putSignature($signature);
				}
				foreach ($customerAccount->getContracts() as $contract){
					$this->putContract($contract);
				}
				foreach ($customerAccount->getAdjustments() as $adjustment){
					$this->putAdjustment($adjustment);
				}
			}
			else {
				if ($clientObject instanceof PaymentOption){
					$this->putPaymentOption($clientObject);
				}
				else {
					if ($clientObject instanceof PaymentPlan){
						$this->putPaymentPlan($clientObject);
					}
					else {
						if ($clientObject instanceof RevenueTransaction){
							$this->putRevenueTransaction($clientObject);
						}
						else {
							if ($clientObject instanceof AssetTransaction){
								$this->putAssetTransaction($clientObject);
							}
							else {
								if ($clientObject instanceof Address){
									$this->putAddress($clientObject);
								}
								else {
									if ($clientObject instanceof Phone){
										$this->putPhone($clientObject);
									}
									else {
										if ($clientObject instanceof EmailAddress){
											$this->putEmailAddress($clientObject);
										}
										else {
											if ($clientObject instanceof Note){
												$this->putNote($clientObject);
											}
											else {
												if ($clientObject instanceof Action){
													$this->putAction($clientObject);
												}
												else {
													if ($clientObject instanceof Contract){
														$this->putContract($clientObject);
													}
													else {
														if ($clientObject instanceof Letter){
															$this->putLetter($clientObject);
														}
														else {
															if ($clientObject instanceof Email){
																$this->putEmail($clientObject);
															}
															else {
																if ($clientObject instanceof ScheduledPayment){
																	$this->putScheduledPayment($clientObject);
																}
																else {
																	if ($clientObject instanceof Document){
																		$this->putDocument($clientObject);
																	}
																	else {
																		if ($clientObject instanceof AgentCommission){
																			$this->putAgentCommission($clientObject);
																		}
																		else {
																			if ($clientObject instanceof Signature){
																				$this->putSignature($clientObject);
																			}
																			else {
																				if ($clientObject instanceof Adjustment){
																					$this->putAdjustment($clientObject);
																				}
																				else {
																					if ($clientObject instanceof User){
																						$user = $clientObject;
																						$this->putUser($user);
																						foreach ($user->getCustomerAccounts() as $customerAccount){
																							$this->putCustomerAccount($customerAccount);
																						}
																						foreach ($user->getAddresses() as $address){
																							$this->putAddress($address);
																						}
																						foreach ($user->getPhones() as $phone){
																							$this->putPhone($phone);
																						}
																						foreach ($user->getEmailAddresses() as $emailAddress){
																							$this->putEmailAddress($emailAddress);
																						}
																						foreach ($user->getNotes() as $note){
																							$this->putNote($note);
																						}
																						foreach ($user->getActions() as $action){
																							$this->putAction($action);
																						}
																						foreach ($user->getAgentCommissions() as $agentCommission){
																							$this->putAgentCommission($agentCommission);
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	/*
	 *$method.comment
	 */
	public function toMap(){
		$this->toMaps($this->clientObjectsList);
	}
	/*
	 *$method.comment
	 */
	public function clearClientObjectsList(){
		$this->clientObjectsList->clear();
	}
	/*
	 *$method.comment
	 */
	public function clear(){
		$this->customerAccounts->clear();
		$this->paymentOptions->clear();
		$this->revenueTransactions->clear();
		$this->assetTransactions->clear();
		$this->paymentPlans->clear();
		$this->addresses->clear();
		$this->phones->clear();
		$this->emailAddresses->clear();
		$this->notes->clear();
		$this->actions->clear();
		$this->letters->clear();
		$this->emails->clear();
		$this->scheduledPayments->clear();
		$this->documents->clear();
		$this->agentCommissions->clear();
		$this->users->clear();
		$this->contracts->clear();
		$this->adjustments->clear();
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount($refId,$id){
		$customerAccount = $this->customerAccounts->get($refId);
		if (is_null($customerAccount)){
			foreach (Helper::getCustomerAccountValues($this->customerAccounts) as $customer){
				if (!is_null($customer->getId()) && $id == $customer->getId()){
					$customerAccount = $customer;
				}
			}
		}
		return $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentOption($refId,$id){
		$paymentOption = $this->paymentOptions->get($refId);
		if (is_null($paymentOption)){
			foreach (Helper::getPaymentOptionValues($this->paymentOptions) as $option){
				if (!is_null($option->getId()) && $id == $option->getId()){
					$paymentOption = $option;
				}
			}
		}
		return $paymentOption;
	}
	/*
	 *$method.comment
	 */
	public function getRevenueTransaction($refId,$id){
		$revenueTransaction = $this->revenueTransactions->get($refId);
		if (is_null($revenueTransaction)){
			foreach (Helper::getRevenueTransactionValues($this->revenueTransactions) as $transaction){
				if (!is_null($transaction->getId()) && $id == $transaction->getId()){
					$revenueTransaction = $transaction;
				}
			}
		}
		return $revenueTransaction;
	}
	/*
	 *$method.comment
	 */
	public function getAssetTransaction($refId,$id){
		$assetTransaction = $this->assetTransactions->get($refId);
		if (is_null($assetTransaction)){
			foreach (Helper::getAssetTransactionValues($this->assetTransactions) as $transaction){
				if (!is_null($transaction->getId()) && $id == $transaction->getId()){
					$assetTransaction = $transaction;
				}
			}
		}
		return $assetTransaction;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentPlan($refId,$id){
		$paymentPlan = $this->paymentPlans->get($refId);
		if (is_null($paymentPlan)){
			foreach (Helper::getPaymentPlanValues($this->paymentPlans) as $plan){
				if (!is_null($plan->getId()) && $id == $plan->getId()){
					$paymentPlan = $plan;
				}
			}
		}
		return $paymentPlan;
	}
	/*
	 *$method.comment
	 */
	public function getAddress($refId,$id){
		$address = $this->addresses->get($refId);
		if (is_null($address)){
			foreach (Helper::getAddressesValues($this->addresses) as $a){
				if (!is_null($a->getId()) && $id == $a->getId()){
					$address = $a;
				}
			}
		}
		return $address;
	}
	/*
	 *$method.comment
	 */
	public function getPhone($refId,$id){
		$phone = $this->phones->get($refId);
		if (is_null($phone)){
			foreach (Helper::getPhonesValues($this->phones) as $p){
				if (!is_null($p->getId()) && $id == $p->getId()){
					$phone = $p;
				}
			}
		}
		return $phone;
	}
	/*
	 *$method.comment
	 */
	public function getEmailAddress($refId,$id){
		$emailAddress = $this->emailAddresses->get($refId);
		if (is_null($emailAddress)){
			foreach (Helper::getEmailAddressesValues($this->emailAddresses) as $a){
				if (!is_null($a->getId()) && $id == $a->getId()){
					$emailAddress = $a;
				}
			}
		}
		return $emailAddress;
	}
	/*
	 *$method.comment
	 */
	public function getNote($refId,$id){
		$note = $this->notes->get($refId);
		if (is_null($note)){
			foreach (Helper::getNoteValues($this->notes) as $n){
				if (!is_null($n->getId()) && $id == $n->getId()){
					$note = $n;
				}
			}
		}
		return $note;
	}
	/*
	 *$method.comment
	 */
	public function getAction($refId,$id){
		$action = $this->actions->get($refId);
		if (is_null($action)){
			foreach (Helper::getActionValues($this->actions) as $a){
				if (!is_null($a->getId()) && $id == $a->getId()){
					$action = $a;
				}
			}
		}
		return $action;
	}
	/*
	 *$method.comment
	 */
	public function getLetter($refId,$id){
		$letter = $this->letters->get($refId);
		if (is_null($letter)){
			foreach (Helper::getLetterValues($this->letters) as $l){
				if (!is_null($l->getId()) && $id == $l->getId()){
					$letter = $l;
				}
			}
		}
		return $letter;
	}
	/*
	 *$method.comment
	 */
	public function getEmail($refId,$id){
		$email = $this->emails->get($refId);
		if (is_null($email)){
			foreach (Helper::getEmailValues($this->emails) as $e){
				if (!is_null($e->getId()) && $id == $e->getId()){
					$email = $e;
				}
			}
		}
		return $email;
	}
	/*
	 *$method.comment
	 */
	public function getScheduledPayment($refId,$id){
		$scheduledPayment = $this->scheduledPayments->get($refId);
		if (is_null($scheduledPayment)){
			foreach (Helper::getScheduledPaymentValues($this->scheduledPayments) as $sp){
				if (!is_null($sp->getId()) && $id == $sp->getId()){
					$scheduledPayment = $sp;
				}
			}
		}
		return $scheduledPayment;
	}
	/*
	 *$method.comment
	 */
	public function getDocument($refId,$id){
		$document = $this->documents->get($refId);
		if (is_null($document)){
			foreach (Helper::getDocumentValues($this->documents) as $d){
				if (!is_null($d->getId()) && $id == $d->getId()){
					$document = $d;
				}
			}
		}
		return $document;
	}
	/*
	 *$method.comment
	 */
	public function getContract($refId,$id){
		$contract = $this->contracts->get($refId);
		if (is_null($contract)){
			foreach (Helper::getContractValues($this->contracts) as $c){
				if (!is_null($c->getId()) && $id == $c->getId()){
					$contract = $c;
				}
			}
		}
		return $contract;
	}
	/*
	 *$method.comment
	 */
	public function getAdjustment($refId,$id){
		$adjustment = $this->adjustments->get($refId);
		if (is_null($adjustment)){
			foreach (Helper::getAdjustmentValues($this->adjustments) as $a){
				if (!is_null($a->getId()) && $id == $a->getId()){
					$adjustment = $a;
				}
			}
		}
		return $adjustment;
	}
	/*
	 *$method.comment
	 */
	public function getAdjustments(){
		return $this->adjustments;
	}
	/*
	 *$method.comment
	 */
	private function putCustomerAccount($customerAccount){
		if (is_null($this->getClientObject($customerAccount))){
			$this->customerAccounts->put($customerAccount->getRefId(), $customerAccount);
		}
		if (!is_null($customerAccount->getCollectorCode())){
			$user = $this->users->get($customerAccount->getCollectorCode());
			if (!is_null($user) && !($user->getCustomerAccounts()->contains($customerAccount))){
				$user->addCustomerAccount($customerAccount);
				$customerAccount->setUser($user);
			}
		}
	}
	/*
	 *$method.comment
	 */
	private function putPaymentOption($paymentOption){
		if (is_null($this->getClientObject($paymentOption))){
			$this->paymentOptions->put($paymentOption->getRefId(), $paymentOption);
			$paymentOption->getCustomerAccount()->addPaymentOption($paymentOption);
		}
	}
	/*
	 *$method.comment
	 */
	private function putRevenueTransaction($transaction){
		if (is_null($this->getClientObject($transaction))){
			$this->revenueTransactions->put($transaction->getRefId(), $transaction);
			$transaction->getCustomerAccount()->addRevenueTransaction($transaction);
		}
	}
	/*
	 *$method.comment
	 */
	private function putAssetTransaction($transaction){
		if (is_null($this->getClientObject($transaction))){
			$this->assetTransactions->put($transaction->getRefId(), $transaction);
			$transaction->getCustomerAccount()->addAssetTransaction($transaction);
		}
	}
	/*
	 *$method.comment
	 */
	private function putPaymentPlan($paymentPlan){
		if (is_null($this->getClientObject($paymentPlan))){
			$this->paymentPlans->put($paymentPlan->getRefId(), $paymentPlan);
			$paymentPlan->getCustomerAccount()->addPaymentPlan($paymentPlan);
		}
	}
	/*
	 *$method.comment
	 */
	private function putAddress($address){
		if (is_null($address->getCustomerAccount()->getId()) || !is_null($address->getCustomerAccount()->getCode())){
			if (is_null($this->getClientObject($address))){
				$this->addresses->put($address->getRefId(), $address);
				$address->getCustomerAccount()->addAddress($address);
			}
			if (!is_null($address->getCreatorCode())){
				$user = $this->users->get($address->getCreatorCode());
				$address->setUser($user);
				if (!is_null($user) && !($user->getAddresses()->contains($address))){
					$user->addAddress($address);
				}
			}
		}
	}
	/*
	 *$method.comment
	 */
	private function putPhone($phone){
		if (is_null($this->getClientObject($phone))){
			$this->phones->put($phone->getRefId(), $phone);
			$phone->getCustomerAccount()->addPhone($phone);
		}
		if (!is_null($phone->getCreatorCode())){
			$user = $this->users->get($phone->getCreatorCode());
			$phone->setUser($user);
			if (!is_null($user) && !($user->getPhones()->contains($phone))){
				$user->addPhone($phone);
			}
		}
	}
	/*
	 *$method.comment
	 */
	private function putEmailAddress($emailAddress){
		if (is_null($this->getClientObject($emailAddress))){
			$this->emailAddresses->put($emailAddress->getRefId(), $emailAddress);
			$emailAddress->getCustomerAccount()->addEmailAddress($emailAddress);
		}
		if (!is_null($emailAddress->getCreatorCode())){
			$user = $this->users->get($emailAddress->getCreatorCode());
			$emailAddress->setUser($user);
			if (!is_null($user) && !($user->getEmailAddresses()->contains($emailAddress))){
				$user->addEmailAddress($emailAddress);
			}
		}
	}
	/*
	 *$method.comment
	 */
	private function putNote($note){
		if (is_null($this->getClientObject($note))){
			$this->notes->put($note->getRefId(), $note);
			$note->getCustomerAccount()->addNote($note);
		}
		if (!is_null($note->getCreatorCode())){
			$user = $this->users->get($note->getCreatorCode());
			$note->setUser($user);
			if (!is_null($user) && !($user->getNotes()->contains($note))){
				$user->addNote($note);
			}
		}
	}
	/*
	 *$method.comment
	 */
	private function putAction($action){
		if (is_null($this->getClientObject($action))){
			$this->actions->put($action->getRefId(), $action);
			$action->getCustomerAccount()->addAction($action);
		}
		if (!is_null($action->getCreatorCode())){
			$user = $this->users->get($action->getCreatorCode());
			$action->setUser($user);
			if (!is_null($user) && !($user->getActions()->contains($action))){
				$user->addAction($action);
			}
		}
	}
	/*
	 *$method.comment
	 */
	private function putLetter($letter){
		if (is_null($this->getClientObject($letter))){
			$this->letters->put($letter->getRefId(), $letter);
			$letter->getCustomerAccount()->addLetter($letter);
		}
	}
	/*
	 *$method.comment
	 */
	private function putEmail($email){
		if (is_null($this->getClientObject($email))){
			$this->emails->put($email->getRefId(), $email);
			$email->getCustomerAccount()->addEmail($email);
		}
	}
	/*
	 *$method.comment
	 */
	private function putScheduledPayment($scheduledPayment){
		if (is_null($this->getClientObject($scheduledPayment))){
			$this->scheduledPayments->put($scheduledPayment->getRefId(), $scheduledPayment);
			$scheduledPayment->getCustomerAccount()->addScheduledPayment($scheduledPayment);
		}
	}
	/*
	 *$method.comment
	 */
	private function putDocument($document){
		if (is_null($this->getClientObject($document))){
			$this->documents->put($document->getRefId(), $document);
			$document->getCustomerAccount()->addDocument($document);
		}
	}
	/*
	 *$method.comment
	 */
	private function putSignature($signature){
		if (is_null($this->getClientObject($signature))){
			$this->signatures->put($signature->getRefId(), $signature);
			$signature->getCustomerAccount()->addSignature($signature);
		}
	}
	/*
	 *$method.comment
	 */
	private function putContract($contract){
		if (is_null($this->getClientObject($contract))){
			$this->contracts->put($contract->getRefId(), $contract);
			$contract->getCustomerAccount()->addContract($contract);
		}
	}
	/*
	 *$method.comment
	 */
	private function putAdjustment($adjustment){
		if (is_null($this->getClientObject($adjustment))){
			$this->adjustments->put($adjustment->getRefId(), $adjustment);
			$adjustment->getCustomerAccount()->addAdjustment($adjustment);
		}
	}
	/*
	 *$method.comment
	 */
	private function putUser($user){
		if (is_null($this->getClientObject($user))){
			$this->users->put($user->getUserName(), $user);
		}
	}
	/*
	 *$method.comment
	 */
	private function putAgentCommission($agentCommission){
		if (is_null($this->getClientObject($agentCommission))){
			$this->agentCommissions->put($agentCommission->getRefId(), $agentCommission);
			$agentCommission->getUser()->addAgentCommission($agentCommission);
		}
	}
	/*
	 *$method.comment
	 */
	private function getClientObject($clientObject){
		$list = null;
		if ($clientObject instanceof CustomerAccount){
			$list = Helper::getCustomerAccountValues($this->customerAccounts);
		}
		else {
			if ($clientObject instanceof PaymentPlan){
				$list = Helper::getPaymentPlanValues($this->paymentPlans);
			}
			else {
				if ($clientObject instanceof Address){
					$list = Helper::getAddressesValues($this->addresses);
				}
				else {
					if ($clientObject instanceof Phone){
						$list = Helper::getPhonesValues($this->phones);
					}
					else {
						if ($clientObject instanceof EmailAddress){
							$list = Helper::getEmailAddressesValues($this->emailAddresses);
						}
						else {
							if ($clientObject instanceof PaymentOption){
								$list = Helper::getPaymentOptionValues($this->paymentOptions);
							}
							else {
								if ($clientObject instanceof RevenueTransaction){
									$list = Helper::getRevenueTransactionValues($this->revenueTransactions);
								}
								else {
									if ($clientObject instanceof AssetTransaction){
										$list = Helper::getAssetTransactionValues($this->assetTransactions);
									}
									else {
										if ($clientObject instanceof Note){
											$list = Helper::getNoteValues($this->notes);
										}
										else {
											if ($clientObject instanceof Action){
												$list = Helper::getActionValues($this->actions);
											}
											else {
												if ($clientObject instanceof Contract){
													$list = Helper::getContractValues($this->contracts);
												}
												else {
													if ($clientObject instanceof Letter){
														$list = Helper::getLetterValues($this->letters);
													}
													else {
														if ($clientObject instanceof Email){
															$list = Helper::getEmailValues($this->emails);
														}
														else {
															if ($clientObject instanceof ScheduledPayment){
																$list = Helper::getScheduledPaymentValues($this->scheduledPayments);
															}
															else {
																if ($clientObject instanceof Document){
																	$list = Helper::getDocumentValues($this->documents);
																}
																else {
																	if ($clientObject instanceof AuditLog){
																		$list = Helper::getAuditLogValues($this->auditLogs);
																	}
																	else {
																		if ($clientObject instanceof Signature){
																			$list = Helper::getSignatureValues($this->signatures);
																		}
																		else {
																			if ($clientObject instanceof AgentCommission){
																				$list = Helper::getAgentCommissionValues($this->agentCommissions);
																			}
																			else {
																				if ($clientObject instanceof Adjustment){
																					$list = Helper::getAdjustmentValues($this->adjustments);
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if (!is_null($list)){
			$id = $clientObject->getId();
			foreach ($list as $obj){
				$clntObject = $obj;
				if (!is_null($clntObject->getId()) && !is_null($id) && $id == $clntObject->getId()){
					return $clntObject;
				}
			}
		}
		else {
			if ($clientObject instanceof User){
				$users = Helper::getUserValues($this->users);
				$userName = $clientObject->getUserName();
				foreach ($users as $user){
					if ($userName == $user->getUserName()){
						return $user;
					}
				}
			}
		}
		return null;
	}
}

?>

