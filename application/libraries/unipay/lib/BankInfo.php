<?php



/*
 *$comment
 */
class BankInfo extends ClientObject {
		/*
	 *@var null
	 */
	private $bankName;
		/*
	 *@var null
	 */
	private $address;
		/*
	 *@var null
	 */
	private $country;
		/*
	 *@var null
	 */
	private $phoneNumber;
		/*
	 *@var null
	 */
	private $paymentOptionType;
		/*
	 *@var null
	 */
	private $accountType;
		/*
	 *@var null
	 */
	private $accountCategory;
		
	/*
	 *$constructor.comment
	 */
	function __construct(){
		//initialization block
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
	}

	/*
	 *$method.comment
	 */
	public function getBankName(){
		return $bankName;
	}
	/*
	 *$method.comment
	 */
	 function setBankName($bankName){
		$this->bankName = $bankName;
	}
	/*
	 *$method.comment
	 */
	public function getAddress(){
		return $address;
	}
	/*
	 *$method.comment
	 */
	 function setAddress($address){
		$this->address = $address;
	}
	/*
	 *$method.comment
	 */
	public function getCountry(){
		return $country;
	}
	/*
	 *$method.comment
	 */
	 function setCountry($country){
		$this->country = $country;
	}
	/*
	 *$method.comment
	 */
	public function getPhoneNumber(){
		return $phoneNumber;
	}
	/*
	 *$method.comment
	 */
	 function setPhoneNumber($phoneNumber){
		$this->phoneNumber = $phoneNumber;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentOptionType(){
		return $paymentOptionType;
	}
	/*
	 *$method.comment
	 */
	 function setPaymentOptionType($paymentOptionType){
		$this->paymentOptionType = $paymentOptionType;
	}
	/*
	 *$method.comment
	 */
	public function getAccountType(){
		return $accountType;
	}
	/*
	 *$method.comment
	 */
	 function setAccountType($accountType){
		$this->accountType = $accountType;
	}
	/*
	 *$method.comment
	 */
	public function getAccountCategory(){
		return $accountCategory;
	}
	/*
	 *$method.comment
	 */
	 function setAccountCategory($accountCategory){
		$this->accountCategory = $accountCategory;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "BankInfo";
	}
}

?>

