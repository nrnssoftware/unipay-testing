<?php



/*
 *$comment
 */
class AuditLogDetail extends ClientObject {
		/*
	 *@var null
	 */
	private $fieldName;
		/*
	 *@var null
	 */
	private $oldValue;
		/*
	 *@var null
	 */
	private $newValue;
		/*
	 *@var null
	 */
	private $description;
		
	/*
	 *$constructor.comment
	 */
	function __construct(){
		//initialization block
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
	}

	/*
	 *$method.comment
	 */
	public function getFieldName(){
		return $fieldName;
	}
	/*
	 *$method.comment
	 */
	 function setFieldName($fieldName){
		$this->fieldName = $fieldName;
	}
	/*
	 *$method.comment
	 */
	public function getOldValue(){
		return $oldValue;
	}
	/*
	 *$method.comment
	 */
	 function setOldValue($oldValue){
		$this->oldValue = $oldValue;
	}
	/*
	 *$method.comment
	 */
	public function getNewValue(){
		return $newValue;
	}
	/*
	 *$method.comment
	 */
	 function setNewValue($newValue){
		$this->newValue = $newValue;
	}
	/*
	 *$method.comment
	 */
	public function getCode(){
		return strval($this->getId());
	}
	/*
	 *$method.comment
	 */
	public function getDescription(){
		return $description;
	}
	/*
	 *$method.comment
	 */
	 function setDescription($description){
		$this->description = $description;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "AuditLogDetail";
	}
}

?>

