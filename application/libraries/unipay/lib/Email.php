<?php



/*
 *$comment
 */
class Email extends ClientObject {
		/*
	 *@var null
	 */
	private $emailCode;
		/*
	 *@var null
	 */
	private $creatorCode;
		/*
	 *@var null
	 */
	private $dueDate;
		/*
	 *@var null
	 */
	private $status;
		/*
	 *@var null
	 */
	private $firstName;
		/*
	 *@var null
	 */
	private $lastName;
		/*
	 *@var null
	 */
	private $toEmail;
		/*
	 *@var null
	 */
	private $subject;
		/*
	 *@var null
	 */
	private $message;
		/*
	 *@var null
	 */
	private $attachmentCode;
		/*
	 *@var null
	 */
	private $fileExtention;
		/*
	 *@var null
	 */
	private $response;
		/*
	 *@var null
	 */
	private $customerAccount;
		/*
	 *@var null
	 */
	private $isVisibleExternally;
		
	/*
	 *$constructor.comment
	 */
	function __construct(){
		//initialization block
		$this->isVisibleExternally = false;
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
	}

	/*
	 *$method.comment
	 */
	public function getEmailCode(){
		return $this->emailCode;
	}
	/*
	 *$method.comment
	 */
	public function setEmailCode($emailCode){
		$this->emailCode = $emailCode;
	}
	/*
	 *$method.comment
	 */
	public function getCreatorCode(){
		return $this->creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function setCreatorCode($creatorCode){
		$this->creatorCode = $creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function getDueDate(){
		return $this->dueDate;
	}
	/*
	 *$method.comment
	 */
	public function setDueDate($dueDate){
		$this->dueDate = $dueDate;
	}
	/*
	 *$method.comment
	 */
	public function getStatus(){
		return $this->status;
	}
	/*
	 *$method.comment
	 */
	public function setStatus($status){
		$this->status = $status;
	}
	/*
	 *$method.comment
	 */
	public function getFirstName(){
		return $this->firstName;
	}
	/*
	 *$method.comment
	 */
	public function setFirstName($firstName){
		$this->firstName = $firstName;
	}
	/*
	 *$method.comment
	 */
	public function getLastName(){
		return $this->lastName;
	}
	/*
	 *$method.comment
	 */
	public function setLastName($lastName){
		$this->lastName = $lastName;
	}
	/*
	 *$method.comment
	 */
	public function getToEmail(){
		return $this->toEmail;
	}
	/*
	 *$method.comment
	 */
	public function setToEmail($toEmail){
		$this->toEmail = $toEmail;
	}
	/*
	 *$method.comment
	 */
	public function getSubject(){
		return $this->subject;
	}
	/*
	 *$method.comment
	 */
	public function setSubject($subject){
		$this->subject = $subject;
	}
	/*
	 *$method.comment
	 */
	public function getMessage(){
		return $this->message;
	}
	/*
	 *$method.comment
	 */
	public function setMessage($message){
		$this->message = $message;
	}
	/*
	 *$method.comment
	 */
	public function getAttachmentCode(){
		return $this->attachmentCode;
	}
	/*
	 *$method.comment
	 */
	public function setAttachmentCode($attachmentCode){
		$this->attachmentCode = $attachmentCode;
	}
	/*
	 *$method.comment
	 */
	public function getFileExtention(){
		return $this->fileExtention;
	}
	/*
	 *$method.comment
	 */
	public function setFileExtention($fileExtention){
		$this->fileExtention = $fileExtention;
	}
	/*
	 *$method.comment
	 */
	public function getResponse(){
		return $this->response;
	}
	/*
	 *$method.comment
	 */
	public function setResponse($response){
		$this->response = $response;
	}
	/*
	 *$method.comment
	 */
	public function getIsVisibleExternally(){
		return $this->isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function setIsVisibleExternally($isVisibleExternally){
		$this->isVisibleExternally = $isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccount($customerAccount){
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function cancel(){
		$this->status = EmailStatusType::Cancelled;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "Email";
	}
}

?>

