<?php


/*
 *$comment
 */
class User extends ClientObject {
		/*
	 *@var null
	 */
	private $userName;
		/*
	 *@var null
	 */
	private $name;
		/*
	 *@var null
	 */
	private $isActive;
		/*
	 *@var null
	 */
	private $roleType;
		/*
	 *@var null
	 */
	private $email;
		/*
	 *@var null
	 */
	private $phone;
		/*
	 *@var null
	 */
	private $customerAccountCode;
		/*
	 *@var null
	 */
	private $customerAccount;
		/*
	 *@var null
	 */
	private $isAgentCommissionAllowed;
		/*
	 *@var null
	 */
	private $customerAccounts;
		/*
	 *@var null
	 */
	private $addresses;
		/*
	 *@var null
	 */
	private $phones;
		/*
	 *@var null
	 */
	private $emailAddresses;
		/*
	 *@var null
	 */
	private $notes;
		/*
	 *@var null
	 */
	private $actions;
		/*
	 *@var null
	 */
	private $agentCommissions;
		
	/*
	 *$constructor.comment
	 */
	function __construct(){
		//initialization block
		$this->isActive = false;
		$this->isAgentCommissionAllowed = false;
		$this->customerAccounts = new ArrayList();
		$this->addresses = new ArrayList();
		$this->phones = new ArrayList();
		$this->emailAddresses = new ArrayList();
		$this->notes = new ArrayList();
		$this->actions = new ArrayList();
		$this->agentCommissions = new ArrayList();
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
	}

	/*
	 *$method.comment
	 */
	public function getUserName(){
		return $this->userName;
	}
	/*
	 *$method.comment
	 */
	 function setUserName($userName){
		$this->userName = $userName;
	}
	/*
	 *$method.comment
	 */
	public function getName(){
		return $name;
	}
	/*
	 *$method.comment
	 */
	 function setName($name){
		$this->name = $name;
	}
	/*
	 *$method.comment
	 */
	public function getIsActive(){
		return $this->isActive;
	}
	/*
	 *$method.comment
	 */
	 function setIsActive($isActive){
		$this->isActive = $isActive;
	}
	/*
	 *$method.comment
	 */
	public function getRoleType(){
		return $this->roleType;
	}
	/*
	 *$method.comment
	 */
	 function setRoleType($roleType){
		$this->roleType = $roleType;
	}
	/*
	 *$method.comment
	 */
	public function getEmail(){
		return $this->email;
	}
	/*
	 *$method.comment
	 */
	 function setEmail($email){
		$this->email = $email;
	}
	/*
	 *$method.comment
	 */
	public function getPhone(){
		return $this->phone;
	}
	/*
	 *$method.comment
	 */
	 function setPhone($phone){
		$this->phone = $phone;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccountCode(){
		return $this->customerAccountCode;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccountCode($customerAccountCode){
		$this->customerAccountCode = $customerAccountCode;
	}
	/*
	 *$method.comment
	 */
	public function getIsAgentCommissionAllowed(){
		return $this->isAgentCommissionAllowed;
	}
	/*
	 *$method.comment
	 */
	 function setIsAgentCommissionAllowed($isAgentCommissionAllowed){
		$this->isAgentCommissionAllowed = $isAgentCommissionAllowed;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccount($customerAccount){
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccounts(){
		return new ArrayList($this->customerAccounts);
	}
	/*
	 *$method.comment
	 */
	public function getAddresses(){
		return new ArrayList($this->addresses);
	}
	/*
	 *$method.comment
	 */
	public function getPhones(){
		return new ArrayList($this->phones);
	}
	/*
	 *$method.comment
	 */
	public function getEmailAddresses(){
		return new ArrayList($this->emailAddresses);
	}
	/*
	 *$method.comment
	 */
	public function getNotes(){
		return new ArrayList($this->notes);
	}
	/*
	 *$method.comment
	 */
	public function getActions(){
		return new ArrayList($this->actions);
	}
	/*
	 *$method.comment
	 */
	public function getAgentCommissions(){
		return new ArrayList($this->agentCommissions);
	}
	/*
	 *$method.comment
	 */
	public function addCustomerAccount($customerAccount){
		$hasObject = false;
		foreach ($this->customerAccounts as $account){
			if ($account->getRefId() == $customerAccount->getRefId()){
				$hasObject = true;
				break;
			}
		}
		if (!($this->customerAccounts->contains($customerAccount)) && !($hasObject)){
			$this->customerAccounts->add($customerAccount);
		}
	}
	/*
	 *$method.comment
	 */
	public function addAddress($address){
		$hasObject = false;
		foreach ($this->addresses as $a){
			if ($a->getRefId() == $address->getRefId()){
				$hasObject = true;
				break;
			}
		}
		if (!($this->addresses->contains($address)) && !($hasObject)){
			$this->addresses->add($address);
		}
	}
	/*
	 *$method.comment
	 */
	public function addPhone($phone){
		$hasObject = false;
		foreach ($this->phones as $p){
			if ($p->getRefId() == $phone->getRefId()){
				$hasObject = true;
				break;
			}
		}
		if (!($this->phones->contains($phone)) && !($hasObject)){
			$this->phones->add($phone);
		}
	}
	/*
	 *$method.comment
	 */
	public function addEmailAddress($emailAddress){
		$hasObject = false;
		foreach ($this->emailAddresses as $a){
			if ($a->getRefId() == $emailAddress->getRefId()){
				$hasObject = true;
				break;
			}
		}
		if (!($this->emailAddresses->contains($emailAddress)) && !($hasObject)){
			$this->emailAddresses->add($emailAddress);
		}
	}
	/*
	 *$method.comment
	 */
	public function addNote($note){
		$hasObject = false;
		foreach ($this->notes as $n){
			if ($n->getRefId() == $note->getRefId()){
				$hasObject = true;
				break;
			}
		}
		if (!($this->notes->contains($note)) && !($hasObject)){
			$this->notes->add($note);
		}
	}
	/*
	 *$method.comment
	 */
	public function addAction($action){
		$hasObject = false;
		foreach ($this->actions as $a){
			if ($a->getRefId() == $action->getRefId()){
				$hasObject = true;
				break;
			}
		}
		if (!($this->actions->contains($action)) && !($hasObject)){
			$this->actions->add($action);
		}
	}
	/*
	 *$method.comment
	 */
	public function addAgentCommission($agentCommission){
		$hasObject = false;
		foreach ($this->agentCommissions as $commission){
			if ($commission->getRefId() == $agentCommission->getRefId()){
				$hasObject = true;
				break;
			}
		}
		if (!($this->agentCommissions->contains($agentCommission)) && !($hasObject)){
			$this->agentCommissions->add($agentCommission);
		}
	}
	/*
	 *$method.comment
	 */
	public function removeCustomerAccount($customerAccount){
		$this->customerAccounts->remove($customerAccount);
	}
	/*
	 *$method.comment
	 */
	public function removeNote($note){
		$this->notes->remove($note);
	}
	/*
	 *$method.comment
	 */
	public function removeAction($action){
		$this->actions->remove($action);
	}
	/*
	 *$method.comment
	 */
	public function removeAgentCommission($agentCommission){
		$this->agentCommissions->remove($agentCommission);
	}
	/*
	 *$method.comment
	 */
	public function setAgentCommissions($agentCommissions){
		$this->agentCommissions = $agentCommissions;
	}
	/*
	 *$method.comment
	 */
	public function createAgentCommission($amount,$note){
		$commission = new AgentCommission();
		$commission->setAgentCode($this->userName);
		$commission->setAmount($amount);
		$commission->setMerchantAccountCode($this->getMerchantAccountCode());
		$commission->setCommissionType(CommissionType::Adjustment);
		$commission->setNote($note);
		$commission->setUser($this);
		$this->addAgentCommission($commission);
		return $commission;
	}
	/*
	 *$method.comment
	 */
	public function adjustAgentCommission($agentCommission,$note){
		if (!is_null($agentCommission->getAdjustmentAgentCommission())){
			throw new Exception("Unable to adjust selected Agent Commission; selected Agent Commission was adjusted.");
		}
		$commission = new AgentCommission();
		$commission->setAgentCode($agentCommission->getAgentCode());
		$commission->setMerchantAccountCode($agentCommission->getMerchantAccountCode());
		$commission->setCollectorAccountCode($agentCommission->getCollectorAccountCode());
		$commission->setAmount(-($agentCommission->getAmount()));
		$commission->setCommissionType(CommissionType::Adjustment);
		$commission->setNote($note);
		$commission->setCustomerAccount($agentCommission->getCustomerAccount());
		$commission->setPaymentOption($agentCommission->getPaymentOption());
		$commission->setSource($agentCommission->getSource());
		$commission->setTarget($agentCommission->getTarget());
		$commission->setUser($agentCommission->getUser());
		$agentCommission->setAdjustmentAgentCommission($commission);
		$this->addAgentCommission($commission);
		return $commission;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "User";
	}
}

?>

