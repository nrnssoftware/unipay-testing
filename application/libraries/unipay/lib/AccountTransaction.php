<?php



/*
 *$comment
 */
class AccountTransaction extends AccountActivity {
		/*
	 *@var null
	 */
	const MERCHANT_ACCOUNT_CODE_ASC = "transaction.merchantAccountCode";
		/*
	 *@var null
	 */
	const MERCHANT_ACCOUNT_CODE_DESC = "transaction.merchantAccountCode desc";
		/*
	 *@var null
	 */
	const CODE_ASC = "transaction.refCode";
		/*
	 *@var null
	 */
	const CODE_DESC = "transaction.refCode desc";
		/*
	 *@var null
	 */
	const CREATE_DATE_ASC = "transaction.createDate";
		/*
	 *@var null
	 */
	const CREATE_DATE_DESC = "transaction.createDate desc";
		/*
	 *@var null
	 */
	const ACCOUNT_ACTIVITY_TYPE_ASC = "transaction.accountActivityCl";
		/*
	 *@var null
	 */
	const ACCOUNT_ACTIVITY_TYPE_DESC = "transaction.accountActivityCl desc";
		/*
	 *@var null
	 */
	const CUSTOMER_ACCOUNT_CODE_ASC = "transaction.customerAccount.refCode";
		/*
	 *@var null
	 */
	const CUSTOMER_ACCOUNT_CODE_DESC = "transaction.customerAccount.refCode desc";
		/*
	 *@var null
	 */
	const AMOUNT_ASC = "transaction.amount";
		/*
	 *@var null
	 */
	const AMOUNT_DESC = "transaction.amount desc";
		/*
	 *@var null
	 */
	const BALANCE_ASC = "transaction.balance";
		/*
	 *@var null
	 */
	const BALANCE_DESC = "transaction.balance desc";
		/*
	 *@var null
	 */
	private $amount;
		/*
	 *@var null
	 */
	private $balance;
		/*
	 *@var null
	 */
	private $note;
		/*
	 *@var null
	 */
	private $creatorCode;
		/*
	 *@var null
	 */
	private $sellerCode;
		/*
	 *@var null
	 */
	private $shiftCode;
		/*
	 *@var null
	 */
	private $terminalCode;
		/*
	 *@var null
	 */
	private $taxAmount;
		/*
	 *@var null
	 */
	private $dueDate;
		/*
	 *@var null
	 */
	private $adjustmentTransaction;
		/*
	 *@var null
	 */
	private $historicalBalance;
		/*
	 *@var null
	 */
	private $isVisibleExternally;
		/*
	 *@var null
	 */
	private $posterCode;
		/*
	 *@var null
	 */
	private $isCollectionsTransaction;
		/*
	 *@var null
	 */
	private $balancingGroup;
		
	/*
	 *$constructor.comment
	 */
	function __construct($amount = null, $balance = null, $note = null, $createDate = null, $creatorCode = null, $sellerCode = null, $shiftCode = null, $terminalCode = null, $taxAmount = null, $dueDate = null, $adjustmentTransaction = null){
		//initialization block
		$this->MERCHANT_ACCOUNT_CODE_ASC = "transaction.merchantAccountCode";
		$this->MERCHANT_ACCOUNT_CODE_DESC = "transaction.merchantAccountCode desc";
		$this->CODE_ASC = "transaction.refCode";
		$this->CODE_DESC = "transaction.refCode desc";
		$this->CREATE_DATE_ASC = "transaction.createDate";
		$this->CREATE_DATE_DESC = "transaction.createDate desc";
		$this->ACCOUNT_ACTIVITY_TYPE_ASC = "transaction.accountActivityCl";
		$this->ACCOUNT_ACTIVITY_TYPE_DESC = "transaction.accountActivityCl desc";
		$this->CUSTOMER_ACCOUNT_CODE_ASC = "transaction.customerAccount.refCode";
		$this->CUSTOMER_ACCOUNT_CODE_DESC = "transaction.customerAccount.refCode desc";
		$this->AMOUNT_ASC = "transaction.amount";
		$this->AMOUNT_DESC = "transaction.amount desc";
		$this->BALANCE_ASC = "transaction.balance";
		$this->BALANCE_DESC = "transaction.balance desc";
		$this->isVisibleExternally = false;
		$this->isCollectionsTransaction = false;
		if (func_num_args() == 0) {
		//default constructor
		parent::__construct();		return;
		}
		//constructor with parameters
		parent::__construct();
		$this->amount = $amount;
		$this->balance = $balance;
		$this->note = $note;
		$this->setCreateDate($createDate);
		$this->creatorCode = $creatorCode;
		$this->sellerCode = $sellerCode;
		$this->shiftCode = $shiftCode;
		$this->terminalCode = $terminalCode;
		$this->taxAmount = $taxAmount;
		$this->dueDate = $dueDate;
		$this->adjustmentTransaction = $adjustmentTransaction;
	
	}

	/*
	 *$method.comment
	 */
	public function getAmount(){
		return $this->amount;
	}
	/*
	 *$method.comment
	 */
	public function setAmount($amount){
		$this->amount = $amount;
		$this->balance = $amount;
	}
	/*
	 *$method.comment
	 */
	public function getBalance(){
		return $this->balance;
	}
	/*
	 *$method.comment
	 */
	 function setBalance($balance){
		$this->balance = $balance;
	}
	/*
	 *$method.comment
	 */
	public function getNote(){
		return $this->note;
	}
	/*
	 *$method.comment
	 */
	public function setNote($note){
		$this->note = $note;
	}
	/*
	 *$method.comment
	 */
	public function getCreatorCode(){
		return $this->creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function setCreatorCode($creatorCode){
		$this->creatorCode = $creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function getSellerCode(){
		return $this->sellerCode;
	}
	/*
	 *$method.comment
	 */
	public function setSellerCode($sellerCode){
		$this->sellerCode = $sellerCode;
	}
	/*
	 *$method.comment
	 */
	public function getShiftCode(){
		return $this->shiftCode;
	}
	/*
	 *$method.comment
	 */
	public function setShiftCode($shiftCode){
		$this->shiftCode = $shiftCode;
	}
	/*
	 *$method.comment
	 */
	public function getTerminalCode(){
		return $this->terminalCode;
	}
	/*
	 *$method.comment
	 */
	public function setTerminalCode($terminalCode){
		$this->terminalCode = $terminalCode;
	}
	/*
	 *$method.comment
	 */
	public function getTaxAmount(){
		return $this->taxAmount;
	}
	/*
	 *$method.comment
	 */
	public function setTaxAmount($taxAmount){
		$this->taxAmount = $taxAmount;
	}
	/*
	 *$method.comment
	 */
	public function getAdjustmentTransaction(){
		return $this->adjustmentTransaction;
	}
	/*
	 *$method.comment
	 */
	 function setAdjustmentTransaction($adjustmentTransaction){
		$this->adjustmentTransaction = $adjustmentTransaction;
	}
	/*
	 *$method.comment
	 */
	public function getDueDate(){
		return $this->dueDate;
	}
	/*
	 *$method.comment
	 */
	public function setDueDate($dueDate){
		$this->dueDate = $dueDate;
	}
	/*
	 *$method.comment
	 */
	public function getHistoricalBalance(){
		return $this->historicalBalance;
	}
	/*
	 *$method.comment
	 */
	 function setHistoricalBalance($historicalBalance){
		$this->historicalBalance = $historicalBalance;
	}
	/*
	 *$method.comment
	 */
	public function getIsVisibleExternally(){
		return $this->isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function setIsVisibleExternally($isVisibleExternally){
		$this->isVisibleExternally = $isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function getPosterCode(){
		return $this->posterCode;
	}
	/*
	 *$method.comment
	 */
	public function setPosterCode($posterCode){
		$this->posterCode = $posterCode;
	}
	/*
	 *$method.comment
	 */
	public function getIsCollectionsTransaction(){
		return $this->isCollectionsTransaction;
	}
	/*
	 *$method.comment
	 */
	 function setIsCollectionsTransaction($isCollectionsTransaction){
		$this->isCollectionsTransaction = $isCollectionsTransaction;
	}
	/*
	 *$method.comment
	 */
	public function getBalancingGroup(){
		return $this->balancingGroup;
	}
	/*
	 *$method.comment
	 */
	public function setBalancingGroup($balancingGroup){
		$this->balancingGroup = $balancingGroup;
	}
}

?>

