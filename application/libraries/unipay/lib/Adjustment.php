<?php



/*
 *$comment
 */
class Adjustment extends ClientObject {
		/*
	 *@var null
	 */
	private $type;
		/*
	 *@var null
	 */
	private $length;
		/*
	 *@var null
	 */
	private $value;
		/*
	 *@var null
	 */
	private $renewalAmount;
		/*
	 *@var null
	 */
	private $creatorCode;
		/*
	 *@var null
	 */
	private $expirationDate;
		/*
	 *@var null
	 */
	private $adjustmentBeginDate;
		/*
	 *@var null
	 */
	private $adjustmentEndDate;
		/*
	 *@var null
	 */
	private $isProcessed;
		/*
	 *@var null
	 */
	private $isVisibleExternally;
		/*
	 *@var null
	 */
	private $notes;
		/*
	 *@var null
	 */
	private $adjustmentReasonCode;
		/*
	 *@var null
	 */
	private $customerAccount;
		/*
	 *@var null
	 */
	private $paymentPlan;
		/*
	 *@var null
	 */
	private $contract;
		
	/*
	 *$constructor.comment
	 */
	function __construct(){
		//initialization block
		$this->isProcessed = false;
		$this->isVisibleExternally = false;
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
	}

	/*
	 *$method.comment
	 */
	public function getType(){
		return $this->type;
	}
	/*
	 *$method.comment
	 */
	public function setType($type){
		$this->type = $type;
	}
	/*
	 *$method.comment
	 */
	public function getRenewalAmount(){
		return $this->renewalAmount;
	}
	/*
	 *$method.comment
	 */
	 function setRenewalAmount($renewalAmount){
		$this->renewalAmount = $renewalAmount;
	}
	/*
	 *$method.comment
	 */
	public function getLength(){
		return $this->length;
	}
	/*
	 *$method.comment
	 */
	 function setLength($length){
		$this->length = $length;
	}
	/*
	 *$method.comment
	 */
	public function getValue(){
		return $this->value;
	}
	/*
	 *$method.comment
	 */
	 function setValue($value){
		$this->value = $value;
	}
	/*
	 *$method.comment
	 */
	public function getExpirationDate(){
		return $this->expirationDate;
	}
	/*
	 *$method.comment
	 */
	 function setExpirationDate($expirationDate){
		$this->expirationDate = $expirationDate;
	}
	/*
	 *$method.comment
	 */
	public function getAdjustmentBeginDate(){
		return $this->adjustmentBeginDate;
	}
	/*
	 *$method.comment
	 */
	public function setAdjustmentBeginDate($adjustmentBeginDate){
		$this->adjustmentBeginDate = $adjustmentBeginDate;
	}
	/*
	 *$method.comment
	 */
	public function getAdjustmentEndDate(){
		return $this->adjustmentEndDate;
	}
	/*
	 *$method.comment
	 */
	public function setAdjustmentEndDate($adjustmentEndDate){
		$this->adjustmentEndDate = $adjustmentEndDate;
	}
	/*
	 *$method.comment
	 */
	public function getIsProcessed(){
		return $this->isProcessed;
	}
	/*
	 *$method.comment
	 */
	 function setIsProcessed($isProcessed){
		$this->isProcessed = $isProcessed;
	}
	/*
	 *$method.comment
	 */
	public function getNotes(){
		return $this->notes;
	}
	/*
	 *$method.comment
	 */
	public function setNotes($notes){
		$this->notes = $notes;
	}
	/*
	 *$method.comment
	 */
	public function getAdjustmentReasonCode(){
		return $this->adjustmentReasonCode;
	}
	/*
	 *$method.comment
	 */
	public function setAdjustmentReasonCode($adjustmentReasonCode){
		$this->adjustmentReasonCode = $adjustmentReasonCode;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccount($customerAccount){
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentPlan(){
		return $this->paymentPlan;
	}
	/*
	 *$method.comment
	 */
	 function setPaymentPlan($paymentPlan){
		$this->paymentPlan = $paymentPlan;
	}
	/*
	 *$method.comment
	 */
	public function getContract(){
		return $this->contract;
	}
	/*
	 *$method.comment
	 */
	 function setContract($contract){
		$this->contract = $contract;
	}
	/*
	 *$method.comment
	 */
	public function getCreatorCode(){
		return $this->creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function setCreatorCode($creatorCode){
		$this->creatorCode = $creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function getIsVisibleExternally(){
		return $this->isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function setIsVisibleExternally($isVisibleExternally){
		$this->isVisibleExternally = $isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "Adjustment";
	}
}

?>

