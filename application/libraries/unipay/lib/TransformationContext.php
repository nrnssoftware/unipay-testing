<?php


/*
 *$comment
 */
class TransformationContext {
		/*
	 *@var null
	 */
	private $customerAccounts;
		/*
	 *@var null
	 */
	private $paymentOptions;
		/*
	 *@var null
	 */
	private $addresses;
		/*
	 *@var null
	 */
	private $phones;
		/*
	 *@var null
	 */
	private $emailAddresses;
		/*
	 *@var null
	 */
	private $revenueTransactions;
		/*
	 *@var null
	 */
	private $assetTransactions;
		/*
	 *@var null
	 */
	private $paymentPlans;
		/*
	 *@var null
	 */
	private $notes;
		/*
	 *@var null
	 */
	private $actions;
		/*
	 *@var null
	 */
	private $letters;
		/*
	 *@var null
	 */
	private $emails;
		/*
	 *@var null
	 */
	private $scheduledPayments;
		/*
	 *@var null
	 */
	private $documents;
		/*
	 *@var null
	 */
	private $auditLogs;
		/*
	 *@var null
	 */
	private $signatures;
		/*
	 *@var null
	 */
	private $users;
		/*
	 *@var null
	 */
	private $agentCommissions;
		/*
	 *@var null
	 */
	private $adjustments;
		/*
	 *@var null
	 */
	private $contracts;
		
	/*
	 *$constructor.comment
	 */
	function __construct($sessionContext = null){
		//initialization block
		if (func_num_args() == 0) {
		//default constructor
		$this->customerAccounts = new HashMap();
		$this->paymentOptions = new HashMap();
		$this->revenueTransactions = new HashMap();
		$this->assetTransactions = new HashMap();
		$this->paymentPlans = new HashMap();
		$this->notes = new HashMap();
		$this->actions = new HashMap();
		$this->letters = new HashMap();
		$this->emails = new HashMap();
		$this->documents = new HashMap();
		$this->auditLogs = new HashMap();
		$this->signatures = new HashMap();
		$this->scheduledPayments = new HashMap();
		$this->users = new HashMap();
		$this->agentCommissions = new HashMap();
		$this->adjustments = new HashMap();
		$this->contracts = new HashMap();
		$this->addresses = new HashMap();
		$this->phones = new HashMap();
		$this->emailAddresses = new HashMap();

		return;
		}
		//constructor with parameters
		$this->customerAccounts = $sessionContext->getCustomerAccounts();
		$this->paymentOptions = $sessionContext->getPaymentOptions();
		$this->revenueTransactions = $sessionContext->getRevenueTransactions();
		$this->assetTransactions = $sessionContext->getAssetTransactions();
		$this->paymentPlans = $sessionContext->getPaymentPlans();
		$this->notes = $sessionContext->getNotes();
		$this->actions = $sessionContext->getActions();
		$this->letters = $sessionContext->getLetters();
		$this->emails = $sessionContext->getEmails();
		$this->scheduledPayments = $sessionContext->getScheduledPayments();
		$this->documents = $sessionContext->getDocuments();
		$this->auditLogs = $sessionContext->getAuditLogs();
		$this->signatures = $sessionContext->getSignatures();
		$this->users = $sessionContext->getUsers();
		$this->agentCommissions = $sessionContext->getAgentCommissions();
		$this->contracts = $sessionContext->getContracts();
		$this->adjustments = $sessionContext->getAdjustments();
		$this->addresses = $sessionContext->getAddresses();
		$this->phones = $sessionContext->getPhones();
		$this->emailAddresses = $sessionContext->getEmailAddresses();
	
	}

	/*
	 *$method.comment
	 */
	public function serializeList($merchantAccountCode,$password,$userCode,$list,$config){
		$root = null;
		if (is_null($merchantAccountCode) && is_null($password)){
			$root = new ElementFacade("response");
		}
		else {
			$root = new ElementFacade("request");
			ElementFacade::setAttributesToRoot($root, $merchantAccountCode, $password, $userCode, $config);
		}
		if (is_null($list) || $list->isEmpty()){
			return $root;
		}
		$obj = $list->get(0);
		if (($obj instanceof RevenueTransaction) || ($obj instanceof AssetTransaction)){
			Helper::sort($list);
		}
		foreach ($list as $clientObject){
			if ($clientObject instanceof CustomerAccount){
				$customerAccount = $clientObject;
				$this->addCustomerAccountElement($root, $customerAccount);
				if (!is_null($customerAccount->getDefaultAddress())){
					$this->addAddressElement($root, $customerAccount->getDefaultAddress());
				}
				if (!is_null($customerAccount->getDefaultHomePhone())){
					$this->addPhoneElement($root, $customerAccount->getDefaultHomePhone());
				}
				if (!is_null($customerAccount->getDefaultWorkPhone())){
					$this->addPhoneElement($root, $customerAccount->getDefaultWorkPhone());
				}
				if (!is_null($customerAccount->getDefaultEmailAddress())){
					$this->addEmailAddressElement($root, $customerAccount->getDefaultEmailAddress());
				}
				if (!is_null($customerAccount->getDefaultContract())){
					$this->addContractElement($root, $customerAccount->getDefaultContract());
				}
				if (!is_null($customerAccount->getDefaultPaymentOption())){
					$this->addPaymentOptionElement($root, $customerAccount->getDefaultPaymentOption());
				}
				if (!is_null($customerAccount->getDefaultPaymentPlan())){
					$this->addPaymentPlanElement($root, $customerAccount->getDefaultPaymentPlan());
				}
				foreach ($customerAccount->getAddresses() as $address){
					if (!(($address == $address->getCustomerAccount()->getDefaultAddress()))){
						$this->addAddressElement($root, $address);
					}
				}
				foreach ($customerAccount->getPhones() as $phone){
					if (!(($phone == $phone->getCustomerAccount()->getDefaultHomePhone())) && !(($phone == $phone->getCustomerAccount()->getDefaultWorkPhone()))){
						$this->addPhoneElement($root, $phone);
					}
				}
				foreach ($customerAccount->getEmailAddresses() as $emailAddress){
					if (!(($emailAddress == $emailAddress->getCustomerAccount()->getDefaultEmailAddress()))){
						$this->addEmailAddressElement($root, $emailAddress);
					}
				}
				foreach ($customerAccount->getPaymentOptions() as $paymentOption){
					if (!(($paymentOption == $paymentOption->getCustomerAccount()->getDefaultPaymentOption()))){
						$this->addPaymentOptionElement($root, $paymentOption);
					}
				}
				$revenueTransactionList = $customerAccount->getRevenueTransactions();
				if (!($revenueTransactionList->isEmpty())){
					Helper::sort($revenueTransactionList);
					foreach ($revenueTransactionList as $transaction){
						$this->addRevenueTransactionElement($root, $transaction);
					}
				}
				$assettransactionList = $customerAccount->getAssetTransactions();
				if (!($assettransactionList->isEmpty())){
					Helper::sort($assettransactionList);
					foreach ($assettransactionList as $transaction){
						$this->addAssetTransactionElement($root, $transaction);
					}
				}
				foreach ($customerAccount->getPaymentPlans() as $paymentPlan){
					if (!(($paymentPlan == $paymentPlan->getCustomerAccount()->getDefaultPaymentPlan()))){
						$this->addPaymentPlanElement($root, $paymentPlan);
					}
				}
				foreach ($customerAccount->getNotesList() as $note){
					$this->addNoteElement($root, $note);
				}
				foreach ($customerAccount->getActions() as $action){
					$this->addActionElement($root, $action);
				}
				foreach ($customerAccount->getContracts() as $contract){
					if (!(($contract == $contract->getCustomerAccount()->getDefaultContract()))){
						$this->addContractElement($root, $contract);
					}
				}
				foreach ($customerAccount->getLetters() as $letter){
					$this->addLetterElement($root, $letter);
				}
				foreach ($customerAccount->getEmails() as $email){
					$this->addEmailElement($root, $email);
				}
				foreach ($customerAccount->getScheduledPayments() as $scheduledPayment){
					$this->addScheduledPaymentElement($root, $scheduledPayment);
				}
				foreach ($customerAccount->getDocuments() as $document){
					$this->addDocumentElement($root, $document);
				}
			}
			else {
				if ($clientObject instanceof Address){
					$this->addAddressElement($root, $clientObject);
				}
				else {
					if ($clientObject instanceof Phone){
						$this->addPhoneElement($root, $clientObject);
					}
					else {
						if ($clientObject instanceof EmailAddress){
							$this->addEmailAddressElement($root, $clientObject);
						}
						else {
							if ($clientObject instanceof PaymentOption){
								$this->addPaymentOptionElement($root, $clientObject);
							}
							else {
								if ($clientObject instanceof PaymentPlan){
									$this->addPaymentPlanElement($root, $clientObject);
								}
								else {
									if ($clientObject instanceof RevenueTransaction){
										$this->addRevenueTransactionElement($root, $clientObject);
									}
									else {
										if ($clientObject instanceof AssetTransaction){
											$this->addAssetTransactionElement($root, $clientObject);
										}
										else {
											if ($clientObject instanceof Note){
												$this->addNoteElement($root, $clientObject);
											}
											else {
												if ($clientObject instanceof Action){
													$this->addActionElement($root, $clientObject);
												}
												else {
													if ($clientObject instanceof Contract){
														$this->addContractElement($root, $clientObject);
													}
													else {
														if ($clientObject instanceof Letter){
															$this->addLetterElement($root, $clientObject);
														}
														else {
															if ($clientObject instanceof Email){
																$this->addEmailElement($root, $clientObject);
															}
															else {
																if ($clientObject instanceof ScheduledPayment){
																	$this->addScheduledPaymentElement($root, $clientObject);
																}
																else {
																	if ($clientObject instanceof Document){
																		$this->addDocumentElement($root, $clientObject);
																	}
																	else {
																		if ($clientObject instanceof AuditLog){
																			$this->addAuditLogElement($root, $clientObject);
																		}
																		else {
																			if ($clientObject instanceof Adjustment){
																				$this->addAdjustmentElement($root, $clientObject);
																			}
																			else {
																				if ($clientObject instanceof User){
																					$user = $clientObject;
																					$this->addUserElement($root, $user);
																					if (!is_null($user->getCustomerAccount())){
																						$customerAccount = $user->getCustomerAccount();
																						$this->addCustomerAccountElement($root, $customerAccount);
																						if (!is_null($customerAccount->getDefaultPaymentOption())){
																							$this->addPaymentOptionElement($root, $customerAccount->getDefaultPaymentOption());
																						}
																						if (!is_null($customerAccount->getDefaultPaymentPlan())){
																							$this->addPaymentPlanElement($root, $customerAccount->getDefaultPaymentPlan());
																						}
																					}
																					foreach ($user->getAgentCommissions() as $agentCommission){
																						$this->addAgentCommissionElement($root, $agentCommission);
																					}
																				}
																				else {
																					if ($clientObject instanceof AgentCommission){
																						$agentCommission = $clientObject;
																						$this->addAgentCommissionElement($root, $agentCommission);
																						if (!is_null($agentCommission->getUser())){
																							$this->addUserElement($root, $agentCommission->getUser());
																						}
																						if (!is_null($agentCommission->getCustomerAccount())){
																							$customerAccount = $agentCommission->getCustomerAccount();
																							$this->addCustomerAccountElement($root, $customerAccount);
																							if (!is_null($customerAccount->getDefaultPaymentOption())){
																								$this->addPaymentOptionElement($root, $customerAccount->getDefaultPaymentOption());
																							}
																							if (!is_null($customerAccount->getDefaultPaymentPlan())){
																								$this->addPaymentPlanElement($root, $customerAccount->getDefaultPaymentPlan());
																							}
																						}
																						if (!is_null($agentCommission->getPaymentOption())){
																							$this->addPaymentOptionElement($root, $agentCommission->getPaymentOption());
																						}
																						if (!is_null($agentCommission->getSource())){
																							$this->addRevenueTransactionElement($root, $agentCommission->getSource());
																						}
																						if (!is_null($agentCommission->getTarget())){
																							$this->addAssetTransactionElement($root, $agentCommission->getTarget());
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return $root;
	}
	/*
	 *$method.comment
	 */
	public function deserializeFacade($facade,$isQueryResponse){
		$clientObjects = new LinkedList();
		$elements = $facade->getNodeList();
		$needConvertToCustomerAccountList = true;
		foreach ($elements as $element){
			$transformationHelper = new XMLTransformationHelper($element);
			if ("customerAccount" == $element->getName()){
				$customerAccount = $this->getCustomerAccount($element, "refId");
				$transformationHelper->customerAccountFromXML($customerAccount, $this);
				$clientObjects->add($customerAccount);
			}
			else {
				if ("address" == $element->getName()){
					$address = $this->getAddress($element, "refId");
					$transformationHelper->addressFromXML($address, $this);
					$clientObjects->add($address);
				}
			}
			if ("phone" == $element->getName()){
				$phone = $this->getPhone($element, "refId");
				$transformationHelper->phoneFromXML($phone, $this);
				$clientObjects->add($phone);
			}
			if ("emailAddress" == $element->getName()){
				$emailAddress = $this->getEmailAddress($element, "refId");
				$transformationHelper->emailAddressFromXML($emailAddress, $this);
				$clientObjects->add($emailAddress);
			}
			else {
				if ("paymentOption" == $element->getName()){
					$paymentOption = $this->getPaymentOption($element, "refId");
					$transformationHelper->paymentOptionFromXML($paymentOption, $this);
					$clientObjects->add($paymentOption);
				}
				else {
					if ("revenueTransaction" == $element->getName()){
						$transaction = $this->getRevenueTransaction($element, "refId");
						$transformationHelper->revenueTransactionFromXML($transaction, $this);
						$clientObjects->add($transaction);
					}
					else {
						if ("assetTransaction" == $element->getName()){
							$transaction = $this->getAssetTransaction($element, "refId");
							$transformationHelper->assetTransactionFromXML($transaction, $this);
							$clientObjects->add($transaction);
						}
						else {
							if ("paymentPlan" == $element->getName()){
								$paymentPlan = $this->getPaymentPlan($element, "refId");
								$transformationHelper->paymentPlanFromXML($paymentPlan, $this);
								$clientObjects->add($paymentPlan);
							}
							else {
								if ("note" == $element->getName()){
									$note = $this->getNote($element, "refId");
									$transformationHelper->noteFromXML($note, $this);
									$clientObjects->add($note);
								}
								else {
									if ("action" == $element->getName()){
										$action = $this->getAction($element, "refId");
										$transformationHelper->actionFromXML($action, $this);
										$clientObjects->add($action);
									}
									else {
										if ("letter" == $element->getName()){
											$letter = $this->getLetter($element, "refId");
											$transformationHelper->letterFromXML($letter, $this);
											$clientObjects->add($letter);
										}
										else {
											if ("contract" == $element->getName()){
												$contract = $this->getContract($element, "refId");
												$transformationHelper->contractFromXML($contract, $this);
												$clientObjects->add($contract);
											}
											else {
												if ("email" == $element->getName()){
													$email = $this->getEmail($element, "refId");
													$transformationHelper->emailFromXML($email, $this);
													$clientObjects->add($email);
												}
												else {
													if ("scheduledPayment" == $element->getName()){
														$scheduledPayment = $this->getScheduledPayment($element, "refId");
														$transformationHelper->scheduledPaymentFromXML($scheduledPayment, $this);
														$clientObjects->add($scheduledPayment);
													}
													else {
														if ("document" == $element->getName()){
															$document = $this->getDocument($element, "refId");
															$transformationHelper->documentFromXML($document, $this);
															$clientObjects->add($document);
														}
														else {
															if ("auditLog" == $element->getName()){
																$auditLog = $this->getAuditLog($element, "refId");
																$transformationHelper->auditLogFromXML($auditLog, $this);
																$clientObjects->add($auditLog);
															}
															else {
																if ("adjustment" == $element->getName()){
																	$adjustment = $this->getAdjustment($element, "refId");
																	$transformationHelper->adjustmentFromXML($adjustment, $this);
																	$clientObjects->add($adjustment);
																}
																else {
																	if ("user" == $element->getName()){
																		$user = $this->getUser($element, "userName");
																		$transformationHelper->userFromXML($user, $this);
																		$clientObjects->add($user);
																		$needConvertToCustomerAccountList = false;
																	}
																	else {
																		if ("agentCommission" == $element->getName()){
																			$agentCommission = $this->getAgentCommission($element, "refId");
																			$transformationHelper->agentCommissionFromXML($agentCommission, $this);
																			$user = $this->getUser($element, "user");
																			if (!is_null($user)){
																				$user->addAgentCommission($agentCommission);
																			}
																			$clientObjects->add($agentCommission);
																			$needConvertToCustomerAccountList = false;
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		$this->checkAndReplace($clientObjects);
		return ($isQueryResponse || !($needConvertToCustomerAccountList)) ? $clientObjects : Helper::customerAccountListToClientObjectList($this->takeCustomerAccounts());
	}
	/*
	 *$method.comment
	 */
	public function deserializeList($list,$isQueryResponse){
		$clientObjects = new LinkedList();
		$needConvertToCustomerAccountList = true;
		if (is_null($list)){
			return $clientObjects;
		}
		foreach ($list as $clientObject){
			if ($clientObject instanceof CustomerAccount){
				$customerAccount = $this->getCustomerAccountBasic($clientObject->getRefId(), $clientObject);
				$this->synchronizeClientObject($clientObject, $customerAccount);
				$clientObjects->add($customerAccount);
				$this->checkAndReplaceCustomerAccount($customerAccount);
				$customerAccount->clear();
				$customer = $clientObject;
				if (!is_null($customer->getDefaultAddress())){
					$defaultAddress = $this->deserializeAddress($customer->getDefaultAddress());
					$customerAccount->setDefaultAddress($defaultAddress);
				}
				if (!is_null($customer->getDefaultHomePhone())){
					$defaultHomePhone = $this->deserializePhone($customer->getDefaultHomePhone());
					$customerAccount->setDefaultHomePhone($defaultHomePhone);
				}
				if (!is_null($customer->getDefaultWorkPhone())){
					$defaultWorkPhone = $this->deserializePhone($customer->getDefaultWorkPhone());
					$customerAccount->setDefaultWorkPhone($defaultWorkPhone);
				}
				if (!is_null($customer->getDefaultCellPhone())){
					$defaultCellPhone = $this->deserializePhone($customer->getDefaultCellPhone());
					$customerAccount->setDefaultCellPhone($defaultCellPhone);
				}
				if (!is_null($customer->getDefaultEmailAddress())){
					$defaultEmailAddress = $this->deserializeEmailAddress($customer->getDefaultEmailAddress());
					$customerAccount->setDefaultEmailAddress($defaultEmailAddress);
				}
				if (!is_null($customer->getDefaultContract())){
					$defaultContract = $this->deserializeContract($customer->getDefaultContract());
					$customerAccount->setDefaultContract($defaultContract);
				}
				if (!is_null($customer->getDefaultPaymentPlan())){
					$defaultPaymentPlan = $this->deserializePaymentPlan($customer->getDefaultPaymentPlan());
					$customerAccount->setDefaultPaymentPlan($defaultPaymentPlan);
				}
				if (!is_null($customer->getDefaultPaymentOption())){
					$defaultPaymentOption = $this->deserializePaymentOption($customer->getDefaultPaymentOption());
					$customerAccount->setDefaultPaymentOption($defaultPaymentOption);
				}
				foreach ($customer->getAddresses() as $address){
					$a = $this->deserializeAddress($address);
					if (!($customerAccount->getAddresses()->contains($a))){
						$customerAccount->getAddresses()->add($a);
					}
				}
				foreach ($customer->getPhones() as $phone){
					$p = $this->deserializePhone($phone);
					if (!($customerAccount->getPhones()->contains($p))){
						$customerAccount->getPhones()->add($p);
					}
				}
				foreach ($customer->getEmailAddresses() as $emailAddress){
					$a = $this->deserializeEmailAddress($emailAddress);
					if (!($customerAccount->getEmailAddresses()->contains($a))){
						$customerAccount->getEmailAddresses()->add($a);
					}
				}
				foreach ($customer->getPaymentOptions() as $paymentOption){
					$option = $this->deserializePaymentOption($paymentOption);
					if (!($customerAccount->getPaymentOptions()->contains($option))){
						$customerAccount->getPaymentOptions()->add($option);
					}
				}
				foreach ($customer->getPaymentPlans() as $paymentPlan){
					$plan = $this->deserializePaymentPlan($paymentPlan);
					if (!($customerAccount->getPaymentPlans()->contains($plan))){
						$customerAccount->getPaymentPlans()->add($plan);
					}
				}
				foreach ($customer->getRevenueTransactions() as $revenueTransaction){
					$transaction = $this->deserializeRevenueTransaction($revenueTransaction);
					if (!($customerAccount->getRevenueTransactions()->contains($transaction))){
						$customerAccount->addRevenueTransaction($transaction);
					}
				}
				foreach ($customer->getAssetTransactions() as $assetTransaction){
					$transaction = $this->deserializeAssetTransaction($assetTransaction);
					if (!($customerAccount->getAssetTransactions()->contains($transaction))){
						$customerAccount->addAssetTransaction($transaction);
					}
				}
				foreach ($customer->getActions() as $action){
					$a = $this->deserializeAction($action);
					if (!($customerAccount->getActions()->contains($a))){
						$customerAccount->getActions()->add($a);
					}
				}
				foreach ($customer->getNotesList() as $note){
					$n = $this->deserializeNote($note);
					if (!($customerAccount->getNotesList()->contains($n))){
						$customerAccount->getNotesList()->add($n);
					}
				}
				foreach ($customer->getLetters() as $letter){
					$l = $this->deserializeLetter($letter);
					if (!($customerAccount->getLetters()->contains($l))){
						$customerAccount->getLetters()->add($l);
					}
				}
				foreach ($customer->getEmails() as $email){
					$e = $this->deserializeEmail($email);
					if (!($customerAccount->getEmails()->contains($e))){
						$customerAccount->getEmails()->add($e);
					}
				}
				foreach ($customer->getScheduledPayments() as $scheduledPayment){
					$sp = $this->deserializeScheduledPayment($scheduledPayment);
					if (!($customerAccount->getScheduledPayments()->contains($sp))){
						$customerAccount->getScheduledPayments()->add($sp);
					}
				}
				foreach ($customer->getDocuments() as $document){
					$d = $this->deserializeDocument($document);
					if (!($customerAccount->getDocuments()->contains($d))){
						$customerAccount->getDocuments()->add($d);
					}
				}
				foreach ($customer->getAuditLogs() as $document){
					$a = $this->deserializeAuditLog($document);
					if (!($customerAccount->getAuditLogs()->contains($a))){
						$customerAccount->getAuditLogs()->add($a);
					}
				}
				foreach ($customer->getSignatures() as $signature){
					$s = $this->deserializeSignature($signature);
					if (!($customerAccount->getSignatures()->contains($s))){
						$customerAccount->getSignatures()->add($s);
					}
				}
				foreach ($customer->getContracts() as $contract){
					$c = $this->deserializeContract($contract);
					if (!($customerAccount->getContracts()->contains($c))){
						$customerAccount->getContracts()->add($c);
					}
				}
				foreach ($customer->getAdjustments() as $adjustment){
					$a = $this->deserializeAdjustment($adjustment);
					if (!($customerAccount->getAdjustments()->contains($a))){
						$customerAccount->getAdjustments()->add($a);
					}
				}
			}
			else {
				if ($clientObject instanceof Address){
					$clientObjects->add($this->deserializeAddress($clientObject));
				}
				else {
					if ($clientObject instanceof Phone){
						$clientObjects->add($this->deserializePhone($clientObject));
					}
					else {
						if ($clientObject instanceof EmailAddress){
							$clientObjects->add($this->deserializeEmailAddress($clientObject));
						}
						else {
							if ($clientObject instanceof PaymentOption){
								$clientObjects->add($this->deserializePaymentOption($clientObject));
							}
							else {
								if ($clientObject instanceof PaymentPlan){
									$clientObjects->add($this->deserializePaymentPlan($clientObject));
								}
								else {
									if ($clientObject instanceof RevenueTransaction){
										$clientObjects->add($this->deserializeRevenueTransaction($clientObject));
									}
									else {
										if ($clientObject instanceof AssetTransaction){
											$clientObjects->add($this->deserializeAssetTransaction($clientObject));
										}
										else {
											if ($clientObject instanceof Action){
												$clientObjects->add($this->deserializeAction($clientObject));
											}
											else {
												if ($clientObject instanceof Contract){
													$clientObjects->add($this->deserializeContract($clientObject));
												}
												else {
													if ($clientObject instanceof Note){
														$clientObjects->add($this->deserializeNote($clientObject));
													}
													else {
														if ($clientObject instanceof Letter){
															$clientObjects->add($this->deserializeLetter($clientObject));
														}
														else {
															if ($clientObject instanceof Email){
																$clientObjects->add($this->deserializeEmail($clientObject));
															}
															else {
																if ($clientObject instanceof ScheduledPayment){
																	$clientObjects->add($this->deserializeScheduledPayment($clientObject));
																}
																else {
																	if ($clientObject instanceof Document){
																		$clientObjects->add($this->deserializeDocument($clientObject));
																	}
																	else {
																		if ($clientObject instanceof AuditLog){
																			$clientObjects->add($this->deserializeAuditLog($clientObject));
																		}
																		else {
																			if ($clientObject instanceof Signature){
																				$clientObjects->add($this->deserializeSignature($clientObject));
																			}
																			else {
																				if ($clientObject instanceof Adjustment){
																					$clientObjects->add($this->deserializeAdjustment($clientObject));
																				}
																				else {
																					if ($clientObject instanceof User){
																						$clientObjects->add($this->deserializeUser($clientObject));
																						$needConvertToCustomerAccountList = false;
																					}
																					else {
																						if ($clientObject instanceof AgentCommission){
																							$clientObjects->add($this->deserializeAgentCommission($clientObject));
																							$needConvertToCustomerAccountList = false;
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return ($isQueryResponse || !($needConvertToCustomerAccountList)) ? $clientObjects : Helper::customerAccountListToClientObjectList($this->takeCustomerAccounts());
	}
	/*
	 *$method.comment
	 */
	private function deserializePaymentOption($clientObject){
		$paymentOption = $this->getPaymentOptionBasic($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $paymentOption);
		$customerAccount = $this->getCustomerAccountBasic($paymentOption->getCustomerAccount()->getId(), $paymentOption->getCustomerAccount());
		$paymentOption->setCustomerAccount($customerAccount);
		if (!($customerAccount->getPaymentOptions()->contains($paymentOption))){
			$customerAccount->addPaymentOption($paymentOption);
		}
		$this->checkAndReplacePaymentOption($paymentOption);
		return $paymentOption;
	}
	/*
	 *$method.comment
	 */
	private function deserializeAddress($clientObject){
		$address = $this->getAddressBasic($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $address);
		$customerAccount = $this->getCustomerAccountBasic($address->getCustomerAccount()->getId(), $address->getCustomerAccount());
		$address->setCustomerAccount($customerAccount);
		if (!($customerAccount->getAddresses()->contains($address))){
			$customerAccount->addAddress($address);
		}
		$this->checkAndReplaceAddress($address);
		return $address;
	}
	/*
	 *$method.comment
	 */
	private function deserializePhone($clientObject){
		$phone = $this->getPhoneBasic($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $phone);
		$customerAccount = $this->getCustomerAccountBasic($phone->getCustomerAccount()->getId(), $phone->getCustomerAccount());
		$phone->setCustomerAccount($customerAccount);
		if (!($customerAccount->getPhones()->contains($phone))){
			$customerAccount->addPhone($phone);
		}
		$this->checkAndReplacePhone($phone);
		return $phone;
	}
	/*
	 *$method.comment
	 */
	private function deserializeEmailAddress($clientObject){
		$emailAddress = $this->getEmailAddressBasic($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $emailAddress);
		$customerAccount = $this->getCustomerAccountBasic($emailAddress->getCustomerAccount()->getId(), $emailAddress->getCustomerAccount());
		$emailAddress->setCustomerAccount($customerAccount);
		if (!($customerAccount->getEmailAddresses()->contains($emailAddress))){
			$customerAccount->addEmailAddress($emailAddress);
		}
		$this->checkAndReplaceEmailAddress($emailAddress);
		return $emailAddress;
	}
	/*
	 *$method.comment
	 */
	private function deserializePaymentPlan($clientObject){
		$paymentPlan = $this->getPaymentPlanBasic($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $paymentPlan);
		$customerAccount = $this->getCustomerAccountBasic($paymentPlan->getCustomerAccount()->getId(), $paymentPlan->getCustomerAccount());
		$paymentPlan->setCustomerAccount($customerAccount);
		if (!is_null($paymentPlan->getContract())){
			$paymentPlan->setContract($this->getContractBasic($paymentPlan->getContract()->getId(), $paymentPlan->getContract()->getRefId(), $paymentPlan->getContract()));
		}
		if (!is_null($paymentPlan->getPaymentOption())){
			$paymentPlan->setPaymentOption($this->getPaymentOptionBasic($paymentPlan->getPaymentOption()->getRefId(), $paymentPlan->getPaymentOption()));
		}
		if (!is_null($paymentPlan->getLinkedParentPlan())){
			$paymentPlan->setLinkedParentPlan($this->getPaymentPlanBasic($paymentPlan->getLinkedParentPlan()->getRefId(), $paymentPlan->getLinkedParentPlan()));
		}
		if (!($customerAccount->getPaymentPlans()->contains($paymentPlan))){
			$customerAccount->addPaymentPlan($paymentPlan);
		}
		$this->checkAndReplacePaymentPlan($paymentPlan);
		return $paymentPlan;
	}
	/*
	 *$method.comment
	 */
	private function deserializeRevenueTransaction($clientObject){
		$transaction = $this->getRevenueTransactionBasic($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $transaction);
		$customerAccount = $this->getCustomerAccountBasic($transaction->getCustomerAccount()->getId(), $transaction->getCustomerAccount());
		$transaction->setCustomerAccount($customerAccount);
		if (!is_null($transaction->getAdjustmentTransaction())){
			$transaction->setAdjustmentTransaction($this->getRevenueTransactionBasic($transaction->getAdjustmentTransaction()->getRefId(), $transaction->getAdjustmentTransaction()));
		}
		if (!($customerAccount->getRevenueTransactions()->contains($transaction))){
			$customerAccount->addRevenueTransaction($transaction);
		}
		$this->checkAndReplaceRevenueTransaction($transaction);
		return $transaction;
	}
	/*
	 *$method.comment
	 */
	private function deserializeAssetTransaction($clientObject){
		$transaction = $this->getAssetTransactionBasic($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $transaction);
		$customerAccount = $this->getCustomerAccountBasic($transaction->getCustomerAccount()->getId(), $transaction->getCustomerAccount());
		$transaction->setCustomerAccount($customerAccount);
		if (!is_null($transaction->getAdjustmentTransaction())){
			$transaction->setAdjustmentTransaction($this->getAssetTransactionBasic($transaction->getAdjustmentTransaction()->getRefId(), $transaction->getAdjustmentTransaction()));
		}
		if (!($customerAccount->getAssetTransactions()->contains($transaction))){
			$customerAccount->addAssetTransaction($transaction);
		}
		$this->checkAndReplaceAssetTransaction($transaction);
		return $transaction;
	}
	/*
	 *$method.comment
	 */
	private function deserializeNote($clientObject){
		$note = $this->getNoteBasic($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $note);
		$customerAccount = $this->getCustomerAccountBasic($note->getCustomerAccount()->getId(), $note->getCustomerAccount());
		$note->setCustomerAccount($customerAccount);
		if (!($customerAccount->getNotesList()->contains($note))){
			$customerAccount->addNote($note);
		}
		$this->checkAndReplaceNote($note);
		return $note;
	}
	/*
	 *$method.comment
	 */
	private function deserializeAction($clientObject){
		$action = $this->getActionBasic($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $action);
		$customerAccount = $this->getCustomerAccountBasic($action->getCustomerAccount()->getId(), $action->getCustomerAccount());
		$action->setCustomerAccount($customerAccount);
		if (!($customerAccount->getActions()->contains($action))){
			$customerAccount->addAction($action);
		}
		$this->checkAndReplaceAction($action);
		return $action;
	}
	/*
	 *$method.comment
	 */
	private function deserializeLetter($clientObject){
		$letter = $this->getLetterBasic($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $letter);
		$customerAccount = $this->getCustomerAccountBasic($letter->getCustomerAccount()->getId(), $letter->getCustomerAccount());
		$letter->setCustomerAccount($customerAccount);
		if (!($customerAccount->getLetters()->contains($letter))){
			$customerAccount->addLetter($letter);
		}
		$this->checkAndReplaceLetter($letter);
		return $letter;
	}
	/*
	 *$method.comment
	 */
	private function deserializeEmail($clientObject){
		$email = $this->getEmailBasic($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $email);
		$customerAccount = $this->getCustomerAccountBasic($email->getCustomerAccount()->getId(), $email->getCustomerAccount());
		$email->setCustomerAccount($customerAccount);
		if (!($customerAccount->getEmails()->contains($email))){
			$customerAccount->addEmail($email);
		}
		$this->checkAndReplaceEmail($email);
		return $email;
	}
	/*
	 *$method.comment
	 */
	private function deserializeScheduledPayment($clientObject){
		$scheduledPayment = $this->getScheduledPaymentBasic($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $scheduledPayment);
		$customerAccount = $this->getCustomerAccountBasic($scheduledPayment->getCustomerAccount()->getId(), $scheduledPayment->getCustomerAccount());
		$scheduledPayment->setCustomerAccount($customerAccount);
		if (!($customerAccount->getScheduledPayments()->contains($scheduledPayment))){
			$customerAccount->addScheduledPayment($scheduledPayment);
		}
		$this->checkAndReplaceScheduledPayment($scheduledPayment);
		return $scheduledPayment;
	}
	/*
	 *$method.comment
	 */
	private function deserializeDocument($clientObject){
		$document = $this->getDocumentBasic($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $document);
		$customerAccount = $this->getCustomerAccountBasic($document->getCustomerAccount()->getId(), $document->getCustomerAccount());
		$document->setCustomerAccount($customerAccount);
		if (!($customerAccount->getDocuments()->contains($document))){
			$customerAccount->addDocument($document);
		}
		$this->checkAndReplaceDocument($document);
		return $document;
	}
	/*
	 *$method.comment
	 */
	private function deserializeAuditLog($clientObject){
		$auditLog = $this->getAuditLogBasic($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $auditLog);
		$customerAccount = $this->getCustomerAccountBasic($auditLog->getCustomerAccount()->getId(), $auditLog->getCustomerAccount());
		$auditLog->setCustomerAccount($customerAccount);
		if (!($customerAccount->getAuditLogs()->contains($auditLog))){
			$customerAccount->addAuditLog($auditLog);
		}
		$this->checkAndReplaceAuditLog($auditLog);
		return $auditLog;
	}
	/*
	 *$method.comment
	 */
	private function deserializeSignature($clientObject){
		$signature = $this->getSignature($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $signature);
		$customerAccount = $this->getCustomerAccountBasic($signature->getCustomerAccount()->getId(), $signature->getCustomerAccount());
		$signature->setCustomerAccount($customerAccount);
		if (!($customerAccount->getSignatures()->contains($signature))){
			$customerAccount->addSignature($signature);
		}
		$this->checkAndReplaceSignature($signature);
		return $signature;
	}
	/*
	 *$method.comment
	 */
	private function deserializeContract($clientObject){
		$contract = $this->getContractBasic($clientObject->getId(), $clientObject->getRefId(), $clientObject);
		if (is_null($contract)){
			$contract = $this->getContractBasic($clientObject->getId(), $clientObject->getRefId(), $clientObject);
		}
		$this->synchronizeClientObject($clientObject, $contract);
		$customerAccount = $this->getCustomerAccountBasic($contract->getCustomerAccount()->getId(), $contract->getCustomerAccount());
		$contract->setCustomerAccount($customerAccount);
		if (!is_null($contract->getDefaultPaymentPlan())){
			$contract->setDefaultPaymentPlan($this->getPaymentPlanBasic($contract->getDefaultPaymentPlan()->getRefId(), $contract->getDefaultPaymentPlan()));
		}
		if (!($customerAccount->getContracts()->contains($contract))){
			$customerAccount->addContract($contract);
		}
		$this->checkAndReplaceContract($contract);
		return $contract;
	}
	/*
	 *$method.comment
	 */
	private function deserializeAdjustment($clientObject){
		$adjustment = $this->getAdjustmentBasic($clientObject->getId(), $clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $adjustment);
		$customerAccount = $this->getCustomerAccountBasic($adjustment->getCustomerAccount()->getId(), $adjustment->getCustomerAccount());
		$adjustment->setCustomerAccount($customerAccount);
		if (!is_null($adjustment->getPaymentPlan())){
			$adjustment->setPaymentPlan($this->getPaymentPlanBasic($adjustment->getPaymentPlan()->getRefId(), $adjustment->getPaymentPlan()));
		}
		if (!is_null($adjustment->getContract())){
			$adjustment->setContract($this->getContractBasic($adjustment->getContract()->getId(), $adjustment->getContract()->getRefId(), $adjustment->getContract()));
		}
		if (!($customerAccount->getAdjustments()->contains($adjustment))){
			$customerAccount->addAdjustment($adjustment);
		}
		$this->checkAndReplaceAdjustment($adjustment);
		return $adjustment;
	}
	/*
	 *$method.comment
	 */
	private function deserializeUser($clientObject){
		$user = $this->getUserBasic($clientObject->getUserName(), $clientObject);
		$this->synchronizeClientObject($clientObject, $user);
		if (!is_null($user->getCustomerAccount())){
			$customerAccount = $this->getCustomerAccountBasic($user->getCustomerAccount()->getId(), $user->getCustomerAccount());
			$user->setCustomerAccount($customerAccount);
		}
		return $user;
	}
	/*
	 *$method.comment
	 */
	private function deserializeAgentCommission($clientObject){
		$agentCommission = $this->getAgentCommissionBasic($clientObject->getRefId(), $clientObject);
		$this->synchronizeClientObject($clientObject, $agentCommission);
		$user = $this->getUserBasic($agentCommission->getAgentCode(), $agentCommission->getUser());
		if (!is_null($user)){
			$agentCommission->setUser($user);
			$user->addAgentCommission($agentCommission);
		}
		if (!is_null($agentCommission->getCustomerAccount())){
			$customerAccount = $this->getCustomerAccountBasic($agentCommission->getCustomerAccount()->getId(), $agentCommission->getCustomerAccount());
			$agentCommission->setCustomerAccount($customerAccount);
		}
		if (!is_null($agentCommission->getPaymentOption())){
			$paymentOption = $this->getPaymentOptionBasic($agentCommission->getPaymentOption()->getRefId(), $agentCommission->getPaymentOption());
			$agentCommission->setPaymentOption($paymentOption);
		}
		if (!is_null($agentCommission->getSource())){
			$source = $this->getRevenueTransactionBasic($agentCommission->getSource()->getRefId(), $agentCommission->getSource());
			$agentCommission->setSource($source);
		}
		if (!is_null($agentCommission->getTarget())){
			$target = $this->getAssetTransactionBasic($agentCommission->getTarget()->getRefId(), $agentCommission->getTarget());
			$agentCommission->setTarget($target);
		}
		$this->checkAndReplaceAgentCommission($agentCommission);
		return $agentCommission;
	}
	/*
	 *$method.comment
	 */
	public function deserializeMapConfig($facade,$config){
		$transformationHelper = new XMLTransformationHelper($facade);
		$transformationHelper->getAttributeFromRoot($config);
	}
	/*
	 *$method.comment
	 */
	public function serealizeQuery($merchantAccountCode,$password,$queryName,$parameters,$config){
		$root = new ElementFacade("request");
		ElementFacade::setAttributesToRoot($root, $merchantAccountCode, $password, null, $config);
		$query = $root->createChild($queryName);
		XMLTransformationHelper::queryParametersToXML($query, $parameters);
		return $root;
	}
	/*
	 *$method.comment
	 */
	public function deserealizeQueryParameters($facade){
		$queryName = $this->getQueryName($facade);
		$transformationHelper = new XMLTransformationHelper($facade->getChild($queryName));
		if (SessionConnection::FIND_CUSTOMER_ACCOUNT == $queryName){
			return $transformationHelper->getCustomerAccountParameters();
		}
		else {
			if (SessionConnection::FIND_PAYMENT_OPTION == $queryName){
				return $transformationHelper->getPaymentOptionParameters();
			}
			else {
				if (SessionConnection::FIND_PAYMENT_PLAN == $queryName){
					return $transformationHelper->getPaymentPlanParameters();
				}
				else {
					if (SessionConnection::FIND_ACCOUNT_TRANSACTION == $queryName){
						return $transformationHelper->getAccountTransactionParameters();
					}
					else {
						if (SessionConnection::FIND_ADDRESS == $queryName){
							return $transformationHelper->getAddressParameters();
						}
						else {
							if (SessionConnection::FIND_PHONE == $queryName){
								return $transformationHelper->getPhoneParameters();
							}
							else {
								if (SessionConnection::FIND_EMAIL_ADDRESS == $queryName){
									return $transformationHelper->getEmailAddressParameters();
								}
								else {
									if (SessionConnection::LOAD_CUSTOMER_ACCOUNT == $queryName){
										return $transformationHelper->getLoadCustomerAccountParameters();
									}
									else {
										if (SessionConnection::LOAD_OBJECT == $queryName){
											return $transformationHelper->getLoadObjectParameters();
										}
										else {
											if (SessionConnection::FIND_NOTE == $queryName){
												return $transformationHelper->getNoteParameters();
											}
											else {
												if (SessionConnection::FIND_ACTION == $queryName){
													return $transformationHelper->getActionParameters();
												}
												else {
													if (SessionConnection::FIND_LETTER == $queryName){
														return $transformationHelper->getLetterParameters();
													}
													else {
														if (SessionConnection::FIND_EMAIL == $queryName){
															return $transformationHelper->getEmailParameters();
														}
														else {
															if (SessionConnection::FIND_SCHEDULED_PAYMENT == $queryName){
																return $transformationHelper->getScheduledPaymentParameters();
															}
															else {
																if (SessionConnection::FIND_DOCUMENT == $queryName){
																	return $transformationHelper->getDocumentParameters();
																}
																else {
																	if (SessionConnection::FIND_AUDIT_LOG == $queryName){
																		return $transformationHelper->getAuditLogParameters();
																	}
																	else {
																		if (SessionConnection::FIND_CUSTOMER_ACCOUNT_SPECIAL == $queryName){
																			return $transformationHelper->getCustomerAccountSpecialParameters();
																		}
																		else {
																			if (SessionConnection::FIND_USER == $queryName){
																				return $transformationHelper->getUserParameters();
																			}
																			else {
																				if (SessionConnection::FIND_AGENT_COMMISION == $queryName){
																					return $transformationHelper->getAgentCommissionParameters();
																				}
																				else {
																					if (SessionConnection::FIND_CONTRACT == $queryName){
																						return $transformationHelper->getContractParameters();
																					}
																					else {
																						if (SessionConnection::FIND_ADJUSTMENT == $queryName){
																							return $transformationHelper->getAdjustmentParameters();
																						}
																						else {
																							throw new Exception("Unknown find method.");
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	/*
	 *$method.comment
	 */
	public function getMerchantAccountCode($facade){
		return !is_null($facade->getAttributeValue("merchantAccountCode")) ? $facade->getInteger("merchantAccountCode") : null;
	}
	/*
	 *$method.comment
	 */
	public function getPassword($facade){
		return $facade->getAttributeValue("password");
	}
	/*
	 *$method.comment
	 */
	public function getUserCode($facade){
		return $facade->getAttributeValue("userCode");
	}
	/*
	 *$method.comment
	 */
	public function getQueryName($facade){
		$queryName = null;
		if (!is_null($facade->getChild(SessionConnection::FIND_CUSTOMER_ACCOUNT))){
			$queryName = SessionConnection::FIND_CUSTOMER_ACCOUNT;
		}
		else {
			if (!is_null($facade->getChild(SessionConnection::LOAD_CUSTOMER_ACCOUNT))){
				$queryName = SessionConnection::LOAD_CUSTOMER_ACCOUNT;
			}
			else {
				if (!is_null($facade->getChild(SessionConnection::FIND_PAYMENT_OPTION))){
					$queryName = SessionConnection::FIND_PAYMENT_OPTION;
				}
				else {
					if (!is_null($facade->getChild(SessionConnection::FIND_PAYMENT_PLAN))){
						$queryName = SessionConnection::FIND_PAYMENT_PLAN;
					}
					else {
						if (!is_null($facade->getChild(SessionConnection::FIND_ACCOUNT_TRANSACTION))){
							$queryName = SessionConnection::FIND_ACCOUNT_TRANSACTION;
						}
						else {
							if (!is_null($facade->getChild(SessionConnection::LOAD_OBJECT))){
								$queryName = SessionConnection::LOAD_OBJECT;
							}
							else {
								if (!is_null($facade->getChild(SessionConnection::FIND_NOTE))){
									$queryName = SessionConnection::FIND_NOTE;
								}
								else {
									if (!is_null($facade->getChild(SessionConnection::FIND_ACTION))){
										$queryName = SessionConnection::FIND_ACTION;
									}
									else {
										if (!is_null($facade->getChild(SessionConnection::FIND_LETTER))){
											$queryName = SessionConnection::FIND_LETTER;
										}
										else {
											if (!is_null($facade->getChild(SessionConnection::FIND_EMAIL))){
												$queryName = SessionConnection::FIND_EMAIL;
											}
											else {
												if (!is_null($facade->getChild(SessionConnection::FIND_SCHEDULED_PAYMENT))){
													$queryName = SessionConnection::FIND_SCHEDULED_PAYMENT;
												}
												else {
													if (!is_null($facade->getChild(SessionConnection::FIND_DOCUMENT))){
														$queryName = SessionConnection::FIND_DOCUMENT;
													}
													else {
														if (!is_null($facade->getChild(SessionConnection::FIND_AUDIT_LOG))){
															$queryName = SessionConnection::FIND_AUDIT_LOG;
														}
														else {
															if (!is_null($facade->getChild(SessionConnection::FIND_CUSTOMER_ACCOUNT_SPECIAL))){
																$queryName = SessionConnection::FIND_CUSTOMER_ACCOUNT_SPECIAL;
															}
															else {
																if (!is_null($facade->getChild(SessionConnection::FIND_AGENT_COMMISION))){
																	$queryName = SessionConnection::FIND_AGENT_COMMISION;
																}
																else {
																	if (!is_null($facade->getChild(SessionConnection::FIND_USER))){
																		$queryName = SessionConnection::FIND_USER;
																	}
																	else {
																		if (!is_null($facade->getChild(SessionConnection::FIND_CONTRACT))){
																			$queryName = SessionConnection::FIND_CONTRACT;
																		}
																		else {
																			if (!is_null($facade->getChild(SessionConnection::FIND_ADJUSTMENT))){
																				$queryName = SessionConnection::FIND_ADJUSTMENT;
																			}
																			else {
																				if (!is_null($facade->getChild(SessionConnection::FIND_ADDRESS))){
																					$queryName = SessionConnection::FIND_ADDRESS;
																				}
																				else {
																					if (!is_null($facade->getChild(SessionConnection::FIND_PHONE))){
																						$queryName = SessionConnection::FIND_PHONE;
																					}
																					else {
																						if (!is_null($facade->getChild(SessionConnection::FIND_EMAIL_ADDRESS))){
																							$queryName = SessionConnection::FIND_EMAIL_ADDRESS;
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return $queryName;
	}
	/*
	 *$method.comment
	 */
	public function isQueryRequest($facade){
		return !is_null($this->getQueryName($facade));
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->customerAccounts->containsKey($refId) ? $this->customerAccounts->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new CustomerAccount();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->customerAccounts->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccountBasic($refId,$customerAccount){
		$clientObject = $this->customerAccounts->containsKey($refId) ? $this->customerAccounts->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->customerAccounts->put($refId, $customerAccount);
		return $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentOption($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->paymentOptions->containsKey($refId) ? $this->paymentOptions->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new PaymentOption();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->paymentOptions->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getAddress($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->addresses->containsKey($refId) ? $this->addresses->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new Address();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->addresses->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getPhone($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->phones->containsKey($refId) ? $this->phones->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new Phone();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->phones->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getEmailAddress($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->emailAddresses->containsKey($refId) ? $this->emailAddresses->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new EmailAddress();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->emailAddresses->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentOptionBasic($refId,$paymentOption){
		$clientObject = $this->paymentOptions->containsKey($refId) ? $this->paymentOptions->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->paymentOptions->put($refId, $paymentOption);
		return $paymentOption;
	}
	/*
	 *$method.comment
	 */
	public function getRevenueTransaction($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->revenueTransactions->containsKey($refId) ? $this->revenueTransactions->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new RevenueTransaction();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->revenueTransactions->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getRevenueTransactionBasic($refId,$transaction){
		$clientObject = $this->revenueTransactions->containsKey($refId) ? $this->revenueTransactions->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->revenueTransactions->put($refId, $transaction);
		return $transaction;
	}
	/*
	 *$method.comment
	 */
	public function getAssetTransaction($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->assetTransactions->containsKey($refId) ? $this->assetTransactions->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new AssetTransaction();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->assetTransactions->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getAssetTransactionBasic($refId,$transaction){
		$clientObject = $this->assetTransactions->containsKey($refId) ? $this->assetTransactions->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->assetTransactions->put($refId, $transaction);
		return $transaction;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentPlan($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->paymentPlans->containsKey($refId) ? $this->paymentPlans->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new PaymentPlan();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->paymentPlans->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentPlanBasic($refId,$paymentPlan){
		$clientObject = $this->paymentPlans->containsKey($refId) ? $this->paymentPlans->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->paymentPlans->put($refId, $paymentPlan);
		return $paymentPlan;
	}
	/*
	 *$method.comment
	 */
	public function getAddressBasic($refId,$address){
		$clientObject = $this->addresses->containsKey($refId) ? $this->addresses->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->addresses->put($refId, $address);
		return $address;
	}
	/*
	 *$method.comment
	 */
	public function getPhoneBasic($refId,$phone){
		$clientObject = $this->phones->containsKey($refId) ? $this->phones->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->phones->put($refId, $phone);
		return $phone;
	}
	/*
	 *$method.comment
	 */
	public function getEmailAddressBasic($refId,$emailAddress){
		$clientObject = $this->emailAddresses->containsKey($refId) ? $this->emailAddresses->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->emailAddresses->put($refId, $emailAddress);
		return $emailAddress;
	}
	/*
	 *$method.comment
	 */
	public function getNote($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->notes->containsKey($refId) ? $this->notes->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new Note();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->notes->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getNoteBasic($refId,$note){
		$clientObject = $this->notes->containsKey($refId) ? $this->notes->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->notes->put($refId, $note);
		return $note;
	}
	/*
	 *$method.comment
	 */
	public function getAction($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->actions->containsKey($refId) ? $this->actions->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new Action();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->actions->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getActionBasic($refId,$action){
		$clientObject = $this->actions->containsKey($refId) ? $this->actions->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->actions->put($refId, $action);
		return $action;
	}
	/*
	 *$method.comment
	 */
	public function getLetter($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->letters->containsKey($refId) ? $this->letters->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new Letter();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->letters->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getLetterBasic($refId,$letter){
		$clientObject = $this->letters->containsKey($refId) ? $this->letters->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->letters->put($refId, $letter);
		return $letter;
	}
	/*
	 *$method.comment
	 */
	public function getEmail($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->emails->containsKey($refId) ? $this->emails->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new Email();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->emails->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getEmailBasic($refId,$email){
		$clientObject = $this->emails->containsKey($refId) ? $this->emails->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->emails->put($refId, $email);
		return $email;
	}
	/*
	 *$method.comment
	 */
	public function getScheduledPayment($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->scheduledPayments->containsKey($refId) ? $this->scheduledPayments->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new ScheduledPayment();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->scheduledPayments->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getScheduledPaymentBasic($refId,$scheduledPayment){
		$clientObject = $this->scheduledPayments->containsKey($refId) ? $this->scheduledPayments->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->scheduledPayments->put($refId, $scheduledPayment);
		return $scheduledPayment;
	}
	/*
	 *$method.comment
	 */
	public function getDocument($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->documents->containsKey($refId) ? $this->documents->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new Document();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->documents->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getAuditLog($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->auditLogs->containsKey($refId) ? $this->auditLogs->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new AuditLog();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->auditLogs->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getDocumentBasic($refId,$document){
		$clientObject = $this->documents->containsKey($refId) ? $this->documents->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->documents->put($refId, $document);
		return $document;
	}
	/*
	 *$method.comment
	 */
	public function getAuditLogBasic($refId,$auditLog){
		$clientObject = $this->auditLogs->containsKey($refId) ? $this->auditLogs->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->auditLogs->put($refId, $auditLog);
		return $auditLog;
	}
	/*
	 *$method.comment
	 */
	public function getSignature($refId,$signature){
		$clientObject = $this->signatures->containsKey($refId) ? $this->signatures->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->signatures->put($refId, $signature);
		return $signature;
	}
	/*
	 *$method.comment
	 */
	public function getContract($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->contracts->containsKey($refId) ? $this->contracts->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new Contract();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->contracts->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getContractBasic($id,$refId,$contract){
		$clientObject = $this->contracts->containsKey($id) ? $this->contracts->get($id) : null;
		if (is_null($clientObject)){
			$clientObject = $this->contracts->containsKey($refId) ? $this->contracts->get($refId) : null;
		}
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->contracts->put($refId, $contract);
		return $contract;
	}
	/*
	 *$method.comment
	 */
	public function getAdjustmentBasic($id,$refId,$adjustment){
		$clientObject = $this->adjustments->containsKey($id) ? $this->adjustments->get($id) : null;
		if (is_null($clientObject)){
			$clientObject = $this->adjustments->containsKey($refId) ? $this->adjustments->get($refId) : null;
		}
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->adjustments->put($refId, $adjustment);
		return $adjustment;
	}
	/*
	 *$method.comment
	 */
	public function getAdjustment($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->adjustments->containsKey($refId) ? $this->adjustments->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new Adjustment();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->adjustments->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getUser($facade,$attributeName){
		$userName = $facade->getAttributeValue($attributeName);
		$clientObject = $this->users->containsKey($userName) ? $this->users->get($userName) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new User();
		$clientObject->setUserName($userName);
		$this->users->put($userName, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getUserBasic($userName,$user){
		$clientObject = $this->users->containsKey($userName) ? $this->users->get($userName) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->users->put($userName, $user);
		return $user;
	}
	/*
	 *$method.comment
	 */
	public function getAgentCommission($facade,$attributeName){
		$refId = Helper::getRefId($facade->getAttributeValue($attributeName));
		$clientObject = $this->agentCommissions->containsKey($refId) ? $this->agentCommissions->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$clientObject = new AgentCommission();
		if (!(Helper::isNewObject($facade->getAttributeValue($attributeName)))){
			$clientObject->setId($refId);
			$clientObject->setRefId($refId);
		}
		$this->agentCommissions->put($refId, $clientObject);
		return $clientObject;
	}
	/*
	 *$method.comment
	 */
	public function getAgentCommissionBasic($refId,$agentCommission){
		$clientObject = $this->agentCommissions->containsKey($refId) ? $this->agentCommissions->get($refId) : null;
		if (!is_null($clientObject)){
			return $clientObject;
		}
		$this->agentCommissions->put($refId, $agentCommission);
		return $agentCommission;
	}
	/*
	 *$method.comment
	 */
	private function takeCustomerAccounts(){
		return new ArrayList(Helper::getCustomerAccountValues($this->customerAccounts));
	}
	/*
	 *$method.comment
	 */
	private function addCustomerAccountElement($root,$customerAccount){
		$caElement = $root->createChild("customerAccount");
		$caTransformationHelper = new XMLTransformationHelper($caElement);
		$caTransformationHelper->customerAccountToXML($customerAccount);
	}
	/*
	 *$method.comment
	 */
	private function addPaymentOptionElement($root,$paymentOption){
		$poElement = $root->createChild("paymentOption");
		$poTransformationHelper = new XMLTransformationHelper($poElement);
		$poTransformationHelper->paymentOptionToXML($paymentOption);
	}
	/*
	 *$method.comment
	 */
	private function addPaymentPlanElement($root,$paymentPlan){
		$ppElement = $root->createChild("paymentPlan");
		$ppTransformationHelper = new XMLTransformationHelper($ppElement);
		$ppTransformationHelper->paymentPlanToXML($paymentPlan);
	}
	/*
	 *$method.comment
	 */
	private function addRevenueTransactionElement($root,$transaction){
		$rtElement = $root->createChild("revenueTransaction");
		$rtTransformationHelper = new XMLTransformationHelper($rtElement);
		$rtTransformationHelper->revenueTransactionToXML($transaction);
	}
	/*
	 *$method.comment
	 */
	private function addAssetTransactionElement($root,$transaction){
		$atElement = $root->createChild("assetTransaction");
		$atTransformationHelper = new XMLTransformationHelper($atElement);
		$atTransformationHelper->assetTransactionToXML($transaction);
	}
	/*
	 *$method.comment
	 */
	private function addNoteElement($root,$note){
		$noteElement = $root->createChild("note");
		$noteTransformationHelper = new XMLTransformationHelper($noteElement);
		$noteTransformationHelper->noteToXML($note);
	}
	/*
	 *$method.comment
	 */
	private function addActionElement($root,$action){
		$actionElement = $root->createChild("action");
		$actionTransformationHelper = new XMLTransformationHelper($actionElement);
		$actionTransformationHelper->actionToXML($action);
	}
	/*
	 *$method.comment
	 */
	private function addAddressElement($root,$address){
		$addressElement = $root->createChild("address");
		$addressTransformationHelper = new XMLTransformationHelper($addressElement);
		$addressTransformationHelper->addressToXML($address);
	}
	/*
	 *$method.comment
	 */
	private function addPhoneElement($root,$phone){
		$phoneElement = $root->createChild("phone");
		$phoneTransformationHelper = new XMLTransformationHelper($phoneElement);
		$phoneTransformationHelper->phoneToXML($phone);
	}
	/*
	 *$method.comment
	 */
	private function addEmailAddressElement($root,$emailAddress){
		$emailAddressElement = $root->createChild("emailAddress");
		$emailAddressTransformationHelper = new XMLTransformationHelper($emailAddressElement);
		$emailAddressTransformationHelper->emailAddressToXML($emailAddress);
	}
	/*
	 *$method.comment
	 */
	private function addContractElement($root,$contract){
		$contractElement = $root->createChild("contract");
		$contractTransformationHelper = new XMLTransformationHelper($contractElement);
		$contractTransformationHelper->contractToXML($contract);
	}
	/*
	 *$method.comment
	 */
	private function addLetterElement($root,$letter){
		$letterElement = $root->createChild("letter");
		$letterTransformationHelper = new XMLTransformationHelper($letterElement);
		$letterTransformationHelper->letterToXML($letter);
	}
	/*
	 *$method.comment
	 */
	private function addEmailElement($root,$email){
		$letterElement = $root->createChild("email");
		$emailTransformationHelper = new XMLTransformationHelper($letterElement);
		$emailTransformationHelper->emailToXML($email);
	}
	/*
	 *$method.comment
	 */
	private function addScheduledPaymentElement($root,$scheduledPayment){
		$scheduledPaymentElement = $root->createChild("scheduledPayment");
		$scheduledPaymentTransformationHelper = new XMLTransformationHelper($scheduledPaymentElement);
		$scheduledPaymentTransformationHelper->scheduledPaymentToXML($scheduledPayment);
	}
	/*
	 *$method.comment
	 */
	private function addDocumentElement($root,$document){
		$documentElement = $root->createChild("document");
		$documentTransformationHelper = new XMLTransformationHelper($documentElement);
		$documentTransformationHelper->documentToXML($document);
	}
	/*
	 *$method.comment
	 */
	private function addAuditLogElement($root,$auditLog){
		$auditLogElement = $root->createChild("auditLog");
		$auditLogTransformationHelper = new XMLTransformationHelper($auditLogElement);
		$auditLogTransformationHelper->auditLogToXML($auditLog);
	}
	/*
	 *$method.comment
	 */
	private function addAdjustmentElement($root,$adjustment){
		$adjustmentElement = $root->createChild("adjustment");
		$adjustmentTransformationHelper = new XMLTransformationHelper($adjustmentElement);
		$adjustmentTransformationHelper->adjustmentToXML($adjustment);
	}
	/*
	 *$method.comment
	 */
	private function addUserElement($root,$user){
		$userElement = $root->createChild("user");
		$userTransformationHelper = new XMLTransformationHelper($userElement);
		$userTransformationHelper->userToXML($user);
	}
	/*
	 *$method.comment
	 */
	private function addAgentCommissionElement($root,$agentCommission){
		$agentCommissionElement = $root->createChild("agentCommission");
		$agentCommissionTransformationHelper = new XMLTransformationHelper($agentCommissionElement);
		$agentCommissionTransformationHelper->agentCommissionToXML($agentCommission);
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceCustomerAccount($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->customerAccounts->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->customerAccounts->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplacePaymentOption($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->paymentOptions->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->paymentOptions->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplacePaymentPlan($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->paymentPlans->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->paymentPlans->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceAddress($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->addresses->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->addresses->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplacePhone($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->phones->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->phones->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceEmailAddress($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->emailAddresses->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->emailAddresses->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceRevenueTransaction($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->revenueTransactions->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->revenueTransactions->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceAssetTransaction($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->assetTransactions->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->assetTransactions->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceNote($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->notes->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->notes->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceAction($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->actions->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->actions->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceLetter($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->letters->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->letters->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceEmail($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->emails->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->emails->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceScheduledPayment($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->scheduledPayments->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->scheduledPayments->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceDocument($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->documents->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->documents->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceAuditLog($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->auditLogs->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->auditLogs->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceSignature($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->signatures->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->signatures->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceContract($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->contracts->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->contracts->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceAdjustment($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->adjustments->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->adjustments->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplaceAgentCommission($clientObject){
		$id = $clientObject->getId();
		$refId = $clientObject->getRefId();
		if (!is_null($id) && !(($refId == $id))){
			$this->agentCommissions->remove($clientObject->getRefId());
			$clientObject->setRefId($id);
			$this->agentCommissions->put($id, $clientObject);
		}
	}
	/*
	 *$method.comment
	 */
	private function checkAndReplace($clientObjects){
		foreach ($clientObjects as $clientObject){
			if ($clientObject instanceof CustomerAccount){
				$this->checkAndReplaceCustomerAccount($clientObject);
			}
			else {
				if ($clientObject instanceof Address){
					$this->checkAndReplaceAddress($clientObject);
				}
				else {
					if ($clientObject instanceof Phone){
						$this->checkAndReplacePhone($clientObject);
					}
					else {
						if ($clientObject instanceof EmailAddress){
							$this->checkAndReplaceEmailAddress($clientObject);
						}
						else {
							if ($clientObject instanceof PaymentOption){
								$this->checkAndReplacePaymentOption($clientObject);
							}
							else {
								if ($clientObject instanceof PaymentPlan){
									$this->checkAndReplacePaymentPlan($clientObject);
								}
								else {
									if ($clientObject instanceof RevenueTransaction){
										$this->checkAndReplaceRevenueTransaction($clientObject);
									}
									else {
										if ($clientObject instanceof AssetTransaction){
											$this->checkAndReplaceAssetTransaction($clientObject);
										}
										else {
											if ($clientObject instanceof Note){
												$this->checkAndReplaceNote($clientObject);
											}
											else {
												if ($clientObject instanceof Action){
													$this->checkAndReplaceAction($clientObject);
												}
												else {
													if ($clientObject instanceof Contract){
														$this->checkAndReplaceContract($clientObject);
													}
													else {
														if ($clientObject instanceof Letter){
															$this->checkAndReplaceLetter($clientObject);
														}
														else {
															if ($clientObject instanceof Email){
																$this->checkAndReplaceEmail($clientObject);
															}
															else {
																if ($clientObject instanceof ScheduledPayment){
																	$this->checkAndReplaceScheduledPayment($clientObject);
																}
																else {
																	if ($clientObject instanceof AgentCommission){
																		$this->checkAndReplaceAgentCommission($clientObject);
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	/*
	 *$method.comment
	 */
	private function synchronizeClientObject($newClientObject,$oldClientObject){
		if (!is_null($oldClientObject)){
			$fields = $this->getFields($newClientObject->getClass());
			AccessibleObject::setAccessible($fields, true);
			foreach ($fields as $field){
				try{
					if ($field->getModifiers() == 25){
continue;					}
					$className = $field->getType()->getSimpleName();
					if (!(($field->getName() == "serialVersionUID")) && !(($field->getName() == "customerAccount")) && !(($field->getName() == "user"))){
						if (!(($className == "List")) || ($className == "List" && !(($newClientObject instanceof CustomerAccount)))){
							$field->set($oldClientObject, $field->get($newClientObject));
						}
					}
				}
				catch(IllegalArgumentException $e){
					throw new Exception("System is unavailable.");
				}
				catch(IllegalAccessException $e){
					throw new Exception("System is unavailable.");
				}
			}
		}
	}
	/*
	 *$method.comment
	 */
	private function getFields($clazz){
		$fields = $clazz->getDeclaredFields();
		if (!(($clazz->getSuperclass()->getSimpleName() == "Object"))){
			$parentFields = $this->getFields($clazz->getSuperclass());
			$temp = array();
			System::arraycopy($fields, 0, $temp, 0, $fields->length);
			System::arraycopy($parentFields, 0, $temp, $fields->length, $parentFields->length);
			$fields = $temp;
		}
		return $fields;
	}
}

?>

