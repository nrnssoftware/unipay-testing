<?php

class ElementFacade {
	
	/** The element. */
	private $element;
	private $document;

	function __construct($element) {
		if ($element instanceof DOMElement){
			$this->element = $element;
			$this->document = $element->ownerDocument;
		}
		else {
			$this->document = new DOMDocument('1.0', "UTF-8");
			$this->document->formatOutput = true;
			$this->element = $this->document->createElement($element);
			$this->document->appendChild($this->element);
		}
	}
	
	function __destruct() {
		$this->element = null;
		$this->document = null;
	}

	/**
	 * Gets the name.
	 * 
	 * @return String
	 */
	public function getName(){
		return $this->element->nodeName;
	}
	
	/**
	 * Gets the child.
	 * 
	 * @param name
	 *            the name
	 * 
	 * @return DOMNode
	 */
	public function getChild($name){
		$nodeList = $this->element->getElementsByTagName($name);
		return $nodeList->length == 0 ? null : new ElementFacade($nodeList->item(0));
	}
	
	/**
	 * Gets the text.
	 * 
	 * @return String
	 */
	public function getText(){
		return $this->element->is_null(firstChild) ? null : 
			$this->element->firstChild->nodeValue;
	}
	
	/**
	 * Gets the attribute value.
	 * 
	 * @param attributeName
	 *            the attribute name
	 * 
	 * @return String
	 */
	public function getAttributeValue($attributeName){
		return $this->element->getAttribute($attributeName);
	}
	
	
	/**
	 * Creates the child.
	 * 
	 * @param childName
	 *            the child name
	 * 
	 * @return ElementFacade
	 */
	public function createChild($childName) {
		$child = $this->document->createElement($childName);
		$this->element->appendChild($child);
		return new ElementFacade($child);
	}

	/**
	 * Gets the node list.
	 * 
	 * @return ArrayList<ElementFacade>
	 */
	public function getNodeList() {
		$list = new ArrayList();
		$children = $this->element->childNodes;

		for ($i=0; $i<$children->length; $i++)
			$list->add(new ElementFacade($children->item($i)));

		return $list;
	}
	
	/**
	 * Sets the attribute.
	 * 
	 * @param attributeName
	 *            the attribute name
	 * @param value
	 *            the value
	 * @return void
	 */
	public function setAttribute($attributeName, $value) {

		if (!is_null($value)) {
			$transformedValue = null;
			if ($value instanceof DateTime)
				$transformedValue = Helper::formatDate($value);
			else if (is_bool($value)){
				if ($value) $transformedValue = "1";
				else return;//$transformedValue = "0";
			}
			else $transformedValue = strval($value);

			if (is_null($transformedValue) || $transformedValue == "")
				return;//do nothing
			
			$this->element->setAttribute($attributeName, $transformedValue);
		}
	}
	/**
	 * Adds the content.
	 * 
	 * @param content
	 *            the content
	 * @return void
	 */
	public function addContent($content) {
		if (is_null($this->element->firstChild)){
			$text = $this->document->createTextNode($content);
			$this->element->appendChild($text);
		}
		else {
			$this->element->firstChild->nodeValue = $this->element->firstChild->nodeValue.$content;
		}
	}
	
	/**
	 * Gets the document.
	 * 
	 * @return DOMDocument
	 */
	public function getDocument() {
		return $this->document;
	}
	
	/**
	 * Checks if is new object.
	 * 
	 * @param facade
	 *            the facade
	 * @param attributeName
	 *            the attribute name
	 * 
	 * @return boolean
	 */
	public static function isNewObject($facade, $attributeName) {
		return Helper::isNewObject($facade->getAttributeValue($attributeName));
	}

	/**
	 * Gets the id.
	 * 
	 * @param facade
	 *            the facade
	 * @param attributeName
	 *            the attribute name
	 * 
	 * @return Number
	 */
	public static function getId($facade, $attributeName) {
		return intval(Helper::getId($facade->getAttributeValue($attributeName)));
	}

	/**
	 * Gets the string.
	 * 
	 * @param attributeName
	 *            the attribute name
	 * 
	 * @return String
	 */
	public function getString($attributeName) {
		return $this->element->getAttribute($attributeName);
	}
	
	/**
	 * Gets the integer.
	 * 
	 * @param attributeName
	 *            the attribute name
	 * 
	 * @return Integer
	 */
	public function getInteger($attributeName) {
		return $this->element->hasAttribute($attributeName) 
		     ? intval($this->element->getAttribute($attributeName)) : null;
	}

	/**
	 * Gets the long.
	 * 
	 * @param attributeName
	 *            the attribute name
	 * 
	 * @return Integer
	 */
	public function getLong($attributeName) {
		return $this->getInteger($attributeName);
	}

	/**
	 * Gets the boolean.
	 * 
	 * @param attributeName
	 *            the attribute name
	 * 
	 * @return the boolean
	 */
	public function getBoolean($attributeName) {
		return is_null($this->element->getAttribute($attributeName))
		     ? false : (bool)$this->element->getAttribute($attributeName);
	}
	public function getBooleanBasic($attributeName) {
		return is_null($this->element->getAttribute($attributeName))
		     ? null : (bool)$this->element->getAttribute($attributeName);
	}
	
	/**
	 * Gets the date.
	 * 
	 * @param attributeName
	 *            the attribute name
	 * 
	 * @return DateTime
	 */
	public function getDate($attributeName) {
//		return $this->element->hasAttribute($attributeName)
//		     ? Helper::parseDate($this->element->getAttribute($attributeName)) : null;
		
		return !is_null($this->element->getAttribute($attributeName))
		     ? Helper::parseDate($this->element->getAttribute($attributeName)) : null;
	}
	
	/**
	 * Checks if is attribute value null.
	 * 
	 * @param attributeName
	 *            the attribute name
	 * 
	 * @return Boolean
	 */
	public function isAttributeValueNull($attributeName) {
		$value = $this->element->getAttribute($attributeName);
		return (is_null($value) || trim($value) == "");
	}
	
	/**
	 * Sets the attributes to root.
	 * 
	 * @param facade
	 *            the facade
	 * @param merchantAccountCode
	 *            the merchant account code
	 * @param password
	 *            the password
	 * @return void
	 */
	public static function setAttributesToRoot($facade,
			$merchantAccountCode, $password) {
		
		$facade->setAttribute("merchantAccountCode", $merchantAccountCode);
		$facade->setAttribute("password", $password);
	}
	
}


?>
