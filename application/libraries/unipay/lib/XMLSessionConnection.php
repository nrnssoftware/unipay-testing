<?php


/*
 *$comment
 */
class XMLSessionConnection {
		/*
	 *@var null
	 */
	private $merchantAccountCode;
		/*
	 *@var null
	 */
	private $password;
		/*
	 *@var null
	 */
	private $config;
		

	/*
	 *$method.comment
	 */
	public function login($merchantAccountCode,$password,$config){
		$this->merchantAccountCode = $merchantAccountCode;
		$this->password = $password;
		$this->config = $config;
	}
	/*
	 *$method.comment
	 */
	public function logout(){
		$this->merchantAccountCode = null;
		$this->password = null;
		$this->config = null;
	}
	/*
	 *$method.comment
	 */
	public function save($sessionContext,$userCode,$clientObjects){
		if ($clientObjects->isEmpty()){
			return ;
		}
		$context = new TransformationContext($sessionContext);
		$facade = Helper::doPost($context->serializeList($this->merchantAccountCode, $this->password, $userCode, $clientObjects, $this->config), $this->config);
		$context->deserializeFacade($facade, false);
	}
	/*
	 *$method.comment
	 */
	public function find($sessionContext,$queryName,$parameters){
		$context = new TransformationContext($sessionContext);
		$facade = Helper::doPost($context->serealizeQuery($this->merchantAccountCode, $this->password, $queryName, $parameters, $this->config), $this->config);
		return Helper::toClientObjectList($context->deserializeFacade($facade, true));
	}
	/*
	 *$method.comment
	 */
	public function findSpecial($queryName,$parameters){
		throw new Exception("Not implemented yet.");
	}
	/*
	 *$method.comment
	 */
	public function loadDocumentContent($documentCode,$customerAccountCode,$useDocumentAccesInterval){
		throw new Exception("Not implemented yet.");
	}
	/*
	 *$method.comment
	 */
	public function saveDocumentContent($userCode,$customerAccount,$documentContent){
		throw new Exception("Not implemented yet.");
	}
	/*
	 *$method.comment
	 */
	public function updateLastPaymentDate($customerAccountCode,$documentId){
		throw new Exception("Not implemented yet.");
	}
	/*
	 *$method.comment
	 */
	public function exportCustomerAccountSpecial($parameters,$reportExportType){
		throw new Exception("Not implemented yet.");
	}
	/*
	 *$method.comment
	 */
	public function executeReport($customerAccount,$reportName,$parameters){
		throw new Exception("Not implemented yet.");
	}
}

?>

