<?php


/*
 *$comment
 */
class ScheduledPayment extends ClientObject {
		/*
	 *@var null
	 */
	private $amount;
		/*
	 *@var null
	 */
	private $dueDate;
		/*
	 *@var null
	 */
	private $status;
		/*
	 *@var null
	 */
	private $creatorCode;
		/*
	 *@var null
	 */
	private $posterCode;
		/*
	 *@var null
	 */
	private $note;
		/*
	 *@var null
	 */
	private $isVisibleExternally;
		/*
	 *@var null
	 */
	private $isInvoiceRequired;
		/*
	 *@var null
	 */
	private $customerAccount;
		/*
	 *@var null
	 */
	private $paymentOption;
		/*
	 *@var null
	 */
	private $assetTransaction;
		
	/*
	 *$constructor.comment
	 */
	function __construct(){
		//initialization block
		$this->isVisibleExternally = false;
		$this->isInvoiceRequired = false;
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
	}

	/*
	 *$method.comment
	 */
	public function getAmount(){
		return $this->amount;
	}
	/*
	 *$method.comment
	 */
	public function setAmount($amount){
		$this->amount = $amount;
	}
	/*
	 *$method.comment
	 */
	public function getDueDate(){
		return $this->dueDate;
	}
	/*
	 *$method.comment
	 */
	public function setDueDate($dueDate){
		$this->dueDate = $dueDate;
	}
	/*
	 *$method.comment
	 */
	public function getStatus(){
		return $this->status;
	}
	/*
	 *$method.comment
	 */
	public function setStatus($status){
		$this->status = $status;
	}
	/*
	 *$method.comment
	 */
	public function getCreatorCode(){
		return $this->creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function setCreatorCode($creatorCode){
		$this->creatorCode = $creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function getPosterCode(){
		return $this->posterCode;
	}
	/*
	 *$method.comment
	 */
	public function setPosterCode($posterCode){
		$this->posterCode = $posterCode;
	}
	/*
	 *$method.comment
	 */
	public function getNote(){
		return $note;
	}
	/*
	 *$method.comment
	 */
	public function setNote($note){
		$this->note = $note;
	}
	/*
	 *$method.comment
	 */
	public function getIsVisibleExternally(){
		return $this->isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function setIsVisibleExternally($isVisibleExternally){
		$this->isVisibleExternally = $isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function getIsInvoiceRequired(){
		return $isInvoiceRequired;
	}
	/*
	 *$method.comment
	 */
	public function setIsInvoiceRequired($isInvoiceRequired){
		$this->isInvoiceRequired = $isInvoiceRequired;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccount($customerAccount){
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentOption(){
		return $this->paymentOption;
	}
	/*
	 *$method.comment
	 */
	public function setPaymentOption($paymentOption){
		$this->paymentOption = $paymentOption;
	}
	/*
	 *$method.comment
	 */
	public function getAssetTransaction(){
		return $this->assetTransaction;
	}
	/*
	 *$method.comment
	 */
	 function setAssetTransaction($assetTransaction){
		$this->assetTransaction = $assetTransaction;
	}
	/*
	 *$method.comment
	 */
	public function cancel(){
		$this->status = ScheduledPaymentStatusType::Cancelled;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "ScheduledPayment";
	}
}

?>

