<?php


/*
 *$comment
 */
abstract class SessionConnection {

	/** 
	 * @var null
	 */
	const DEBUG  = "debug";
	/** 
	 * @var null
	 */
	const IMPORT_LOG_CODE  = "importLogCode";
	/** 
	 * @var null
	 */
	const PROCESSOR_HOST  = "processor.host";
	/** 
	 * @var null
	 */
	const CONNECTION_TYPE  = "connection.type";
	/** 
	 * @var null
	 */
	const CONNECTION_TYPE_RMI  = "rmi";
	/** 
	 * @var null
	 */
	const CONNECTION_TYPE_XML  = "xml";
	/** 
	 * @var null
	 */
	const PAYMENT_OPTION_VERIFICATION_AMOUNT  = "payment-option.verification-amount";
	/** 
	 * @var null
	 */
	const KEYSTORE_PATH  = "keystore.path";
	/** 
	 * @var null
	 */
	const KEYSTORE_PASSWORD  = "keystore.pasword";
	/** 
	 * @var null
	 */
	const FIND_CUSTOMER_ACCOUNT  = "findCustomerAccount";
	/** 
	 * @var null
	 */
	const FIND_CUSTOMER_ACCOUNT_SPECIAL  = "findCustomerAccountSpecial";
	/** 
	 * @var null
	 */
	const COUNT_CUSTOMER_ACCOUNT_SPECIAL  = "countCustomerAccountSpecial";
	/** 
	 * @var null
	 */
	const FIND_PAYMENT_OPTION  = "findPaymentOption";
	/** 
	 * @var null
	 */
	const FIND_ACCOUNT_TRANSACTION  = "findAccountTransaction";
	/** 
	 * @var null
	 */
	const FIND_ADDRESS  = "findAddress";
	/** 
	 * @var null
	 */
	const FIND_PHONE  = "findPhone";
	/** 
	 * @var null
	 */
	const FIND_EMAIL_ADDRESS  = "findEmailAddress";
	/** 
	 * @var null
	 */
	const FIND_NOTE  = "findNote";
	/** 
	 * @var null
	 */
	const FIND_ACTION  = "findAction";
	/** 
	 * @var null
	 */
	const FIND_LETTER  = "findLetter";
	/** 
	 * @var null
	 */
	const FIND_EMAIL  = "findEmail";
	/** 
	 * @var null
	 */
	const FIND_DOCUMENT  = "findDocument";
	/** 
	 * @var null
	 */
	const FIND_AUDIT_LOG  = "findAuditLog";
	/** 
	 * @var null
	 */
	const FIND_PAYMENT_PLAN  = "findPaymentPlan";
	/** 
	 * @var null
	 */
	const FIND_SCHEDULED_PAYMENT  = "findScheduledPayment";
	/** 
	 * @var null
	 */
	const FIND_USER  = "findUser";
	/** 
	 * @var null
	 */
	const FIND_AGENT_COMMISION  = "findAgentCommission";
	/** 
	 * @var null
	 */
	const FIND_SIGNATURE  = "findSignature";
	/** 
	 * @var null
	 */
	const FIND_CONTRACT  = "findContract";
	/** 
	 * @var null
	 */
	const FIND_ADJUSTMENT  = "findAdjustment";
	/** 
	 * @var null
	 */
	const LOAD_CUSTOMER_ACCOUNT  = "loadCustomerAccount";
	/** 
	 * @var null
	 */
	const LOAD_OBJECT  = "loadObject";
	/** 
	 * @var null
	 */
	const CLIENT_IP_ADDRESS  = "clientIpAddress";
	/** 
	 * @var null
	 */
	const CLIENT_EMAIL_DOMAIN  = "clientEmailDomain";
	/** 
	 * @var null
	 */
	const IS_IMPORT  = "isImport";
	/** 
	 * @var null
	 */
	const IS_LITE  = "isLite";
	/** 
	 * @var null
	 */
	const SOURCE  = "source";
	/** 
	 * @var null
	 */
	const SOURCE_ICONTRACT  = "source.icontract";
	/** 
	 * @var null
	 */
	const CLIENT_EMAIL_ENCODED  = "clientEmailEncoded";
	/** 
	 * @var null
	 */
	const CLIENT_USER_NAME_ENCODED  = "clientUserNameEncoded";
	/** 
	 * @var null
	 */
	const CLIENT_PASSWORD_ENCODED  = "clientPasswordEncoded";

	/** 
	 * $method.comment
	 */
	public abstract function login($merchantAccountCode, $password, $config);
	/** 
	 * $method.comment
	 */
	public abstract function logout();
	/** 
	 * $method.comment
	 */
	public abstract function save($sessionContext, $userCode, $clientObjects);
	/** 
	 * $method.comment
	 */
	public abstract function find($sessionContext, $queryName, $parameters);
	/** 
	 * $method.comment
	 */
	public abstract function findSpecial($queryName, $parameters);
	/** 
	 * $method.comment
	 */
	public abstract function loadDocumentContent($documentCode, $customerAccountCode, $useDocumentAccesInterval);
	/** 
	 * $method.comment
	 */
	public abstract function saveDocumentContent($userCode, $customerAccount, $documentContent);
	/** 
	 * $method.comment
	 */
	public abstract function updateLastPaymentDate($customerAccountCode, $documentId);
	/** 
	 * $method.comment
	 */
	public abstract function exportCustomerAccountSpecial($parameters, $reportExportType);
	/** 
	 * $method.comment
	 */
	public abstract function executeReport($customerAccount, $reportName, $parameters);
}
?>

