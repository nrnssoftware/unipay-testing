<?php


/*
 *$comment
 */
class XMLTransformationHelper {
		/*
	 *@var null
	 */
	private $facade;
		
	/*
	 *$constructor.comment
	 */
	function __construct($facade = null){
		//initialization block
		//constructor with parameters
		$this->facade = $facade;
	
	}

	/*
	 *$method.comment
	 */
	public function customerAccountToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $this->refIdToXML($clientObject->getId(), $clientObject->getRefId()));
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("customerCode", $clientObject->getCustomerCode());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("merchantAccountExternalCode", $clientObject->getMerchantAccountExternalCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("firstName", $clientObject->getFirstName());
		$this->addAtribute("lastName", $clientObject->getLastName());
		$this->addAtribute("middleName", $clientObject->getMiddleName());
		$this->addAtribute("title", $clientObject->getTitle());
		$this->addAtribute("suffix", $clientObject->getSuffix());
		$this->addAtribute("type", $clientObject->getType());
		$this->addAtribute("isActive", $clientObject->getIsActive());
		$this->addAtribute("balance", $clientObject->getBalance());
		$this->addAtribute("beneficiaryInfo", $clientObject->getBeneficiaryInfo());
		$this->addAtribute("customerAccountGroupCode", $clientObject->getCustomerAccountGroupCode());
		$this->addAtribute("notes", $clientObject->getNotes());
		$this->addAtribute("lastUpdateDate", $clientObject->getLastUpdateDate());
		$this->addAtribute("birthDate", $clientObject->getBirthDate());
		$this->addAtribute("lastPaymentDate", $clientObject->getLastPaymentDate());
		$this->addAtribute("lastReturnReason", $clientObject->getLastReturnReason());
		$this->addAtribute("statusChangeDate", $clientObject->getStatusChangeDate());
		$this->addAtribute("socialSecurity", $clientObject->getSocialSecurity());
		$this->addAtribute("collectorCode", $clientObject->getCollectorCode());
		$this->addAtribute("sentCollectionsBalance", $clientObject->getSentCollectionsBalance());
		$this->addAtribute("sentCollectionsDate", $clientObject->getSentCollectionsDate());
		$this->addAtribute("isMarketingEmailAllowed", $clientObject->getIsMarketingEmailAllowed());
		$this->addAtribute("isBillingEmailAllowed", $clientObject->getIsBillingEmailAllowed());
		$this->addAtribute("isMailDisallowed", $clientObject->getIsMailDisallowed());
		$this->addAtribute("isEmailDisallowed", $clientObject->getIsEmailDisallowed());
		$this->addAtribute("isCallingDisallowed", $clientObject->getIsCallingDisallowed());
		$this->addAtribute("lastActionDate", $clientObject->getLastActionDate());
		$this->addAtribute("collectionsPriorityCl", $clientObject->getCollectionsPriorityType());
		$this->addAtribute("externalIdentifier", $clientObject->getExternalIdentifier());
		$this->addAtribute("writeOffBalance", $clientObject->getWriteOffBalance());
		$this->addAtribute("customFields", Helper::toString($clientObject->getCustomFields()));
		$this->addAtribute("isChargebackRequested", $clientObject->getIsChargebackRequested());
		$this->addAtribute("isBankruptcyFiled", $clientObject->getIsBankruptcyFiled());
		$this->addAtribute("isLegalDispute", $clientObject->getIsLegalDispute());
		$this->addAtribute("isCancellationDispute", $clientObject->getIsCancellationDispute());
		$this->addAtribute("contractDocumentCode", $clientObject->getContractDocumentCode());
		$this->addAtribute("isDeceased", $clientObject->getIsDeceased());
		$this->addAtribute("isSkipTraceRequired", $clientObject->getIsSkipTraceRequired());
		$this->addAtribute("returnFeeBalance", $clientObject->getReturnFeeBalance());
		$this->addAtribute("lateFeeBalance", $clientObject->getLateFeeBalance());
		$this->addAtribute("paymentPlanValue", $clientObject->getPaymentPlanValue());
		$this->addAtribute("synchronizationCode", $clientObject->getSynchronizationCode());
		$this->addAtribute("defaultBillingCycleCode", $clientObject->getDefaultBillingCycleCode());
		$this->addAtribute("defaultBillingDate", $clientObject->getDefaultBillingDate());
		$this->addAtribute("isReinstated", $clientObject->getIsReinstated());
		$this->addAtribute("isReinstateProcess", $clientObject->getIsReinstateProcess());
		$this->addAtribute("status", $clientObject->getStatus());
		$this->addAtribute("driverLicenseNumber", $clientObject->getDriverLicenseNumber());
		$this->addAtribute("driverLicenseState", $clientObject->getDriverLicenseState());
		$this->addAtribute("isCollectionsFeeApplied", $clientObject->getIsCollectionsFeeApplied());
		$this->addAtribute("buyerCode", $clientObject->getBuyerCode());
		$this->addAtribute("buyerFirstName", $clientObject->getBuyerFirstName());
		$this->addAtribute("buyerLastName", $clientObject->getBuyerLastName());
		$this->addAtribute("buyerMiddleName", $clientObject->getBuyerMiddleName());
		$this->addAtribute("buyerStreet1", $clientObject->getBuyerStreet1());
		$this->addAtribute("buyerStreet2", $clientObject->getBuyerStreet2());
		$this->addAtribute("buyerCity", $clientObject->getBuyerCity());
		$this->addAtribute("buyerState", $clientObject->getBuyerState());
		$this->addAtribute("buyerZipCode", $clientObject->getBuyerZipCode());
		$this->addAtribute("buyerBirthDate", $clientObject->getBuyerBirthDate());
		$this->addAtribute("buyerDriverLicenseNumber", $clientObject->getBuyerDriverLicenseNumber());
		$this->addAtribute("buyerSocialSecurity", $clientObject->getBuyerSocialSecurity());
		$this->addAtribute("buyerPhone", $clientObject->getBuyerPhone());
		$this->addAtribute("creditAgencyStatus", $clientObject->getCreditAgencyStatus());
		$this->addAtribute("isTax1Exempt", $clientObject->getIsTax1Exempt());
		$this->addAtribute("isTax2Exempt", $clientObject->getIsTax2Exempt());
		$this->addAtribute("isTax3Exempt", $clientObject->getIsTax3Exempt());
		$this->addAtribute("isTax4Exempt", $clientObject->getIsTax4Exempt());
		$this->addAtribute("isTax5Exempt", $clientObject->getIsTax5Exempt());
		$this->addAtribute("customDate1", $clientObject->getCustomDate1());
		$this->addAtribute("skipTraceModeType", $clientObject->getSkipTraceModeType());
		$this->addAtribute(SessionConnection::IS_LITE, $clientObject->getIsLite());
		if (is_null($clientObject->getId()) || !is_null($clientObject->getCode())){
			if (!is_null($clientObject->getDefaultAddress())){
				$this->addAtribute("defaultAddress", $this->refIdToXML($clientObject->getDefaultAddress()->getId(), $clientObject->getDefaultAddress()->getRefId()));
			}
			if (!is_null($clientObject->getDefaultHomePhone())){
				$this->addAtribute("defaultHomePhone", $this->refIdToXML($clientObject->getDefaultHomePhone()->getId(), $clientObject->getDefaultHomePhone()->getRefId()));
			}
			if (!is_null($clientObject->getDefaultWorkPhone())){
				$this->addAtribute("defaultWorkPhone", $this->refIdToXML($clientObject->getDefaultWorkPhone()->getId(), $clientObject->getDefaultWorkPhone()->getRefId()));
			}
			if (!is_null($clientObject->getDefaultCellPhone())){
				$this->addAtribute("defaultCellPhone", $this->refIdToXML($clientObject->getDefaultCellPhone()->getId(), $clientObject->getDefaultCellPhone()->getRefId()));
			}
			if (!is_null($clientObject->getDefaultEmailAddress())){
				$this->addAtribute("defaultEmailAddress", $this->refIdToXML($clientObject->getDefaultEmailAddress()->getId(), $clientObject->getDefaultEmailAddress()->getRefId()));
			}
			if (!is_null($clientObject->getDefaultContract())){
				$this->addAtribute("defaultContract", $this->refIdToXML($clientObject->getDefaultContract()->getId(), $clientObject->getDefaultContract()->getRefId()));
			}
			if (!is_null($clientObject->getDefaultPaymentOption())){
				$this->addAtribute("defaultPaymentOption", $this->refIdToXML($clientObject->getDefaultPaymentOption()->getId(), $clientObject->getDefaultPaymentOption()->getRefId()));
			}
			if (!is_null($clientObject->getDefaultPaymentPlan())){
				$this->addAtribute("defaultPaymentPlan", $this->refIdToXML($clientObject->getDefaultPaymentPlan()->getId(), $clientObject->getDefaultPaymentPlan()->getRefId()));
			}
			if (!is_null($clientObject->getLastAction())){
				$this->addAtribute("lastAction", $this->refIdToXML($clientObject->getLastAction()->getId(), $clientObject->getLastAction()->getRefId()));
			}
		}
	}
	/*
	 *$method.comment
	 */
	public function customerAccountFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId(Helper::getRefId($this->facade->getAttributeValue("refId")));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setMerchantAccountExternalCode($this->getString("merchantAccountExternalCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCustomerCode($this->getString("customerCode"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setIsActive($this->getBoolean("isActive"));
		$clientObject->setType($this->getString("type"));
		$clientObject->setFirstName($this->getString("firstName"));
		$clientObject->setLastName($this->getString("lastName"));
		$clientObject->setMiddleName($this->getString("middleName"));
		$clientObject->setTitle($this->getString("title"));
		$clientObject->setSuffix($this->getString("suffix"));
		$clientObject->setBeneficiaryInfo($this->getString("beneficiaryInfo"));
		$clientObject->setCustomerAccountGroupCode($this->getString("customerAccountGroupCode"));
		$clientObject->setNotes($this->getString("notes"));
		$clientObject->setBirthDate($this->getDate("birthDate"));
		$clientObject->setBalance($this->getInteger("balance"));
		$clientObject->setLastUpdateDate($this->getDate("lastUpdateDate"));
		$clientObject->setLastPaymentDate($this->getDate("lastPaymentDate"));
		$clientObject->setLastReturnReason($this->getString("lastReturnReason"));
		$clientObject->setStatusChangeDate($this->getDate("statusChangeDate"));
		$clientObject->setSocialSecurity($this->getString("socialSecurity"));
		$clientObject->setCollectorCode($this->getString("collectorCode"));
		$clientObject->setSentCollectionsBalance($this->getInteger("sentCollectionsBalance"));
		$clientObject->setSentCollectionsDate($this->getDate("sentCollectionsDate"));
		$clientObject->setIsMailDisallowed($this->getBoolean("isMailDisallowed"));
		$clientObject->setIsMarketingEmailAllowed($this->getBoolean("isMarketingEmailAllowed"));
		$clientObject->setIsBillingEmailAllowed($this->getBoolean("isBillingEmailAllowed"));
		$clientObject->setIsEmailDisallowed($this->getBoolean("isEmailDisallowed"));
		$clientObject->setIsCallingDisallowed($this->getBoolean("isCallingDisallowed"));
		$clientObject->setLastActionDate($this->getDate("lastActionDate"));
		$clientObject->setCollectionsPriorityType($this->getString("collectionsPriorityCl"));
		$clientObject->setExternalIdentifier($this->getString("externalIdentifier"));
		$clientObject->setWriteOffBalance($this->getInteger("writeOffBalance"));
		$clientObject->setCustomFields(Helper::fromString($this->getString("customFields")));
		$clientObject->setIsChargebackRequested($this->getBoolean("isChargebackRequested"));
		$clientObject->setIsBankruptcyFiled($this->getBoolean("isBankruptcyFiled"));
		$clientObject->setIsLegalDispute($this->getBoolean("isLegalDispute"));
		$clientObject->setIsCancellationDispute($this->getBoolean("isCancellationDispute"));
		$clientObject->setContractDocumentCode($this->getLong("contractDocumentCode"));
		$clientObject->setIsDeceased($this->getBoolean("isDeceased"));
		$clientObject->setIsSkipTraceRequired($this->getBoolean("isSkipTraceRequired"));
		$clientObject->setSkipTraceModeType($this->getString("skipTraceModeType"));
		$clientObject->setReturnFeeBalance($this->getInteger("returnFeeBalance"));
		$clientObject->setLateFeeBalance($this->getInteger("lateFeeBalance"));
		$clientObject->setPaymentPlanValue($this->getInteger("paymentPlanValue"));
		$clientObject->setSynchronizationCode($this->getString("synchronizationCode"));
		$clientObject->setDefaultBillingCycleCode($this->getString("defaultBillingCycleCode"));
		$clientObject->setDefaultBillingDate($this->getDate("defaultBillingDate"));
		$clientObject->setIsReinstated($this->getBoolean("isReinstated"));
		$clientObject->setIsReinstateProcess($this->getBoolean("isReinstateProcess"));
		$clientObject->setStatus($this->getString("status"));
		$clientObject->setIsCollectionsFeeApplied($this->getBoolean("isCollectionsFeeApplied"));
		$clientObject->setDriverLicenseNumber($this->getString("driverLicenseNumber"));
		$clientObject->setDriverLicenseState($this->getString("driverLicenseState"));
		$clientObject->setBuyerCode($this->getString("buyerCode"));
		$clientObject->setBuyerFirstName($this->getString("buyerFirstName"));
		$clientObject->setBuyerLastName($this->getString("buyerLastName"));
		$clientObject->setBuyerMiddleName($this->getString("buyerMiddleName"));
		$clientObject->setBuyerStreet1($this->getString("buyerStreet1"));
		$clientObject->setBuyerStreet2($this->getString("buyerStreet2"));
		$clientObject->setBuyerCity($this->getString("buyerCity"));
		$clientObject->setBuyerState($this->getString("buyerState"));
		$clientObject->setBuyerZipCode($this->getString("buyerZipCode"));
		$clientObject->setBuyerPhone($this->getString("buyerPhone"));
		$clientObject->setBuyerBirthDate($this->getDate("buyerBirthDate"));
		$clientObject->setBuyerSocialSecurity($this->getString("buyerSocialSecurity"));
		$clientObject->setBuyerDriverLicenseNumber($this->getString("buyerDriverLicenseNumber"));
		$clientObject->setCreditAgencyStatus($this->getString("creditAgencyStatus"));
		$clientObject->setIsTax1Exempt($this->getBoolean("isTax1Exempt"));
		$clientObject->setIsTax2Exempt($this->getBoolean("isTax2Exempt"));
		$clientObject->setIsTax3Exempt($this->getBoolean("isTax3Exempt"));
		$clientObject->setIsTax4Exempt($this->getBoolean("isTax4Exempt"));
		$clientObject->setIsTax5Exempt($this->getBoolean("isTax5Exempt"));
		$clientObject->setCustomDate1($this->getDate("customDate1"));
		$clientObject->setIsLite($this->getBoolean(SessionConnection::IS_LITE));
		if (!($this->isAttributeValueNull("defaultAddress"))){
			$clientObject->setDefaultAddress($context->getAddress($this->facade, "defaultAddress"));
		}
		if (!($this->isAttributeValueNull("defaultHomePhone"))){
			$clientObject->setDefaultHomePhone($context->getPhone($this->facade, "defaultHomePhone"));
		}
		if (!($this->isAttributeValueNull("defaultWorkPhone"))){
			$clientObject->setDefaultWorkPhone($context->getPhone($this->facade, "defaultWorkPhone"));
		}
		if (!($this->isAttributeValueNull("defaultCellPhone"))){
			$clientObject->setDefaultCellPhone($context->getPhone($this->facade, "defaultCellPhone"));
		}
		if (!($this->isAttributeValueNull("defaultEmailAddress"))){
			$clientObject->setDefaultEmailAddress($context->getEmailAddress($this->facade, "defaultEmailAddress"));
		}
		if (!($this->isAttributeValueNull("defaultContract"))){
			$clientObject->setDefaultContract($context->getContract($this->facade, "defaultContract"));
		}
		if (!($this->isAttributeValueNull("defaultPaymentOption"))){
			$clientObject->setDefaultPaymentOption($context->getPaymentOption($this->facade, "defaultPaymentOption"));
		}
		if (!($this->isAttributeValueNull("defaultPaymentPlan"))){
			$clientObject->setDefaultPaymentPlan($context->getPaymentPlan($this->facade, "defaultPaymentPlan"));
		}
		if (!($this->isAttributeValueNull("lastAction"))){
			$clientObject->setLastAction($context->getAction($this->facade, "lastAction"));
		}
	}
	/*
	 *$method.comment
	 */
	public function contractToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $this->refIdToXML($clientObject->getId(), $clientObject->getRefId()));
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("name", $clientObject->getName());
		$this->addAtribute("type", $clientObject->getType());
		$this->addAtribute("status", $clientObject->getStatus());
		$this->addAtribute("creatorCode", $clientObject->getCreatorCode());
		$this->addAtribute("sellerCode", $clientObject->getSellerCode());
		$this->addAtribute("importCode", $clientObject->getImportCode());
		$this->addAtribute("effectiveDate", $clientObject->getEffectiveDate());
		$this->addAtribute("expirationDate", $clientObject->getExpirationDate());
		$this->addAtribute("level", $clientObject->getLevel());
		$this->addAtribute("length", $clientObject->getLength());
		$this->addAtribute("value", $clientObject->getValue());
		$this->addAtribute("industryType", $clientObject->getIndustryType());
		$this->addAtribute("freezeBeginDate", $clientObject->getFreezeBeginDate());
		$this->addAtribute("freezeEndDate", $clientObject->getFreezeEndDate());
		$this->addAtribute("documentCode", $clientObject->getDocumentCode());
		$this->addAtribute("isVisibleExternally", $clientObject->getIsVisibleExternally());
		if (!is_null($clientObject->getCustomerAccount())){
			$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
		}
		if (!is_null($clientObject->getDefaultPaymentPlan())){
			$this->addAtribute("defaultPaymentPlan", $this->refIdToXML($clientObject->getDefaultPaymentPlan()->getId(), $clientObject->getDefaultPaymentPlan()->getRefId()));
		}
		if (!is_null($clientObject->getInvoice())){
			$this->addAtribute("invoice", $this->refIdToXML($clientObject->getInvoice()->getId(), $clientObject->getInvoice()->getRefId()));
		}
		if (!is_null($clientObject->getFreezeAdjustment())){
			$this->addAtribute("freezeAdjustment", $clientObject->getFreezeAdjustment()->getRefId());
			$adjustment = $this->facade->createChild("freezeAdjustment");
			$adjustmentTransformationHelper = new XMLTransformationHelper($adjustment);
			$adjustmentTransformationHelper->adjustmentToXML($clientObject->getFreezeAdjustment());
		}
		if (!is_null($clientObject->getCancellationAdjustment())){
			$this->addAtribute("cancellationAdjustment", $clientObject->getCancellationAdjustment()->getRefId());
			$adjustment = $this->facade->createChild("cancellationAdjustment");
			$adjustmentTransformationHelper = new XMLTransformationHelper($adjustment);
			$adjustmentTransformationHelper->adjustmentToXML($clientObject->getCancellationAdjustment());
		}
	}
	/*
	 *$method.comment
	 */
	public function contractFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId(Helper::getRefId($this->facade->getAttributeValue("refId")));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setName($this->getString("name"));
		$clientObject->setType($this->getString("type"));
		$clientObject->setStatus($this->getString("status"));
		$clientObject->setCreatorCode($this->getString("creatorCode"));
		$clientObject->setSellerCode($this->getString("sellerCode"));
		$clientObject->setImportCode($this->getString("importCode"));
		$clientObject->setEffectiveDate($this->getDate("effectiveDate"));
		$clientObject->setExpirationDate($this->getDate("expirationDate"));
		$clientObject->setLevel($this->getInteger("level"));
		$clientObject->setLength($this->getInteger("length"));
		$clientObject->setValue($this->getInteger("value"));
		$clientObject->setFreezeBeginDate($this->getDate("freezeBeginDate"));
		$clientObject->setFreezeEndDate($this->getDate("freezeEndDate"));
		$clientObject->setIsVisibleExternally($this->getBoolean("isVisibleExternally"));
		$clientObject->setIndustryType($this->getString("industryType"));
		if (!($this->isAttributeValueNull("customerAccount"))){
			$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
			$customerAccount->addContract($clientObject);
			$clientObject->setCustomerAccount($customerAccount);
		}
		if (!($this->isAttributeValueNull("defaultPaymentPlan"))){
			$clientObject->setDefaultPaymentPlan($context->getPaymentPlan($this->facade, "defaultPaymentPlan"));
		}
		if (!($this->isAttributeValueNull("invoice"))){
			$clientObject->setInvoice($context->getRevenueTransaction($this->facade, "invoice"));
		}
		$children = $this->facade->getNodeList();
		if (!is_null($children)){
			foreach ($children as $child){
				if ("freezeAdjustment" == $child->getName()){
					$adjustment = null;
					if (is_null($clientObject->getFreezeAdjustment())){
						$adjustment = new Adjustment();
					}
					else {
						$adjustment = $clientObject->getFreezeAdjustment();
					}
					$adjustmentTransformationHelper = new XMLTransformationHelper($child);
					$adjustmentTransformationHelper->adjustmentFromXML($adjustment, $context);
					$clientObject->setFreezeAdjustment($adjustment);
					$adjustment->setContract($clientObject);
					$adjustment->setPaymentPlan($clientObject->getDefaultPaymentPlan());
				}
				else {
					if ("cancellationAdjustment" == $child->getName()){
						$adjustment = null;
						if (is_null($clientObject->getCancellationAdjustment())){
							$adjustment = new Adjustment();
						}
						else {
							$adjustment = $clientObject->getCancellationAdjustment();
						}
						$adjustmentTransformationHelper = new XMLTransformationHelper($child);
						$adjustmentTransformationHelper->adjustmentFromXML($adjustment, $context);
						$clientObject->setCancellationAdjustment($adjustment);
						$adjustment->setContract($clientObject);
						$adjustment->setPaymentPlan($clientObject->getDefaultPaymentPlan());
					}
				}
			}
		}
	}
	/*
	 *$method.comment
	 */
	public function paymentOptionToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $this->refIdToXML($clientObject->getId(), $clientObject->getRefId()));
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("isActive", $clientObject->getIsActive());
		$this->addAtribute("type", $clientObject->getType());
		$this->addAtribute("number", $clientObject->getNumber());
		$this->addAtribute("accessory", $clientObject->getAccessory());
		$this->addAtribute("holderName", $clientObject->getHolderName());
		$this->addAtribute("street1", $clientObject->getStreet1());
		$this->addAtribute("street2", $clientObject->getStreet2());
		$this->addAtribute("city", $clientObject->getCity());
		$this->addAtribute("state", $clientObject->getState());
		$this->addAtribute("zipCode", $clientObject->getZipCode());
		$this->addAtribute("countryType", $clientObject->getCountryType());
		$this->addAtribute("lastUpdateDate", $clientObject->getLastUpdateDate());
		$this->addAtribute("issue", $clientObject->getIssue());
		$this->addAtribute("maskedNumber", $clientObject->getMaskedNumber());
		$this->addAtribute("maskedAccessory", $clientObject->getMaskedAccessory());
		$this->addAtribute("cvv2", $clientObject->getCvv2());
		$this->addAtribute("phone", $clientObject->getPhone());
		$this->addAtribute("isVisibleExternally", $clientObject->getIsVisibleExternally());
		$this->addAtribute("tokenCode", $clientObject->getTokenCode());
		$this->addAtribute("isUpdateRequired", $clientObject->getIsUpdateRequired());
		$this->addAtribute("lastAccountUpdateDate", $clientObject->getLastAccountUpdateDate());
		if (!is_null($clientObject->getBankInfo())){
			$this->addAtribute("bankInfo", $clientObject->getBankInfo()->getId());
			$bankInfo = $this->facade->createChild("bankInfo");
			$bankInfoTransformationHelper = new XMLTransformationHelper($bankInfo);
			$bankInfoTransformationHelper->bankInfoToXML($clientObject->getBankInfo());
		}
	}
	/*
	 *$method.comment
	 */
	public function paymentOptionFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId(Helper::getRefId($this->facade->getAttributeValue("refId")));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setIsActive($this->getBoolean("isActive"));
		$clientObject->setHolderName($this->getString("holderName"));
		$clientObject->setNumber($this->getString("number"));
		$clientObject->setAccessory($this->getString("accessory"));
		$clientObject->setStreet1($this->getString("street1"));
		$clientObject->setStreet2($this->getString("street2"));
		$clientObject->setCity($this->getString("city"));
		$clientObject->setState($this->getString("state"));
		$clientObject->setZipCode($this->getString("zipCode"));
		$clientObject->setCountryType($this->getString("countryType"));
		$clientObject->setType($this->getString("type"));
		$clientObject->setIssue($this->getString("issue"));
		$clientObject->setCvv2($this->getString("cvv2"));
		$clientObject->setPhone($this->getString("phone"));
		$clientObject->setMaskedAccessory($this->getString("maskedAccessory"));
		$clientObject->setMaskedNumber($this->getString("maskedNumber"));
		$clientObject->setLastUpdateDate($this->getDate("lastUpdateDate"));
		$clientObject->setIsVisibleExternally($this->getBoolean("isVisibleExternally"));
		$clientObject->setTokenCode($this->getString("tokenCode"));
		$clientObject->setIsUpdateRequired($this->getBoolean("isUpdateRequired"));
		$clientObject->setLastAccountUpdateDate($this->getDate("lastAccountUpdateDate"));
		if (!($this->isAttributeValueNull("bankInfo"))){
			$clientObject->setBankInfo($this->getBankInfo($this->facade->getNodeList()));
		}
		$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
		$customerAccount->addPaymentOption($clientObject);
		$clientObject->setCustomerAccount($customerAccount);
		if (is_null($customerAccount->getMerchantAccountCode())){
			$customerAccount->setMerchantAccountCode($clientObject->getMerchantAccountCode());
		}
	}
	/*
	 *$method.comment
	 */
	public function revenueTransactionToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $this->refIdToXML($clientObject->getId(), $clientObject->getRefId()));
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("accountActivityType", $clientObject->getAccountActivityType());
		$this->addAtribute("amount", $clientObject->getAmount());
		$this->addAtribute("balance", $clientObject->getBalance());
		$this->addAtribute("historicalBalance", $clientObject->getHistoricalBalance());
		$this->addAtribute("note", $clientObject->getNote());
		$this->addAtribute("creatorCode", $clientObject->getCreatorCode());
		$this->addAtribute("sellerCode", $clientObject->getSellerCode());
		$this->addAtribute("shiftCode", $clientObject->getShiftCode());
		$this->addAtribute("terminalCode", $clientObject->getTerminalCode());
		$this->addAtribute("taxAmount", $clientObject->getTaxAmount());
		if (!is_null($clientObject->getAdjustmentTransaction())){
			$this->addAtribute("adjustmentTransaction", $this->refIdToXML($clientObject->getAdjustmentTransaction()->getId(), $clientObject->getAdjustmentTransaction()->getRefId()));
		}
		$this->addAtribute("dueDate", $clientObject->getDueDate());
		$this->addAtribute("itemCode", $clientObject->getItemCode());
		$this->addAtribute("isComplimentary", $clientObject->getIsComplimentary());
		$this->addAtribute("isVisibleExternally", $clientObject->getIsVisibleExternally());
		$this->addAtribute("isForceStatement", $clientObject->getIsForceStatement());
		$this->addAtribute("posterCode", $clientObject->getPosterCode());
		$this->addAtribute("balancingGroup", $clientObject->getBalancingGroup());
		if (!is_null($clientObject->getPaymentOption())){
			$this->addAtribute("paymentOption", $this->refIdToXML($clientObject->getPaymentOption()->getId(), $clientObject->getPaymentOption()->getRefId()));
		}
	}
	/*
	 *$method.comment
	 */
	public function revenueTransactionFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId(Helper::getRefId($this->facade->getAttributeValue("refId")));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setAccountActivityType($this->getString("accountActivityType"));
		$clientObject->setAmount($this->getInteger("amount"));
		$clientObject->setBalance($this->getInteger("balance"));
		$clientObject->setHistoricalBalance($this->getInteger("historicalBalance"));
		$clientObject->setNote($this->getString("note"));
		$clientObject->setCreatorCode($this->getString("creatorCode"));
		$clientObject->setSellerCode($this->getString("sellerCode"));
		$clientObject->setShiftCode($this->getString("shiftCode"));
		$clientObject->setTerminalCode($this->getString("terminalCode"));
		$clientObject->setTaxAmount($this->getInteger("taxAmount"));
		$clientObject->setDueDate($this->getDate("dueDate"));
		$clientObject->setItemCode($this->getString("itemCode"));
		$clientObject->setIsComplimentary($this->getBoolean("isComplimentary"));
		$clientObject->setIsVisibleExternally($this->getBoolean("isVisibleExternally"));
		$clientObject->setIsForceStatement($this->getBoolean("isForceStatement"));
		$clientObject->setPosterCode($this->getString("posterCode"));
		$clientObject->setBalancingGroup($this->getString("balancingGroup"));
		$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
		$customerAccount->addRevenueTransaction($clientObject);
		$clientObject->setCustomerAccount($customerAccount);
		if (is_null($customerAccount->getMerchantAccountCode())){
			$customerAccount->setMerchantAccountCode($clientObject->getMerchantAccountCode());
		}
		if (!($this->isAttributeValueNull("paymentOption"))){
			$clientObject->setPaymentOption($context->getPaymentOption($this->facade, "paymentOption"));
		}
		if (!($this->isAttributeValueNull("adjustmentTransaction"))){
			$clientObject->setAdjustmentTransaction($context->getRevenueTransaction($this->facade, "adjustmentTransaction"));
		}
	}
	/*
	 *$method.comment
	 */
	public function assetTransactionToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $this->refIdToXML($clientObject->getId(), $clientObject->getRefId()));
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("accountActivityType", $clientObject->getAccountActivityType());
		$this->addAtribute("amount", $clientObject->getAmount());
		$this->addAtribute("balance", $clientObject->getBalance());
		$this->addAtribute("historicalBalance", $clientObject->getHistoricalBalance());
		$this->addAtribute("note", $clientObject->getNote());
		$this->addAtribute("creatorCode", $clientObject->getCreatorCode());
		$this->addAtribute("sellerCode", $clientObject->getSellerCode());
		$this->addAtribute("shiftCode", $clientObject->getShiftCode());
		$this->addAtribute("terminalCode", $clientObject->getTerminalCode());
		$this->addAtribute("taxAmount", $clientObject->getTaxAmount());
		$this->addAtribute("dueDate", $clientObject->getDueDate());
		$this->addAtribute("maskedAccountNumber", $clientObject->getMaskedAccountNumber());
		$this->addAtribute("maskedAccessory", $clientObject->getMaskedAccessory());
		if (!is_null($clientObject->getAdjustmentTransaction())){
			$this->addAtribute("adjustmentTransaction", $this->refIdToXML($clientObject->getAdjustmentTransaction()->getId(), $clientObject->getAdjustmentTransaction()->getRefId()));
		}
		$this->addAtribute("transactionType", $clientObject->getTransactionType());
		$this->addAtribute("isPrepayment", $clientObject->getIsPrepayment());
		$this->addAtribute("isCollectionsTransaction", $clientObject->getIsCollectionsTransaction());
		$this->addAtribute("isVisibleExternally", $clientObject->getIsVisibleExternally());
		$this->addAtribute("transactionSourceType", $clientObject->getTransactionSourceType());
		$this->addAtribute("posterCode", $clientObject->getPosterCode());
		$this->addAtribute("balancingGroup", $clientObject->getBalancingGroup());
		$this->addAtribute("checkNumber", $clientObject->getCheckNumber());
		if (!is_null($clientObject->getCaptureInfo())){
			$this->addAtribute("captureInfo", $clientObject->getCaptureInfo()->getRefId());
			$child = $this->facade->createChild("captureInfo");
			$transformationHelper = new XMLTransformationHelper($child);
			$transformationHelper->captureInfoToXML($clientObject->getCaptureInfo());
		}
	}
	/*
	 *$method.comment
	 */
	public function assetTransactionFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId(Helper::getRefId($this->facade->getAttributeValue("refId")));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setAccountActivityType($this->getString("accountActivityType"));
		$clientObject->setAmount($this->getInteger("amount"));
		$clientObject->setBalance($this->getInteger("balance"));
		$clientObject->setHistoricalBalance($this->getInteger("historicalBalance"));
		$clientObject->setNote($this->getString("note"));
		$clientObject->setCreatorCode($this->getString("creatorCode"));
		$clientObject->setSellerCode($this->getString("sellerCode"));
		$clientObject->setShiftCode($this->getString("shiftCode"));
		$clientObject->setTerminalCode($this->getString("terminalCode"));
		$clientObject->setTaxAmount($this->getInteger("taxAmount"));
		$clientObject->setTransactionType($this->getString("transactionType"));
		$clientObject->setIsPrepayment($this->getBoolean("isPrepayment"));
		$clientObject->setIsCollectionsTransaction($this->getBoolean("isCollectionsTransaction"));
		$clientObject->setAccountNumber($this->getString("accountNumber"));
		$clientObject->setAccessory($this->getString("accessory"));
		$clientObject->setMaskedAccountNumber($this->getString("maskedAccountNumber"));
		$clientObject->setMaskedAccessory($this->getString("maskedAccessory"));
		$clientObject->setDueDate($this->getDate("dueDate"));
		$clientObject->setIsVisibleExternally($this->getBoolean("isVisibleExternally"));
		$clientObject->setTransactionSourceType($this->getString("transactionSourceType"));
		$clientObject->setPosterCode($this->getString("posterCode"));
		$clientObject->setBalancingGroup($this->getString("balancingGroup"));
		$clientObject->setCheckNumber($this->getString("checkNumber"));
		$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
		$customerAccount->addAssetTransaction($clientObject);
		$clientObject->setCustomerAccount($customerAccount);
		if (is_null($customerAccount->getMerchantAccountCode())){
			$customerAccount->setMerchantAccountCode($clientObject->getMerchantAccountCode());
		}
		if (!($this->isAttributeValueNull("adjustmentTransaction"))){
			$clientObject->setAdjustmentTransaction($context->getAssetTransaction($this->facade, "adjustmentTransaction"));
		}
		$child = $this->facade->getChild("captureInfo");
		if (!is_null($child)){
			$captureInfo = null;
			if (is_null($clientObject->getCaptureInfo())){
				$captureInfo = new CaptureInfo();
			}
			else {
				$captureInfo = $clientObject->getCaptureInfo();
			}
			$transformationHelper = new XMLTransformationHelper($child);
			$transformationHelper->captureInfoFromXML($captureInfo, $context);
			$captureInfo->setTaxAmount($clientObject->getTaxAmount());
			$captureInfo->setMerchantAccountCode($clientObject->getMerchantAccountCode());
			$captureInfo->setTransactionType($clientObject->getTransactionType());
			$clientObject->setCaptureInfo($captureInfo);
		}
	}
	/*
	 *$method.comment
	 */
	private function captureInfoToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $clientObject->getRefId());
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("accountNumber", $clientObject->getAccountNumber());
		$this->addAtribute("accessory", $clientObject->getAccessory());
		$this->addAtribute("transactionType", $clientObject->getTransactionType());
		$this->addAtribute("holderName", $clientObject->getHolderName());
		$this->addAtribute("street", $clientObject->getStreet());
		$this->addAtribute("city", $clientObject->getCity());
		$this->addAtribute("state", $clientObject->getState());
		$this->addAtribute("zipCode", $clientObject->getZipCode());
		$this->addAtribute("phone", $clientObject->getPhone());
		$this->addAtribute("email", $clientObject->getEmail());
		$this->addAtribute("taxAmount", $clientObject->getTaxAmount());
		$this->addAtribute("requestDate", $clientObject->getRequestDate());
		$this->addAtribute("responseDate", $clientObject->getResponseDate());
		$this->addAtribute("returnType", $clientObject->getReturnType());
		$this->addAtribute("approvalCode", $clientObject->getApprovalCode());
		$this->addAtribute("referenceNumber", $clientObject->getReferenceNumber());
		$this->addAtribute("cvv2", $clientObject->getCvv2());
		$this->addAtribute("trackData", $clientObject->getTrackData());
		$this->addAtribute("sysMessage", $clientObject->getSysMessage());
		$this->addAtribute("tokenCode", $clientObject->getTokenCode());
		if (!is_null($clientObject->getBankInfo())){
			$this->addAtribute("bankInfo", $clientObject->getBankInfo()->getId());
			$bankInfo = $this->facade->createChild("bankInfo");
			$bankInfoTransformationHelper = new XMLTransformationHelper($bankInfo);
			$bankInfoTransformationHelper->bankInfoToXML($clientObject->getBankInfo());
		}
	}
	/*
	 *$method.comment
	 */
	private function captureInfoFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId($this->getLong("refId"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setTransactionType($this->getString("transactionType"));
		$clientObject->setHolderName($this->getString("holderName"));
		$clientObject->setStreet($this->getString("street"));
		$clientObject->setCity($this->getString("city"));
		$clientObject->setState($this->getString("state"));
		$clientObject->setZipCode($this->getString("zipCode"));
		$clientObject->setPhone($this->getString("phone"));
		$clientObject->setEmail($this->getString("email"));
		$clientObject->setTaxAmount($this->getInteger("taxAmount"));
		$clientObject->setAccessory($this->getString("accessory"));
		$clientObject->setCvv2($this->getString("cvv2"));
		$clientObject->setTokenCode($this->getString("tokenCode"));
		$clientObject->setReferenceNumber($this->getString("referenceNumber"));
		$clientObject->setRequestDate($this->getDate("requestDate"));
		$clientObject->setResponseDate($this->getDate("responseDate"));
		$clientObject->setReturnType($this->getString("returnType"));
		$clientObject->setApprovalCode($this->getString("approvalCode"));
		$clientObject->setSysMessage($this->getString("sysMessage"));
		$clientObject->setAccountNumber($this->getString("accountNumber"));
		$clientObject->setTrackData($this->getString("trackData"));
		if (!($this->isAttributeValueNull("bankInfo"))){
			$clientObject->setBankInfo($this->getBankInfo($this->facade->getNodeList()));
		}
	}
	/*
	 *$method.comment
	 */
	public function adjustmentToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $clientObject->getRefId());
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("adjustmentReasonCode", $clientObject->getAdjustmentReasonCode());
		$this->addAtribute("type", $clientObject->getType());
		$this->addAtribute("renewalAmount", $clientObject->getRenewalAmount());
		$this->addAtribute("length", $clientObject->getLength());
		$this->addAtribute("value", $clientObject->getValue());
		$this->addAtribute("expirationDate", $clientObject->getExpirationDate());
		$this->addAtribute("adjustmentBeginDate", $clientObject->getAdjustmentBeginDate());
		$this->addAtribute("adjustmentEndDate", $clientObject->getAdjustmentEndDate());
		$this->addAtribute("isProcessed", $clientObject->getIsProcessed());
		$this->addAtribute("notes", $clientObject->getNotes());
		$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
		if (!is_null($clientObject->getPaymentPlan())){
			$this->addAtribute("paymentPlan", $this->refIdToXML($clientObject->getPaymentPlan()->getId(), $clientObject->getPaymentPlan()->getRefId()));
		}
	}
	/*
	 *$method.comment
	 */
	public function adjustmentFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId($this->getLong("refId"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setAdjustmentReasonCode($this->getString("adjustmentReasonCode"));
		$clientObject->setType($this->getString("type"));
		$clientObject->setRenewalAmount($this->getInteger("renewalAmount"));
		$clientObject->setLength($this->getInteger("length"));
		$clientObject->setValue($this->getInteger("value"));
		$clientObject->setExpirationDate($this->getDate("expirationDate"));
		$clientObject->setAdjustmentBeginDate($this->getDate("adjustmentBeginDate"));
		$clientObject->setAdjustmentEndDate($this->getDate("adjustmentEndDate"));
		$clientObject->setIsProcessed($this->getBoolean("isProcessed"));
		$clientObject->setNotes($this->getString("notes"));
		$clientObject->setCustomerAccount($context->getCustomerAccount($this->facade, "customerAccount"));
		if (!($this->isAttributeValueNull("paymentPlan"))){
			$clientObject->setPaymentPlan($context->getPaymentPlan($this->facade, "paymentPlan"));
		}
	}
	/*
	 *$method.comment
	 */
	public function addressToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $clientObject->getRefId());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("creatorCode", $clientObject->getCreatorCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("zipCode", $clientObject->getZipCode());
		$this->addAtribute("city", $clientObject->getCity());
		$this->addAtribute("state", $clientObject->getState());
		$this->addAtribute("street1", $clientObject->getStreet1());
		$this->addAtribute("street2", $clientObject->getStreet2());
		$this->addAtribute("countryType", $clientObject->getCountryType());
		$this->addAtribute("addressSourceType", $clientObject->getAddressSourceType());
		$this->addAtribute("addressType", $clientObject->getAddressType());
		$this->addAtribute("isBad", $clientObject->getIsBad());
		$this->addAtribute("badReason", $clientObject->getBadReason());
		$this->addAtribute("name", $clientObject->getName());
		$this->addAtribute("isSkipTraceAllowed", $clientObject->getIsSkipTraceAllowed());
		$this->addAtribute("lastSkipTraceDate", $clientObject->getLastSkipTraceDate());
		$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
	}
	/*
	 *$method.comment
	 */
	public function addressFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId($this->getLong("refId"));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setCreatorCode($this->getString("creatorCode"));
		$clientObject->setZipCode($this->getString("zipCode"));
		$clientObject->setCity($this->getString("city"));
		$clientObject->setState($this->getString("state"));
		$clientObject->setStreet1($this->getString("street1"));
		$clientObject->setStreet2($this->getString("street2"));
		$clientObject->setCountryType($this->getString("countryType"));
		$clientObject->setAddressSourceType($this->getString("addressSourceType"));
		$clientObject->setAddressType($this->getString("addressType"));
		$clientObject->setIsBad($this->getBoolean("isBad"));
		$clientObject->setBadReason($this->getString("badReason"));
		$clientObject->setName($this->getString("name"));
		$clientObject->setIsSkipTraceAllowed($this->getBoolean("isSkipTraceAllowed"));
		$clientObject->setLastSkipTraceDate($this->getDate("lastSkipTraceDate"));
		$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
		$customerAccount->addAddress($clientObject);
		$clientObject->setCustomerAccount($customerAccount);
		if (is_null($customerAccount->getMerchantAccountCode())){
			$customerAccount->setMerchantAccountCode($clientObject->getMerchantAccountCode());
		}
	}
	/*
	 *$method.comment
	 */
	public function phoneToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $clientObject->getRefId());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("creatorCode", $clientObject->getCreatorCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("phone", $clientObject->getPhone());
		$this->addAtribute("extention", $clientObject->getExtention());
		$this->addAtribute("addressSourceType", $clientObject->getAddressSourceType());
		$this->addAtribute("phoneType", $clientObject->getPhoneType());
		$this->addAtribute("isBad", $clientObject->getIsBad());
		$this->addAtribute("badReason", $clientObject->getBadReason());
		$this->addAtribute("isSkipTraceAllowed", $clientObject->getIsSkipTraceAllowed());
		$this->addAtribute("lastSkipTraceDate", $clientObject->getLastSkipTraceDate());
		$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
	}
	/*
	 *$method.comment
	 */
	public function phoneFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId($this->getLong("refId"));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setCreatorCode($this->getString("creatorCode"));
		$clientObject->setPhone($this->getString("phone"));
		$clientObject->setExtention($this->getString("extention"));
		$clientObject->setAddressSourceType($this->getString("addressSourceType"));
		$clientObject->setPhoneType($this->getString("phoneType"));
		$clientObject->setIsBad($this->getBoolean("isBad"));
		$clientObject->setBadReason($this->getString("badReason"));
		$clientObject->setIsSkipTraceAllowed($this->getBoolean("isSkipTraceAllowed"));
		$clientObject->setLastSkipTraceDate($this->getDate("lastSkipTraceDate"));
		$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
		$customerAccount->addPhone($clientObject);
		$clientObject->setCustomerAccount($customerAccount);
		if (is_null($customerAccount->getMerchantAccountCode())){
			$customerAccount->setMerchantAccountCode($clientObject->getMerchantAccountCode());
		}
	}
	/*
	 *$method.comment
	 */
	public function emailAddressToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $clientObject->getRefId());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("creatorCode", $clientObject->getCreatorCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("email", $clientObject->getEmail());
		$this->addAtribute("addressSourceType", $clientObject->getAddressSourceType());
		$this->addAtribute("emailType", $clientObject->getEmailType());
		$this->addAtribute("isBad", $clientObject->getIsBad());
		$this->addAtribute("badReason", $clientObject->getBadReason());
		$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
	}
	/*
	 *$method.comment
	 */
	public function emailAddressFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId($this->getLong("refId"));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setCreatorCode($this->getString("creatorCode"));
		$clientObject->setEmail($this->getString("email"));
		$clientObject->setAddressSourceType($this->getString("addressSourceType"));
		$clientObject->setEmailType($this->getString("emailType"));
		$clientObject->setIsBad($this->getBoolean("isBad"));
		$clientObject->setBadReason($this->getString("badReason"));
		$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
		$customerAccount->addEmailAddress($clientObject);
		$clientObject->setCustomerAccount($customerAccount);
		if (is_null($customerAccount->getMerchantAccountCode())){
			$customerAccount->setMerchantAccountCode($clientObject->getMerchantAccountCode());
		}
	}
	/*
	 *$method.comment
	 */
	public function noteToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $clientObject->getRefId());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("noteCode", $clientObject->getNoteCode());
		$this->addAtribute("creatorCode", $clientObject->getCreatorCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("description", $clientObject->getDescription());
		$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
	}
	/*
	 *$method.comment
	 */
	public function noteFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId($this->getLong("refId"));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setNoteCode($this->getString("noteCode"));
		$clientObject->setCreatorCode($this->getString("creatorCode"));
		$clientObject->setDescription($this->getString("description"));
		$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
		$customerAccount->addNote($clientObject);
		$clientObject->setCustomerAccount($customerAccount);
		if (is_null($customerAccount->getMerchantAccountCode())){
			$customerAccount->setMerchantAccountCode($clientObject->getMerchantAccountCode());
		}
	}
	/*
	 *$method.comment
	 */
	public function actionToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $clientObject->getRefId());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("actionCode", $clientObject->getActionCode());
		$this->addAtribute("creatorCode", $clientObject->getCreatorCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
	}
	/*
	 *$method.comment
	 */
	public function actionFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId($this->getLong("refId"));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setActionCode($this->getString("actionCode"));
		$clientObject->setCreatorCode($this->getString("creatorCode"));
		$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
		$customerAccount->addAction($clientObject);
		$clientObject->setCustomerAccount($customerAccount);
		if (is_null($customerAccount->getMerchantAccountCode())){
			$customerAccount->setMerchantAccountCode($clientObject->getMerchantAccountCode());
		}
	}
	/*
	 *$method.comment
	 */
	public function letterToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $clientObject->getRefId());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("letterCode", $clientObject->getLetterCode());
		$this->addAtribute("creatorCode", $clientObject->getCreatorCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("dueDate", $clientObject->getDueDate());
		$this->addAtribute("status", $clientObject->getStatus());
		$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
		$this->addAtribute("name", $clientObject->getName());
		$this->addAtribute("city", $clientObject->getCity());
		$this->addAtribute("street1", $clientObject->getStreet1());
		$this->addAtribute("street2", $clientObject->getStreet2());
		$this->addAtribute("state", $clientObject->getState());
		$this->addAtribute("zipCode", $clientObject->getZipCode());
		$this->addAtribute("email", $clientObject->getEmail());
		$this->addAtribute("documentCode", $clientObject->getDocumentCode());
		$this->addAtribute("isStatement", $clientObject->getIsStatement());
		$this->addAtribute("customField1", $clientObject->getCustomField1());
		$this->addAtribute("customField2", $clientObject->getCustomField2());
		$this->addAtribute("customField3", $clientObject->getCustomField3());
		$this->addAtribute("customField4", $clientObject->getCustomField4());
		$this->addAtribute("customField5", $clientObject->getCustomField5());
		$this->addAtribute("isVisibleExternally", $clientObject->getIsVisibleExternally());
	}
	/*
	 *$method.comment
	 */
	public function letterFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId($this->getLong("refId"));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setDueDate($this->getDate("dueDate"));
		$clientObject->setLetterCode($this->getString("letterCode"));
		$clientObject->setCreatorCode($this->getString("creatorCode"));
		$clientObject->setStatus($this->getString("status"));
		$clientObject->setName($this->getString("name"));
		$clientObject->setCity($this->getString("city"));
		$clientObject->setStreet1($this->getString("street1"));
		$clientObject->setStreet2($this->getString("street2"));
		$clientObject->setState($this->getString("state"));
		$clientObject->setZipCode($this->getString("zipCode"));
		$clientObject->setEmail($this->getString("email"));
		$clientObject->setDocumentCode($this->getLong("documentCode"));
		$clientObject->setIsStatement($this->getBoolean("isStatement"));
		$clientObject->setCustomField1($this->getString("customField1"));
		$clientObject->setCustomField2($this->getString("customField2"));
		$clientObject->setCustomField3($this->getString("customField3"));
		$clientObject->setCustomField4($this->getString("customField4"));
		$clientObject->setCustomField5($this->getString("customField5"));
		$clientObject->setIsVisibleExternally($this->getBoolean("isVisibleExternally"));
		$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
		$customerAccount->addLetter($clientObject);
		$clientObject->setCustomerAccount($customerAccount);
		if (is_null($customerAccount->getMerchantAccountCode())){
			$customerAccount->setMerchantAccountCode($clientObject->getMerchantAccountCode());
		}
	}
	/*
	 *$method.comment
	 */
	public function emailToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $clientObject->getRefId());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("emailCode", $clientObject->getEmailCode());
		$this->addAtribute("creatorCode", $clientObject->getCreatorCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("dueDate", $clientObject->getDueDate());
		$this->addAtribute("status", $clientObject->getStatus());
		$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
		$this->addAtribute("firstName", $clientObject->getFirstName());
		$this->addAtribute("lastName", $clientObject->getLastName());
		$this->addAtribute("toEmail", $clientObject->getToEmail());
		$this->addAtribute("subject", $clientObject->getSubject());
		$this->addAtribute("message", $clientObject->getMessage());
		$this->addAtribute("attachmentCode", $clientObject->getAttachmentCode());
		$this->addAtribute("fileExtention", $clientObject->getFileExtention());
		$this->addAtribute("response", $clientObject->getResponse());
		$this->addAtribute("isVisibleExternally", $clientObject->getIsVisibleExternally());
	}
	/*
	 *$method.comment
	 */
	public function emailFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId($this->getLong("refId"));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setDueDate($this->getDate("dueDate"));
		$clientObject->setEmailCode($this->getString("emailCode"));
		$clientObject->setCreatorCode($this->getString("creatorCode"));
		$clientObject->setStatus($this->getString("status"));
		$clientObject->setFirstName($this->getString("firstName"));
		$clientObject->setLastName($this->getString("lastName"));
		$clientObject->setToEmail($this->getString("toEmail"));
		$clientObject->setSubject($this->getString("subject"));
		$clientObject->setMessage($this->getString("Message"));
		$clientObject->setAttachmentCode($this->getLong("attachmentCode"));
		$clientObject->setFileExtention($this->getString("fileExtention"));
		$clientObject->setResponse($this->getString("response"));
		$clientObject->setIsVisibleExternally($this->getBoolean("isVisibleExternally"));
		$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
		$customerAccount->addEmail($clientObject);
		$clientObject->setCustomerAccount($customerAccount);
		if (is_null($customerAccount->getMerchantAccountCode())){
			$customerAccount->setMerchantAccountCode($clientObject->getMerchantAccountCode());
		}
	}
	/*
	 *$method.comment
	 */
	public function scheduledPaymentToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $clientObject->getRefId());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("amount", $clientObject->getAmount());
		$this->addAtribute("dueDate", $clientObject->getDueDate());
		$this->addAtribute("status", $clientObject->getStatus());
		$this->addAtribute("creatorCode", $clientObject->getCreatorCode());
		$this->addAtribute("posterCode", $clientObject->getPosterCode());
		$this->addAtribute("isInvoiceRequired", $clientObject->getIsInvoiceRequired());
		$this->addAtribute("isVisibleExternally", $clientObject->getIsVisibleExternally());
		$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
		$this->addAtribute("paymentOption", $this->refIdToXML($clientObject->getPaymentOption()->getId(), $clientObject->getPaymentOption()->getRefId()));
		if (!is_null($clientObject->getAssetTransaction())){
			$this->addAtribute("assetTransaction", $this->refIdToXML($clientObject->getAssetTransaction()->getId(), $clientObject->getAssetTransaction()->getRefId()));
		}
	}
	/*
	 *$method.comment
	 */
	public function scheduledPaymentFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId($this->getLong("refId"));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setAmount($this->getInteger("amount"));
		$clientObject->setDueDate($this->getDate("dueDate"));
		$clientObject->setStatus($this->getString("status"));
		$clientObject->setCreatorCode($this->getString("creatorCode"));
		$clientObject->setPosterCode($this->getString("posterCode"));
		$clientObject->setIsInvoiceRequired($this->getBoolean("isInvoiceRequired"));
		$clientObject->setIsVisibleExternally($this->getBoolean("isVisibleExternally"));
		$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
		$customerAccount->addScheduledPayment($clientObject);
		$clientObject->setCustomerAccount($customerAccount);
		$clientObject->setPaymentOption($context->getPaymentOption($this->facade, "paymentOption"));
		if (!($this->isAttributeValueNull("assetTransaction"))){
			$clientObject->setAssetTransaction($context->getAssetTransaction($this->facade, "assetTransaction"));
		}
		if (is_null($customerAccount->getMerchantAccountCode())){
			$customerAccount->setMerchantAccountCode($clientObject->getMerchantAccountCode());
		}
	}
	/*
	 *$method.comment
	 */
	public function documentToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $clientObject->getRefId());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("documentCode", $clientObject->getDocumentCode());
		$this->addAtribute("documentContentCode", $clientObject->getDocumentContentCode());
		$this->addAtribute("creatorCode", $clientObject->getCreatorCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("description", $clientObject->getDescription());
		$this->addAtribute("isVisibleExternally", $clientObject->getIsVisibleExternally());
		$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
	}
	/*
	 *$method.comment
	 */
	public function documentFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId($this->getLong("refId"));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setDocumentCode($this->getString("documentCode"));
		$clientObject->setDocumentContentCode($this->getLong("documentContentCode"));
		$clientObject->setCreatorCode($this->getString("creatorCode"));
		$clientObject->setDescription($this->getString("description"));
		$clientObject->setIsVisibleExternally($this->getBoolean("isVisibleExternally"));
		$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
		$customerAccount->addDocument($clientObject);
		$clientObject->setCustomerAccount($customerAccount);
		if (is_null($customerAccount->getMerchantAccountCode())){
			$customerAccount->setMerchantAccountCode($clientObject->getMerchantAccountCode());
		}
	}
	/*
	 *$method.comment
	 */
	public function auditLogFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setUserCode($this->getString("userCode"));
		$clientObject->setObjectCode($this->getString("objectCode"));
		$clientObject->setObjectType($this->getString("objectType"));
		$children = $this->facade->getNodeList();
		if (!is_null($children)){
			$details = $clientObject->getAuditLogDetails();
			foreach ($children as $child){
				$detail = new AuditLogDetail();
				$auditLogDetailTransformationHelper = new XMLTransformationHelper($child);
				$auditLogDetailTransformationHelper->auditLogDetailFromXML($detail);
				$details->add($detail);
			}
		}
		$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
		$customerAccount->addAuditLog($clientObject);
		$clientObject->setCustomerAccount($customerAccount);
		if (is_null($customerAccount->getMerchantAccountCode())){
			$customerAccount->setMerchantAccountCode($clientObject->getMerchantAccountCode());
		}
	}
	/*
	 *$method.comment
	 */
	public function auditLogToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $clientObject->getRefId());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("userCode", $clientObject->getUserCode());
		$this->addAtribute("objectCode", $clientObject->getObjectCode());
		$this->addAtribute("objectType", $clientObject->getObjectType());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
		foreach ($clientObject->getAuditLogDetails() as $detail){
			$auditLogDetailFacade = $this->facade->createChild("auditLogDetail");
			$auditLogDetailTransformationHelper = new XMLTransformationHelper($auditLogDetailFacade);
			$auditLogDetailTransformationHelper->auditLogDetailToXML($detail);
		}
	}
	/*
	 *$method.comment
	 */
	private function auditLogDetailFromXML($clientObject){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setFieldName($this->getString("fieldName"));
		$clientObject->setOldValue($this->getString("oldValue"));
		$clientObject->setNewValue($this->getString("newValue"));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
	}
	/*
	 *$method.comment
	 */
	private function auditLogDetailToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("fieldName", $clientObject->getFieldName());
		$this->addAtribute("oldValue", $clientObject->getOldValue());
		$this->addAtribute("newValue", $clientObject->getNewValue());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
	}
	/*
	 *$method.comment
	 */
	public function bankInfoFromXML($clientObject){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId($this->getLong("refId"));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setBankName($this->getString("bankName"));
		$clientObject->setAddress($this->getString("address"));
		$clientObject->setCountry($this->getString("country"));
		$clientObject->setPhoneNumber($this->getString("phoneNumber"));
		$clientObject->setPaymentOptionType($this->getString("paymentOptionType"));
		$clientObject->setAccountType($this->getString("accountType"));
		$clientObject->setAccountCategory($this->getString("accountCategory"));
	}
	/*
	 *$method.comment
	 */
	public function bankInfoToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("bankName", $clientObject->getBankName());
		$this->addAtribute("address", $clientObject->getAddress());
		$this->addAtribute("country", $clientObject->getCountry());
		$this->addAtribute("phoneNumber", $clientObject->getPhoneNumber());
		$this->addAtribute("paymentOptionType", $clientObject->getPaymentOptionType());
		$this->addAtribute("accountType", $clientObject->getAccountType());
		$this->addAtribute("accountCategory", $clientObject->getAccountCategory());
	}
	/*
	 *$method.comment
	 */
	private function chargeToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $clientObject->getRefId());
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("type", $clientObject->getType());
		$this->addAtribute("index", $clientObject->getIndex());
		$this->addAtribute("isPrepaid", $clientObject->getIsPrepaid());
	}
	/*
	 *$method.comment
	 */
	private function chargeFromXML($clientObject){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId($this->getLong("refCode"));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setIndex($this->getInteger("index"));
		$clientObject->setType($this->getString("type"));
		$clientObject->setIsPrepaid($this->getBoolean("isPrepaid"));
		if (!is_null($clientObject->getId())){
			$clientObject->setCode($this->getString("code"));
		}
	}
	/*
	 *$method.comment
	 */
	public function paymentPlanToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $this->refIdToXML($clientObject->getId(), $clientObject->getRefId()));
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("type", $clientObject->getType());
		$this->addAtribute("status", $clientObject->getStatus());
		$this->addAtribute("amount", $clientObject->getAmount());
		$this->addAtribute("length", $clientObject->getLength());
		$this->addAtribute("value", $clientObject->getValue());
		$this->addAtribute("deferredLength", $clientObject->getDeferredLength());
		$this->addAtribute("deferredValue", $clientObject->getDeferredValue());
		$this->addAtribute("billingCycleCode", $clientObject->getBillingCycleCode());
		$this->addAtribute("firstBillingDate", $clientObject->getFirstBillingDate());
		$this->addAtribute("nextBillingDate", $clientObject->getNextBillingDate());
		$this->addAtribute("lastInvoicingDate", $clientObject->getLastInvoicingDate());
		$this->addAtribute("lastProcessingDate", $clientObject->getLastProcessingDate());
		$this->addAtribute("lastUpdateDate", $clientObject->getLastUpdateDate());
		$this->addAtribute("creatorCode", $clientObject->getCreatorCode());
		$this->addAtribute("sellerCode", $clientObject->getSellerCode());
		$this->addAtribute("itemCode", $clientObject->getItemCode());
		$this->addAtribute("taxCode", $clientObject->getTaxCode());
		$this->addAtribute("taxAmount", $clientObject->getTaxAmount());
		$this->addAtribute("renewalTaxAmount", $clientObject->getRenewalTaxAmount());
		$this->addAtribute("recurrenceCount", $clientObject->getRecurrenceCount());
		$this->addAtribute("recurrence", $clientObject->getRecurrence());
		
		if (!is_null($clientObject->getContract())){
			$this->addAtribute("contract", $this->refIdToXML($clientObject->getContract()->getId(), $clientObject->getContract()->getRefId()));
		}
		if (!is_null($clientObject->getPaymentOption())){
			$this->addAtribute("paymentOption", $this->refIdToXML($clientObject->getPaymentOption()->getId(), $clientObject->getPaymentOption()->getRefId()));
		}
		if (!is_null($clientObject->getLinkedParentPlan())){
			$this->addAtribute("parentPaymentPlan", $this->refIdToXML($clientObject->getLinkedParentPlan()->getId(), $clientObject->getLinkedParentPlan()->getRefId()));
		}
		$this->addAtribute("groupCode1", $clientObject->getGroupCode1());
		$this->addAtribute("groupCode2", $clientObject->getGroupCode2());
		$this->addAtribute("groupCode3", $clientObject->getGroupCode3());
		$this->addAtribute("groupCode4", $clientObject->getGroupCode4());
		$this->addAtribute("groupCode5", $clientObject->getGroupCode5());
		$this->addAtribute("groupCode6", $clientObject->getGroupCode6());
		$this->addAtribute("groupCode7", $clientObject->getGroupCode7());
		$this->addAtribute("groupCode8", $clientObject->getGroupCode8());
		$this->addAtribute("linkAccount", $clientObject->getLinkAccount());
		$this->addAtribute("terminatorCode", $clientObject->getTerminatorCode());
		$this->addAtribute("customFields", Helper::toString($clientObject->getCustomFields()));
		$this->addAtribute("contractDocumentCode", $clientObject->getContractDocumentCode());
		$this->addAtribute("isVisibleExternally", $clientObject->getIsVisibleExternally());
		$this->addAtribute("isAgainstBalance", $clientObject->getIsAgainstBalance());
		$this->addAtribute("renewalAmount", $clientObject->getRenewalAmount());
		$this->addAtribute("renewedDate", $clientObject->getRenewedDate());
		$this->addAtribute("idDocumentCode", $clientObject->getIdDocumentCode());
		if (!is_null($clientObject->getAdjustment())){
			$this->addAtribute("adjustment", $clientObject->getAdjustment()->getRefId());
			$adjustment = $this->facade->createChild("adjustment");
			$adjustmentTransformationHelper = new XMLTransformationHelper($adjustment);
			$adjustmentTransformationHelper->adjustmentToXML($clientObject->getAdjustment());
		}
		if (!is_null($clientObject->getCancellationAdjustment())){
			$this->addAtribute("cancellationAdjustment", $clientObject->getCancellationAdjustment()->getRefId());
			$adjustment = $this->facade->createChild("cancellationAdjustment");
			$adjustmentTransformationHelper = new XMLTransformationHelper($adjustment);
			$adjustmentTransformationHelper->adjustmentToXML($clientObject->getCancellationAdjustment());
		}
		$index = 0;
		foreach ($clientObject->getChargesInternal() as $charge){
			$charge->setIndex($index);
			$index++;
			$chargeFacade = $this->facade->createChild("charge");
			$chargeTransformationHelper = new XMLTransformationHelper($chargeFacade);
			$chargeTransformationHelper->chargeToXML($charge);
		}
	}
	/*
	 *$method.comment
	 */
	public function paymentPlanFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId(Helper::getRefId($this->facade->getAttributeValue("refId")));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setCreatorCode($this->getString("creatorCode"));
		$clientObject->setSellerCode($this->getString("sellerCode"));
		$clientObject->setAmount($this->getInteger("amount"));
		$clientObject->setType($this->getString("type"));
		$clientObject->setStatus($this->getString("status"));
		$clientObject->setFirstBillingDate($this->getDate("firstBillingDate"));
		$clientObject->setBillingCycleCode($this->getString("billingCycleCode"));
		$clientObject->setNextBillingDate($this->getDate("nextBillingDate"));
		$clientObject->setItemCode($this->getString("itemCode"));
		$clientObject->setTaxCode($this->getString("taxCode"));
		$clientObject->setGroupCode1($this->getString("groupCode1"));
		$clientObject->setGroupCode2($this->getString("groupCode2"));
		$clientObject->setGroupCode3($this->getString("groupCode3"));
		$clientObject->setGroupCode4($this->getString("groupCode4"));
		$clientObject->setGroupCode5($this->getString("groupCode5"));
		$clientObject->setGroupCode6($this->getString("groupCode6"));
		$clientObject->setGroupCode7($this->getString("groupCode7"));
		$clientObject->setGroupCode8($this->getString("groupCode8"));
		$clientObject->setLinkAccount($this->getInteger("linkAccount"));
		$clientObject->setLastInvoicingDate($this->getDate("lastInvocingDate"));
		$clientObject->setLastProcessingDate($this->getDate("lastProcessingDate"));
		$clientObject->setLastUpdateDate($this->getDate("lastUpdateDate"));
		$clientObject->setTerminatorCode($this->getString("terminatorCode"));
		$clientObject->setCustomFields(Helper::fromString($this->getString("customFields")));
		$clientObject->setContractDocumentCode($this->getLong("contractDocumentCode"));
		$clientObject->setIsVisibleExternally($this->getBoolean("isVisibleExternally"));
		$clientObject->setIsAgainstBalance($this->getBoolean("isAgainstBalance"));
		$clientObject->setRenewalAmount($this->getInteger("renewalAmount"));
		$clientObject->setRenewedDate($this->getDate("renewedDate"));
		$clientObject->setTaxAmount($this->getInteger("taxAmount"));
		$clientObject->setRenewalTaxAmount($this->getInteger("renewalTaxAmount"));
		$clientObject->setIdDocumentCode($this->getLong("idDocumentCode"));
		$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
		$customerAccount->addPaymentPlan($clientObject);
		$clientObject->setCustomerAccount($customerAccount);
		$clientObject->setRecurrenceCount($this->getInteger("recurrenceCount"));
		$clientObject->setRecurrence($this->getInteger("recurrence"));
		
		if (is_null($customerAccount->getMerchantAccountCode())){
			$customerAccount->setMerchantAccountCode($clientObject->getMerchantAccountCode());
		}
		if (!($this->isAttributeValueNull("contract"))){
			$clientObject->setContract($context->getContract($this->facade, "contract"));
		}
		if (!($this->isAttributeValueNull("paymentOption"))){
			$clientObject->setPaymentOption($context->getPaymentOption($this->facade, "paymentOption"));
		}
		if (!($this->isAttributeValueNull("parentPaymentPlan"))){
			$clientObject->setLinkedParentPlan($context->getPaymentPlan($this->facade, "parentPaymentPlan"));
		}
		$children = $this->facade->getNodeList();
		if (!is_null($children)){
			$countCharges = $this->getCountCharge($children);
			$tempList = $clientObject->getCharges();
			$rangeCharges = $countCharges < $tempList->size() ? $countCharges : $tempList->size();
			$clientObject->createListCharges($countCharges);
			$indexes = array();
			for ($i = 0;$i < $countCharges;$i++){
				$indexes[$i] = false;
			}
			$chargePosition = 0;
			foreach ($children as $child){
				if ("charge" == $child->getName()){
					$charge = null;
					if ($chargePosition < $rangeCharges){
						$charge = $tempList->get($chargePosition);
					}
					else {
						$charge = new Charge();
					}
					$chargeTransformationHelper = new XMLTransformationHelper($child);
					$chargeTransformationHelper->chargeFromXML($charge);
					$charge->setMerchantAccountCode($clientObject->getMerchantAccountCode());
					$charge->setPaymentPlan($clientObject);
					if (is_null($charge->getType())){
						$charge->setType($clientObject->getType());
					}
					try{
						if ($indexes[$chargePosition]){
							throw new Exception("Dublicate index of charge. PaymentPlan code: %1$s; charge`s index: %2$s", $clientObject->getCode(), $chargePosition);
						}
						$indexes[$chargePosition] = true;
						$clientObject->addCharge($charge, $chargePosition);
						$chargePosition = $chargePosition + 1;
					}
					catch(ArrayIndexOutOfBoundsException $aiobe){
						throw new Exception("Invalid value of index of charge. PaymentPlan code: %1$s; charge`s index: %2$s", $clientObject->getCode(), $chargePosition);
					}
				}
				else {
					if ("adjustment" == $child->getName()){
						$adjustment = null;
						if (is_null($clientObject->getAdjustment())){
							$adjustment = new Adjustment();
						}
						else {
							$adjustment = $clientObject->getAdjustment();
						}
						$adjustmentTransformationHelper = new XMLTransformationHelper($child);
						$adjustmentTransformationHelper->adjustmentFromXML($adjustment, $context);
						$clientObject->setAdjustment($adjustment);
						$adjustment->setPaymentPlan($clientObject);
						$adjustment->setContract($clientObject->getContract());
					}
					else {
						if ("cancellationAdjustment" == $child->getName()){
							$adjustment = null;
							if (is_null($clientObject->getCancellationAdjustment())){
								$adjustment = new Adjustment();
							}
							else {
								$adjustment = $clientObject->getCancellationAdjustment();
							}
							$adjustmentTransformationHelper = new XMLTransformationHelper($child);
							$adjustmentTransformationHelper->adjustmentFromXML($adjustment, $context);
							$clientObject->setCancellationAdjustment($adjustment);
							$adjustment->setPaymentPlan($clientObject);
							$adjustment->setContract($clientObject->getContract());
						}
					}
				}
			}
		}
		$clientObject->setLength(!is_null($clientObject->getCharges()) ? $this->getLengthPaymentPlan($clientObject->getCharges()) : 0);
		$clientObject->setValue($clientObject->getLength() * $clientObject->getAmount());
		$clientObject->setDeferredLength(!is_null($clientObject->getCharges()) ? $clientObject->getCharges()->size() - $clientObject->getLength() : 0);
		$clientObject->setDeferredValue($clientObject->getDeferredLength() * $clientObject->getAmount());
	}
	/*
	 *$method.comment
	 */
	public function userToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $this->refIdToXML($clientObject->getId(), $clientObject->getRefId()));
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("isActive", $clientObject->getIsActive());
		$this->addAtribute("userName", $clientObject->getUserName());
		$this->addAtribute("name", $clientObject->getName());
		$this->addAtribute("roleType", $clientObject->getRoleType());
		$this->addAtribute("email", $clientObject->getEmail());
		$this->addAtribute("phone", $clientObject->getPhone());
		$this->addAtribute("isAgentCommissionAllowed", $clientObject->getIsAgentCommissionAllowed());
		if (!is_null($clientObject->getCustomerAccount())){
			$this->addAtribute("customerAccountCode", $clientObject->getCustomerAccountCode());
			$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
		}
	}
	/*
	 *$method.comment
	 */
	public function userFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId(Helper::getRefId($this->facade->getAttributeValue("refId")));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setIsActive($this->getBoolean("isActive"));
		$clientObject->setUserName($this->getString("userName"));
		$clientObject->setName($this->getString("name"));
		$clientObject->setRoleType($this->getString("roleType"));
		$clientObject->setEmail($this->getString("email"));
		$clientObject->setPhone($this->getString("phone"));
		$clientObject->setCustomerAccountCode($this->getString("customerAccountCode"));
		$clientObject->setIsAgentCommissionAllowed($this->getBoolean("isAgentCommissionAllowed"));
		if (!($this->facade->isAttributeValueNull("customerAccount"))){
			$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
			$customerAccount->setUser($clientObject);
			$clientObject->setCustomerAccount($customerAccount);
			$clientObject->addCustomerAccount($customerAccount);
		}
	}
	/*
	 *$method.comment
	 */
	public function agentCommissionToXML($clientObject){
		$this->addAtribute("id", $clientObject->getId());
		$this->addAtribute("refId", $this->refIdToXML($clientObject->getId(), $clientObject->getRefId()));
		$this->addAtribute("code", $clientObject->getCode());
		$this->addAtribute("merchantAccountCode", $clientObject->getMerchantAccountCode());
		$this->addAtribute("collectorAccountCode", $clientObject->getCollectorAccountCode());
		$this->addAtribute("createDate", $clientObject->getCreateDate());
		$this->addAtribute("creatorCode", $clientObject->getCreatorCode());
		$this->addAtribute("agentCode", $clientObject->getAgentCode());
		$this->addAtribute("amount", $clientObject->getAmount());
		$this->addAtribute("commissionType", $clientObject->getCommissionType());
		$this->addAtribute("note", $clientObject->getNote());
		if (!is_null($clientObject->getUser())){
			$this->addAtribute("user", $clientObject->getUser()->getUserName());
		}
		if (!is_null($clientObject->getCustomerAccount())){
			$this->addAtribute("customerAccount", $this->refIdToXML($clientObject->getCustomerAccount()->getId(), $clientObject->getCustomerAccount()->getRefId()));
		}
		if (!is_null($clientObject->getPaymentOption())){
			$this->addAtribute("paymentOption", $this->refIdToXML($clientObject->getPaymentOption()->getId(), $clientObject->getPaymentOption()->getRefId()));
		}
		if (!is_null($clientObject->getSource())){
			$this->addAtribute("source", $this->refIdToXML($clientObject->getSource()->getId(), $clientObject->getSource()->getRefId()));
		}
		if (!is_null($clientObject->getTarget())){
			$this->addAtribute("target", $this->refIdToXML($clientObject->getTarget()->getId(), $clientObject->getTarget()->getRefId()));
		}
		if (!is_null($clientObject->getAdjustmentAgentCommission())){
			$this->addAtribute("adjustmentAgentCommission", $this->refIdToXML($clientObject->getAdjustmentAgentCommission()->getId(), $clientObject->getAdjustmentAgentCommission()->getRefId()));
		}
	}
	/*
	 *$method.comment
	 */
	public function agentCommissionFromXML($clientObject,$context){
		$clientObject->setId($this->getLong("id"));
		$clientObject->setRefId(Helper::getRefId($this->facade->getAttributeValue("refId")));
		$clientObject->setMerchantAccountCode($this->getInteger("merchantAccountCode"));
		$clientObject->setCollectorAccountCode($this->getInteger("collectorAccountCode"));
		$clientObject->setCode($this->getString("code"));
		$clientObject->setCreateDate($this->getDate("createDate"));
		$clientObject->setCreatorCode($this->getString("creatorCode"));
		$clientObject->setAgentCode($this->getString("agentCode"));
		$clientObject->setAmount($this->getInteger("amount"));
		$clientObject->setCommissionType($this->getString("commissionType"));
		$clientObject->setNote($this->getString("note"));
		$clientObject->setUser($context->getUser($this->facade, "user"));
		if (!($this->facade->isAttributeValueNull("customerAccount"))){
			$customerAccount = $context->getCustomerAccount($this->facade, "customerAccount");
			$clientObject->setCustomerAccount($customerAccount);
			if (is_null($customerAccount->getMerchantAccountCode())){
				$customerAccount->setMerchantAccountCode($clientObject->getMerchantAccountCode());
			}
		}
		if (!($this->isAttributeValueNull("paymentOption"))){
			$clientObject->setPaymentOption($context->getPaymentOption($this->facade, "paymentOption"));
		}
		if (!($this->isAttributeValueNull("source"))){
			$clientObject->setSource($context->getRevenueTransaction($this->facade, "source"));
		}
		if (!($this->isAttributeValueNull("target"))){
			$clientObject->setTarget($context->getAssetTransaction($this->facade, "target"));
		}
		if (!($this->isAttributeValueNull("adjustmentAgentCommission"))){
			$clientObject->setAdjustmentAgentCommission($context->getAgentCommission($this->facade, "adjustmentAgentCommission"));
		}
	}
	/*
	 *$method.comment
	 */
	public static function queryParametersToXML($facade,$parameters){
		Helper::setQueryParameters($facade, $parameters);
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccountParameters(){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put("code", $this->getString("code"));
		$parameters->put("customerCode", $this->getString("customerCode"));
		$parameters->put("firstName", $this->getString("firstName"));
		$parameters->put("lastName", $this->getString("lastName"));
		$parameters->put("middleName", $this->getString("middleName"));
		$parameters->put("title", $this->getString("title"));
		$parameters->put("suffix", $this->getString("suffix"));
		$parameters->put("isActive", $this->getBooleanBasic("isActive"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put("phone", $this->getString("phone"));
		$parameters->put("email", $this->getString("email"));
		$parameters->put("zipCode", $this->getString("zipCode"));
		$parameters->put("socialSecurity", $this->getString("socialSecurity"));
		$parameters->put("driverLicenseNumber", $this->getString("driverLicenseNumber"));
		$parameters->put("creditCardNumber", $this->getString("creditCardNumber"));
		$parameters->put("bankAccountNumber", $this->getString("bankAccountNumber"));
		$parameters->put("contractCode", $this->getString("contractCode"));
		$parameters->put("paymentPlanCode", $this->getString("paymentPlanCode"));
		$parameters->put("fromBalance", $this->getInteger("fromBalance"));
		$parameters->put("toBalance", $this->getInteger("toBalance"));
		$parameters->put("accountGroupCode", $this->getString("customerAccountGroupCode"));
		$parameters->put("collectorCode", $this->getString("collectorCode"));
		$parameters->put("isNullCollectorCode", $this->getBooleanBasic("isNullCollectorCode"));
		$parameters->put("lastActionCode", $this->getString("lastActionCode"));
		$parameters->put("collectionsPriorityType", $this->getString("collectionsPriorityType"));
		$parameters->put("fromSentCollectionsDate", $this->getDate("fromSentCollectionsDate"));
		$parameters->put("toSentCollectionsDate", $this->getDate("toSentCollectionsDate"));
		$parameters->put("isNullSentCollectionsDate", $this->getBooleanBasic("isNullSentCollectionsDate"));
		$parameters->put("fromAgeCollectionsDate", $this->getDate("fromAgeCollectionsDate"));
		$parameters->put("toAgeCollectionsDate", $this->getDate("toAgeCollectionsDate"));
		$parameters->put("isLite", $this->getBoolean("isLite"));
		$parameters->put(Session::ORDER_BY, $this->getString(Session::ORDER_BY));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccountSpecialParameters(){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put("creditCardExpirationPeriod", Helper::asList($this->getString("creditCardExpirationPeriod")));
		$parameters->put("hasInactivePaymentOptionReferences", $this->getBooleanBasic("hasInactivePaymentOptionReferences"));
		$parameters->put("hasStatementPaymentOption", $this->getBooleanBasic("hasStatementPaymentOption"));
		$parameters->put("fromPaymentPlanExpirationDate", $this->getDate("fromPaymentPlanExpirationDate"));
		$parameters->put("toPaymentPlanExpirationDate", $this->getDate("toPaymentPlanExpirationDate"));
		$parameters->put("fromDeferredLength", $this->getInteger("fromDeferredLength"));
		$parameters->put("toDeferredLength", $this->getInteger("toDeferredLength"));
		$parameters->put("fromAgeAccountTransactionDueDate", $this->getDate("fromAgeAccountTransactionDueDate"));
		$parameters->put("toAgeAccountTransactionDueDate", $this->getDate("toAgeAccountTransactionDueDate"));
		$parameters->put("fromLastDeclineDate", $this->getDate("fromLastDeclineDate"));
		$parameters->put("toLastDeclineDate", $this->getDate("toLastDeclineDate"));
		$parameters->put("fromSoftDeclineCount", $this->getInteger("fromSoftDeclineCount"));
		$parameters->put("toSoftDeclineCount", $this->getInteger("toSoftDeclineCount"));
		$parameters->put("declineType", $this->getString("declineType"));
		$parameters->put("isChargebackRequested", $this->getBooleanBasic("isChargebackRequested"));
		$parameters->put("isBankruptcyFiled", $this->getBooleanBasic("isBankruptcyFiled"));
		$parameters->put("isLegalDispute", $this->getBooleanBasic("isLegalDispute"));
		$parameters->put("isCancellationDispute", $this->getBooleanBasic("isCancellationDispute"));
		$parameters->put("needToBeReinstated", $this->getBooleanBasic("needToBeReinstated"));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getLoadCustomerAccountParameters(){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put("code", $this->getString("code"));
		$parameters->put("loadPaymentOption", $this->getString("loadPaymentOption"));
		$parameters->put("loadPaymentPlan", $this->getString("loadPaymentPlan"));
		$parameters->put("loadContract", $this->getString("loadContract"));
		$parameters->put("loadAccountTransactionFromDate", $this->getDate("loadAccountTransactionFromDate"));
		$parameters->put("loadAccountTransactionToDate", $this->getDate("loadAccountTransactionToDate"));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getAddressParameters(){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getPhoneParameters(){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getEmailAddressParameters(){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentOptionParameters(){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put("code", $this->getString("code"));
		$parameters->put("isActive", $this->getBooleanBasic("isActive"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put("holderName", $this->getString("holderName"));
		$parameters->put("number", $this->getString("number"));
		$parameters->put("tokenCode", $this->getString("tokenCode"));
		$parameters->put("accessory", $this->getString("accessory"));
		$parameters->put("street1", $this->getString("street1"));
		$parameters->put("street2", $this->getString("street2"));
		$parameters->put("city", $this->getString("city"));
		$parameters->put("state", $this->getString("state"));
		$parameters->put("zipCode", $this->getString("zipCode"));
		$parameters->put("paymentOptionCl", $this->getString("type"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("isVisibleExternallyOnly", $this->getBooleanBasic("isVisibleExternallyOnly"));
		$parameters->put(Session::ORDER_BY, $this->getString(Session::ORDER_BY));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentPlanParameters(){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put("code", $this->getString("code"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put("fromNextBillingDate", $this->getDate("fromNextBillingDate"));
		$parameters->put("toNextBillingDate", $this->getDate("toNextBillingDate"));
		$parameters->put("fromFirstBillingDate", $this->getDate("fromFirstBillingDate"));
		$parameters->put("toFirstBillingDate", $this->getDate("toFirstBillingDate"));
		$parameters->put("status", $this->getString("status"));
		$parameters->put("fromAmount", $this->getInteger("fromAmount"));
		$parameters->put("toAmount", $this->getInteger("toAmount"));
		$parameters->put("itemCode", $this->getString("itemCode"));
		$parameters->put("refSellerCode", $this->getString("sellerCode"));
		$parameters->put("billingCycleCode", $this->getString("billingCycleCode"));
		$parameters->put("refGroupCode1", $this->getString("groupCode1"));
		$parameters->put("refGroupCode2", $this->getString("groupCode2"));
		$parameters->put("refGroupCode3", $this->getString("groupCode3"));
		$parameters->put("refGroupCode4", $this->getString("groupCode4"));
		$parameters->put("refGroupCode5", $this->getString("groupCode5"));
		$parameters->put("refGroupCode6", $this->getString("groupCode6"));
		$parameters->put("refGroupCode7", $this->getString("groupCode7"));
		$parameters->put("refGroupCode8", $this->getString("groupCode8"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("paymentOptionCode", $this->getString("paymentOptionCode"));
		$parameters->put("contractCode", $this->getString("contractCode"));
		$parameters->put("paymentOptionNumber", $this->getString("paymentOptionNumber"));
		$parameters->put("isVisibleExternallyOnly", $this->getBooleanBasic("isVisibleExternallyOnly"));
		$parameters->put(Session::ORDER_BY, $this->getString(Session::ORDER_BY));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getAccountTransactionParameters(){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put("code", $this->getString("code"));
		$parameters->put("accountActivityCl", $this->getString("accountActivityCl"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("customerAccountName", $this->getString("customerAccountName"));
		$parameters->put("isActiveCustomerAccount", $this->getBooleanBasic("isActiveCustomerAccount"));
		$parameters->put("itemCode", $this->getString("itemCode"));
		$parameters->put("accountNumber", $this->getString("accountNumber"));
		$parameters->put("fromAmount", $this->getInteger("fromAmount"));
		$parameters->put("toAmount", $this->getInteger("toAmount"));
		$parameters->put("fromBalance", $this->getInteger("fromBalance"));
		$parameters->put("toBalance", $this->getInteger("toBalance"));
		$parameters->put("isVisibleExternallyOnly", $this->getBooleanBasic("isVisibleExternallyOnly"));
		$parameters->put(Session::ORDER_BY, $this->getString(Session::ORDER_BY));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getNoteParameters(){
		$parameters = new HashMap();
		$parameters->put("code", $this->getString("code"));
		$parameters->put("noteCode", $this->getString("noteCode"));
		$parameters->put("creatorCode", $this->getString("creatorCode"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put("description", $this->getString("description"));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getActionParameters(){
		$parameters = new HashMap();
		$parameters->put("code", $this->getString("code"));
		$parameters->put("actionCode", $this->getString("actionCode"));
		$parameters->put("creatorCode", $this->getString("creatorCode"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getLetterParameters(){
		$parameters = new HashMap();
		$parameters->put("code", $this->getString("code"));
		$parameters->put("letterCode", $this->getString("letterCode"));
		$parameters->put("letterStatusType", $this->getString("letterStatusType"));
		$parameters->put("creatorCode", $this->getString("creatorCode"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put("isVisibleExternallyOnly", $this->getBooleanBasic("isVisibleExternallyOnly"));
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getEmailParameters(){
		$parameters = new HashMap();
		$parameters->put("code", $this->getString("code"));
		$parameters->put("emailCode", $this->getString("emailCode"));
		$parameters->put("emailStatusType", $this->getString("emailStatusType"));
		$parameters->put("creatorCode", $this->getString("creatorCode"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put("isVisibleExternallyOnly", $this->getBooleanBasic("isVisibleExternallyOnly"));
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getContractParameters(){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put("collectorAccountCode", $this->getInteger("collectorAccountCode"));
		$parameters->put("code", $this->getString("code"));
		$parameters->put("name", $this->getString("name"));
		$parameters->put("type", $this->getString("type"));
		$parameters->put("status", $this->getString("status"));
		$parameters->put("creatorCode", $this->getString("creatorCode"));
		$parameters->put("sellerCode", $this->getString("sellerCode"));
		$parameters->put("importCode", $this->getString("importCode"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put("fromFreezeBeginDate", $this->getDate("fromFreezeBeginDate"));
		$parameters->put("toFreezeBeginDate", $this->getDate("toFreezeBeginDate"));
		$parameters->put("fromFreezeEndDate", $this->getDate("fromFreezeEndDate"));
		$parameters->put("toFreezeEndDate", $this->getDate("toFreezeEndDate"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("defaultPaymentPlanCode", $this->getString("defaultPaymentPlanCode"));
		$parameters->put("isVisibleExternallyOnly", $this->getBooleanBasic("isVisibleExternallyOnly"));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getScheduledPaymentParameters(){
		$parameters = new HashMap();
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put("code", $this->getString("code"));
		$parameters->put("status", $this->getString("status"));
		$parameters->put("creatorCode", $this->getString("creatorCode"));
		$parameters->put("posterCode", $this->getString("posterCode"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put("fromDueDate", $this->getDate("fromDueDate"));
		$parameters->put("toDueDate", $this->getDate("toDueDate"));
		$parameters->put("isVisibleExternallyOnly", $this->getBooleanBasic("isVisibleExternallyOnly"));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getDocumentParameters(){
		$parameters = new HashMap();
		$parameters->put("code", $this->getString("code"));
		$parameters->put("documentCode", $this->getString("documentCode"));
		$parameters->put("creatorCode", $this->getString("creatorCode"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put("isVisibleExternallyOnly", $this->getBooleanBasic("isVisibleExternallyOnly"));
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getAuditLogParameters(){
		$parameters = new HashMap();
		$parameters->put("userCode", $this->getString("userCode"));
		$parameters->put("objectType", $this->getString("objectType"));
		$parameters->put("objectCode", $this->getString("objectCode"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getAdjustmentParameters(){
		$parameters = new HashMap();
		$parameters->put("code", $this->getString("code"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("contractCode", $this->getString("contractCode"));
		$parameters->put("paymentPlanCode", $this->getString("paymentPlanCode"));
		$parameters->put("adjustmentReasonCode", $this->getString("adjustmentReasonCode"));
		$parameters->put("type", $this->getString("type"));
		$parameters->put("isVisibleExternallyOnly", $this->getBooleanBasic("isVisibleExternallyOnly"));
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getUserParameters(){
		$parameters = new HashMap();
		$parameters->put("userName", $this->getString("userName"));
		$parameters->put("name", $this->getString("name"));
		$parameters->put("roleType", $this->getString("roleType"));
		$parameters->put("isActive", $this->getBooleanBasic("isActive"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getAgentCommissionParameters(){
		$parameters = new HashMap();
		$parameters->put("agentCode", $this->getString("agentCode"));
		$parameters->put("creatorCode", $this->getString("creatorCode"));
		$parameters->put("commissionType", $this->getString("commissionType"));
		$parameters->put("customerAccountCode", $this->getString("customerAccountCode"));
		$parameters->put("fromCreateDate", $this->getDate("fromCreateDate"));
		$parameters->put("toCreateDate", $this->getDate("toCreateDate"));
		$parameters->put("merchantAccountCode", $this->getInteger("merchantAccountCode"));
		$parameters->put(Session::FIRST_RESULT, $this->getInteger(Session::FIRST_RESULT));
		$parameters->put(Session::MAX_RESULTS, $this->getInteger(Session::MAX_RESULTS));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getLoadObjectParameters(){
		$parameters = new HashMap();
		$parameters->put("className", $this->getString("className"));
		$parameters->put("id", $this->getLong("id"));
		return $parameters;
	}
	/*
	 *$method.comment
	 */
	public function getAttributeFromRoot($config){
		$config->put("version", $this->getString("version"));
		$config->put("payment-option.verification-amount", $this->getInteger("payment-option.verification-amount"));
		$config->put("clientIpAddress", $this->getString("clientIpAddress"));
		$config->put("clientEmailDomain", $this->getString("clientEmailDomain"));
		$config->put("clientEmailEncoded", $this->getString("clientEmailEncoded"));
		$config->put("clientUserNameEncoded", $this->getString("clientUserNameEncoded"));
		$config->put("clientPasswordEncoded", $this->getString("clientPasswordEncoded"));
		$config->put(SessionConnection::DEBUG, $this->getBooleanBasic(SessionConnection::DEBUG));
		$config->put(SessionConnection::IS_IMPORT, $this->getBooleanBasic(SessionConnection::IS_IMPORT));
	}
	/*
	 *$method.comment
	 */
	public function getString($attributeName){
		return $this->facade->getString($attributeName);
	}
	/*
	 *$method.comment
	 */
	public function getInteger($attributeName){
		return $this->facade->getInteger($attributeName);
	}
	/*
	 *$method.comment
	 */
	public function getLong($attributeName){
		return $this->facade->getLong($attributeName);
	}
	/*
	 *$method.comment
	 */
	public function getBoolean($attributeName){
		return $this->facade->getBoolean($attributeName);
	}
	/*
	 *$method.comment
	 */
	public function getBooleanBasic($attributeName){
		return $this->facade->getBooleanBasic($attributeName);
	}
	/*
	 *$method.comment
	 */
	public function getDate($attributeName){
		return $this->facade->getDate($attributeName);
	}
	/*
	 *$method.comment
	 */
	public function addAtribute($attributeName,$value){
		$this->facade->setAttribute($attributeName, $value);
	}
	/*
	 *$method.comment
	 */
	public function isAttributeValueNull($attributeName){
		return $this->facade->isAttributeValueNull($attributeName);
	}
	/*
	 *$method.comment
	 */
	private function refIdToXML($id,$refId){
		if (is_null($id) || !(($id == $refId))){
			return "#" . strval($refId);
		}
		else {
			return strval($refId);
		}
	}
	/*
	 *$method.comment
	 */
	private function getCountCharge($children){
		$count = 0;
		foreach ($children as $child){
			if ("charge" == $child->getName()){
				$count++;
			}
		}
		return $count;
	}
	/*
	 *$method.comment
	 */
	private function getLengthPaymentPlan($charges){
		$length = 0;
		foreach ($charges as $charge){
			if (ChargeType::Fixed == $charge->getType() || ChargeType::Perpetual == $charge->getType() || ChargeType::Complimentary == $charge->getType()){
				$length++;
			}
		}
		return $length;
	}
	/*
	 *$method.comment
	 */
	private function getBankInfo($children){
		if ($children->isEmpty()){
			return null;
		}
		$bankInfo = new BankInfo();
		foreach ($children as $child){
			if ("bankInfo" == $child->getName()){
				$bankInfoTransformationHelper = new XMLTransformationHelper($child);
				$bankInfoTransformationHelper->bankInfoFromXML($bankInfo);
				break;
			}
		}
		return $bankInfo;
	}
}

?>

