<?php

/*
 *$comment
 */
class ClientObject {
		/*
	 *@var null
	 */
	private $id;
		/*
	 *@var null
	 */
	private $createDate;
		/*
	 *@var null
	 */
	private $code;
		/*
	 *@var null
	 */
	private $merchantAccountCode;
		/*
	 *@var null
	 */
	private $collectorAccountCode;
		/*
	 *@var null
	 */
	private $isLite;
		/*
	 *@var null
	 */
	private $refId;
		/**
	 * Used to uniquely identify objects that don't have id defined. Not really a hashCode.
	 */
	private $_hashCode;
	

	/*
	 *$method.comment
	 */
	public function getId(){
		return $this->id;
	}
	/*
	 *$method.comment
	 */
	 function setId($id){
		$this->id = $id;
	}
	/*
	 *$method.comment
	 */
	public function getCreateDate(){
		return $this->createDate;
	}
	/*
	 *$method.comment
	 */
	 function setCreateDate($createDate){
		$this->createDate = $createDate;
	}
	/*
	 *$method.comment
	 */
	public function getCode(){
		return $this->code;
	}
	/*
	 *$method.comment
	 */
	public function setCode($code){
		$this->code = $code;
	}
	/*
	 *$method.comment
	 */
	public function getMerchantAccountCode(){
		return $this->merchantAccountCode;
	}
	/*
	 *$method.comment
	 */
	public function setMerchantAccountCode($merchantAccountCode){
		$this->merchantAccountCode = $merchantAccountCode;
	}
	/*
	 *$method.comment
	 */
	public function getCollectorAccountCode(){
		return $this->collectorAccountCode;
	}
	/*
	 *$method.comment
	 */
	 function setCollectorAccountCode($collectorAccountCode){
		$this->collectorAccountCode = $collectorAccountCode;
	}
	/*
	 *$method.comment
	 */
	 function getIsLite(){
		return $this->isLite;
	}
	/*
	 *$method.comment
	 */
	 function setIsLite($isLite){
		$this->isLite = $isLite;
	}
	/*
	 *$method.comment
	 */
	public function type(){
	}
	/*
	 *$method.comment
	 */
	public function setRefId($refId){
		$this->refId = $refId;
	}
	/*
	 *$method.comment
	 */
	public function setRefIdSpecial($refId){
		$this->refId = intval($refId);
	}
	/*
	 *$method.comment
	 */
	public function getRefId(){
		return $this->refId;
	}
	public function hashCode(){
		static $seed = 0;
		if ($this->_hashCode == null){
			$this->_hashCode = ++$seed;
		}
		return $this->_hashCode;
	}
}

?>

