<?php

class ArrayList implements Iterator {
	/**
	 * @var	array
	 */
	private $_elements;
	
	/**
	 * Create an ArrayList with the specified elements.
	 *
	 * @access	public
	 * @param	string	$elements
	 */
	function ArrayList($elements = null) {
		if ($elements instanceof ArrayList) {
			$this->_elements = array ( );
			$this->addAll ( $elements );
		} else if (is_numeric ( $elements )) {
			$this->_elements = array ( );
		} else if (! empty ( $elements )) {
			$this->_elements = $elements;
		} else
			$this->_elements = array ( );
	}
	/**
	 * Appends the specified element to the end of this list.
	 *
	 * @access	public
	 */
	function add($index, $element=null) {
		if (!is_numeric($index)){
			return (array_push ( $this->_elements, $index )) ? TRUE : FALSE;		
		}
		else {
    		$arrayTmp = array_splice($this->_elements, $index);
    		$this->_elements[] = $element;
    		$this->_elements = array_merge($this->_elements,$arrayTmp);
		}
	}
	/**
	 * Appends all of the elements in the specified ArrayList to the end of
	 * this list, in the order that they are returned by the specified
	 * ArrayList's ListIterator.
	 *
	 * @access	public
	 * @param	ArrayList	$list
	 * @return	boolean
	 */
	function addAll($list) {
		$before = $this->size ();
		if ($list instanceof ArrayList) {
			foreach ( $list as $item) {
				$this->add ( $item );
			}
		}
		$after = $this->size ();
		return ($before < $after);
	}
	/**
	 * Removes all of the elements from this list.
	 *
	 * @access	public
	 */
	function clear() {
		$this->_elements = array ( );
	}
	/**
	 * Returns true if this list contains the specified element.
	 *
	 * @access	public
	 * @param	mixed	$element
	 * @return	boolean
	 */
	function contains($element) {
		return (array_search ( $element, $this->_elements )) ? TRUE : FALSE;
	}
	/**
	 * Returns the element at the specified position in this list.
	 *
	 * @access	public
	 * @param	integer	$index
	 * @return	mixed
	 */
	function get($index) {
		return $this->_elements [$index];
	}
	/**
	 * Searches for the first occurence of the given argument.
	 *
	 * @access	public
	 * @param	mixed	$element
	 * @return	mixed
	 */
	function indexOf($element) {
		return array_search ( $element, $this->_elements );
	}
	/**
	 * Tests if this list has no elements.
	 *
	 * @access	public
	 * @return	boolean
	 */
	function isEmpty() {
		return empty ( $this->_elements );
	}
	/**
	 * Returns the index of the last occurrence of the specified object in this
	 * list.
	 *
	 * @access	public
	 * @param	mixed	$element
	 * @return	mixed
	 */
	function lastIndexOf($element) {
		for($i = (count ( $this->_elements ) - 1); $i > 0; $i --) {
			if ($element == $this->get ( $i )) {
				return $i;
			}
		}
	}
	/**
	 * Removes the element at the specified position in this list.
	 *
	 * @access	public
	 * @param	integer	$index
	 * @return	mixed
	 */
	function remove($index) {
		$element = $this->get ( $index );
		if (! is_null ( $element )) {
			array_splice ( $this->_elements, $index, 1 );
		}
		return $element;
	}
	/**
	 * Removes from this List all of the elements whose index is between start,
	 * inclusive and end, exclusive.
	 *
	 * @access	public
	 * @param	integer	$start
	 * @param	integer	$end
	 */
	function removeRange($start, $end) {
		array_splice ( $this->_elements, $start, $end );
	}
	/**
	 * Replaces the element at the specified position in this list with the
	 * specified element.
	 *
	 * @access	public
	 * @param	integer	$index
	 * @param	mixed	$element
	 * @return	mixed
	 */
	function set($index, $element) {
		$previous = $this->get ( $index );
		$this->_elements [$index] = $element;
		return $previous;
	}
	/**
	 * Returns the number of elements in this list.
	 *
	 * @access	public
	 * @return	integer
	 */
	function size() {
		return count ( $this->_elements );
	}
	/**
	 * Returns an array containing all of the elements in this list in the
	 * correct order.
	 *
	 * @access	public
	 * @return	array
	 */
	function toArray() {
		return $this->_elements;
	}
	
	/** 
	 * A switch to keep track of the end of the array 
	 */
	private $valid = FALSE;
	
	/** 
	 * Return the array "pointer" to the first element 
	 * PHP's reset() returns false if the array has no elements 
	 */
	function rewind() {
		$this->valid = (FALSE !== reset ( $this->_elements ));
	}
	
	/** 
	 * Return the current array element 
	 */
	function current() {
		return current ( $this->_elements );
	}
	
	/** 
	 * Return the key of the current array element 
	 */
	function key() {
		return key ( $this->_elements );
	}
	
	/** 
	 * Move forward by one 
	 * PHP's next() returns false if there are no more elements 
	 */
	function next() {
		$this->valid = (FALSE !== next ( $this->_elements ));
	}
	
	/** 
	 * Is the current element valid? 
	 */
	function valid() {
		return $this->valid;
	}

}

?>