<?php


/*
 *$comment
 */
class Address extends ClientObject {
		/*
	 *@var null
	 */
	private $name;
		/*
	 *@var null
	 */
	private $street1;
		/*
	 *@var null
	 */
	private $street2;
		/*
	 *@var null
	 */
	private $city;
		/*
	 *@var null
	 */
	private $state;
		/*
	 *@var null
	 */
	private $zipCode;
		/*
	 *@var null
	 */
	private $countryType;
		/*
	 *@var null
	 */
	private $addressSourceType;
		/*
	 *@var null
	 */
	private $addressType;
		/*
	 *@var null
	 */
	private $isBad;
		/*
	 *@var null
	 */
	private $badReason;
		/*
	 *@var null
	 */
	private $isSkipTraceAllowed;
		/*
	 *@var null
	 */
	private $lastSkipTraceDate;
		/*
	 *@var null
	 */
	private $creatorCode;
		/*
	 *@var null
	 */
	private $customerAccount;
		/*
	 *@var null
	 */
	private $user;
		/*
	 *@var null
	 */
	private $session;
		
	/*
	 *$constructor.comment
	 */
	function __construct(){
		//initialization block
		$this->isBad = false;
		$this->isSkipTraceAllowed = true;
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
	}

	/*
	 *$method.comment
	 */
	public function getCreatorCode(){
		return $this->creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function setCreatorCode($creatorCode){
		$this->creatorCode = $creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function getName(){
		return $this->name;
	}
	/*
	 *$method.comment
	 */
	public function setName($name){
		$this->name = $name;
	}
	/*
	 *$method.comment
	 */
	public function getStreet1(){
		return $this->street1;
	}
	/*
	 *$method.comment
	 */
	public function setStreet1($street1){
		$this->street1 = $street1;
	}
	/*
	 *$method.comment
	 */
	public function getStreet2(){
		return $this->street2;
	}
	/*
	 *$method.comment
	 */
	public function setStreet2($street2){
		$this->street2 = $street2;
	}
	/*
	 *$method.comment
	 */
	public function getCity(){
		return $this->city;
	}
	/*
	 *$method.comment
	 */
	public function setCity($city){
		$this->city = $city;
	}
	/*
	 *$method.comment
	 */
	public function getState(){
		return $this->state;
	}
	/*
	 *$method.comment
	 */
	public function setState($state){
		$this->state = $state;
	}
	/*
	 *$method.comment
	 */
	public function getAddressSourceType(){
		return $this->addressSourceType;
	}
	/*
	 *$method.comment
	 */
	public function setAddressSourceType($addressSourceType){
		$this->addressSourceType = $addressSourceType;
	}
	/*
	 *$method.comment
	 */
	public function setZipCode($zipCode){
		$this->zipCode = $zipCode;
	}
	/*
	 *$method.comment
	 */
	public function getZipCode(){
		return $this->zipCode;
	}
	/*
	 *$method.comment
	 */
	public function setAddressType($addressType){
		$this->addressType = $addressType;
	}
	/*
	 *$method.comment
	 */
	public function getAddressType(){
		return $this->addressType;
	}
	/*
	 *$method.comment
	 */
	public function setIsBad($isBad){
		$this->isBad = $isBad;
	}
	/*
	 *$method.comment
	 */
	public function getIsBad(){
		return $this->isBad;
	}
	/*
	 *$method.comment
	 */
	public function setBadReason($badReason){
		$this->badReason = $badReason;
	}
	/*
	 *$method.comment
	 */
	public function getBadReason(){
		return $this->badReason;
	}
	/*
	 *$method.comment
	 */
	public function setIsSkipTraceAllowed($isSkipTraceAllowed){
		$this->isSkipTraceAllowed = $isSkipTraceAllowed;
	}
	/*
	 *$method.comment
	 */
	public function getIsSkipTraceAllowed(){
		return $this->isSkipTraceAllowed;
	}
	/*
	 *$method.comment
	 */
	public function setLastSkipTraceDate($lastSkipTraceDate){
		$this->lastSkipTraceDate = $lastSkipTraceDate;
	}
	/*
	 *$method.comment
	 */
	public function getLastSkipTraceDate(){
		return $this->lastSkipTraceDate;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccount($customerAccount){
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getCountryType(){
		return $this->countryType;
	}
	/*
	 *$method.comment
	 */
	public function setCountryType($countryType){
		$this->countryType = $countryType;
	}
	/*
	 *$method.comment
	 */
	public function getUser(){
		if (!is_null($this->user)){
			return $this->user;
		}
		else {
			if (is_null($this->creatorCode)){
				return null;
			}
			else {
				return $this->session->loadUser($this->creatorCode);
			}
		}
	}
	/*
	 *$method.comment
	 */
	 function setUser($user){
		$this->user = $user;
	}
	/*
	 *$method.comment
	 */
	 function setSession($session){
		$this->session = $session;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "Address";
	}
}

?>

