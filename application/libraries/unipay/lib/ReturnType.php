<?php

/*
 *$comment
 */
class ReturnType {
		/*
	 *@var null
	 */
	const C01 = "C01: Account number is incorrect or is formatted incorrectly";	/*
	 *@var null
	 */
	const C02 = "C02: Due to merger or consolidation, a once valid transit/routing number must be changed";	/*
	 *@var null
	 */
	const C03 = "C03: Due to a merger or consolidation, the transit/routing number must be changed; and account number structure is no longer valid";	/*
	 *@var null
	 */
	const C04 = "C04: Customer has changed name or ODFI has submitted the name incorrectly";	/*
	 *@var null
	 */
	const C05 = "C05: Transaction code is incorrect and is causing entry to be routed to the wrong account application";	/*
	 *@var null
	 */
	const C06 = "C06: Account number is incorrect and transaction is being routed to the wrong type of account";	/*
	 *@var null
	 */
	const C07 = "C07: Due to a merger or consolidation, a transit/routing number must be changed; account number structure is no longer valid; and, the transaction should be routed to another account typr";	/*
	 *@var null
	 */
	const C08 = "C08: Incorrect Foreign Receiving DFI Identification";	/*
	 *@var null
	 */
	const C09 = "C09: Individuals ID number is incorrect";	/*
	 *@var null
	 */
	const C10 = "C10: Due to Merger or consolidation, the company name carried on the file of the ODFI is no longer valid";	/*
	 *@var null
	 */
	const C11 = "C11: Due to merger or consolidation, the company ID is no longer valid and should be changed";	/*
	 *@var null
	 */
	const C12 = "C12: Due to merger or consolidation, the compnay name and company ID are no longer valid";	/*
	 *@var null
	 */
	const C13 = "C13: Entry Detail Record was correct, but information in the addenda record was unclear/formated incorrectly (i.e, addenda information is not formatted in NSI or NACHA endorsed banking conventions).";	/*
	 *@var null
	 */
	const C61 = "C61: Misrouted Notification of Change";	/*
	 *@var null
	 */
	const C62 = "C62: Incorrect Trace Number";	/*
	 *@var null
	 */
	const C63 = "C63: Incorrect Company Identification Number";	/*
	 *@var null
	 */
	const C64 = "C64: Incorrect Individual Identification Number";	/*
	 *@var null
	 */
	const C65 = "C65: Incorrectly Formatted Addenda Information";	/*
	 *@var null
	 */
	const C66 = "C66: Incorrect Discretionary Data";	/*
	 *@var null
	 */
	const C67 = "C67: Routing Number Not From Original Entry Detail";	/*
	 *@var null
	 */
	const C68 = "C68: DFI Account Number Not from Original Entry Detail Record";	/*
	 *@var null
	 */
	const C69 = "C69: Incorrect Transaction Code";	/*
	 *@var null
	 */
	const R01 = "R01: Insufficient Funds";	/*
	 *@var null
	 */
	const R02 = "R02: Account Closed";	/*
	 *@var null
	 */
	const R03 = "R03: No Account";	/*
	 *@var null
	 */
	const R04 = "R04: Invalid Account";	/*
	 *@var null
	 */
	const R06 = "R06: ODFI Requests Return";	/*
	 *@var null
	 */
	const R07 = "R07: Revoked Authorization";	/*
	 *@var null
	 */
	const R08 = "R08: Stop Payment or Stop on Source Document";	/*
	 *@var null
	 */
	const R09 = "R09: Uncollected Funds";	/*
	 *@var null
	 */
	const R10 = "R10: Advised as Unauthorized, Itme is Ineligible, Notice not provided, Signatures not Genuine, or Item Altered, Improper Source Document, Amount of Entyr not Accurately Obtained";	/*
	 *@var null
	 */
	const R11 = "R11: Check Safekeeping";	/*
	 *@var null
	 */
	const R12 = "R12: Account at Other Branch";	/*
	 *@var null
	 */
	const R13 = "R13: RDFI Not Qualified to Participate";	/*
	 *@var null
	 */
	const R14 = "R14: Death of Representative Payee";	/*
	 *@var null
	 */
	const R15 = "R15: Death of Benificiary or Account Holder";	/*
	 *@var null
	 */
	const R16 = "R16: Account Frozen";	/*
	 *@var null
	 */
	const R17 = "R17: File Record Edit Criteria";	/*
	 *@var null
	 */
	const R18 = "R18: Improper Effective Entry Date";	/*
	 *@var null
	 */
	const R19 = "R19: Amount Field Error";	/*
	 *@var null
	 */
	const R20 = "R20: Non-Transaction Account";	/*
	 *@var null
	 */
	const R21 = "R21: Invalid Company ID";	/*
	 *@var null
	 */
	const R22 = "R22: Invaild Individual ID Number";	/*
	 *@var null
	 */
	const R23 = "R23: Credit Entry Refused by Receiver";	/*
	 *@var null
	 */
	const R24 = "R24: Duplicate Entry";	/*
	 *@var null
	 */
	const R25 = "R25: Addenda Error";	/*
	 *@var null
	 */
	const R26 = "R26: Mandatory Field Error";	/*
	 *@var null
	 */
	const R27 = "R27: Trace Number Error";	/*
	 *@var null
	 */
	const R28 = "R28: Routing Number Check Digit Error";	/*
	 *@var null
	 */
	const R29 = "R29: Corporate Entry Unauthorized";	/*
	 *@var null
	 */
	const R30 = "R30: RDFI Not Participant in Program";	/*
	 *@var null
	 */
	const R31 = "R31: ODFI Permits Late Return";	/*
	 *@var null
	 */
	const R32 = "R32: RDFI Non-Settlement";	/*
	 *@var null
	 */
	const R33 = "R33: Return of XCK Entry";	/*
	 *@var null
	 */
	const R34 = "R34: Limited Participation DFI";	/*
	 *@var null
	 */
	const R35 = "R35: Return of Improper Debit Entry";	/*
	 *@var null
	 */
	const R36 = "R36: Return of Improper Credit Entry";	/*
	 *@var null
	 */
	const R37 = "R37: Source Document Presented for Payment";	/*
	 *@var null
	 */
	const R50 = "R50: State Law Affecting RCK Acceptance";	/*
	 *@var null
	 */
	const R51 = "R51: Item is Ineligible; Notice not Provided; Signature Not Genuine; Item Altered; Amount of Entry not Accurately Obtained";	/*
	 *@var null
	 */
	const R52 = "R52: Stop Payment on Iten";	/*
	 *@var null
	 */
	const R53 = "R53: Item and ACH Entry Presented for Payment";	/*
	 *@var null
	 */
	const R61 = "R61: Misrouted Return";	/*
	 *@var null
	 */
	const R62 = "R62: Incorrect Trace Number";	/*
	 *@var null
	 */
	const R63 = "R63: Incorrect Dollar Amount";	/*
	 *@var null
	 */
	const R64 = "R64: Incorrect Individual ID";	/*
	 *@var null
	 */
	const R65 = "R65: Incorrect Transaction Code";	/*
	 *@var null
	 */
	const R66 = "R66: Incorrect Company ID";	/*
	 *@var null
	 */
	const R67 = "R67: Duplicate Return";	/*
	 *@var null
	 */
	const R68 = "R68: Untimely Return";	/*
	 *@var null
	 */
	const R69 = "R69: Multiple Errors";	/*
	 *@var null
	 */
	const R70 = "R70: Permissible Return Not Accepted";	/*
	 *@var null
	 */
	const R71 = "R71: Misrouted Dishonored Return";	/*
	 *@var null
	 */
	const R72 = "R72: Untimely Dishonored Return";	/*
	 *@var null
	 */
	const R73 = "R73: Timely Original Return";	/*
	 *@var null
	 */
	const R74 = "R74: Corrected Return";	/*
	 *@var null
	 */
	const R80 = "R80: Cross Border Coding Error";	/*
	 *@var null
	 */
	const R81 = "R81: Non-Participant in Cross Border Program";	/*
	 *@var null
	 */
	const R82 = "R82: Invalid Foreign Receiving DFI Identification";	/*
	 *@var null
	 */
	const R83 = "R83: Foreign Receiving DFI Unable to Settle";	/*
	 *@var null
	 */
	const R84 = "R84: Entry Not Processed by OGO";	/*
	 *@var null
	 */
	const A01 = "A01: Approved: XXXXXX (approval code)";	/*
	 *@var null
	 */
	const A02 = "A02: Credit Posted";	/*
	 *@var null
	 */
	const A03 = "A03: Void Posted";	/*
	 *@var null
	 */
	const D01 = "D01: Denied by customer's bank";	/*
	 *@var null
	 */
	const D02 = "D02: Invalid Expiration Date";	/*
	 *@var null
	 */
	const D03 = "D03: Insufficient funds";	/*
	 *@var null
	 */
	const D04 = "D04: Hold - Pick up card";	/*
	 *@var null
	 */
	const D05 = "D05: Invalid card number";	/*
	 *@var null
	 */
	const D06 = "D06: No account";	/*
	 *@var null
	 */
	const D07 = "D07: Incorrect PIN";	/*
	 *@var null
	 */
	const D08 = "D08: CVV2 is invalid";	/*
	 *@var null
	 */
	const D09 = "D09: Duplicate Transaction";	/*
	 *@var null
	 */
	const D10 = "D10: Card reported LOST";	/*
	 *@var null
	 */
	const D11 = "D11: Card reported STOLEN";	/*
	 *@var null
	 */
	const D12 = "D12: Service not allowed";	/*
	 *@var null
	 */
	const D13 = "D13: Stop Recurring";	/*
	 *@var null
	 */
	const D14 = "D14: Unattempted Batch Decline";	/*
	 *@var null
	 */
	const E01 = "E01: Transaction Error";	/*
	 *@var null
	 */
	const E02 = "E02: System is temporarity unavailable";
}
?>

