<?php


/*
 *$comment
 */
class Note extends ClientObject {
		/*
	 *@var null
	 */
	private $noteCode;
		/*
	 *@var null
	 */
	private $creatorCode;
		/*
	 *@var null
	 */
	private $description;
		/*
	 *@var null
	 */
	private $customerAccount;
		/*
	 *@var null
	 */
	private $user;
		/*
	 *@var null
	 */
	private $session;
		
	/*
	 *$constructor.comment
	 */
	function __construct($noteCode = null, $creatorCode = null, $description = null, $createDate = null, $customerAccount = null, $merchantAccountCode = null, $id = null){
		//initialization block
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
		//constructor with parameters
		$this->noteCode = $noteCode;
		$this->creatorCode = $creatorCode;
		$this->description = $description;
		$this->setCreateDate($createDate);
		$this->customerAccount = $customerAccount;
		$this->setMerchantAccountCode($merchantAccountCode);
		if (is_null($id)){
			$this->setRefIdSpecial($this->hashCode());
		}
		else {
			$this->setId($id);
			$this->setRefId($id);
			$this->setCode(strval($id));
		}
	
	}

	/*
	 *$method.comment
	 */
	public function getNoteCode(){
		return $this->noteCode;
	}
	/*
	 *$method.comment
	 */
	public function setNoteCode($noteCode){
		$this->noteCode = $noteCode;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccount($customerAccount){
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getCreatorCode(){
		return $this->creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function setCreatorCode($creatorCode){
		$this->creatorCode = $creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function getDescription(){
		return $this->description;
	}
	/*
	 *$method.comment
	 */
	public function setDescription($description){
		$this->description = $description;
	}
	/*
	 *$method.comment
	 */
	public function getUser(){
		if (!is_null($this->user)){
			return $this->user;
		}
		else {
			if (is_null($this->creatorCode)){
				return null;
			}
			else {
				return $this->session->loadUser($this->creatorCode);
			}
		}
	}
	/*
	 *$method.comment
	 */
	 function setUser($user){
		$this->user = $user;
	}
	/*
	 *$method.comment
	 */
	 function setSession($session){
		$this->session = $session;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "Note";
	}
}

?>

