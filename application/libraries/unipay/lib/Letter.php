<?php



/*
 *$comment
 */
class Letter extends ClientObject {
		/*
	 *@var null
	 */
	private $letterCode;
		/*
	 *@var null
	 */
	private $creatorCode;
		/*
	 *@var null
	 */
	private $dueDate;
		/*
	 *@var null
	 */
	private $status;
		/*
	 *@var null
	 */
	private $customerAccount;
		/*
	 *@var null
	 */
	private $name;
		/*
	 *@var null
	 */
	private $city;
		/*
	 *@var null
	 */
	private $street1;
		/*
	 *@var null
	 */
	private $street2;
		/*
	 *@var null
	 */
	private $state;
		/*
	 *@var null
	 */
	private $zipCode;
		/*
	 *@var null
	 */
	private $email;
		/*
	 *@var null
	 */
	private $documentCode;
		/*
	 *@var null
	 */
	private $isStatement;
		/*
	 *@var null
	 */
	private $customField1;
		/*
	 *@var null
	 */
	private $customField2;
		/*
	 *@var null
	 */
	private $customField3;
		/*
	 *@var null
	 */
	private $customField4;
		/*
	 *@var null
	 */
	private $customField5;
		/*
	 *@var null
	 */
	private $isVisibleExternally;
		
	/*
	 *$constructor.comment
	 */
	function __construct(){
		//initialization block
		$this->isStatement = false;
		$this->isVisibleExternally = false;
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
	}

	/*
	 *$method.comment
	 */
	public function getLetterCode(){
		return $this->letterCode;
	}
	/*
	 *$method.comment
	 */
	public function setLetterCode($letterCode){
		$this->letterCode = $letterCode;
	}
	/*
	 *$method.comment
	 */
	public function getCreatorCode(){
		return $this->creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function setCreatorCode($creatorCode){
		$this->creatorCode = $creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function getDueDate(){
		return $this->dueDate;
	}
	/*
	 *$method.comment
	 */
	public function setDueDate($dueDate){
		$this->dueDate = $dueDate;
	}
	/*
	 *$method.comment
	 */
	public function getStatus(){
		return $this->status;
	}
	/*
	 *$method.comment
	 */
	public function setStatus($status){
		$this->status = $status;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccount($customerAccount){
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getName(){
		return $this->name;
	}
	/*
	 *$method.comment
	 */
	public function setName($name){
		$this->name = $name;
	}
	/*
	 *$method.comment
	 */
	public function getCity(){
		return $this->city;
	}
	/*
	 *$method.comment
	 */
	public function setCity($city){
		$this->city = $city;
	}
	/*
	 *$method.comment
	 */
	public function getStreet1(){
		return $this->street1;
	}
	/*
	 *$method.comment
	 */
	public function setStreet1($street1){
		$this->street1 = $street1;
	}
	/*
	 *$method.comment
	 */
	public function getStreet2(){
		return $this->street2;
	}
	/*
	 *$method.comment
	 */
	public function setStreet2($street2){
		$this->street2 = $street2;
	}
	/*
	 *$method.comment
	 */
	public function getState(){
		return $this->state;
	}
	/*
	 *$method.comment
	 */
	public function setState($state){
		$this->state = $state;
	}
	/*
	 *$method.comment
	 */
	public function getZipCode(){
		return $this->zipCode;
	}
	/*
	 *$method.comment
	 */
	public function setZipCode($zipCode){
		$this->zipCode = $zipCode;
	}
	/*
	 *$method.comment
	 */
	public function getEmail(){
		return $this->email;
	}
	/*
	 *$method.comment
	 */
	public function setEmail($email){
		$this->email = $email;
	}
	/*
	 *$method.comment
	 */
	public function getDocumentCode(){
		return $this->documentCode;
	}
	/*
	 *$method.comment
	 */
	 function setDocumentCode($documentCode){
		$this->documentCode = $documentCode;
	}
	/*
	 *$method.comment
	 */
	public function getIsStatement(){
		return $this->isStatement;
	}
	/*
	 *$method.comment
	 */
	public function setIsStatement($isStatement){
		$this->isStatement = $isStatement;
	}
	/*
	 *$method.comment
	 */
	public function getCustomField1(){
		return $this->customField1;
	}
	/*
	 *$method.comment
	 */
	public function setCustomField1($customField1){
		$this->customField1 = $customField1;
	}
	/*
	 *$method.comment
	 */
	public function getCustomField2(){
		return $this->customField2;
	}
	/*
	 *$method.comment
	 */
	public function setCustomField2($customField2){
		$this->customField2 = $customField2;
	}
	/*
	 *$method.comment
	 */
	public function getCustomField3(){
		return $this->customField3;
	}
	/*
	 *$method.comment
	 */
	public function setCustomField3($customField3){
		$this->customField3 = $customField3;
	}
	/*
	 *$method.comment
	 */
	public function getCustomField4(){
		return $this->customField4;
	}
	/*
	 *$method.comment
	 */
	public function setCustomField4($customField4){
		$this->customField4 = $customField4;
	}
	/*
	 *$method.comment
	 */
	public function getCustomField5(){
		return $this->customField5;
	}
	/*
	 *$method.comment
	 */
	public function setCustomField5($customField5){
		$this->customField5 = $customField5;
	}
	/*
	 *$method.comment
	 */
	public function getIsVisibleExternally(){
		return $this->isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function setIsVisibleExternally($isVisibleExternally){
		$this->isVisibleExternally = $isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function cancel(){
		$this->status = LetterStatusType::Cancelled;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "Letter";
	}
}

?>

