<?php


/*
 *$comment
 */
class PaymentPlan extends ClientObject {
		/*
	 *@var null
	 */
	const MERCHANT_ACCOUNT_CODE_ASC = "paymentPlan.merchantAccountCode";
		/*
	 *@var null
	 */
	const MERCHANT_ACCOUNT_CODE_DESC = "paymentPlan.merchantAccountCode desc";
		/*
	 *@var null
	 */
	const CODE_ASC = "paymentPlan.refCode";
		/*
	 *@var null
	 */
	const CODE_DESC = "paymentPlan.refCode desc";
		/*
	 *@var null
	 */
	const CREATE_DATE_ASC = "paymentPlan.createDate";
		/*
	 *@var null
	 */
	const CREATE_DATE_DESC = "paymentPlan.createDate desc";
		/*
	 *@var null
	 */
	const FIRST_BILLING_DATE_ASC = "paymentPlan.firstBillingDate";
		/*
	 *@var null
	 */
	const FIRST_BILLING_DATE_DESC = "paymentPlan.firstBillingDate desc";
		/*
	 *@var null
	 */
	const NEXT_BILLING_DATE_ASC = "paymentPlan.billingCycle.nextBillingDate";
		/*
	 *@var null
	 */
	const NEXT_BILLING_DATE_DESC = "paymentPlan.billingCycle.nextBillingDate desc";
		/*
	 *@var null
	 */
	const STATUS_ASC = "paymentPlan.status";
		/*
	 *@var null
	 */
	const STATUS_DESC = "paymentPlan.status desc";
		/*
	 *@var null
	 */
	const AMOUNT_ASC = "paymentPlan.amount";
		/*
	 *@var null
	 */
	const AMOUNT_DESC = "paymentPlan.amount desc";
		/*
	 *@var null
	 */
	const ITEM_CODE_ASC = "paymentPlan.item.refCode";
		/*
	 *@var null
	 */
	const ITEM_CODE_DESC = "paymentPlan.item.refCode desc";
		/*
	 *@var null
	 */
	const SELLER_CODE_ASC = "paymentPlan.refSellerCode";
		/*
	 *@var null
	 */
	const SELLER_CODE_DESC = "paymentPlan.refSellerCode desc";
		/*
	 *@var null
	 */
	const BILLING_CYCLE_CODE_ASC = "paymentPlan.billingCycle.refCode";
		/*
	 *@var null
	 */
	const BILLING_CYCLE_CODE_DESC = "paymentPlan.billingCycle.refCode desc";
		/*
	 *@var null
	 */
	const GROUP_CODE1_ASC = "paymentPlan.refGroupCode1";
		/*
	 *@var null
	 */
	const GROUP_CODE1_DESC = "paymentPlan.refGroupCode1 desc";
		/*
	 *@var null
	 */
	const GROUP_CODE2_ASC = "paymentPlan.refGroupCode2";
		/*
	 *@var null
	 */
	const GROUP_CODE2_DESC = "paymentPlan.refGroupCode2 desc";
		/*
	 *@var null
	 */
	const GROUP_CODE3_ASC = "paymentPlan.refGroupCode3";
		/*
	 *@var null
	 */
	const GROUP_CODE3_DESC = "paymentPlan.refGroupCode3 desc";
		/*
	 *@var null
	 */
	const GROUP_CODE4_ASC = "paymentPlan.refGroupCode4";
		/*
	 *@var null
	 */
	const GROUP_CODE4_DESC = "paymentPlan.refGroupCode4 desc";
		/*
	 *@var null
	 */
	const GROUP_CODE5_ASC = "paymentPlan.refGroupCode5";
		/*
	 *@var null
	 */
	const GROUP_CODE5_DESC = "paymentPlan.refGroupCode5 desc";
		/*
	 *@var null
	 */
	const GROUP_CODE6_ASC = "paymentPlan.refGroupCode6";
		/*
	 *@var null
	 */
	const GROUP_CODE6_DESC = "paymentPlan.refGroupCode6 desc";
		/*
	 *@var null
	 */
	const GROUP_CODE7_ASC = "paymentPlan.refGroupCode7";
		/*
	 *@var null
	 */
	const GROUP_CODE7_DESC = "paymentPlan.refGroupCode7 desc";
		/*
	 *@var null
	 */
	const GROUP_CODE8_ASC = "paymentPlan.refGroupCode8";
		/*
	 *@var null
	 */
	const GROUP_CODE8_DESC = "paymentPlan.refGroupCode8 desc";
		/*
	 *@var null
	 */
	const CUSTOMER_ACCOUNT_CODE_ASC = "paymentPlan.customerAccount.refCode";
		/*
	 *@var null
	 */
	const CUSTOMER_ACCOUNT_CODE_DESC = "paymentPlan.customerAccount.refCode desc";
		/*
	 *@var null
	 */
	const PAYMENT_OPTION_CODE_ASC = "paymentPlan.paymentOption.refCode";
		/*
	 *@var null
	 */
	const PAYMENT_OPTION_CODE_DESC = "paymentPlan.paymentOption.refCode desc";
		/*
	 *@var null
	 */
	private $lastInvoicingDate;
		/*
	 *@var null
	 */
	private $lastProcessingDate;
		/*
	 *@var null
	 */
	private $creatorCode;
		/*
	 *@var null
	 */
	private $sellerCode;
		/*
	 *@var null
	 */
	private $note;
		/*
	 *@var null
	 */
	private $length;
		/*
	 *@var null
	 */
	private $value;
		/*
	 *@var null
	 */
	private $deferredLength;
		/*
	 *@var null
	 */
	private $deferredValue;
		/*
	 *@var null
	 */
	private $amount;
		/*
	 *@var null
	 */
	private $groupCode1;
		/*
	 *@var null
	 */
	private $groupCode2;
		/*
	 *@var null
	 */
	private $groupCode3;
		/*
	 *@var null
	 */
	private $groupCode4;
		/*
	 *@var null
	 */
	private $groupCode5;
		/*
	 *@var null
	 */
	private $groupCode6;
		/*
	 *@var null
	 */
	private $groupCode7;
		/*
	 *@var null
	 */
	private $groupCode8;
		/*
	 *@var null
	 */
	private $type;
		/*
	 *@var null
	 */
	private $firstBillingDate;
		/*
	 *@var null
	 */
	private $nextBillingDate;
		/*
	 *@var null
	 */
	private $billingCycleCode;
		/*
	 *@var null
	 */
	private $itemCode;
		/*
	 *@var null
	 */
	private $taxCode;
		/*
	 *@var null
	 */
	private $taxAmount;
		/*
	 *@var null
	 */
	private $renewalTaxAmount;
		/*
	 *@var null
	 */
	private $lastUpdateDate;
		/*
	 *@var null
	 */
	private $status;
		/*
	 *@var null
	 */
	private $linkAccount;
		/*
	 *@var null
	 */
	private $charges;
		/*
	 *@var null
	 */
	private $paymentOption;
		/*
	 *@var null
	 */
	private $customerAccount;
		/*
	 *@var null
	 */
	private $contract;
		/*
	 *@var null
	 */
	private $linkedParentPlan;
		/*
	 *@var null
	 */
	private $adjustment;
		/*
	 *@var null
	 */
	private $cancellationAdjustment;
		/*
	 *@var null
	 */
	private $freezeAdjustment;
		/*
	 *@var null
	 */
	private $isWelcomeLetterSent;
		/*
	 *@var null
	 */
	private $terminatorCode;
		/*
	 *@var null
	 */
	private $cancellationDate;
		/*
	 *@var null
	 */
	private $customFields;
		/*
	 *@var null
	 */
	private $contractDocumentCode;
		/*
	 *@var null
	 */
	private $isVisibleExternally;
		/*
	 *@var null
	 */
	private $isAgainstBalance;
		/*
	 *@var null
	 */
	private $renewalAmount;
		/*
	 *@var null
	 */
	private $renewedDate;
		/*
	 *@var null
	 */
	private $idDocumentCode;
		/*
	 *@var null
	 */
	private $firstChargeIndex;
		/*
	 *@var null
	 */
	private $isReinstateProcess;
		
	private $recurrence = 1;
	
	private $recurrenceCount = 1;
	
	/*
	 *$constructor.comment
	 */
	function __construct($createDate = null, $lastInvocingDate = null, $lastProcessingDate = null, $code = null, $sellerCode = null, $length = null, $value = null, $deferredLength = null, $deferredValue = null, $amount = null, $groupCode1 = null, $groupCode2 = null, $groupCode3 = null, $groupCode4 = null, $groupCode5 = null, $groupCode6 = null, $groupCode7 = null, $groupCode8 = null, $merchantAccountCode = null, $type = null, $firstBillingDate = null, $billingCycleCode = null, $itemCode = null, $taxCode = null, $paymentOption = null, $customerAccount = null, $id = null, $lastUpdateDate = null, $nextBillingDate = null, $status = null){
		//initialization block
		$this->MERCHANT_ACCOUNT_CODE_ASC = "paymentPlan.merchantAccountCode";
		$this->MERCHANT_ACCOUNT_CODE_DESC = "paymentPlan.merchantAccountCode desc";
		$this->CODE_ASC = "paymentPlan.refCode";
		$this->CODE_DESC = "paymentPlan.refCode desc";
		$this->CREATE_DATE_ASC = "paymentPlan.createDate";
		$this->CREATE_DATE_DESC = "paymentPlan.createDate desc";
		$this->FIRST_BILLING_DATE_ASC = "paymentPlan.firstBillingDate";
		$this->FIRST_BILLING_DATE_DESC = "paymentPlan.firstBillingDate desc";
		$this->NEXT_BILLING_DATE_ASC = "paymentPlan.billingCycle.nextBillingDate";
		$this->NEXT_BILLING_DATE_DESC = "paymentPlan.billingCycle.nextBillingDate desc";
		$this->STATUS_ASC = "paymentPlan.status";
		$this->STATUS_DESC = "paymentPlan.status desc";
		$this->AMOUNT_ASC = "paymentPlan.amount";
		$this->AMOUNT_DESC = "paymentPlan.amount desc";
		$this->ITEM_CODE_ASC = "paymentPlan.item.refCode";
		$this->ITEM_CODE_DESC = "paymentPlan.item.refCode desc";
		$this->SELLER_CODE_ASC = "paymentPlan.refSellerCode";
		$this->SELLER_CODE_DESC = "paymentPlan.refSellerCode desc";
		$this->BILLING_CYCLE_CODE_ASC = "paymentPlan.billingCycle.refCode";
		$this->BILLING_CYCLE_CODE_DESC = "paymentPlan.billingCycle.refCode desc";
		$this->GROUP_CODE1_ASC = "paymentPlan.refGroupCode1";
		$this->GROUP_CODE1_DESC = "paymentPlan.refGroupCode1 desc";
		$this->GROUP_CODE2_ASC = "paymentPlan.refGroupCode2";
		$this->GROUP_CODE2_DESC = "paymentPlan.refGroupCode2 desc";
		$this->GROUP_CODE3_ASC = "paymentPlan.refGroupCode3";
		$this->GROUP_CODE3_DESC = "paymentPlan.refGroupCode3 desc";
		$this->GROUP_CODE4_ASC = "paymentPlan.refGroupCode4";
		$this->GROUP_CODE4_DESC = "paymentPlan.refGroupCode4 desc";
		$this->GROUP_CODE5_ASC = "paymentPlan.refGroupCode5";
		$this->GROUP_CODE5_DESC = "paymentPlan.refGroupCode5 desc";
		$this->GROUP_CODE6_ASC = "paymentPlan.refGroupCode6";
		$this->GROUP_CODE6_DESC = "paymentPlan.refGroupCode6 desc";
		$this->GROUP_CODE7_ASC = "paymentPlan.refGroupCode7";
		$this->GROUP_CODE7_DESC = "paymentPlan.refGroupCode7 desc";
		$this->GROUP_CODE8_ASC = "paymentPlan.refGroupCode8";
		$this->GROUP_CODE8_DESC = "paymentPlan.refGroupCode8 desc";
		$this->CUSTOMER_ACCOUNT_CODE_ASC = "paymentPlan.customerAccount.refCode";
		$this->CUSTOMER_ACCOUNT_CODE_DESC = "paymentPlan.customerAccount.refCode desc";
		$this->PAYMENT_OPTION_CODE_ASC = "paymentPlan.paymentOption.refCode";
		$this->PAYMENT_OPTION_CODE_DESC = "paymentPlan.paymentOption.refCode desc";
		$this->taxAmount = 0;
		$this->renewalTaxAmount = 0;
		$this->charges = new ArrayList();
		$this->isVisibleExternally = false;
		$this->isAgainstBalance = false;
		$this->isReinstateProcess = false;
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
		//constructor with parameters
		$this->setCreateDate($createDate);
		$this->lastInvoicingDate = $lastInvocingDate;
		$this->lastProcessingDate = $lastProcessingDate;
		$this->setCode($code);
		$this->sellerCode = $sellerCode;
		$this->length = $length;
		$this->value = $value;
		$this->deferredLength = $deferredLength;
		$this->deferredValue = $deferredValue;
		$this->amount = $amount;
		$this->groupCode1 = $groupCode1;
		$this->groupCode2 = $groupCode2;
		$this->groupCode3 = $groupCode3;
		$this->groupCode4 = $groupCode4;
		$this->groupCode5 = $groupCode5;
		$this->groupCode6 = $groupCode6;
		$this->groupCode7 = $groupCode7;
		$this->groupCode8 = $groupCode8;
		$this->setMerchantAccountCode($merchantAccountCode);
		$this->type = $type;
		$this->firstBillingDate = $firstBillingDate;
		$this->nextBillingDate = $nextBillingDate;
		$this->billingCycleCode = $billingCycleCode;
		$this->itemCode = $itemCode;
		$this->taxCode = $taxCode;
		$this->paymentOption = $paymentOption;
		$this->customerAccount = $customerAccount;
		$this->setId($id);
		$this->lastUpdateDate = $lastUpdateDate;
		$this->status = $status;
		$this->setRefId(is_null($id) ? $this->hashCode() : $id);
	
	}

	/*
	 *$method.comment
	 */
	public function getLastInvoicingDate(){
		return $this->lastInvoicingDate;
	}
	/*
	 *$method.comment
	 */
	 function setLastInvoicingDate($lastInvoicingDate){
		$this->lastInvoicingDate = $lastInvoicingDate;
	}
	/*
	 *$method.comment
	 */
	public function getLastProcessingDate(){
		return $this->lastProcessingDate;
	}
	/*
	 *$method.comment
	 */
	 function setLastProcessingDate($lastProcessingDate){
		$this->lastProcessingDate = $lastProcessingDate;
	}
	/*
	 *$method.comment
	 */
	public function getCreatorCode(){
		return $this->creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function setCreatorCode($creatorCode){
		$this->creatorCode = $creatorCode;
	}
	/*
	 *$method.comment
	 */
	public function getSellerCode(){
		return $this->sellerCode;
	}
	/*
	 *$method.comment
	 */
	public function setSellerCode($sellerCode){
		$this->sellerCode = $sellerCode;
	}
	/*
	 *$method.comment
	 */
	public function getNote(){
		return $this->note;
	}
	/*
	 *$method.comment
	 */
	public function setNote($note){
		$this->note = $note;
	}
	/*
	 *$method.comment
	 */
	public function getLength(){
		return $this->length;
	}
	/*
	 *$method.comment
	 */
	 function setLength($length){
		$this->length = $length;
	}
	/*
	 *$method.comment
	 */
	public function getValue(){
		return $this->value;
	}
	/*
	 *$method.comment
	 */
	 function setValue($value){
		$this->value = $value;
	}
	/*
	 *$method.comment
	 */
	public function getDeferredLength(){
		return $this->deferredLength;
	}
	/*
	 *$method.comment
	 */
	 function setDeferredLength($deferredLength){
		$this->deferredLength = $deferredLength;
	}
	/*
	 *$method.comment
	 */
	public function getDeferredValue(){
		return $this->deferredValue;
	}
	/*
	 *$method.comment
	 */
	 function setDeferredValue($deferredValue){
		$this->deferredValue = $deferredValue;
	}
	/*
	 *$method.comment
	 */
	public function getAmount(){
		return $this->amount;
	}
	/*
	 *$method.comment
	 */
	public function setAmount($amount){
		$this->amount = $amount;
	}
	/*
	 *$method.comment
	 */
	public function getGroupCode1(){
		return $this->groupCode1;
	}
	/*
	 *$method.comment
	 */
	public function setGroupCode1($groupCode1){
		$this->groupCode1 = $groupCode1;
	}
	/*
	 *$method.comment
	 */
	public function getGroupCode2(){
		return $this->groupCode2;
	}
	/*
	 *$method.comment
	 */
	public function setGroupCode2($groupCode2){
		$this->groupCode2 = $groupCode2;
	}
	/*
	 *$method.comment
	 */
	public function getGroupCode3(){
		return $this->groupCode3;
	}
	/*
	 *$method.comment
	 */
	public function setGroupCode3($groupCode3){
		$this->groupCode3 = $groupCode3;
	}
	/*
	 *$method.comment
	 */
	public function getGroupCode4(){
		return $this->groupCode4;
	}
	/*
	 *$method.comment
	 */
	public function setGroupCode4($groupCode4){
		$this->groupCode4 = $groupCode4;
	}
	/*
	 *$method.comment
	 */
	public function getGroupCode5(){
		return $this->groupCode5;
	}
	/*
	 *$method.comment
	 */
	public function setGroupCode5($groupCode5){
		$this->groupCode5 = $groupCode5;
	}
	/*
	 *$method.comment
	 */
	public function getGroupCode6(){
		return $this->groupCode6;
	}
	/*
	 *$method.comment
	 */
	public function setGroupCode6($groupCode6){
		$this->groupCode6 = $groupCode6;
	}
	/*
	 *$method.comment
	 */
	public function getGroupCode7(){
		return $this->groupCode7;
	}
	/*
	 *$method.comment
	 */
	public function setGroupCode7($groupCode7){
		$this->groupCode7 = $groupCode7;
	}
	/*
	 *$method.comment
	 */
	public function getGroupCode8(){
		return $this->groupCode8;
	}
	/*
	 *$method.comment
	 */
	public function setGroupCode8($groupCode8){
		$this->groupCode8 = $groupCode8;
	}
	/*
	 *$method.comment
	 */
	public function getType(){
		return $this->type;
	}
	/*
	 *$method.comment
	 */
	public function setType($type){
		$this->type = $type;
	}
	/*
	 *$method.comment
	 */
	public function getFirstBillingDate(){
		return $this->firstBillingDate;
	}
	/*
	 *$method.comment
	 */
	public function setFirstBillingDate($firstBillingDate){
		$this->firstBillingDate = $firstBillingDate;
	}
	/*
	 *$method.comment
	 */
	 function createListCharges($size=true){
		$this->charges = new ArrayList($size);
		return $this->charges;
	}
	/*
	 *$method.comment
	 */
	public function getBillingCycleCode(){
		return $this->billingCycleCode;
	}
	/*
	 *$method.comment
	 */
	public function setBillingCycleCode($billingCycleCode){
		if (!is_null($this->getLinkedParentPlan()) && !((PaymentPlanStatus::Cancelled == $this->getLinkedParentPlan()->getStatus() || PaymentPlanStatus::Expired == $this->getLinkedParentPlan()->getStatus())) && !((!is_null($this->getLinkedParentPlan()->getId()) && is_null($this->getLinkedParentPlan()->getCode()))) && !($this->getLinkedParentPlan()->getBillingCycleCode() == $billingCycleCode)){
			throw new Exception("Invalid value for a linked plan; field: billingCycleCode. Value must match the one of the parent plan.");
		}
		$this->billingCycleCode = $billingCycleCode;
	}
	/*
	 *$method.comment
	 */
	public function getItemCode(){
		return $this->itemCode;
	}
	/*
	 *$method.comment
	 */
	public function setItemCode($itemCode){
		$this->itemCode = $itemCode;
	}
	/*
	 *$method.comment
	 */
	public function getTaxCode(){
		return $this->taxCode;
	}
	/*
	 *$method.comment
	 */
	public function setTaxCode($taxCode){
		$this->taxCode = $taxCode;
	}
	/*
	 *$method.comment
	 */
	public function getTaxAmount(){
		return $this->taxAmount;
	}
	/*
	 *$method.comment
	 */
	public function setTaxAmount($taxAmount){
		$this->taxAmount = $taxAmount;
	}
	/*
	 *$method.comment
	 */
	public function getRenewalTaxAmount(){
		return $this->renewalTaxAmount;
	}
	/*
	 *$method.comment
	 */
	public function setRenewalTaxAmount($renewalTaxAmount){
		$this->renewalTaxAmount = $renewalTaxAmount;
	}
	
	public function getRecurrence(){
		return $this->recurrence;
	}
	
	public function setRecurrence($recurrence){
		$this->recurrence = $recurrence;
	}
	
	function  getRecurrenceCount() {
		return $this->recurrenceCount;
	}

	function setRecurrenceCount($recurrenceCount) {
		return $this->recurrenceCount = $recurrenceCount;
	}
	/*
	 *$method.comment
	 */
	 function getChargesInternal(){
		return $this->charges;
	}
	/*
	 *$method.comment
	 */
	public function getCharges(){
		return new ArrayList($this->charges);
	}
	/*
	 *$method.comment
	 */
	public function getStatus(){
		return $this->status;
	}
	/*
	 *$method.comment
	 */
	 function setStatus($status){
		$this->status = $status;
	}
	/*
	 *$method.comment
	 */
	 function setCharges($charges){
		$this->charges = $charges;
	}
	/*
	 *$method.comment
	 */
	public function getPaymentOption(){
		return $this->paymentOption;
	}
	/*
	 *$method.comment
	 */
	public function setPaymentOption($paymentOption){
		$this->paymentOption = $paymentOption;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getContract(){
		return $this->contract;
	}
	/*
	 *$method.comment
	 */
	public function setContract($contract){
		$this->contract = $contract;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccount($customerAccount){
		if (!is_null($this->getLinkedParentPlan()) && !((!is_null($this->getLinkedParentPlan()->getId()) && is_null($this->getLinkedParentPlan()->getCode()))) && !($this->getLinkedParentPlan()->getCustomerAccount()->getId() == $customerAccount->getId())){
			throw new Exception("Invalid value for a linked plan; field: customerAccount. Value must match the one of the parent plan.");
		}
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function getLinkedParentPlan(){
		return $this->linkedParentPlan;
	}
	/*
	 *$method.comment
	 */
	 function setLinkedParentPlan($linkedParentPlan){
		$this->linkedParentPlan = $linkedParentPlan;
	}
	/*
	 *$method.comment
	 */
	public function createLinkedPaymentPlanBasic($code,$type,$amount,$length){
		if (!(PaymentPlanType::Fixed == $this->type)){
			throw new Exception("Unable to create child payment plan. Able to create child paymentPlan only for fixed payment plan.");
		}
		$paymentPlan = $this->createLinkedPaymentPlan();
		$paymentPlan->setAmount($amount);
		$paymentPlan->setType($type);
		$paymentPlan->setCode($code);
		if (!is_null($length)){
			$paymentPlan->add(0, $length, false);
		}
		return $paymentPlan;
	}
	/*
	 *$method.comment
	 */
	public function createLinkedPaymentPlanExtended($code,$type,$amount,$length,$paymentOption,$sellerCode,$itemCode,$taxCode,$groupCode1,$groupCode2,$groupCode3,$groupCode4,$groupCode5,$groupCode6,$groupCode7,$groupCode8){
		$paymentPlan = $this->createLinkedPaymentPlanBasic($code, $type, $amount, $length);
		$paymentPlan->setPaymentOption($paymentOption);
		$paymentPlan->setSellerCode($sellerCode);
		$paymentPlan->setItemCode($itemCode);
		$paymentPlan->setTaxCode($taxCode);
		$paymentPlan->setGroupCode1($groupCode1);
		$paymentPlan->setGroupCode2($groupCode2);
		$paymentPlan->setGroupCode3($groupCode3);
		$paymentPlan->setGroupCode4($groupCode4);
		$paymentPlan->setGroupCode5($groupCode5);
		$paymentPlan->setGroupCode6($groupCode6);
		$paymentPlan->setGroupCode7($groupCode7);
		$paymentPlan->setGroupCode8($groupCode8);
		return $paymentPlan;
	}
	/*
	 *$method.comment
	 */
	public function add($position,$length,$isFreeze){
		if (is_null($length) || $length <= 0){
			throw new Exception("Invalid field value. Object: %1$s; Code: %2$s; Field: %3$s.", "PaymentPlan", is_null($this->getCode()) ? "unspecified" : $this->getCode(), "length");
		}
		for ($i = $position;$i < $length + $position;$i++){
			$charge = new Charge(new DateTime(null, new DateTimeZone('EST')), null, false, $isFreeze ? ChargeType::Freeze : $this->type, $this->getMerchantAccountCode(), is_null($this->customerAccount->getSentCollectionsDate()) ? null : $this->customerAccount->getCollectorAccountCode(), $this, null, null);
			$this->addCharge($charge, $position);
		}
	}
	/*
	 *$method.comment
	 */
	public function removeRange($position,$length){
		$totalLength = is_null($this->deferredLength) ? $this->length : $this->deferredLength + $this->length;
		if ($totalLength < $position + $length - 1){
			throw new Exception("Unable to execute removeRange: (position + length) exceed length of Payment Plan.");
		}
		for ($i = $position + $length - 1;$i > $position - 1;$i--){
			$this->removePosition($i);
		}
		$this->resetChargeIndexes();
	}
	/*
	 *$method.comment
	 */
	public function replaceRange($position,$length){
		$totalLength = is_null($this->deferredLength) ? $this->length : $this->deferredLength + $this->length;
		if ($totalLength < $position + $length - 1){
			throw new Exception("Unable to execute replaceRange: (position + length) exceed length of Payment Plan.");
		}
		for ($i = $position;$i < $position + $length;$i++){
			$charges->get($i)->setType(ChargeType::Freeze);
		}
	}
	/*
	 *$method.comment
	 */
	public function replace($chargesList){
		foreach ($chargesList as $repacedCharge){
			foreach ($this->charges as $charge){
				if ($repacedCharge == $charge){
					$charge->setType(ChargeType::Freeze);
					break;
				}
			}
		}
	}
	/*
	 *$method.comment
	 */
	public function cancel(){
		$this->cancelSpecial(true);
	}
	/*
	 *$method.comment
	 */
	public function cancelSpecial($cancelChildPaymentPlan){
		$this->charges->clear();
		$this->firstChargeIndex = 0;
		$this->length = 0;
		$this->deferredLength = 0;
		$this->value = 0;
		$this->deferredLength = 0;
		$this->status = PaymentPlanStatus::Cancelled;
		$this->customerAccount->addPaymentPlan($this);
		if (!($cancelChildPaymentPlan) && is_null($this->renewedDate) && !is_null($this->renewalAmount)){
			$child = $this->createLinkedPaymentPlan();
			$child->setCode("L" . parent::getCode());
			$child->setAmount($this->renewalAmount);
			$child->setType(PaymentPlanType::Perpetual);
			$child->setCreatorCode($this->creatorCode);
			$child->setSellerCode($this->sellerCode);
			$child->setTaxCode($this->taxCode);
			$child->setItemCode($this->itemCode);
			$child->setNote($this->note);
			$child->setPaymentOption($this->paymentOption);
			$child->setGroupCode1($this->groupCode1);
			$child->setGroupCode2($this->groupCode2);
			$child->setGroupCode3($this->groupCode3);
			$child->setGroupCode4($this->groupCode4);
			$child->setGroupCode5($this->groupCode5);
			$child->setGroupCode6($this->groupCode6);
			$child->setGroupCode7($this->groupCode7);
			$child->setGroupCode8($this->groupCode8);
			$child->setCustomFields($this->customFields);
		}
	}
	/*
	 *$method.comment
	 */
	public function removePosition($position){
		$charge = $this->charges->get($position);
		$this->charges->remove($position);
		$this->decrementChargesListParameters($charge);
		$this->resetChargeIndexes();
	}
	/*
	 *$method.comment
	 */
	public function remove($charge){
		$this->charges->remove($charge);
		$this->decrementChargesListParameters($charge);
		$this->resetChargeIndexes();
	}
	/*
	 *$method.comment
	 */
	 function addCharge($charge,$position){
		if ($this->charges->contains($charge)){
			throw new Exception();
		}
		if (is_null($position) || $position < 0){
			throw new Exception();
		}
		if (is_null($this->amount)){
			throw new Exception("Required field cannot be empty. Object: %1$s; Code: %2$s; Field: %3$s.", "PaymentPlan", is_null($this->getCode()) ? "unspecified" : $this->getCode(), "amount");
		}
		if ($position < ($this->charges->size() - 2) && ChargeType::Deferred == $this->charges->get($position)->getType()){
			throw new Exception(ChargeType::Freeze == $charge->getType() ? "Cannot add freeze charge before deferred charge." : "Cannot add fixed charge before deferred charge.");
		}
		if ($position < $this->charges->size()){
			$this->charges->add($position, $charge);
		}
		else {
			$this->charges->add($charge);
		}
		$this->incrementChargesListParameters($charge);
		$this->resetChargeIndexes();
	}
	/*
	 *$method.comment
	 */
	public function getNextCharge(){
		if ($this->charges->isEmpty()){
			return null;
		}
		return $this->charges->get(0);
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "PaymentPlan";
	}
	/*
	 *$method.comment
	 */
	public function createAdjustmentExtended($code,$type,$notes,$adjustmentReasonCode){
		$adjustment = $this->createAdjustment();
		$adjustment->setCode($code);
		$adjustment->setType($type);
		$adjustment->setNotes($notes);
		$adjustment->setAdjustmentReasonCode($adjustmentReasonCode);
		return $adjustment;
	}
	/*
	 *$method.comment
	 */
	public function isActive(){
		return !((PaymentPlanStatus::Cancelled == $this->status || PaymentPlanStatus::Expired == $this->status));
	}
	/*
	 *$method.comment
	 */
	public function getLastUpdateDate(){
		return $this->lastUpdateDate;
	}
	/*
	 *$method.comment
	 */
	 function setLastUpdateDate($lastUpdateDate){
		$this->lastUpdateDate = $lastUpdateDate;
	}
	/*
	 *$method.comment
	 */
	public function getAdjustment(){
		return $this->adjustment;
	}
	/*
	 *$method.comment
	 */
	public function setAdjustment($adjustment){
		$this->adjustment = $adjustment;
	}
	/*
	 *$method.comment
	 */
	public function getNextBillingDate(){
		return $this->nextBillingDate;
	}
	/*
	 *$method.comment
	 */
	 function setNextBillingDate($nextBillingDate){
		$this->nextBillingDate = $nextBillingDate;
	}
	/*
	 *$method.comment
	 */
	private function incrementChargesListParameters($charge){
		if (ChargeType::Deferred == $charge->getType() || ChargeType::Freeze == $charge->getType()){
			if (!is_null($this->deferredLength)){
				$this->deferredLength++;
			}
			else {
				$this->deferredLength = 1;
			}
			$this->deferredValue = $this->deferredLength * $this->amount;
		}
		else {
			if (!is_null($this->length)){
				$this->length++;
			}
			else {
				$this->length = 1;
			}
			$this->value = $this->length * $this->amount;
		}
	}
	/*
	 *$method.comment
	 */
	private function decrementChargesListParameters($charge){
		if (ChargeType::Deferred == $charge->getType() || ChargeType::Freeze == $charge->getType()){
			$this->deferredLength--;
			$this->deferredValue = $this->deferredLength * $this->amount;
		}
		else {
			$this->length--;
			$this->value = $this->length * $this->amount;
		}
	}
	/*
	 *$method.comment
	 */
	 function getLinkAccount(){
		return $this->linkAccount;
	}
	/*
	 *$method.comment
	 */
	 function setLinkAccount($linkAccount){
		$this->linkAccount = $linkAccount;
	}
	/*
	 *$method.comment
	 */
	public function getTerminatorCode(){
		return $this->terminatorCode;
	}
	/*
	 *$method.comment
	 */
	public function setTerminatorCode($terminatorCode){
		$this->terminatorCode = $terminatorCode;
	}
	/*
	 *$method.comment
	 */
	public function getCancellationDate(){
		return $this->cancellationDate;
	}
	/*
	 *$method.comment
	 */
	 function setCancellationDate($cancellationDate){
		$this->cancellationDate = $cancellationDate;
	}
	/*
	 *$method.comment
	 */
	public function getCustomFields(){
		return $this->customFields;
	}
	/*
	 *$method.comment
	 */
	public function setCustomFields($customFields){
		$this->customFields = $customFields;
	}
	/*
	 *$method.comment
	 */
	public function getContractDocumentCode(){
		return $this->contractDocumentCode;
	}
	/*
	 *$method.comment
	 */
	public function setContractDocumentCode($contractDocumentCode){
		$this->contractDocumentCode = $contractDocumentCode;
	}
	/*
	 *$method.comment
	 */
	public function getIsVisibleExternally(){
		return $this->isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function setIsVisibleExternally($isVisibleExternally){
		$this->isVisibleExternally = $isVisibleExternally;
	}
	/*
	 *$method.comment
	 */
	public function getIsAgainstBalance(){
		return $this->isAgainstBalance;
	}
	/*
	 *$method.comment
	 */
	 function setIsAgainstBalance($isAgainstBalance){
		$this->isAgainstBalance = $isAgainstBalance;
	}
	/*
	 *$method.comment
	 */
	public function getRenewalAmount(){
		return $this->renewalAmount;
	}
	/*
	 *$method.comment
	 */
	public function setRenewalAmount($renewalAmount){
		if (!is_null($renewalAmount) && !(PaymentPlanType::Fixed == $this->type)){
			throw new Exception("Unable to set renewal amount. Able to set renewal amount only for fixed payment plan.");
		}
		if (!is_null($this->renewalAmount) && !($this->renewalAmount == $renewalAmount) && !is_null($this->renewedDate)){
			throw new Exception("Unable to change renewal amount: Child Payment Plan was created.");
		}
		$this->renewalAmount = $renewalAmount;
	}
	/*
	 *$method.comment
	 */
	public function getRenewedDate(){
		return $this->renewedDate;
	}
	/*
	 *$method.comment
	 */
	 function setRenewedDate($renewedDate){
		$this->renewedDate = $renewedDate;
	}
	/*
	 *$method.comment
	 */
	public function getIdDocumentCode(){
		return $this->idDocumentCode;
	}
	/*
	 *$method.comment
	 */
	public function setIdDocumentCode($idDocumentCode){
		$this->idDocumentCode = $idDocumentCode;
	}
	/*
	 *$method.comment
	 */
	public function getFreezeAdjustment(){
		return $this->freezeAdjustment;
	}
	/*
	 *$method.comment
	 */
	public function setFreezeAdjustment($freezeAdjustment){
		$this->freezeAdjustment = $freezeAdjustment;
	}
	/*
	 *$method.comment
	 */
	public function getCancellationAdjustment(){
		return $this->cancellationAdjustment;
	}
	/*
	 *$method.comment
	 */
	public function setCancellationAdjustment($cancellationAdjustment){
		$this->cancellationAdjustment = $cancellationAdjustment;
	}
	/*
	 *$method.comment
	 */
	 function setFirstChargeIndex($firstChargeIndex){
		$this->firstChargeIndex = $firstChargeIndex;
	}
	/*
	 *$method.comment
	 */
	public function getIsWelcomeLetterSent(){
		return $isWelcomeLetterSent;
	}
	/*
	 *$method.comment
	 */
	public function setIsWelcomeLetterSent($isWelcomeLetterSent){
		$this->isWelcomeLetterSent = $isWelcomeLetterSent;
	}
	/*
	 *$method.comment
	 */
	 function getIsReinstateProcess(){
		return $this->isReinstateProcess;
	}
	/*
	 *$method.comment
	 */
	 function setIsReinstateProcess($isReinstateProcess){
		$this->isReinstateProcess = $isReinstateProcess;
	}
	/*
	 *$method.comment
	 */
	private function resetChargeIndexes(){
		$index = $this->firstChargeIndex;
		echo $index;
		foreach ($this->charges as $charge){
			$charge->setIndex($index++);
		}
	}
	/*
	 *$method.comment
	 */
	public function equals($anotherObject){
		if ($this == $anotherObject){
			return true;
		}
		if (is_null($anotherObject) || is_null($this->getId())){
			return false;
		}
		if ($anotherObject instanceof PaymentPlan){
			if ($this->getId() == $anotherObject->getId()){
				return true;
			}
		}
		return false;
	}
	/*
	 *$method.comment
	 */
	public function hashCode(){
		return parent::hashCode();
	}
	/*
	 *$method.comment
	 */
	public function reinstate(){
		if (!(PaymentPlanStatus::Suspended == $this->status)){
			return ;
		}
		if (!is_null($this->customerAccount->getBalance()) && $this->customerAccount->getBalance() > 0){
			throw new Exception("Unable to reinstate Payment Plan: Customer Account has positive balance.");
		}
		$this->isReinstateProcess = true;
	}
	/*
	 *$method.comment
	 */
	public function createAdjustment(){
		if (PaymentPlanStatus::Cancelled == $this->status || PaymentPlanStatus::Expired == $this->status){
			throw new Exception("Unable to create Adjustment: PaymentPlan is inactive. PaymentPlan Code: " . $this->getCode());
		}
		$adjustment = new Adjustment();
		$adjustment->setMerchantAccountCode($this->getMerchantAccountCode());
		$adjustment->setCollectorAccountCode(is_null($this->customerAccount->getSentCollectionsDate()) ? null : $this->customerAccount->getCollectorAccountCode());
		$adjustment->setIsProcessed(true);
		$adjustment->setIsVisibleExternally(true);
		$adjustment->setRenewalAmount($this->getRenewalAmount());
		if (PaymentPlanType::Fixed == $this->type){
			$adjustment->setLength($this->length);
			$adjustment->setValue($this->value);
			if (!is_null($this->getContract())){
				$adjustment->setExpirationDate($this->getContract()->getExpirationDate());
			}
		}
		$adjustment->setPaymentPlan($this);
		$adjustment->setContract($this->getContract());
		$adjustment->setCustomerAccount($this->customerAccount);
		$this->adjustment = $adjustment;
		return $adjustment;
	}
	/*
	 *$method.comment
	 */
	public function createLinkedPaymentPlan(){
		$paymentPlan = new PaymentPlan();
		$paymentPlan->setMerchantAccountCode($this->getMerchantAccountCode());
		$paymentPlan->setCollectorAccountCode($this->customerAccount->getCollectorAccountCode());
		$paymentPlan->setBillingCycleCode($this->billingCycleCode);
		$paymentPlan->setCustomerAccount($this->customerAccount);
		$paymentPlan->setLinkedParentPlan($this);
		$this->linkAccount = !is_null($this->linkAccount) ? $this->linkAccount + 1 : 1;
		$this->customerAccount->addPaymentPlan($paymentPlan);
		return $paymentPlan;
	}
}

?>

