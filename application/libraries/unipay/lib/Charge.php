<?php



/*
 *$comment
 */
class Charge extends ClientObject {
		/*
	 *@var null
	 */
	private $processedDate;
		/*
	 *@var null
	 */
	private $isPrepaid;
		/*
	 *@var null
	 */
	private $type;
		/*
	 *@var null
	 */
	private $index;
		/*
	 *@var null
	 */
	private $paymentPlan;
		
	/*
	 *$constructor.comment
	 */
	function __construct($createDate = null, $processedDate = null, $isPrepaid = null, $type = null, $merchantAccountCode = null, $collectorAccountCode = null, $paymentPlan = null, $code = null, $id = null){
		//initialization block
		$this->isPrepaid = false;
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
		//constructor with parameters
		$this->setCreateDate($createDate);
		$this->processedDate = $processedDate;
		$this->isPrepaid = $isPrepaid;
		$this->type = $type;
		$this->setMerchantAccountCode($merchantAccountCode);
		$this->paymentPlan = $paymentPlan;
		$this->setId($id);
		$this->setCode($code);
		$this->setRefIdSpecial($this->hashCode());
	
	}

	/*
	 *$method.comment
	 */
	public function getProcessedDate(){
		return $this->processedDate;
	}
	/*
	 *$method.comment
	 */
	 function setProcessedDate($processedDate){
		$this->processedDate = $processedDate;
	}
	/*
	 *$method.comment
	 */
	public function getIsPrepaid(){
		return $this->isPrepaid;
	}
	/*
	 *$method.comment
	 */
	public function setIsPrepaid($isPrepaid){
		$this->isPrepaid = $isPrepaid;
	}
	/*
	 *$method.comment
	 */
	public function getType(){
		return $this->type;
	}
	/*
	 *$method.comment
	 */
	 function setType($type){
		$this->type = $type;
	}
	/*
	 *$method.comment
	 */
	public function getBillingDate(){
		$billingDate = null;
		$chargeIndex = 0;
		$needShiftBillingDate = $this->paymentPlan->getRecurrenceCount() > 1;
		
		if (is_null($this->paymentPlan->getLinkedParentPlan())){
			$billingDate = $this->paymentPlan->getNextBillingDate();
		}
		else {
			if (!is_null($this->paymentPlan->getNextBillingDate()) && Helper::compareDate($this->paymentPlan->getNextBillingDate(), $this->paymentPlan->getFirstBillingDate()) < 0){
				$billingDate = $this->paymentPlan->getFirstBillingDate();
				$needShiftBillingDate = false;
			}
			else {
				$billingDate = $this->paymentPlan->getNextBillingDate();
			}
		}
		
		if ($needShiftBillingDate) {
			$billingDate = Helper.getNextBillingDate($billingDate, $this->paymentPlan->getBillingCycleCode(), $this->paymentPlan->getRecurrenceCount() - 1, 1);	
		}
		
		$chargeIndex = $this->index - $this->paymentPlan->getNextCharge()->getIndex();
		return Helper::getNextBillingDate($billingDate, $this->paymentPlan->getBillingCycleCode(), $chargeIndex, $this->paymentPlan->getRecurrence());
	}
	/*
	 *$method.comment
	 */
	public function getPaymentPlan(){
		return $this->paymentPlan;
	}
	/*
	 *$method.comment
	 */
	public function setPaymentPlan($paymentPlan){
		$this->paymentPlan = $paymentPlan;
	}
	/*
	 *$method.comment
	 */
	public function getNextCharge(){
		return $this->paymentPlan->getChargesInternal()->get($this->getPosition() + 1);
	}
	/*
	 *$method.comment
	 */
	public function getPrevCharge(){
		return $paymentPlan->getChargesInternal()->get($this->getPosition() - 1);
	}
	/*
	 *$method.comment
	 */
	public function getPosition(){
		return $this->paymentPlan->getChargesInternal()->indexOf($this);
	}
	/*
	 *$method.comment
	 */
	 function getIndex(){
		return $this->index;
	}
	/*
	 *$method.comment
	 */
	 function setIndex($index){
		$this->index = $index;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "Charge";
	}
	/*
	 *$method.comment
	 */
	public function equals($anotherObject){
		if ($this == $anotherObject){
			return true;
		}
		if (is_null($anotherObject) || is_null($this->getId())){
			return false;
		}
		if ($anotherObject instanceof Charge){
			if ($this->getId() == $anotherObject->getId()){
				return true;
			}
		}
		return false;
	}
	/*
	 *$method.comment
	 */
	public function hashCode(){
		return parent::hashCode();
	}
}

?>

