<?php


/*
 *$comment
 */
class AccountActivity extends ClientObject {
		/*
	 *@var null
	 */
	private $accountActivityType;
		/*
	 *@var null
	 */
	private $sequenceNumber;
		/*
	 *@var null
	 */
	private $customerAccount;
		
	/*
	 *$constructor.comment
	 */
	function __construct($merchantAccountCode = null, $accountActivityType = null, $code = null, $customerAccount = null, $id = null){
		//initialization block
		if (func_num_args() == 0) {
		//default constructor
		$this->setRefIdSpecial($this->hashCode());

		return;
		}
		//constructor with parameters
		$this->setMerchantAccountCode($merchantAccountCode);
		$this->accountActivityType = $accountActivityType;
		$this->setCode($code);
		$this->customerAccount = $customerAccount;
		$this->setId($id);
		$this->setRefId(is_null($id) ? $this->hashCode() : $id);
	
	}

	/*
	 *$method.comment
	 */
	public function getAccountActivityType(){
		return $this->accountActivityType;
	}
	/*
	 *$method.comment
	 */
	public function setAccountActivityType($accountActivityType){
		$this->accountActivityType = $accountActivityType;
	}
	/*
	 *$method.comment
	 */
	public function getCustomerAccount(){
		return $this->customerAccount;
	}
	/*
	 *$method.comment
	 */
	 function setCustomerAccount($customerAccount){
		$this->customerAccount = $customerAccount;
	}
	/*
	 *$method.comment
	 */
	public function setCreateDate($createDate){
		parent::setCreateDate($createDate);
	}
	/*
	 *$method.comment
	 */
	public function getSequenceNumber(){
		return $sequenceNumber;
	}
	/*
	 *$method.comment
	 */
	 function setSequenceNumber($sequenceNumber){
		$this->sequenceNumber = $sequenceNumber;
	}
	/*
	 *$method.comment
	 */
	public function type(){
		return "AccountActivity";
	}
}

?>

