<?php

/**
 * UniPay library for CodeIgniter
 * Author: Chris Read (chris.read@nrnssoftware.com.au)
 * Dependencies: curl, PHP 5.3+, CI 2.1.4+
 * See http://www.unipaygateway.com/api for API details
 */

class unicore {
	public $keys;
	public $merchants;
	public $portfolios;
	public $resellers;
	public $users;

	public function __construct() {
		$this->keys = new unipay_resource();
		$this->merchants = new unipay_merchants();
		$this->portfolios = new unipay_resource();
		$this->resellers = new unipay_resource();
		$this->users = new unipay_resource();
	}

/*
	Planned usage
	$this->merchants->create(); PUT XML -> https://sandbox.unipaygateway.com/api/v01/merchants/~create.xml
	$this->merchants->find(); GET -> https://sandbox.unipaygateway.com/api/v01/merchants/~find.xml

	$this->merchants->delete(code); DELETE -> https://sandbox.unipaygateway.com/api/v01/merchants/1/~delete.xml
	$this->merchants->load(code); GET -> https://sandbox.unipaygateway.com/api/v01/merchants/1/~load.xml
	$this->merchants->modify(code); POST -> https://sandbox.unipaygateway.com/api/v01/merchants/1/~modify.xml
	$this->merchants->verify(code); GET -> https://sandbox.unipaygateway.com/api/v01/merchants/1/~verify.xml
*/

/*
	Actions come in and are appended to the URL like https://sandbox.unipaygateway.com/api/v01/[resource]/~[action].[content type]
	
	ie: https://sandbox.unipaygateway.com/api/v01/merchants/~find.xml

	The endpoints are as follows:
	keys
		pgp
		pgpx
		ssh
		sshx
		linkpoint

	https://sandbox.unipaygateway.com/api/v01/keys/pgp/~find.xml?suppressStatusCodes=false&offset=0&limit=20

	merchants
	portfolios
	resellers
	users

	Request content types are:
	json
	xml
	query
	multipart/binary

	Response content types are:
	json
	xml
	query
	csv
	binary
*/
}

class unipay_merchants extends unipay_resource {
	public function __construct() {
		parent::__construct();
		$this->resource = 'merchants';
	}

	/* CRUD */
	public function create() {
		$this->PUT('create');
	}
	public function load($code) {
		$this->GET('load',$code);
	}
	public function modify($code) {
		$this->POST('modify',$code);
	}
	public function delete($code) {
		$this->DELETE('delete',$code);
	}
	/* Bonus: find and verify */
	public function find() {
		$this->GET('find');
	}
	/* verify() - run background checks on a legal entity for the purpose of underwriting */
	public function verify($code) {
		$this->GET('verify',$code);
	}
}

class merchant_code extends unipay_resource {
	public $code;

	public function __construct() {
		parent::__construct();
		$this->properties = [
			'billing',
			'business_info',
			'charge_info',
			'commissions',
			'contact_info',
			'fees',
			'leads',
			'letters',
			'phases',
			'processing_config',
			'processing_info',
			'profiles',
			'remittance_config',
			'remittance_info',
			'resellers',
			'reserves',
			'submerchants',
			'users'
		];
	}

	// These must be setup as get/set properties
	public function delete() {
		$this->DELETE('delete',$this->code);
	}
	public function load() {
		$this->GET('load',$this->code);
	}
	public function modify() {
		$this->POST('modify',$this->code);
	}
	public function verify() {
		$this->GET('verify',$code);
	}
}

class unipay_resource {
	protected $resource;
	protected $properties;
	protected $ci;

	public function __construct() {
		$this->properties = array();

		$this->ci =& get_instance();
		$this->ci->config->load('unipay');
		// 
	}

	public function __get($name) {
		// For each sub-resource in the merchants, we perform a query to retrieve the information
		if(in_array($name, $this->properties)) {
			// This is a valid property name, go get the data from the server
			// For example, we call:
			// $this->unipay->merchants->code(1)->billing
			// I'd like to do
			// $this->unipay->merchants(1)->business_info->primary_owner->load()
			// And return a BusinessOwner object:
			// 
			// <owner firstName="John" lastName="Smith" socialSecurity="000-00-0000" birthDate="1980-01-01" driverLicense="G635-730-261-136" driverLicenseState="MA" phone="212-339-0101" email="john.smith@sampleuser.com" street1="12 Main St" street2="Suite #3" city="Denver" state="MA" zipCode="30301" countryCode="US" />
			// 
			// And this gets converted to
			// http://url/api/v01/merchants/1/business-info/primary-owner/verification-result
		}
	}

	public function __set($name, $value) {}

	public function PUT($action,$code = '') {
		echo $this->ci->config->item('unicore_api_url').$this->resource.($code != ''?'/'.$code:'').'/~'.$action.'.xml';
	}
	public function GET($action,$code = '') {
		$curl = curl_init('https://sandbox.unipaygateway.com/api/v01/merchants/22000/~load.xml');

		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);                         
		curl_setopt($curl, CURLOPT_USERPWD, 'chris.read@nrnssoftware.com.au:ChriAd89500$');
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);                    
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);                          
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);                           

		$response = curl_exec($curl);                                          
		$resultStatus = curl_getinfo($curl);                                   
		var_dump($resultStatus);
		return simplexml_load_string($response);

		// Now initiate CURL and get some results!
		$curl = curl_init();
		// Most of this is probably not necessary, but we're being explicit at this stage
/*		curl_setopt($curl, CURLOPT_TIMEOUT, 60);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($curl, CURLOPT_FORBID_REUSE, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_POST, false);
*/		
		curl_setopt($curl, CURLOPT_URL, $this->ci->config->item('unicore_api_url').$this->resource.($code != ''?'/'.$code:'').'/~'.$action.'.xml');
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_USERPWD, $this->ci->config->item('unicore_api_username').':'.$this->ci->config->item('unicore_api_password'));
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

		if(TRUE) { // Debug the output
			echo '<pre>';
			curl_setopt($curl, CURLOPT_VERBOSE, true);
			$verbose = fopen('php://temp', 'rw+');
			curl_setopt($curl, CURLOPT_STDERR, $verbose);
			$response = curl_exec($curl);
			$error = curl_error($curl);
			rewind($verbose);
			$verboseLog = stream_get_contents($verbose);
			echo "Verbose information:\n<pre>", htmlspecialchars($verboseLog), "</pre>\n";
			echo $this->ci->config->item('unicore_api_url').$this->resource.($code != ''?'/'.$code:'').'/~'.$action.'.xml';
			var_dump($response);
			echo '</pre>';
		} else {
			$response = curl_exec($curl);
			$error = curl_error($curl);
		}
		$xml = simplexml_load_string($response);
		return $xml;
	}
	public function POST($action,$code = '') {
		echo $this->ci->config->item('unicore_api_url').$this->resource.($code != ''?'/'.$code:'').'/~'.$action.'.xml';
	}
	public function DELETE($action,$code = '') {
		echo $this->ci->config->item('unicore_api_url').$this->resource.($code != ''?'/'.$code:'').'/~'.$action.'.xml';
	}
}
