<?php

/**
 * UniPay library for CodeIgniter
 * Author: Chris Read (chris.read@nrnssoftware.com.au)
 * Dependencies: curl, PHP 5.3+, CI 2.1.4+
 * See http://www.unipaygateway.info/api for API details
 */

class unicharge {
	public function __construct() {
		$this->ci =& get_instance();
		$this->ci->config->load('unipay');
		// $this->ci->config->item('api_url_keys')
	}

	$this->merchants->find(array(['suppressStatusCode' => FALSE, 'offset' => 0, 'limit' => 20, 'code' => 1]));

	public function __call($method, $arguments) {

	}
/*
	Actions come in and are appended to the URL like http://sandbox.unipaygateway.info/api/v01/[resource]/~[action].[content type]
	
	ie: http://sandbox.unipaygateway.info/api/v01/merchants/~find.xml

	The endpoints are as follows:
	keys
		pgp
		pgpx
		ssh
		sshx
		linkpoint

	http://sandbox.unipaygateway.info/api/v01/keys/pgp/~find.xml?suppressStatusCodes=false&offset=0&limit=20

	merchants
	portfolios
	resellers
	users

	Request content types are:
	json
	xml
	query
	multipart/binary

	Response content types are:
	json
	xml
	query
	csv
	binary
*/
}
