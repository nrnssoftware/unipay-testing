<?php

/**
 * UniPay library for CodeIgniter
 * Author: Chris Read (chris.read@nrnssoftware.com.au)
 * Dependencies: curl, PHP 5.3+, CI 2.1.4+
 * See http://www.unipaygateway.info/api for API details
 */

require_once "lib/UniBill.php";

class unibill {
	private $config;
	private $session;

	public function __construct() {
		$this->ci =& get_instance();
		$this->ci->config->load('unipay');
		$this->config = new HashMap();
		$this->config->put(SessionConnection::PROCESSOR_HOST, $this->ci->config->item('unibill_api_url'));
//		$this->config->put("debug", true);
		$this->config->put(SessionConnection::CONNECTION_TYPE, 'xml');
	}

	private function login() {
		$this->session = Session::login($this->ci->config->item('unibill_api_username'), $this->ci->config->item('unibill_api_password'), $this->config);
		$this->session->setUserCode($this->ci->config->item('api_merchant'));
	}

	private function logout() {
		$this->session->logout();
	}

	public function list_all_customers($active = FALSE) {
		$this->login();
		// List the customers for a merchant
		$parameters = new HashMap();
		$parameters->put('merchantAccountCode', $this->ci->config->item('api_merchant'));
		if($active)
			$parameters->put('isActive','1');
		$customers = $this->session->queryCustomerAccount($parameters);
		$this->logout();
		return $customers;
	}

	public function create_customer($customer) {
		$this->login();
		$newcustomer = $this->session->createCustomerAccountExtended(
			null,
			null, //substr(md5(time()),0,4), // Random code string
			$this->ci->config->item('api_account'),
			$customer['first_name'],
			$customer['last_name'],
			$customer['middle_name'],
			$customer['title'],
			$customer['suffix'],
			$customer['gender'],
			$customer['phone_home'],
			$customer['phone_work'],
			$customer['phone_mobile'],
			$customer['email'],
			$customer['street1'],
			$customer['street2'],
			$customer['city'],
			'UK', //$customer['state'],
			$customer['postcode'],
			'',
			'',
			'Added by our application',
			'' //$customer['date_of_birth']
		);
		$result = $this->session->save($newcustomer);
		$result = $this->session->synchronize(); // Holy dooley
		$this->logout();
	}

	public function update_customer($code, $customer) {
		$this->login();
		$updatecustomer = $this->session->loadCustomerAccount($code);
		$updatecustomer->setFirstName($customer['first_name']);
		$updatecustomer->setMiddleName($customer['middle_name']);
		$updatecustomer->setLastName($customer['last_name']);
		$updatecustomer->setSuffix($customer['suffix']);
		$updatecustomer->setTitle($customer['title']);
		$updatecustomer->setBirthDate($customer['date_of_birth']);
		$updatecustomer->setType($customer['gender']);
		$updatecustomer->setStreet1($customer['street1']);
		$updatecustomer->setStreet2($customer['street2']);
		$updatecustomer->setCity($customer['city']);
		$updatecustomer->setState('UK');
		$updatecustomer->setZipCode($customer['postcode']);
		if($customer['phone_home'] != '')
			$updatecustomer->setHomePhone($customer['phone_home']);
		if($customer['phone_work'] != '')
			$updatecustomer->setWorkPhone($customer['phone_work']);
		if($customer['phone_mobile'] != '')
			$updatecustomer->setCellPhone($customer['phone_mobile']);
		$updatecustomer->setEmail($customer['email']);
		try {
			$result = $this->session->save($updatecustomer);
			$result = $this->session->synchronize(); // Holy dooley
		} catch(Exception $ex) {
			echo $ex->getMessage(); // Pass these messages back to the validation service and report to the screen
			die();
		}
		$this->logout();
	}

	public function fetch_customer($code) {
		$this->login();
		$customer = $this->session->loadCustomerAccount($code);
		$this->logout();
		return $customer;
	}

	public function set_active($code,$active) {
		$this->login();
		$updatecustomer = $this->session->loadCustomerAccount($code);
		$updatecustomer->setIsActive($active?'1':'0');
		$result = $this->session->save($updatecustomer);
		try {
			$result = $this->session->synchronize(); // Holy dooley
		} catch(Exception $ex) {
			echo $ex->getMessage(); // Pass these messages back to the validation service and report to the screen
		}
		$this->logout();
	}

}

?>