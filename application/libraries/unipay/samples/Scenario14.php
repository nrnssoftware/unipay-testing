<?php


require_once "lib/UniBill.php";
require_once "Scenario.php";

		//Set up clients' settings
		$config = new HashMap();
		$config->put(SessionConnection::PROCESSOR_HOST, Scenario::PROCESSOR_HOST);
		$config->put("debug", true);
		
		//Set connection type to XML
		$config->put(SessionConnection::CONNECTION_TYPE, "xml");
		
		//Login
		$session = Session::login(Scenario::MERCHANT, Scenario::PASSWORD, $config);
		$session->setUserCode(Scenario::USER_CODE);
		
		//Find customer account
		$customerAccount = $session->loadCustomerAccount(Scenario::CODE.".ca-1");
		
		//Create new payment option
		$paymentOption = $customerAccount->createPaymentOption();
		//code is optinal; if you specify the value, make sure the code is unique
		$paymentOption->setCode(Scenario::CODE.".po-14");
		$paymentOption->setHolderName("Jane Doe");
		$paymentOption->setNumber(Scenario::CC_NUMBER);
		//expiration date
		$paymentOption->setAccessory(Scenario::CC_EXPIRATION_DATE);
		$paymentOption->setStreet1("233 12th Street");
		$paymentOption->setCity("Columbus");
		$paymentOption->setState("CA");
		$paymentOption->setZipCode("31904");
		
		//Find statement payment plan
		$paymentPlan = $session->loadPaymentPlan(Scenario::CODE.".pp-1");
		//Set payment option
		$paymentPlan->setPaymentOption($paymentOption);
		
		//Mark object for persistence
		$session->save($customerAccount);
		
		//Synchronize changes with the server
		try{
			$session->synchronize();
		}
		catch(Exception $ex){
			//Be sure to properly handle exception, this is just a sample solution
			print($ex->getMessage());
			return ;
		}
		//logout
		$session->logout();



?>

