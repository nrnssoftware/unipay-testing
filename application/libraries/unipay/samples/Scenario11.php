<?php


require_once "lib/UniBill.php";
require_once "Scenario.php";

		//Set up clients' settings
		$config = new HashMap();
		$config->put(SessionConnection::PROCESSOR_HOST, Scenario::PROCESSOR_HOST);
		$config->put("debug", true);
		
		//Set connection type to XML
		$config->put(SessionConnection::CONNECTION_TYPE, "xml");
		
		//Login
		$session = Session::login(Scenario::MERCHANT, Scenario::PASSWORD, $config);
		$session->setUserCode(Scenario::USER_CODE);
		
		//Find revenue transaction
		$invoice = $session->loadRevenueTransaction(Scenario::CODE.".rt-10");
		$customerAccount = $invoice->getCustomerAccount();
		
		//Execute reverse invoice
		$reverseInvoice = $customerAccount->reverseRevenueTransaction($invoice);
		$reverseInvoice->setCode(Scenario::CODE.".rt-11");
		$reverseInvoice->setCreatorCode(Scenario::USER_CODE);
		$reverseInvoice->setPosterCode(Scenario::USER_CODE);
		
		//Mark object for persistence
		$session->save($reverseInvoice);
		
		//Find asset transaction
		$payment = $session->loadAssetTransaction(Scenario::CODE.".at-10");
		$customerAccount = $payment->getCustomerAccount();
		$voidPayment = $customerAccount->reversePayment($payment);
		$voidPayment->setCode(Scenario::CODE.".at-11");
		$voidPayment->setTransactionSourceType(TransactionSourceType::POS);
		$voidPayment->setCreatorCode(Scenario::USER_CODE);
		$voidPayment->setPosterCode(Scenario::USER_CODE);
		
		//Mark object for persistence
		$session->save($voidPayment);
		//Synchronize changes with the server
		try{
			$session->synchronize();
		}
		catch(Exception $ex){
			//Be sure to properly handle exception, this is just a sample solution
			print($ex->getMessage());
			return ;
		}
		//Logout
		$session->logout();



?>

