<?php


require_once "lib/UniBill.php";
require_once "Scenario.php";

		//Set up clients' settings
		$config = new HashMap();
		$config->put(SessionConnection::PROCESSOR_HOST, Scenario::PROCESSOR_HOST);
		$config->put("debug", true);
		//Set connection type to XML
		$config->put(SessionConnection::CONNECTION_TYPE, "xml");
		
		//Login
		$session = Session::login(Scenario::MERCHANT, Scenario::PASSWORD, $config);
		$session->setUserCode(Scenario::USER_CODE);
		
		//execute find payment plan
		$paymentPlan = $session->loadPaymentPlan(Scenario::CODE.".pp-3");
		
		//0 - is first, 1 - is second
		$charge = $paymentPlan->getCharges()->get(1);
		$charge->setIsPrepaid(true);
		
		//get customer account
		$customerAccount = $paymentPlan->getCustomerAccount();
		
		//Create payment for downpayment
		$transaction = $customerAccount->createAssetTransaction();
		//code is optinal; if you specify the value, make sure the code is unique
		$transaction->setCode(Scenario::CODE.".at-5");
		//all amounts are in cents
		$transaction->setAmount(3000);
		//Type of transaction
		$transaction->setAccountActivityType(AccountActivityType::Payment);
		$transaction->setTransactionType(AssetTransactionType::Check);
		//check number
		$transaction->setAccountNumber("10100125687");
		$transaction->setIsPrepayment(true);
		$transaction->setDueDate($charge->getBillingDate());
		$transaction->setTransactionSourceType(TransactionSourceType::POS);
		$transaction->setCreatorCode(Scenario::USER_CODE);
		$transaction->setPosterCode(Scenario::USER_CODE);
		
		//Mark object for persistence
		$session->save($customerAccount);
		//Synchronize changes with the server
		try{
			$session->synchronize();
		}
		catch(Exception $ex){
			//Be sure to properly handle exception, this is just a sample solution
			print($ex->getMessage());
			return ;
		}
		//Logout
		$session->logout();



?>

