<?php


require_once "lib/UniBill.php";
require_once "Scenario.php";

		//Set up clients' settings
		$config = new HashMap();
		$config->put(SessionConnection::PROCESSOR_HOST, Scenario::PROCESSOR_HOST);
		$config->put("debug", true);
		
		//Set connection type to XML
		$config->put(SessionConnection::CONNECTION_TYPE, "xml");
		
		//Login
		$session = Session::login(Scenario::MERCHANT, Scenario::PASSWORD, $config);
		$session->setUserCode(Scenario::USER_CODE);
		
		//Find customer account
		$customerAccount = $session->loadCustomerAccount(Scenario::CODE.".ca-2");
		//Create invoice1
		$invoice1 = $customerAccount->createRevenueTransaction();
		//code is optinal; if you specify the value, make sure the code is unique
		$invoice1->setCode(Scenario::CODE.".rt-9-1");
		//all amounts are in cents
		$invoice1->setAmount(2000);
		//Item Codes must be setup in portal prior to being used
		$invoice1->setItemCode(Scenario::ITEM_CODE2);
		//Type of transaction
		$invoice1->setAccountActivityType(AccountActivityType::Invoice);
		$invoice1->setCreatorCode(Scenario::USER_CODE);
		$invoice1->setPosterCode(Scenario::USER_CODE);
		
		//Create payment1
		$payment1 = $customerAccount->createAssetTransaction();
		//code is optinal; if you specify the value, make sure the code is unique
		$payment1->setCode(Scenario::CODE.".at-9-1");
		//all amounts are in cents
		$payment1->setAmount(2000);
		$payment1->setTransactionType(AssetTransactionType::VisaCredit);
		$payment1->setAccountActivityType(AccountActivityType::Payment);
		//Credit card number to charge
		$payment1->setAccountNumber(Scenario::CC_NUMBER);
		//Credit card expiration date
		$payment1->setAccessory(Scenario::CC_EXPIRATION_DATE);
		$payment1->setTransactionSourceType(TransactionSourceType::POS);
		$payment1->setCreatorCode(Scenario::USER_CODE);
		$payment1->setPosterCode(Scenario::USER_CODE);
		
		//Add processing specific info (for better qualification rates)
		$captureInfo1 = $payment1->getCaptureInfo();
		$captureInfo1->setHolderName("John Smith");
		$captureInfo1->setCity("Columbus");
		$captureInfo1->setState("CA");
		$captureInfo1->setStreet("233 12th Street");
		$captureInfo1->setZipCode("31909");
		$captureInfo1->setPhone("2129856472");
		$captureInfo1->setEmail("test@yahoo.com");
		$captureInfo1->setCvv2(Scenario::CC_CVV2);
		
		//Create invoice2
		$invoice2 = $customerAccount->createRevenueTransaction();
		//code is optinal; if you specify the value, make sure the code is unique
		$invoice2->setCode(Scenario::CODE.".rt-9-2");
		//all amounts are in cents
		$invoice2->setAmount(1500);
		//Item Codes must be setup in portal prior to being used
		$invoice2->setItemCode(Scenario::ITEM_CODE2);
		//Type of transaction
		$invoice2->setAccountActivityType(AccountActivityType::Invoice);
		$invoice2->setCreatorCode(Scenario::USER_CODE);
		$invoice2->setPosterCode(Scenario::USER_CODE);
		
		//Create payment2
		$payment2 = $customerAccount->createAssetTransaction();
		//code is optinal; if you specify the value, make sure the code is unique
		$payment2->setCode(Scenario::CODE.".at-9-2");
		///all amounts are in cents
		$payment2->setAmount(1500);
		$payment2->setTransactionType(AssetTransactionType::VisaCredit);
		$payment2->setAccountActivityType(AccountActivityType::Payment);
		//Credit card number to charge
		$payment2->setAccountNumber(Scenario::CC_NUMBER);
		//Credit card expiration date
		$payment2->setAccessory(Scenario::CC_EXPIRATION_DATE);
		$payment2->setTransactionSourceType(TransactionSourceType::POS);
		$payment2->setCreatorCode(Scenario::USER_CODE);
		$payment2->setPosterCode(Scenario::USER_CODE);
		
		//Add processing specific info (for better qualification rates)
		$captureInfo2 = $payment2->getCaptureInfo();
		$captureInfo2->setHolderName("John Smith");
		$captureInfo2->setCity("Columbus");
		$captureInfo2->setState("CA");
		$captureInfo2->setStreet("233 12th Street");
		$captureInfo2->setZipCode("31909");
		$captureInfo2->setPhone("2129856472");
		$captureInfo2->setEmail("test@yahoo.com");
		$captureInfo2->setCvv2(Scenario::CC_CVV2);
		
		//Mark object for persistence
		$session->save($customerAccount);
		//Synchronize changes with the server
		try{
			$session->synchronize();
		}
		catch(Exception $ex){
			//Be sure to properly handle exception, this is just a sample solution
			print($ex->getMessage());
			return ;
		}
		//Logout
		$session->logout();



?>

