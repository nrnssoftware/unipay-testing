<?php


require_once "lib/UniBill.php";
require_once "Scenario.php";

		//Set up clients' settings
		$config = new HashMap();
		$config->put(SessionConnection::PROCESSOR_HOST,Scenario::PROCESSOR_HOST);
		$config->put("debug", true);
		//Set connection type to XML
		$config->put(SessionConnection::CONNECTION_TYPE, "xml");
		
		//Login
		$session = Session::login(Scenario::MERCHANT, Scenario::PASSWORD, $config);
		$session->setUserCode(Scenario::USER_CODE);
		
		//Create customer account
		$customerAccount = $session->createCustomerAccount();
		//code is optinal; if you specify the value, make sure the code is unique
		$customerAccount->setCode(Scenario::CODE.".ca-4");
		$customerAccount->setMerchantAccountCode(Scenario::MERCHANT_ACCOUNT);
		$customerAccount->setFirstName("Mary");
		$customerAccount->setLastName("Doe");
		$customerAccount->setType(CustomerAccountType::Female);
		$customerAccount->setHomePhone("5469856876");
		$customerAccount->setEmail("test@google.com");
		$customerAccount->setStreet1("203 16th Street");
		$customerAccount->setCity("Honolulu");
		$customerAccount->setState("CA");
		$customerAccount->setZipCode("30904");
		
		//Create payment plan
		$paymentPlan = $customerAccount->createPaymentPlan();
		//code is optinal; if you specify the value, make sure the code is unique
		$paymentPlan->setCode(Scenario::CODE.".pp-4");
		//all amounts are in cents
		$paymentPlan->setAmount(3000);
		//Item Codes must be setup in portal prior to being used
		$paymentPlan->setItemCode(Scenario::ITEM_CODE1);
		$paymentPlan->setType(PaymentPlanType::Complimentary);
		//Billing Cycle codes must be setup in portal prior to being used
		$paymentPlan->setBillingCycleCode(Scenario::BILLING_CYCLE_CODE1);
		$paymentPlan->setFirstBillingDate(new DateTime(Scenario::FIRST_BILLIG_DATE1));
		$paymentPlan->setCreatorCode(Scenario::USER_CODE);
		$paymentPlan->setSellerCode(Scenario::USER_CODE);
		
		//Mark object for persistence
		$session->save($customerAccount);
		
		//Synchronize changes with the server
		try{
			$session->synchronize();
		}
		catch(Exception $ex){
			//Be sure to properly handle exception, this is just a sample solution
			print($ex->getMessage());
			return ;
		}
		//Logout
		$session->logout();



?>

