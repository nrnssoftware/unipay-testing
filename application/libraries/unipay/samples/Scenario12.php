<?php


require_once "lib/UniBill.php";
require_once "Scenario.php";

		//Set up clients' settings
		$config = new HashMap();
		$config->put(SessionConnection::PROCESSOR_HOST, Scenario::PROCESSOR_HOST);
		$config->put("debug", true);
		
		//Set connection type to XML
		$config->put(SessionConnection::CONNECTION_TYPE, "xml");
		
		//Login
		$session = Session::login(Scenario::MERCHANT, Scenario::PASSWORD, $config);
		$session->setUserCode(Scenario::USER_CODE);
		
		//Find customer account
		$customerAccount = $session->loadCustomerAccount(Scenario::CODE.".ca-2");
		
		//Create credit invoice
		$creditInvoice = $customerAccount->createCredit();
		$creditInvoice->setCode(Scenario::CODE.".rt-12");
		$creditInvoice->setAmount(1500);
		$creditInvoice->setCreatorCode(Scenario::USER_CODE);
		$creditInvoice->setPosterCode(Scenario::USER_CODE);
		
		//Create credit payment
		$creditPayment = $customerAccount->createRefund();
		$creditPayment->setCode(Scenario::CODE.".at-12");
		$creditPayment->setAmount(1500);
		$creditPayment->setAccountNumber(Scenario::CC_NUMBER);
		$creditPayment->setAccessory(Scenario::CC_EXPIRATION_DATE);
		$creditPayment->setTransactionType(AssetTransactionType::VisaCredit);
		$creditPayment->setTransactionSourceType(TransactionSourceType::POS);
		$creditPayment->setCreatorCode(Scenario::USER_CODE);
		$creditPayment->setPosterCode(Scenario::USER_CODE);
		
		//Add processing specific info (for better qualification rates)
		$captureInfo = $creditPayment->getCaptureInfo();
		$captureInfo->setHolderName("John Smith");
		$captureInfo->setCity("Columbus");
		$captureInfo->setState("CA");
		$captureInfo->setStreet("233 12th Street");
		$captureInfo->setZipCode("31909");
		$captureInfo->setPhone("2129856472");
		$captureInfo->setEmail("test@yahoo.com");
		$captureInfo->setCvv2(Scenario::CC_CVV2);
		
		//Mark object for persistence
		$session->save($customerAccount);
		//Synchronize changes with the server
		try{
			$session->synchronize();
		}
		catch(Exception $ex){
			//Be sure to properly handle exception, this is just a sample solution
			print($ex->getMessage());
			return ;
		}
		//Logout
		$session->logout();



?>

