<?php


require_once "lib/UniBill.php";
require_once "Scenario.php";

		//Set up clients' settings
		$config = new HashMap();
		$config->put(SessionConnection::PROCESSOR_HOST, Scenario::PROCESSOR_HOST);
		$config->put("debug", true);
		//Set connection type to XML
		$config->put(SessionConnection::CONNECTION_TYPE, "xml");
		$session = Session::login(Scenario::MERCHANT, Scenario::PASSWORD, $config);
		$session->setUserCode(Scenario::USER_CODE);
		
		//Find customer account
		$customerAccount = $session->loadCustomerAccount(Scenario::CODE.".ca-1");
		
		//Create payment plan
		$paymentPlan = $customerAccount->createPaymentPlan();
		//code is optinal; if you specify the value, make sure the code is unique
		$paymentPlan->setCode(Scenario::CODE.".pp-3");
		//all amounts are in cents
		$paymentPlan->setAmount(3000);
		//Item Codes must be setup in portal prior to being used
		$paymentPlan->setItemCode(Scenario::ITEM_CODE1);
		//Billing Cycle codes must be setup in portal prior to being used
		$paymentPlan->setBillingCycleCode(Scenario::BILLING_CYCLE_CODE1);
		$paymentPlan->setType(PaymentPlanType::Fixed);
		$paymentPlan->setFirstBillingDate(new DateTime(Scenario::FIRST_BILLIG_DATE1));
		$paymentPlan->add(0, 6, false);
		$paymentPlan->setCreatorCode(Scenario::USER_CODE);
		$paymentPlan->setSellerCode(Scenario::USER_CODE);
		
		//Create child payment plan
		//all amounts are in cents
		$paymentPlan->setRenewalAmount(2000);
		
		//Mark object for persistence
		$session->save($customerAccount);
		//Synchronize changes with the server
		try{
			$session->synchronize();
		}
		catch(Exception $ex){
			//Be sure to properly handle exception, this is just a sample solution
			print($ex->getMessage());
			return ;
		}
		//logout
		$session->logout();



?>

