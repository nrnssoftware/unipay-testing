<?php


require_once "lib/UniBill.php";
require_once "Scenario.php";

		//Set up clients' settings
		$config = new HashMap();
		$config->put(SessionConnection::PROCESSOR_HOST, Scenario::PROCESSOR_HOST);
		$config->put("debug", true);
		
		//Set connection type to XML
		$config->put(SessionConnection::CONNECTION_TYPE, "xml");
		
		//Login
		$session = Session::login(Scenario::MERCHANT, Scenario::PASSWORD, $config);
		$session->setUserCode(Scenario::USER_CODE);
		
		//Find customer account
		$customerAccount = $session->loadCustomerAccount(Scenario::CODE.".ca-2");
		
		//Create new payment option
		$paymentOption = $customerAccount->createPaymentOption();
		//code is optinal; if you specify the value, make sure the code is unique
		$paymentOption->setCode(Scenario::CODE.".po-13");
		$paymentOption->setHolderName("Jane Doe");
		$paymentOption->setNumber(Scenario::CC_NUMBER);
		//expiration date
		$paymentOption->setAccessory(Scenario::CC_EXPIRATION_DATE);
		$paymentOption->setStreet1("233 12th Street");
		$paymentOption->setCity("Columbus");
		$paymentOption->setState("CA");
		$paymentOption->setZipCode("31904");
		
		//Find payment plans by customer account and payment option codes
		$parameters = new HashMap();
		$parameters->put("customerAccountCode", Scenario::CODE.".ca-2");
		$parameters->put("paymentOptionCode", Scenario::CODE.".po-2");
		
		//Take first account in the list
		$list = $session->queryPaymentPlan($parameters);
		//Nothing found; no processing is necessary
		if (is_null($list)){
			return ;
		}
		//Deactivate old payment option (needs to be done only once)
		$oldPaymentOption = $list->get(0)->getPaymentOption();
		
		//Load payment option from server
		$session->loadObject($oldPaymentOption);
		$oldPaymentOption->setIsActive(false);
		//Mark object for persistence
		
		//Make sure we save deactivation
		$session->save($oldPaymentOption);
		foreach ($list as $clientObject){
			$paymentPlan = $clientObject;
			$paymentPlan->setPaymentOption($paymentOption);
			$session->save($paymentPlan);
		}
		
		//Synchronize changes with the server
		try{
			$session->synchronize();
		}
		catch(Exception $ex){
			//Be sure to properly handle exception, this is just a sample solution
			print($ex->getMessage());
			return ;
		}
		//logout
		$session->logout();



?>

