<?php


require_once "lib/UniBill.php";
require_once "Scenario.php";

		//Set up clients' settings
		$config = new HashMap();
		$config->put(SessionConnection::PROCESSOR_HOST, Scenario::PROCESSOR_HOST);
		$config->put("debug", true);
		
		//Set connection type to XML
		$config->put(SessionConnection::CONNECTION_TYPE, "xml");
		
		//Login
		$session = Session::login(Scenario::MERCHANT, Scenario::PASSWORD, $config);
		$session->setUserCode(Scenario::USER_CODE);
		
		//Find payment plan
		$paymentPlan = $session->loadPaymentPlan(Scenario::CODE.".pp-4");
		
		//Execute delete all charges
		$paymentPlan->setTerminatorCode(Scenario::USER_CODE);
		$paymentPlan->cancel();
		
		//Mark object for persistence
		$session->save($paymentPlan);
		//Synchronize changes with the server
		try{
			$session->synchronize();
		}
		catch(Exception $ex){
			//Be sure to properly handle exception, this is just a sample solution
			print($ex->getMessage());
			return ;
		}
		//Logout
		$session->logout();



?>

