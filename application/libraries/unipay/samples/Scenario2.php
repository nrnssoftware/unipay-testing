<?php


require_once "lib/UniBill.php";
require_once "Scenario.php";

		//Set up clients' settings
		$config = new HashMap();
		$config->put(SessionConnection::PROCESSOR_HOST, Scenario::PROCESSOR_HOST);
		$config->put("debug", true);
		//Set connection type to XML
		$config->put(SessionConnection::CONNECTION_TYPE, "xml");
		
		//Login
		$session = Session::login(Scenario::MERCHANT, Scenario::PASSWORD, $config);
		$session->setUserCode(Scenario::USER_CODE);
		
		//Create customer account
		$customerAccount = $session->createCustomerAccount();
		//code is optinal; if you specify the value, make sure the code is unique
		$customerAccount->setCode(Scenario::CODE.".ca-2");
		$customerAccount->setMerchantAccountCode(Scenario::MERCHANT_ACCOUNT);
		$customerAccount->setFirstName("John");
		$customerAccount->setLastName("Smith");
		$customerAccount->setType(CustomerAccountType::Male);
		$customerAccount->setHomePhone("2129856472");
		$customerAccount->setEmail("test@yahoo.com");
		$customerAccount->setStreet1("233 12th Street");
		$customerAccount->setCity("Columbus");
		$customerAccount->setState("NY");
		$customerAccount->setZipCode("31909");
		
		//Create payment option
		$paymentOption = $customerAccount->createPaymentOption();
		//code is optinal; if you specify the value, make sure the code is unique
		$paymentOption->setCode(Scenario::CODE.".po-2");
		$paymentOption->setHolderName("John Smith");
		$paymentOption->setNumber(Scenario::BA_ACCOUNT_NUMBER);
		//routing number
		$paymentOption->setAccessory(Scenario::BA_ROUTING_NUMBER);
		$paymentOption->setType(PaymentOptionType::Checking);
		$paymentOption->setStreet1("233 12th Street");
		$paymentOption->setCity("Columbus");
		$paymentOption->setState("CA");
		$paymentOption->setZipCode("31909");
		
		//Create payment plan
		$paymentPlan = $customerAccount->createPaymentPlan();
		//code is optinal; if you specify the value, make sure the code is unique
		$paymentPlan->setCode(Scenario::CODE.".pp-2");
		//all amounts are in cents
		$paymentPlan->setAmount(1000);
		//Item Codes must be setup in portal prior to being used
		$paymentPlan->setItemCode(Scenario::ITEM_CODE1);
		$paymentPlan->setType(PaymentPlanType::Perpetual);
		//Billing Cycle codes must be setup in portal prior to being used
		$paymentPlan->setBillingCycleCode(Scenario::BILLING_CYCLE_CODE2);
		$paymentPlan->setFirstBillingDate(new DateTime(Scenario::FIRST_BILLIG_DATE2));
		$paymentPlan->setPaymentOption($paymentOption);
		$paymentPlan->setCreatorCode(Scenario::USER_CODE);
		$paymentPlan->setSellerCode(Scenario::USER_CODE);
		
		//Create invoice for downpayment
		$revenueTransaction = $customerAccount->createRevenueTransaction();
		//code is optinal; if you specify the value, make sure the code is unique
		$revenueTransaction->setCode(Scenario::CODE.".rt-2");
		//ll amounts are in cents
		$revenueTransaction->setAmount(5000);
		//Item Codes must be setup in portal prior to being used
		$revenueTransaction->setItemCode(Scenario::ITEM_CODE1);
		//Type of transaction
		$revenueTransaction->setAccountActivityType(AccountActivityType::Invoice);
		$revenueTransaction->setCreatorCode(Scenario::USER_CODE);
		$revenueTransaction->setPosterCode(Scenario::USER_CODE);
		
		//Create payment for downpayment
		$transaction = $customerAccount->createAssetTransaction();
		//code is optinal; if you specify the value, make sure the code is unique
		$transaction->setCode(Scenario::CODE.".at-2");
		//all amounts are in cents
		$transaction->setAmount(5000);
		$transaction->setTransactionType(AssetTransactionType::VisaCredit);
		//Credit card number to charge
		$transaction->setAccountNumber(Scenario::CC_NUMBER);
		//Credit card expiration date
		$transaction->setAccessory(Scenario::CC_EXPIRATION_DATE);
		//Type of transaction
		$transaction->setAccountActivityType(AccountActivityType::Payment);
		$transaction->setTransactionSourceType(TransactionSourceType::POS);
		$transaction->setCreatorCode(Scenario::USER_CODE);
		$transaction->setPosterCode(Scenario::USER_CODE);
		
		//Add processing specific info (for better qualification rates
		$captureInfo = $transaction->getCaptureInfo();
		$captureInfo->setHolderName("John Smith");
		$captureInfo->setCity("Columbus");
		$captureInfo->setState("CA");
		$captureInfo->setStreet("233 12th Street");
		$captureInfo->setZipCode("31909");
		$captureInfo->setPhone("2129856472");
		$captureInfo->setEmail("test@yahoo.com");
		$captureInfo->setCvv2(Scenario::CC_CVV2);
		
		//Mark object for persistence
		$session->save($customerAccount);
		//Synchronize changes with the server
		try{
			$session->synchronize();
		}
		catch(Exception $ex){
			//Be sure to properly handle exception, this is just a sample solution
			print($ex->getMessage());
			return ;
		}
		//Get info from customer account
		print($customerAccount->getCode());
		print($customerAccount->getFirstName());
		print($customerAccount->getLastName());
		print($customerAccount->getCreateDate()->format('m/d/Y'));
		//Get info from payment option
		print($paymentOption->getCode());
		print($paymentOption->getAccessory());
		print($paymentOption->getNumber());
		print($paymentOption->getCreateDate()->format('m/d/Y'));
		//Get info from payment plan
		print($paymentPlan->getCode());
		print($paymentPlan->getFirstBillingDate()->format('m/d/Y'));
		print($paymentPlan->getBillingCycleCode());
		print($paymentPlan->getCreateDate()->format('m/d/Y'));
		//Get info from revenue transaction
		print($revenueTransaction->getCode());
		print($revenueTransaction->getAmount());
		print($revenueTransaction->getAccountActivityType());
		print($revenueTransaction->getCreateDate()->format('m/d/Y'));
		//Get info from asset transaction
		print($transaction->getCode());
		print($transaction->getAccessory());
		print($transaction->getAccountNumber());
		print($transaction->getCreateDate()->format('m/d/Y'));
		//Get info from capture info
		print($captureInfo->getReturnType());
		print($captureInfo->getApprovalCode());
		print($captureInfo->getReferenceNumber());
		//logout
		$session->logout();



?>

