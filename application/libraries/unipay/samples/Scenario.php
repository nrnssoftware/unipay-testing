<?php

/*
 * Configure values to fit your needs
 */

date_default_timezone_set('America/Los_Angeles');

class Scenario {

	const CODE = 'your_sample_code';

    const PROCESSOR_HOST = "https://sandbox.unipaygateway.com/ibilling/xmlhttps";
        
    const USER_CODE = '22000';
    const PASSWORD = 'NrnWare789#';
        
	const MERCHANT = 22000;
	const MERCHANT_ACCOUNT = 22001;

	const BILLING_CYCLE_CODE1 = 'M14';
	const FIRST_BILLIG_DATE1 = '2014-02-14';
	
	const BILLING_CYCLE_CODE2 = 'M28';
	const FIRST_BILLIG_DATE2 = '2014-02-28';
	
	const BILLING_CYCLE_CODE3 = 'W01';
	const FIRST_BILLIG_DATE3 = '2014-02-18';
	
	const ITEM_CODE1 = 'Membership';
	const ITEM_CODE2 = 'TShirt';
	const ITEM_CODE3 = 'NutritionPack';
	
	const CC_EXPIRATION_DATE = '0917';
	
	const CC_NUMBER = '4111111111111111';
	const CC_NUMBER_LAST4 = '1111';
	const CC_CVV2 = '999';
		
	const BA_ACCOUNT_NUMBER = '1000121279381';
	const BA_ROUTING_NUMBER = '610000227';
	
}
?>
