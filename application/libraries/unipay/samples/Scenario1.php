<?php
require_once "lib/UniBill.php";
require_once "Scenario.php";

//Set up clients' settings
$config = new HashMap();
$config->put(SessionConnection::PROCESSOR_HOST, Scenario::PROCESSOR_HOST);

//Amount to be used for test transactions that are processed to validate new payment options; optional
//Set the value to true to view XML exchanged between your client and the server
$config->put(SessionConnection::DEBUG, true);

//Set connection type to XML
$config->put(SessionConnection::CONNECTION_TYPE, "xml");

//Login
$session = Session::login(Scenario::MERCHANT, Scenario::PASSWORD, $config);
$session->setUserCode(Scenario::USER_CODE);

//Create customer account
$customerAccount = $session->createCustomerAccount();
//code is optinal; if you specify the value, make sure the code is unique
$customerAccount->setCode(Scenario::CODE.".ca-1");
$customerAccount->setMerchantAccountCode(Scenario::MERCHANT_ACCOUNT);
$customerAccount->setFirstName("John");
$customerAccount->setLastName("Smith");
$customerAccount->setType(CustomerAccountType::Male);
$customerAccount->setHomePhone("2129856472");
$customerAccount->setEmail("test@yahoo.com");
$customerAccount->setStreet1("233 12th Street");
$customerAccount->setCity("Honolulu");
$customerAccount->setState("CA");
$customerAccount->setZipCode("31904");

//Create payment option
$paymentOption = $customerAccount->createPaymentOption();
//code is optinal; if you specify the value, make sure the code is unique
$paymentOption->setCode(Scenario::CODE.".po-1");
$paymentOption->setHolderName("John Smith");
$paymentOption->setNumber(Scenario::CC_NUMBER);
//expiration date
$paymentOption->setAccessory(Scenario::CC_EXPIRATION_DATE);
$paymentOption->setStreet1("233 12th Street");
$paymentOption->setCity("Honolulu");
$paymentOption->setState("CA");
$paymentOption->setZipCode("31904");

//Create payment plan
$paymentPlan = $customerAccount->createPaymentPlan();
//code is optinal; if you specify the value, make sure the code is unique
$paymentPlan->setCode(Scenario::CODE.".pp-1");
//all amounts are in cents
$paymentPlan->setAmount(5000);
//Item Codes must be setup in portal prior to being used
$paymentPlan->setItemCode(Scenario::ITEM_CODE1);
$paymentPlan->setType(PaymentPlanType::Fixed);
//Billing Cycle codes must be setup in portal prior to being used
$paymentPlan->setBillingCycleCode(Scenario::BILLING_CYCLE_CODE1);
$paymentPlan->setFirstBillingDate(new DateTime(Scenario::FIRST_BILLIG_DATE1));
$paymentPlan->add(0, 12, false);
$paymentPlan->setCreatorCode(Scenario::USER_CODE);
$paymentPlan->setSellerCode(Scenario::USER_CODE);

// Create invoice for downpayment
$revenueTransaction = $customerAccount->createRevenueTransaction();
//code is optinal; if you specify the value, make sure the code is unique
$revenueTransaction->setCode(Scenario::CODE.".rt-1");
//all amounts are in cents
$revenueTransaction->setAmount(10000);
//Item Codes must be setup in portal prior to being used
$revenueTransaction->setItemCode(Scenario::ITEM_CODE1);
//Type of transaction
$revenueTransaction->setAccountActivityType(AccountActivityType::Invoice);
$revenueTransaction->setCreatorCode(Scenario::USER_CODE);
$revenueTransaction->setPosterCode(Scenario::USER_CODE);

//Create payment for downpayment
$transaction = $customerAccount->createAssetTransaction();
//code is optinal; if you specify the value, make sure the code is unique
$transaction->setCode(Scenario::CODE.".at-1");
//all amounts are in cents
$transaction->setAmount(10000);
$transaction->setTransactionType(AssetTransactionType::Cash);
$transaction->setAccountActivityType(AccountActivityType::Payment);
$transaction->setTransactionSourceType(TransactionSourceType::POS);
$transaction->setCreatorCode(Scenario::USER_CODE);
$transaction->setPosterCode(Scenario::USER_CODE);

//Mark object for persistence
$session->save($customerAccount);
//Synchronize changes with the server
try{
	$session->synchronize();
}
catch(Exception $ex){
	//Be sure to properly handle exception, this is just a sample solution
	print($ex->getMessage());
	return ;
}

//Get info from customer account
print($customerAccount->getCode());
print($customerAccount->getFirstName());
print($customerAccount->getLastName());
//Get info from payment option
print($paymentOption->getCode());
print($paymentOption->getAccessory());
print($paymentOption->getNumber());
//Get info from payment plan
print($paymentPlan->getCode());
print($paymentPlan->getFirstBillingDate()->format('m/d/Y'));
print($paymentPlan->getBillingCycleCode());
//Get info from revenue transaction
print($revenueTransaction->getCode());
print($revenueTransaction->getAmount());
print($revenueTransaction->getAccountActivityType());
//Get info from asset transaction
print($transaction->getCode());
print($transaction->getAmount());
print($transaction->getAccountActivityType());
//logout
$session->logout();

?>
