<?php


require_once "lib/UniBill.php";
require_once "Scenario.php";

		//Set up clients' settings
		$config = new HashMap();
		$config->put(SessionConnection::PROCESSOR_HOST, Scenario::PROCESSOR_HOST);
		$config->put("debug", true);
		
		//Set connection type to XML
		$config->put(SessionConnection::CONNECTION_TYPE, "xml");
		
		//Login
		$session = Session::login(Scenario::MERCHANT, Scenario::PASSWORD, $config);
		$session->setUserCode(Scenario::USER_CODE);
		
		//Find customer account
		$customerAccount = $session->loadCustomerAccount(Scenario::CODE.".ca-1");
		
		//Create invoice for downpayment
		$revenueTransaction = $customerAccount->createRevenueTransaction();
		//code is optinal; if you specify the value, make sure the code is unique
		$revenueTransaction->setCode(Scenario::CODE.".rt-10");
		//all amounts are in cents
		$revenueTransaction->setAmount(5000);
		//Item Codes must be setup in portal prior to being used
		$revenueTransaction->setItemCode(Scenario::ITEM_CODE3);
		//Type of transaction
		$revenueTransaction->setAccountActivityType(AccountActivityType::Invoice);
		$revenueTransaction->setCreatorCode(Scenario::USER_CODE);
		$revenueTransaction->setPosterCode(Scenario::USER_CODE);
		
		//Create payment for downpayment
		$transaction = $customerAccount->createAssetTransaction();
		//code is optinal; if you specify the value, make sure the code is unique
		$transaction->setCode(Scenario::CODE.".at-10");
		//all amounts are in cents
		$transaction->setAmount(5000);
		$transaction->setTransactionType(AssetTransactionType::Cash);
		$transaction->setAccountActivityType(AccountActivityType::Payment);
		$transaction->setTransactionSourceType(TransactionSourceType::POS);
		$transaction->setCreatorCode(Scenario::USER_CODE);
		$transaction->setPosterCode(Scenario::USER_CODE);
		
		//Mark object for persistence
		$session->save($customerAccount);
		//Synchronize changes with the server
		try{
			$session->synchronize();
		}
		catch(Exception $ex){
			//Be sure to properly handle exception, this is just a sample solution
			print($ex->getMessage());
			return ;
		}
		//logout
		$session->logout();



?>

