<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('unipay/unibill');
		$this->load->library('session');
		$this->load->helper('url');
	}

	public function index() {
		// List the customers for this merchant
		$data['customers'] = $this->unibill->list_all_customers(FALSE);
		$data['page_scripts'] = array('/r/load.php?index.js');
		$this->defaultLayout('customers/list',$data);
	}

	public function create() {
		$this->load->helper('form');
		$data['customer'] = array( // Blank record
			'code' => '',
			'title' => 'Mr',
			'gender' => CustomerAccountType::Male,
			'first_name' => '',
			'middle_name' => '',
			'last_name' => '',
			'suffix' => '',
			'date_of_birth' => '',
			'license' => '',
			'license_state' => '',
			'street1' => '',
			'street2' => '',
			'city' => '',
			'state' => '',
			'postcode' => '',
			'phone_home' => '',
			'phone_work' => '',
			'phone_mobile' => '',
			'email' => ''
		);
		$data['page_scripts'] = array('/r/load.php?customers/create.js','/r/js/bootstrap-datepicker.js');
		$data['page_css'] = array('/r/css/datepicker.css');
		$this->defaultLayout('customers/create',$data);
	}

	public function update() {
		// Take the form data and insert it into UniPay
		$this->load->library('form_validation');
		$this->form_validation->set_rules('code', 'Code', '');
		$this->form_validation->set_rules('title', 'Title', '');
		$this->form_validation->set_rules('gender', 'Gender', '');
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('middle_name', 'Middle Name', '');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('suffix', 'Suffix', '');
		$this->form_validation->set_rules('date_of_birth', 'Birth Date', '');
		$this->form_validation->set_rules('license', 'Drivers License', '');
		$this->form_validation->set_rules('license_state', 'Drivers License', '');
		$this->form_validation->set_rules('street1', 'Street', 'required');
		$this->form_validation->set_rules('street2', 'Street', '');
		$this->form_validation->set_rules('city', 'City', 'required');
		$this->form_validation->set_rules('state', 'State', '');
		$this->form_validation->set_rules('postcode', 'Postcode', 'required');
		$this->form_validation->set_rules('phone_home', 'Phone Home', 'required');
		$this->form_validation->set_rules('phone_work', 'Phone Work', '');
		$this->form_validation->set_rules('phone_mobile', 'Phone Mobile', '');
		$this->form_validation->set_rules('email', 'Email', 'required');
		// Validate the form, send it back if everything is wrong
		$data['customer'] = array(
			'title' => $this->input->post('title'),
			'gender' => $this->input->post('gender'),
			'first_name' => $this->input->post('first_name'),
			'middle_name' => $this->input->post('middle_name'),
			'last_name' => $this->input->post('last_name'),
			'suffix' => $this->input->post('suffix'),
			'date_of_birth' => new DateTime($this->input->post('date_of_birth')),
			'license' => $this->input->post('license'),
			'license_state' => $this->input->post('license_state'),
			'street1' => $this->input->post('street1'),
			'street2' => $this->input->post('street2'),
			'city' => $this->input->post('city'),
			'state' => $this->input->post('state'),
			'postcode' => $this->input->post('postcode'),
			'phone_home' => $this->input->post('phone_home'),
			'phone_work' => $this->input->post('phone_work'),
			'phone_mobile' => $this->input->post('phone_mobile'),
			'email' => $this->input->post('email'),
			'code' => $this->input->post('code')
		);
		if($this->form_validation->run()) {
			// Create the customer, redirect to the customer list again
			if($this->input->post('code') != '') { // Update
				$this->unibill->update_customer($this->input->post('code'), $data['customer']);
				$this->session->set_flashdata('alert_message','Customer has been updated successfully.');
			} else { // Insert
				$this->unibill->create_customer($data['customer']);
				$this->session->set_flashdata('alert_message','Customer has been added successfully.');
			}
			redirect('/customers');
		} else {
			// Validation failed, go show the form again
			$data['page_scripts'] = array('/r/load.php?index.js');
			$this->defaultLayout('customers/create',$data);
		}
	}

	public function edit($code) {
		$this->load->helper('form');
		// Fetch the customer and send it on!
		$customer = $this->unibill->fetch_customer($code);
		$data['customer'] = array(
			'code' => $customer->getCode(),
			'title' => $customer->getTitle(),
			'gender' => $customer->getType(),
			'first_name' => $customer->getFirstName(),
			'middle_name' => $customer->getMiddleName(),
			'last_name' => $customer->getLastName(),
			'suffix' => $customer->getSuffix(),
			'date_of_birth' => ($customer->getBirthDate() == null)?'':$customer->getBirthDate()->format('m/d/Y'),
			'license' => '',
			'license_state' => '',
			'street1' => $customer->getStreet1(),
			'street2' => $customer->getStreet2(),
			'city' => $customer->getCity(),
			'state' => $customer->getState(),
			'postcode' => $customer->getZipCode(),
			'phone_home' => $customer->getHomePhone(),
			'phone_work' => $customer->getWorkPhone(),
			'phone_mobile' => $customer->getCellPhone(),
			'email' => $customer->getEmail(),
			'payment_plan_count' => $customer->getPaymentPlans()->size(),
			'payment_plans' => $customer->getPaymentPlans()->toArray()
		);
		$data['page_scripts'] = array('/r/load.php?customers/create.js','/r/js/bootstrap-datepicker.js');
		$data['page_css'] = array('/r/css/datepicker.css');
		$this->defaultLayout('customers/create',$data);
	}

	public function activate($code) {
		$this->unibill->set_active($code,TRUE);
		$this->session->set_flashdata('alert_message','Customer has been activated.');
		redirect('/customers');
	}

	public function deactivate($code) {
		$this->unibill->set_active($code,FALSE);
		$this->session->set_flashdata('alert_message','Customer has been removed.');
		redirect('/customers');
	}
}

/* End of file base.php */
/* Location: ./application/controllers/base.php */
