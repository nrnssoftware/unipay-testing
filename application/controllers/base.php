<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base extends MY_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->config('unipay');
		$this->load->library('unipay/unicore');
//		var_dump($this->unicore->merchants->load($this->config->item('api_merchant')));
		$data['page_scripts'] = array('/r/load.php?index.js');
		$data['merchant_number'] = $this->config->item('api_merchant');
		$data['merchant_name'] = 'NRNS Software';
		$data['customer_count'] = 100;
		$this->defaultLayout('index',$data);
	}
}

/* End of file base.php */
/* Location: ./application/controllers/base.php */