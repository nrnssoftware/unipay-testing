<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar_Events extends MY_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$this->load->database();
		$this->output->set_content_type('application/json');
		// Throw out JSON data to the calendar
		// We take two parameters from the GET string, from and to
		$from = $this->input->get('from') / 1000;
		$to = $this->input->get('to') / 1000;
		// Now do the query against the dataset
		$rows = $this->db->query('SELECT * FROM customer_schedule')->result();
		$out = array();
		foreach($rows as $row) {
			$out[] = array(
				'id' => $row->id,
				'title' => $row->schedule_title,
				'url' => 'http://www.slashdot.org',
				'class' => 'event-important',
				'start' => strtotime($row->schedule_start).'000',
				'end' => strtotime($row->schedule_end).'000'
			);
		}
		echo json_encode(array('success' => 1, 'result' => $out));
//		echo '{"success": 0, "error": "Error message"}';
		exit();
	}
}

/* End of file calendar_events.php */
/* Location: ./application/controllers/calendar_events.php */