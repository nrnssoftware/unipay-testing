<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Four_oh_four extends MY_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$data['page_scripts'] = array('/r/load.php?index.js');
		$this->defaultLayout('four_oh_four',$data);
	}
}

/* End of file four_oh_four.php */
/* Location: ./application/controllers/four_oh_four.php */