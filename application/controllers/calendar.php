<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar extends MY_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$data['page_css'] = array('/r/css/calendar.min.css');
		$data['page_scripts'] = array('/r/js/calendar.min.js','/r/js/underscore-min.js','/r/load.php?calendar.js');
		$this->defaultLayout('calendar',$data);
	}
}

/* End of file calendar.php */
/* Location: ./application/controllers/calendar.php */