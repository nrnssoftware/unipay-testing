<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct() {
		parent::__construct();
	}

	protected function defaultLayout($view = 'index', $data = array()) {
		$data = $this->baseLayoutData($data);
		$data['header'] = isset($data['header'])?$data['header']:$this->load->view('layouts/default/header',$data,TRUE);
		$data['footer'] = isset($data['footer'])?$data['footer']:$this->load->view('layouts/default/footer',$data,TRUE);
		$data['article'] = $this->load->view($view,$data,TRUE);
		$this->load->view('layouts/default/index',$data);
	}

	private function baseLayoutData($data = array()) {
		$data['title'] = isset($data['title'])?$data['title']:'UniPay Testing';
		$data['page_css'] = isset($data['page_css'])?$data['page_css']:array();
		$data['page_scripts'] = isset($data['page_scripts'])?$data['page_scripts']:array();
		return $data;
	}
}

/* End of file base.php */
/* Location: ./application/controllers/base.php */