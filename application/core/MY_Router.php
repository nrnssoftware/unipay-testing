<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Extended the core Router class to allow for a default controller/method in the sub-folder in the controllers directory.
 * There's 5 additional lines in _validate_request, apart from that, the function is a direct copy of the one in routes.php
 */

Class MY_Router extends CI_Router {
	function __construct() {
		parent::__construct();
	}

	function _validate_request($segments) {
		if (count($segments) == 0) {
			return $segments;
		}

		// Does the requested controller exist in the root folder?
		if (file_exists(APPPATH.'controllers/'.$segments[0].'.php')) {
			return $segments;
		}

		// Is the controller in a sub-folder?
		if (is_dir(APPPATH.'controllers/'.$segments[0])) {
			// Set the directory and remove it from the segment array
			$this->set_directory($segments[0]);
			$segments = array_slice($segments, 1);

			if (count($segments) > 0) {
				// Does the requested controller exist in the sub-folder?
				if ( ! file_exists(APPPATH.'controllers/'.$this->fetch_directory().$segments[0].'.php')) {
					/*
					 * ******** START CHANGE ********
					 * This is the adjustment to the original _validate_request from router.php in the system folder
					 * At this stage we've figured out that the $segment[0] in the current directory doesn't exist
					 * but instead of the default behaviour of CI to throw a 404, we look to see if there's a default
					 * controller in the directory. We can safely assume that the last part of the segment contains
					 * the method we want to call in the base controller, so set the class and method and also
					 * return the two as an array.
					 */
					// Default controller exists?
					if (file_exists(APPPATH.'controllers/'.$this->fetch_directory().$this->default_controller.'.php')) {
						// Yep, set the class and method
						$this->set_class($this->default_controller);
						$this->set_method($segments[0]);
						// Send it back as an array as well...
						return array($this->default_controller,$segments[0]);
					}
					/* ******** END CHANGE (with the exception of the elseif below) ******** */
					elseif ( ! empty($this->routes['404_override'])) {
						$x = explode('/', $this->routes['404_override']);

						$this->set_directory('');
						$this->set_class($x[0]);
						$this->set_method(isset($x[1]) ? $x[1] : 'index');

						return $x;
					} else {
						show_404($this->fetch_directory().$segments[0]);
					}
				}
			} else {
				// Is the method being specified in the route?
				if (strpos($this->default_controller, '/') !== FALSE) {
					$x = explode('/', $this->default_controller);

					$this->set_class($x[0]);
					$this->set_method($x[1]);
				} else {
					$this->set_class($this->default_controller);
					$this->set_method('index');
				}

				// Does the default controller exist in the sub-folder?
				if ( ! file_exists(APPPATH.'controllers/'.$this->fetch_directory().$this->default_controller.'.php')) {
					$this->directory = '';
					return array();
				}
			}
			return $segments;
		}

		// If we've gotten this far it means that the URI does not correlate to a valid
		// controller class.  We will now see if there is an override
		if ( ! empty($this->routes['404_override'])) {
			$x = explode('/', $this->routes['404_override']);

			$this->set_class($x[0]);
			$this->set_method(isset($x[1]) ? $x[1] : 'index');

			return $x;
		}


		// Nothing else to do at this point but show a 404
		show_404($segments[0]);
	}

}
/* End of file MY_Router.php */
/* Location: ./application/core/MY_Router.php */