<div class="panel panel-default">
	<div class="panel-body">
		<div id="calendar"></div>
	</div>
</div>
<div class="modal hide fade" id="events-modal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Event</h3>
	</div>
	<div class="modal-body" style="height: 400px">
	</div>
	<div class="modal-footer">
		<a href="#" data-dismiss="modal" class="btn">Close</a>
	</div>
</div>