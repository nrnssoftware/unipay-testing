<div class="panel panel-default">
	<div class="panel-body">
<?php
if($this->session->flashdata('alert_message') != '') {
?>
		<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> <?php echo $this->session->flashdata('alert_message'); ?></div>
<?php
}
?>
		<table class="table">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th class="text-center">Active</th>
					<th class="text-right">Balance</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
<?php
foreach($customers->toArray() as $customer) {
	echo '<tr>';
	echo '<td>'.$customer->getCode().'</td>';
	echo '<td>'.$customer->getName().'</td>';
	echo '<td class="text-center"><span class="glyphicon glyphicon-'.($customer->getIsActive() == 1?'ok':'remove').'"></span></td>';
	echo '<td class="text-right">$'.money_format('%i',$customer->getBalance() / 100).'</td>';
	echo '<td>';
	echo '<a href="/customers/edit/'.$customer->getCode().'" class="btn btn-info btn-xs" title="Edit this customer"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;';
	if($customer->getIsActive() == '1')
		echo '<a href="/customers/deactivate/'.$customer->getCode().'" class="btn btn-danger btn-xs" title="Delete this customer"><span class="glyphicon glyphicon-remove"></span></a>';
	else
		echo '<a href="/customers/activate/'.$customer->getCode().'" class="btn btn-success btn-xs" title="Activate this customer"><span class="glyphicon glyphicon-plus"></span></a>';
	echo '</td>';
	echo '</tr>';
}
?>
			</tbody>
		</table>
		<a href="/customers/create" class="btn btn-info btn-xs" title="New customer"><span class="glyphicon glyphicon-plus"></span> Add a new customer</a>
	</div>
</div>
