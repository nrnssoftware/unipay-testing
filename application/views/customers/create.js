function main() {
	$('#date_of_birth').datepicker({
		viewMode: 'years'
	});

	$('#add_plan').click(function() {
		// Show the modal for adding a payment plan to the existing customer
		$('#new_plan').modal();
	});

	$('select').chosen();
}