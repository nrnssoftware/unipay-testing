<div class="panel panel-default">
	<div class="panel-body">
<?php echo validation_errors(); ?>
<?php echo form_open('/customers/update', array('role' => 'form', 'class' => 'form-horizontal')); ?>
		<div class="form-group">
<?php echo form_label('First','first_name',array('class' => 'col-xs-2 control-label')); ?>
			<div class="col-xs-4">
<?php echo form_input(array('name' => 'first_name', 'id' => 'first_name', 'placeholder' => 'First Name', 'class' => 'form-control', 'maxlength' => '32', 'value' => set_value('first_name',$customer['first_name']))); ?>
			</div>
<?php echo form_label('Middle','middle_name',array('class' => 'col-xs-1 control-label')); ?>
			<div class="col-xs-5">
<?php echo form_input(array('name' => 'middle_name', 'id' => 'middle_name', 'placeholder' => 'Middle Name', 'class' => 'form-control', 'maxlength' => '32', 'value' => set_value('middle_name',$customer['middle_name']))); ?>
			</div>
		</div>
		<div class="form-group">
<?php echo form_label('Last','last_name',array('class' => 'col-xs-2 control-label')); ?>
			<div class="col-xs-4">
<?php echo form_input(array('name' => 'last_name', 'id' => 'last_name', 'placeholder' => 'Last Name', 'class' => 'form-control', 'maxlength' => '32', 'value' => set_value('last_name',$customer['last_name']))); ?>
			</div>
<?php echo form_label('Suffix','suffix',array('class' => 'col-xs-1 control-label')); ?>
			<div class="col-xs-2">
<?php echo form_dropdown('suffix', array('' => '', 'Jr' => 'Jr', 'Sr' => 'Sr', 'I' => 'I', 'II' => 'II', 'III' => 'III', 'MD' => 'MD', 'PHD' => 'PHD'), set_value('suffix',$customer['suffix']), 'id="suffix" class="form-control"'); ?>
			</div>
<?php echo form_label('Title','title',array('class' => 'col-xs-1 control-label')); ?>
			<div class="col-xs-2">
<?php echo form_dropdown('title', array('' => '', 'Dr' => 'Dr','Mr' => 'Mr','Mrs' => 'Mrs', 'Ms' => 'Ms'), set_value('title',$customer['title']), 'id="title" class="form-control"'); ?>
			</div>
		</div>
		<div class="form-group">
<?php echo form_label('Birth Date','date_of_birth',array('class' => 'col-xs-2 control-label')); ?>
			<div class="col-xs-2">
<?php echo form_input(array('name' => 'date_of_birth', 'id' => 'date_of_birth', 'placeholder' => 'Birth Date', 'class' => 'form-control', 'maxlength' => '32', 'value' => set_value('date_of_birth',$customer['date_of_birth']))); ?>
			</div>
			<div class="col-xs-1"></div>
<?php echo form_label('Gender','gender',array('class' => 'col-xs-2 control-label')); ?>
			<label class='col-xs-1 control-label'>
<?php echo form_radio('gender', 'M', (set_value('gender',$customer['gender']) == 'M')); ?> Male
			</label>
			<label class='col-xs-2 control-label'>
<?php echo form_radio('gender', 'F', (set_value('gender',$customer['gender']) == 'F')); ?> Female
			</label>
		</div>
		<div class="form-group">
<?php echo form_label('Drivers License','license',array('class' => 'col-xs-2 control-label')); ?>
			<div class="col-xs-2">
<?php echo form_input(array('name' => 'license', 'id' => 'license', 'placeholder' => 'Drivers License', 'class' => 'form-control', 'maxlength' => '32', 'value' => set_value('license',$customer['license']))); ?>
			</div>
<?php echo form_label('Drivers State','license_state',array('class' => 'col-xs-3 control-label')); ?>
			<div class="col-xs-5">
<?php echo form_dropdown('license_state', array('' => '', 'QLD' => 'Queensland','NSW' => 'New South Wales','VIC' => 'Victoria','TAS' => 'Tasmania','SA' => 'South Australia','WA' => 'Western Australia','NT' => 'Northern Territoty', 'ACT','Australian Capital Territory'), set_value('license_state',$customer['license_state']), 'id="license_state" class="form-control"'); ?>
			</div>
		</div>
		<div class="form-group">
<?php echo form_label('Street','street1',array('class' => 'col-xs-2 control-label')); ?>
			<div class="col-xs-10">
<?php echo form_input(array('name' => 'street1', 'id' => 'street1', 'placeholder' => 'Street', 'class' => 'form-control', 'maxlength' => '64', 'value' => set_value('street1',$customer['street1']))); ?>
			</div>
		</div>
		<div class="form-group">
<?php echo form_label('Street','street2',array('class' => 'col-xs-2 control-label')); ?>
			<div class="col-xs-10">
<?php echo form_input(array('name' => 'street2', 'id' => 'street2', 'placeholder' => 'Street', 'class' => 'form-control', 'maxlength' => '64', 'value' => set_value('street2',$customer['street2']))); ?>
			</div>
		</div>
		<div class="form-group">
<?php echo form_label('City','city',array('class' => 'col-xs-2 control-label')); ?>
			<div class="col-xs-4">
<?php echo form_input(array('name' => 'city', 'id' => 'city', 'placeholder' => 'City', 'class' => 'form-control', 'maxlength' => '32', 'value' => set_value('city',$customer['city']))); ?>
			</div>
<?php echo form_label('State','state',array('class' => 'col-xs-1 control-label')); ?>
			<div class="col-xs-2">
<?php echo form_dropdown('state', array('' => '', 'QLD' => 'Queensland','NSW' => 'New South Wales','VIC' => 'Victoria','TAS' => 'Tasmania','SA' => 'South Australia','WA' => 'Western Australia','NT' => 'Northern Territoty', 'ACT','Australian Capital Territory'), set_value('state',$customer['state']), 'id="state" class="form-control"'); ?>
			</div>
<?php echo form_label('Postcode','postcode',array('class' => 'col-xs-1 control-label')); ?>
			<div class="col-xs-2">
<?php echo form_input(array('name' => 'postcode', 'id' => 'postcode', 'placeholder' => 'Postcode', 'class' => 'form-control', 'maxlength' => '4', 'value' => set_value('postcode',$customer['postcode']))); ?>
			</div>
		</div>
		<div class="form-group">
<?php echo form_label('Phone Home','phone_home',array('class' => 'col-xs-2 control-label')); ?>
			<div class="col-xs-2">
<?php echo form_input(array('name' => 'phone_home', 'id' => 'phone_home', 'placeholder' => 'Phone Home', 'class' => 'form-control', 'maxlength' => '32', 'value' => set_value('phone_home',$customer['phone_home']))); ?>
			</div>
<?php echo form_label('Work','phone_work',array('class' => 'col-xs-3 control-label')); ?>
			<div class="col-xs-2">
<?php echo form_input(array('name' => 'phone_work', 'id' => 'phone_work', 'placeholder' => 'Phone Work', 'class' => 'form-control', 'maxlength' => '32', 'value' => set_value('phone_work',$customer['phone_work']))); ?>
			</div>
<?php echo form_label('Mobile','phone_mobile',array('class' => 'col-xs-1 control-label')); ?>
			<div class="col-xs-2">
<?php echo form_input(array('name' => 'phone_mobile', 'id' => 'phone_mobile', 'placeholder' => 'Phone Mobile', 'class' => 'form-control', 'maxlength' => '32', 'value' => set_value('phone_mobile',$customer['phone_mobile']))); ?>
			</div>
		</div>
		<div class="form-group">
<?php echo form_label('Email','email',array('class' => 'col-xs-2 control-label')); ?>
			<div class="col-xs-3">
<?php echo form_input(array('name' => 'email', 'id' => 'email', 'placeholder' => 'Email', 'class' => 'form-control', 'maxlength' => '32', 'value' => set_value('email',$customer['email']))); ?>
			</div>
		</div>
		<div class="form-group">
<?php echo form_hidden('code',set_value('code',$customer['code'])); ?>
			<div class="col-xs-2"></div>
			<div class="col-xs-10">
<?php
if($customer['code'] != '') {
	echo form_button(array('type' => 'submit', 'class' => 'btn btn-warning', 'content' => '<span class="glyphicon glyphicon-save"></span> Update'));
} else {
	echo form_button(array('type' => 'submit', 'class' => 'btn btn-warning', 'content' => '<span class="glyphicon glyphicon-plus"></span> Add'));
}
?>
			</div>
		</div>
<?php echo form_close(); ?>
		</form>
	</div>
<?php
if($customer['code'] != '') {
?>
	<div class="panel-body">
		<table class="table table-striped table-bordered table-hover table-condensed">
			<thead>
				<tr>
					<th>ID</th>
					<th>Created</th>
					<th>Last bill</th>
					<th>Next bill</th>
					<th>Type</th>
					<th># Payments</th>
					<th class="text-right">Amount</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
<?php
	$status_codes = array('S' => 'Suspended', 'U' => 'Unbilled');
	$type_codes = array('P' => 'Perpetual','F' => 'Fixed');
	echo 'Plan count: '.$customer['payment_plan_count'];
	foreach($customer['payment_plans'] as $plan) {
		echo '<tr>';
		echo '<td>'.$plan->getID().'</td>';
		echo '<td>'.$plan->getCreateDate()->format('m/d/Y').'</td>';
		echo '<td>'.$plan->getFirstBillingDate()->format('m/d/y').'</td>';
		echo '<td>'.$plan->getNextBillingDate()->format('m/d/y').'</td>';
		echo '<td>'.$type_codes[$plan->getType()].'</td>';
		echo '<td>'.$plan->getLength().'</td>';
		echo '<td class="text-right">$'.money_format('%i',$plan->getValue() / 100).'</td>';
		echo '<td>'.$status_codes[$plan->getStatus()].'</td>';
		echo '<td><a href="/customers/remove_plan/'.$plan->getID().'" class="btn btn-danger btn-xs" title="Remove this plan"><span class="glyphicon glyphicon-remove"></span></a></td>';
		echo '</tr>';
	}
?>
				<tr>
					<td colspan="8">&nbsp;</td>
					<td><button id="add_plan" class="btn btn-success btn-xs" title="Add a payment plan"><span class="glyphicon glyphicon-plus"></span></button></td>
				</tr>
			</tbody>
		</table>
	</div>
<?php
}
?>
</div>
<div class="modal fade" id="new_plan">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add a payment plan</h4>
			</div>
			<div class="modal-body">
<?php echo form_label('Cycle','cycle',array('class' => 'col-xs-1 control-label')); ?>
				<div class="col-xs-4">
<?php echo form_dropdown('cycle', array('M1' => 'M1', 'M2' => 'M2'), 'M1', 'id="type" class="form-control"'); ?>
				</div>
			</div>
			<div class="modal-body">
<?php echo form_label('Type','type',array('class' => 'col-xs-1 control-label')); ?>
				<div class="col-xs-4">
<?php echo form_dropdown('suffix', array('fixed' => 'Fixed', 'perpetual' => 'Perpetual'), 'fixed', 'id="type" class="form-control"'); ?>
				</div>
<?php echo form_label('Amount','amount',array('class' => 'col-xs-2 control-label')); ?>
				<div class="col-xs-5">
<?php echo form_input(array('name' => 'amount', 'id' => 'amount', 'placeholder' => 'Amount', 'class' => 'form-control', 'maxlength' => '10', 'value' => 0)); ?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Add payment plan</button>
			</div>
		</div>
	</div>
</div>