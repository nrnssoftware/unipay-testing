function main() {
	console.log('Starting the calendar');
	var calendar = $("#calendar").calendar({
		events_source: '/calendar_events',
		modal : "#events-modal", 
		modal_type : "ajax", 
		modal_title : function (e) { return e.title }
	});
	console.log('Done');
}