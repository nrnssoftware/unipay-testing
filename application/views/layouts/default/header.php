<div class="navbar navbar-default" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="/">UniPay Testing</a>
	</div>
	<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav">
			<li><a href="/customers">Customers</a></li>
			<li><a href="/calendar">Calendar</a></li>
		</ul>
	</div>
</div>