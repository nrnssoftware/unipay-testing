<?php
$layout_css = array(
	'/r/css/jquery-ui.min.css',
	'/r/css/bootstrap.min.css',
	'/r/css/base.css',
	'/r/css/chosen.min.css'
);
$css = array_merge($layout_css,$page_css);
$layout_scripts = array(
	'/r/js/jquery-2.0.3.min.js',
	'/r/js/jquery-ui.min.js',
	'/r/js/bootstrap.min.js',
	'/r/js/chosen.jquery.min.js'
);
$scripts = array_merge($layout_scripts,$page_scripts);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<?php
foreach($css as $style) {
	echo '<link rel="stylesheet" type="text/css" href="'.$style.'" />';
}
?>
<script type="text/javascript" src="/r/js/head.load.min.js"></script>
<script type="text/javascript">
head.load("<?php echo implode('","',$scripts); ?>");
</script>
</head>
<body>
<header class="container"><?php echo $header; ?></header>
<article class="container"><?php echo $article; ?></article>
<footer class="container"><?php echo $footer; ?></footer>
<script type="text/javascript">
head.ready(function() {
	if(typeof(main) == 'function') main();
});
</script>
</body>
</html>
